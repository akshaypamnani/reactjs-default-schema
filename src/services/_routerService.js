// routes
import Dashboard from 'Routes/dashboard';
import UserClientManagement from 'Routes/usermanagement';
import Pentest from 'Routes/Pentests';

// async component
import {
	AsyncAboutUsComponent,
	AsyncChatComponent,
	AsyncMailComponent,
	AsyncTodoComponent,
} from 'Components/AsyncComponent/AsyncComponent';
import Assessments from '../routes/Assessments';
import ManageCompany from '../routes/managecompany';
import AddProfile from '../routes/profile';
import CreateClientUser from '../routes/usermanagement/createclientuser';
import CreateRootsecEmp from '../routes/usermanagement/createrootsecEmp';
import CreateCompany from '../routes/managecompany/createcompany';
import ManagePentest from '../routes/pentests/managepentest';
import RootSecNotifications from '../routes/notifications';
import Vulneribility from '../routes/pentests/vulnerabilities';
import AssessmentsCompany from '../routes/AssessmentsCompany';
import ManageAssessment from '../routes/Assessments';
import ManageQuickscan from '../routes/QuickscanAssess';
import CompanyPentests from '../routes/AssessmentsCompany/CompanyPentests';
import AssessmentsVulnerabilities from '../routes/AssessmentsCompany/CompanyVulnerabilities';
import ClientDashboard from '../routes/dashboard/clientdashboard';
import AddClientProfile from '../routes/clientprofile';
import CreateVulnerability from '../routes/pentests/createVulnerability';
import ManageblockIps from '../routes/usermanagement/blockips';
import LabelTranslation from '../routes/usermanagement/labeltranslation';
import PrioritiesVulnerabilities from '../routes/pentests/prioritiesVulnerability';
import AcceptVulneribility from '../routes/pentests/acceptvulnerabilities';
import MultipleAcceptVulneribility from '../routes/pentests/multipleacceptvulnerabilities';
import RequestNewClientUser from '../routes/usermanagement/requestnewuser';
import RequestedUsers from '../routes/usermanagement/requestedusers';
import CreateNewTicket from '../routes/supporttickets/addnewticket';
import ManageTickets from '../routes/supporttickets/managetickets';
import ViewTicket from '../routes/supporttickets/viewticket';
import CompanyTeamUsers from '../routes/usermanagement/companyteamusers';
import TicketDashboard from '../routes/dashboard/ticketdashboard';
import BulkImport from '../routes/bulkimport';
import CreateNewSecurityIncident from '../routes/securityincidents/addnewsecurityincident';
import SecurityIncidentDashboard from '../routes/dashboard/securityincident';
import ViewSecurityIncident from '../routes/securityincidents/viewsecurityincident/index';
import ManageIncident from '../routes/securityincidents/manageincident/index';
import VulnerabilityDashboard from '../routes/dashboard/vulnerabilitydashboard';
import ClientPentests from '../routes/pentests/clientpentests';
import ClientAssessments from '../routes/pentests/clientpentests';

export default [
	{
		path: 'dashboard',
		component: Dashboard
	},
	// {
	// 	path: 'companydashboard',
	// 	component: Dashboard
	// },
	{
		path: 'clientdashboard',
		component: ClientDashboard
	},
	{
		path: 'ticketdashboard',
		component: TicketDashboard
	},
	{
		path: 'vulnerabilitydashboard',
		component: VulnerabilityDashboard
	},
	{
		path: 'about-us',
		component: AsyncAboutUsComponent
	},
	{
		path: 'chat',
		component: AsyncChatComponent
	},
	{
		path: 'mail',
		component: AsyncMailComponent
	},
	{
		path: 'todo',
		component: AsyncTodoComponent
	},
	{
		path: 'userclientmanagement',
		component: UserClientManagement
	},
	{
		path: 'userrootsecmanagement',
		component: UserClientManagement
	},
	{
		path: 'manageblockips',
		component: ManageblockIps
	},
	{
		path: 'labeltranslation',
		component: LabelTranslation
	},
	{
		path: 'pentest/:id',
		component: Pentest
	},
	{
		path: 'assessment/:id',
		component: Pentest
	},
	{
		path: 'quickassessment/:id',
		component: Pentest
	},
	{
		path: 'assessments',
		component: Assessments
	},
	{
		path: 'managecompany',
		component: ManageCompany
	},
	{
		path: 'profile',
		component: AddProfile
	},
	{
		path: 'clientprofile',
		component: AddClientProfile
	},
	{
		path: 'clientuser',
		component: CreateClientUser
	},
	{
		path: 'editClientUser/:id',
		component: CreateClientUser
	},
	{
		path: 'rootsecemployee',
		component: CreateRootsecEmp
	},
	{
		path: 'Editrootsecemployee/:id',
		component: CreateRootsecEmp
	},
	{
		path: 'company',
		component: CreateCompany
	},
	{
		path: 'editcompany/:id',
		component: CreateCompany
	},
	{
		path: 'managepentest',
		component: ManagePentest
	},
	{
		path: 'manageassessment',
		component: ManageAssessment
	},
	{
		path: 'managequickscan',
		component: ManageQuickscan
	},
	{
		path: 'notifications',
		component: RootSecNotifications
	},
	{
		path: 'companyassessments',
		component: AssessmentsCompany
	},
	{
		path: 'companyassessment/:id',
		component: CompanyPentests
	},
	{
		path: 'companyvulnerabilities',
		component: AssessmentsVulnerabilities
	},
	{
		path: 'addvulnerability',
		component: CreateVulnerability
	},
	{
		path: 'editvulnerability/:id',
		component: CreateVulnerability
	},
	{
		path: 'vulnerability/:id',
		component: Vulneribility
	},
	{
		path: 'acceptvulnerability/:id',
		component: AcceptVulneribility
	},
	{
		path: 'acceptvulnerabilities',
		component: MultipleAcceptVulneribility
	},
	{
		path: 'prioritiesvulnerabilities',
		component: PrioritiesVulnerabilities
	},
	{
		path: 'requestnewuser',
		component: RequestNewClientUser
	},
	{
		path: 'editrequestnewuser/:id',
		component: RequestNewClientUser
	},
	{
		path: 'requestedusers',
		component: RequestedUsers
	},
	{
		path: 'teamusers',
		component: CompanyTeamUsers
	},
	{
		path: 'supportticket',
		component: CreateNewTicket
	},
	{
		path: 'editticket/:id',
		component: CreateNewTicket
	},
	{
		path: 'managetickets',
		component: ManageTickets
	},
	{
		path: 'assignedtickets',
		component: ManageTickets
	},
	{
		path: 'opentickets',
		component: ManageTickets
	},
	{
		path: 'closedtickets',
		component: ManageTickets
	},
	{
		path: 'viewticket/:id',
		component: ViewTicket
	},
	{
		path: 'bulkimport',
		component: BulkImport
	},
	{
		path: 'securityincident',
		component: SecurityIncidentDashboard
	},
	{
		path: 'viewsecurityincident',
		component: ViewSecurityIncident
	},
	{
		path: 'addsecurityincident',
		component: CreateNewSecurityIncident
	},
	{
		path: 'manageincident',
		component: ManageIncident
	},
	{
		path: 'editincident/:id',
		component: CreateNewSecurityIncident
	},
	{
		path: 'clientpentests',
		component: ClientPentests
	},
	{
		path: 'clientassessments',
		component: ClientAssessments
	}
]