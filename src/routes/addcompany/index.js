//Add Company

import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';


class AddCompany extends Component {

   state = {
      activeIndex: 0
   }

   handleChange(value) {
      this.setState({ activeIndex: value });
   }

   render() {
      const { activeIndex } = this.state;

      const columns = ["Logo", "Company Name", "Legel Entity", "Jumb of Commerce", "Username", "Email Address" , "IP Address", "Role", "Total Users", "Acties"];
      const data = [
      ];
      const options = {
         filterType: 'dropdown',
         responsive: 'stacked'
      };

      return (
         <div className="data-table-wrapper">
            <PageTitleBar title={<IntlMessages id="managecompany.ManageCompany" />} redirect='/app/managecompany' match={this.props.match} />
            <RctCollapsibleCard fullBlock>
               <MUIDataTable
                  data={data}
                  columns={columns}
                  options={options}
               />
            </RctCollapsibleCard>
         </div>
      );
   }
}
export default AddCompany;