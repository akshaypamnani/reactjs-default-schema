/**
 * Login Page
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { FormGroup } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import { Fab } from '@material-ui/core';
import axios from 'axios';
import Form from 'react-validation/build/form';
import { isEmail } from 'validator';
import Input from 'react-validation/build/input';
import Select from 'react-validation/build/select';
// app config
import AppConfig from 'Constants/AppConfig';
import IntlMessages from 'Util/IntlMessages';
import { injectIntl, intlShape } from "react-intl";
import * as ApiService from '../../../actions/ApiServices';

// redux action
import {
  signinUser,
  Get2FACode,
  signinUserInFirebase,
  signinUserInFirebaseCompany
} from 'Actions';

import * as SessionService from '../../../actions/SessionService';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { NotificationManager } from 'react-notifications';

const email = (value) => {
  if (!isEmail(value)) {
    return <span className="form-error is-visible">{value} <IntlMessages id="is_invalid_email_address" /></span>;
  }
};
const required = (value, props) => {
  if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
    return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
  }
};


class Signin extends Component {

  static contextTypes = {
    intl: intlShape,
  };

  state = {
    Email: '',
    Password: '',
    TwoFACode: '',
    Is2FAGenerated: false, //default false,
    loading: false
  }
  handleClick = () => {
    this.form.validateAll();
  };

  async componentDidMount() {
    localStorage.removeItem('dark-mode');
    await this.AuthenticateIPAddress();
    SessionService.RemoveSession();
  }

  async AuthenticateIPAddress() {
    var AuthenticateIP = await ApiService.AuthGetAPI('IPCheck');
    if (AuthenticateIP && !AuthenticateIP.Authentication) {
      this.props.history.push('/session/401');
    }
  }

  async onUserLogin(e) {
    this.form.validateAll();
    e.preventDefault();
    this.setState({ loading: true });
    if (this.form.getChildContext()._errors.length <= 0) {
      if (!this.state.Is2FAGenerated) {
        await this.Get2FACode();
      } else {
        // await this.props.signinUser(this.state, this.props.history);
        var LoginUser = await this.props.signinUser(this.state, this.props.history);
        if (LoginUser) {
          if (LoginUser.data) {
            setTimeout(() => {
              NotificationManager.success('Login Success !!');
            }, 1000);
            SessionService.SetUserSession(LoginUser.data);
            SessionService.SetAuthTokenSession(LoginUser.Token);
            var UserRole = SessionService.SetUserRole(LoginUser.data.UserRole.Role);

            if (LoginUser.data.IsfirstLogin != 1) {
              if (UserRole.Name == "ClientUser") {
                this.props.history.push('/company/clientdashboard');
              } else {
                this.props.history.push('/#/app/dashboard');
              }
            }
            else {
              this.props.history.push({
                pathname: '/session/resetpassword',
                state: { Email: this.state.Email }
              })
            }

          }
          else {
            this.setState({ loading: false });
            this.props.history.push('/session/login');
          }

        }
      }
    }
    this.setState({ loading: false });
  }

  async Get2FACode() {
    this.setState({ loading: true });
    if (this.state.Email !== '' && this.state.Password !== '') {
      // this.props.signinUserInFirebase(this.state, this.props.history);
      var TwoFA = await this.props.Get2FACode(this.state, this.props.history);
      if (TwoFA && TwoFA.status == 1) {
        NotificationManager.success(this.context.intl.formatMessage({ id: 'Code_has_been_sent_to' }) + ' ' + this.state.Email);
        this.setState({ Is2FAGenerated: true });
      } else if (TwoFA && TwoFA.messageType == 1) {
        NotificationManager.error(this.context.intl.formatMessage({ id: 'Please_Enter_Valid_Password' }));
      } else {
        NotificationManager.error(this.context.intl.formatMessage({ id: 'User_not_found' }) + ' ' + this.state.Email);
      }
    }
    this.setState({ loading: false });
  }

  async onCompanyLogin() {
    this.setState({ loading: true });
    // if (!this.state.Is2FAGenerated) {
    //    this.Get2FACode();
    // } else {
    //    this.props.signinUser(this.state, this.props.history);
    // }

    var LoginUser = await this.props.signinUser(this.state, this.props.history);
    if (LoginUser) {
      if (LoginUser.data) {
        if (LoginUser.data.IsfirstLogin != 1) {
          SessionService.SetUserSession(LoginUser.data);
          SessionService.SetAuthTokenSession(LoginUser.Token);
        }
        else {
          SessionService.SetAuthTokenSession(LoginUser.Token);
          this.props.history.push({
            pathname: '/session/resetpassword',
            state: { Email: this.state.Email }
          })
        }

      }
      else {
        this.setState({ loading: false });
        this.props.history.push('/session/login');
      }

    }
    this.setState({ loading: false });
  }

  onUserSignUp() {
    this.props.history.push('/session/register');
  }

  render() {
    const { Email, Password, TwoFACode, loading } = this.state;
    return (
      <QueueAnim type="bottom" duration={2000}>
        <div className="rct-session-wrapper">
          <AppBar position="static" className="session-header">
          </AppBar>
          <div className="session-inner-wrapper">
            <div className="container logincontainer">
              <div className="row row-eq-height">
                <div className="col-sm-3 col-md-3 col-lg-3"></div>
                {/* <div className="col-sm-5 col-md-5 col-lg-4 imglogoback imgMinheight">
                  <img src={AppConfig.appLogo} alt="session-logo" className="img-fluid" />
                </div> */}
                <div className="col-sm-6 col-md-6 col-lg-6">
                  <div className="session-body text-center">
                    {loading ? <RctSectionLoader /> : ''}
                    {!this.state.Is2FAGenerated ? <div className="session-head" style={{ marginBottom: '1.075rem' }}>
                      <h2><IntlMessages id="Log_in" /></h2>
                      <p className="mb-0"><IntlMessages id="Please_enter_your_e-mail_and_password" /></p>
                    </div> : ''}
                    {this.state.Is2FAGenerated ? <div className="session-head mb-30" style={{ marginTop: '1rem' }}>
                      <h2><IntlMessages id="2FA_Verification" /></h2>
                      <p className="mb-0"><IntlMessages id="Please_enter_the_code_that_is_send_to_you_in_the_email" /></p>
                    </div> : ''}
                    <Form ref={c => { this.form = c }} onSubmit={this.onUserLogin.bind(this)}>
                      {!this.state.Is2FAGenerated ?
                        <FormGroup className="has-wrapper">
                          <Input type="mail" autoFocus maxLength="150" value={Email} name="user-mail" id="user-mail" validations={[required, email]} className="form-control has-input input-lg" placeholder={this.context.intl.formatMessage({ id: 'Enter_Email_Address' })} onChange={(event) => this.setState({ Email: event.target.value })} />
                          <span className="has-icon"><i className="ti-email"></i></span>
                        </FormGroup> : ''}
                      {!this.state.Is2FAGenerated ? <FormGroup className="has-wrapper">
                        <Input value={Password} onCopy={() => false} onPaste={() => setTimeout(() => {
                          this.setState({ Password: '' });
                        }, 100)} type="Password" name="user-pwd" id="pwd" validations={[required]} className="form-control has-input input-lg" placeholder={this.context.intl.formatMessage({ id: 'Password' })} onChange={(event) => this.setState({ Password: event.target.value })} />
                        <span className="has-icon"><i className="ti-lock"></i></span>
                      </FormGroup> : ''}
                      {this.state.Is2FAGenerated ? <FormGroup className="has-wrapper">
                        <Input type="text" autoFocus value={TwoFACode} name="user-2facode" id="2facode" className="form-control has-input input-lg" validations={[required]} placeholder={this.context.intl.formatMessage({ id: '2-FA_code' })} onChange={(event) => this.setState({ TwoFACode: event.target.value })} />
                        <span className="has-icon"><i className="ti-lock"></i></span>
                      </FormGroup> : ''}
                      <FormGroup className="mb-15">
                        <Button type="submit"
                          color="primary"
                          className="btn-block text-white w-100"
                          variant="contained"
                          size="large">
                          <IntlMessages id="Log_in" />
                        </Button>
                        {this.state.Is2FAGenerated ?
                          // <Button onClick={() => this.Get2FACode()}
                          //   color="primary"
                          //   className="btn-block text-white w-100"
                          //   variant="contained"
                          //   size="large">
                          //   Resend TwoFA
                          // </Button>
                          <p className="m-20"><a href="javascript:;" onClick={() => this.Get2FACode()} className="text-muted"><IntlMessages id="Resend_twoFA" /></a></p>
                          : ''}
                      </FormGroup>
                      <p><a href="#/session/forgot-password" className="text-muted">{!this.state.Is2FAGenerated ? <IntlMessages id="Forgot_password?" /> : ''}</a></p>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </QueueAnim>
    );
  }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
  // const { user, loading } = authUser;
  return { authUser }
}

export default connect(mapStateToProps, {
  signinUser,
  Get2FACode,
  signinUserInFirebase,
  signinUserInFirebaseCompany
})(Signin);
