import React, { Component } from 'react';
import { FormGroup } from 'reactstrap';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';
import QueueAnim from 'rc-queue-anim';

// app config
import AppConfig from 'Constants/AppConfig';
import * as ApiService from '../../../actions/ApiServices';
import Form from 'react-validation/build/form';
import { isEmail } from 'validator';
//import Button from 'react-validation/build/Button';
import Input from 'react-validation/build/input';
import Select from 'react-validation/build/select';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
import { NotificationManager } from 'react-notifications';

const email = (value) => {
    if (!isEmail(value)) {
        return <span className="form-error is-visible">{value} <IntlMessages id="is_invalid_email_address" /></span>;
    }
};
const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};


export default class Forgotpwd extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        Email: ""
    }
    handleClick = () => {
        this.form.validateAll();
    };

    handleChange = (event, key) => {
        this.setState({ [key]: event.target.value });
    }

    async Forgotpassword(e) {
        this.form.validateAll();
        e.preventDefault();
        if (this.form.getChildContext()._errors.length <= 0) {
            var data = {
                Email: this.state.Email
            };
            var forgotpassword = await ApiService.AuthPostAPI('ForgotPasswordLink', data);

            if (forgotpassword.status = 1) {
                NotificationManager.success(this.context.intl.formatMessage({ id: 'Link_has_been_sent_to' }) + ' ' + this.state.Email);
            } else {
                NotificationManager.error(this.context.intl.formatMessage({ id: 'Email_Does_not_Exsist' }));
            }
        }
    }
    render() {
        return (
            <QueueAnim type="bottom" duration={2000}>
                <div className="rct-session-wrapper" key="1">
                    <AppBar position="static" className="session-header">
                    </AppBar>
                    <div className="session-inner-wrapper">
                        <div className="container logincontainer">
                            <div className="row row-eq-height">
                                <div className="col-sm-3 col-md-3 col-lg-3"></div>
                                {/* <div className="col-sm-5 col-md-5 col-lg-4 imglogoback">
                                    <img src={AppConfig.appLogo} alt="session-logo" className="img-fluid forgotpwdheight" />
                                </div> */}
                                <div className="col-sm-6 col-md-6 col-lg-6">
                                    <div className="session-body text-center">
                                        <div className="session-head mb-30">
                                            <h2><IntlMessages id="Forgot_Password" /></h2>
                                            <p className="mb-0"><IntlMessages id="Enter_your_email_address_here_for_a_recovery_link" /></p>
                                            {/* <h2>Get started with {AppConfig.brandName}</h2> */}
                                            {/* <p className="mb-0">Most powerful ReactJS admin panel</p> */}
                                        </div>
                                        <Form ref={c => { this.form = c }} onSubmit={this.Forgotpassword.bind(this)}>
                                            <FormGroup className="has-wrapper">
                                                <Input type="mail" maxLength="150" className="form-control has-input input-lg" validations={[required, email]} name="user-mail" value={this.state.Email}
                                                    onChange={(e) => this.handleChange(e, 'Email')} id="user-mail" placeholder={this.context.intl.formatMessage({ id: 'Enter_Email_Address' })}
                                                />
                                                <span className="has-icon"><i className="ti-email"></i></span>
                                            </FormGroup>
                                            <FormGroup>
                                                <Button type="submit"
                                                    color="primary"
                                                    className="btn-block text-white w-100"
                                                    variant="contained"
                                                    size="large">
                                                    <IntlMessages id="Reset_Password" />
                                                </Button>
                                            </FormGroup>
                                            <p><a href="#/session/login" className="text-muted"><IntlMessages id="Already_having_account?_Login" /></a></p>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </QueueAnim>
        );
    }
}
