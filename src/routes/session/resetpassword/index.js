/**
 * Login Page
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { FormGroup } from 'reactstrap';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import { Fab } from '@material-ui/core';
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import * as ApiService from '../../../actions/ApiServices';
//import queryString from 'query-string';

// app config
import AppConfig from 'Constants/AppConfig';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
import IntlMessages from 'Util/IntlMessages';

const isEqual = (value, props, components) => {
    const bothUsed = components.password[0].isUsed && components.confirm[0].isUsed;
    const bothChanged = components.password[0].isChanged && components.confirm[0].isChanged;

    if (bothChanged && bothUsed && components.password[0].value !== components.confirm[0].value) {
        return <span className="form-error is-visible"><IntlMessages id="Passwords_are_not_equal" /></span>;
    }
};
const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};
const IsPassword = (value, props) => {
    if (!(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,16}$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Password_must_contain_at_least_one_uppercase_character,one_lowercase_character,one_digit,one_special_character_and_length_must_be_min_8_char,_max_16char" />  </span>;
    }
};

class ResetPassword extends Component {

    static contextTypes = {
        intl: intlShape,
     };

    state = {
        Email: '',
        Password: '',
        ConformPassword: '',
        token: '',
        loading: false
    }


    handleChange = (event, key) => {                      //updating value
        this.setState({ [key]: event.target.value });
    }


    async componentDidMount() {
        if (this.props.location.state) {
            this.setState({ Email: this.props.location.state.Email });
        } else {
            var params = new URLSearchParams(this.props.location.search);
            var Email = params.get('Email');
            var token = params.get('token');
            if (Email && token) {
                var tokendata = {
                    Email: Email,
                    jwttoken: token
                };
                var Isvalidtoken = await ApiService.AuthWithoutmsgPostAPI('IsValidJWT', tokendata);
                if (Isvalidtoken.Istokenvalid) {
                    this.setState({ Email: Email });
                } else {
                    this.props.history.push('/session/login');
                    setTimeout(() => {
                        window.location.reload(true);
                    }, 1000);
                    return;
                }
            } else {
                this.props.history.push('/session/login');
                setTimeout(() => {
                    window.location.reload(true);
                }, 1000);
                return;
            }
        }
    }

    async  onUserresetPassword(e) {
        this.form.validateAll();
        e.preventDefault();
        this.setState({ loading: true });
        if (this.form.getChildContext()._errors.length <= 0) {
            if (this.state.Password == this.state.ConformPassword) {
                var resetPassword = {
                    Email: this.state.Email,
                    Password: this.state.Password
                };
                var res = await ApiService.AuthWithoutmsgPostAPI('resetPassword', resetPassword);
                if (res) {
                    setTimeout(() => {
                        NotificationManager.success("Password reset success!!");
                    }, 1000);
                    this.props.history.push({
                        pathname: '/#'
                    });
                }
            }
            else {
                NotificationManager.error("Password And ConformPassword are not Same!!!");
            }
        }
        this.setState({ loading: false });
    }





    render() {

        const { loading } = this.state;
        return (
            <QueueAnim type="bottom" duration={2000}>
                <div className="rct-session-wrapper">
                    {/* {loading &&
                        <LinearProgress />
                    } */}
                    <AppBar position="static" className="session-header">
                    </AppBar>
                    <div className="session-inner-wrapper">
                        <div className="container logincontainer">
                            <div className="row row-eq-height">
                                <div className="col-sm-3 col-md-3 col-lg-3"></div>
                                {/* <div className="col-sm-5 col-md-5 col-lg-4 imglogoback">
                                    <img src={AppConfig.appLogo} alt="session-logo" className="img-fluid resetpwdimglogo" />
                                </div> */}
                                <div className="col-sm-6 col-md-6 col-lg-6">
                                    <div className="session-body text-center">
                                        <div className="session-head mb-30">
                                            <h2><IntlMessages id="Set_New_Password"></IntlMessages></h2>
                                            <p className="mb-0"><IntlMessages id="Please_provide_your_own_safe_password" /></p>
                                        </div>
                                        {loading ? <RctSectionLoader /> : ''}
                                        <Form ref={c => { this.form = c }} onSubmit={this.onUserresetPassword.bind(this)}>
                                            <FormGroup className="has-wrapper">
                                                <Input value={this.state.Password} onPaste={() => setTimeout(() => {
                                                    this.setState({ Password: '' });
                                                }, 100)} type="Password" name="password" id="password" className="has-input input-lg form-control" placeholder={this.context.intl.formatMessage({ id: 'Password' })}
                                                    onChange={(e) => this.handleChange(e, 'Password')}
                                                    validations={[required, IsPassword, isEqual]} />
                                                {/* onChange={(event) => this.setState({ Password: event.target.value })} /> */}
                                                <span className="has-icon"><i className="ti-lock"></i></span>
                                                <Input value={this.state.ConformPassword} onPaste={() => setTimeout(() => {
                                                    this.setState({ ConformPassword: '' });
                                                }, 100)} type="Password" name="confirm" id="confirm" className="has-input input-lg form-control" placeholder={this.context.intl.formatMessage({ id: 'Confirm_Password' })}
                                                    onChange={(e) => this.handleChange(e, 'ConformPassword')}
                                                    validations={[required, isEqual]}
                                                />
                                                <span className="has-icon"><i className="ti-lock"></i></span>
                                            </FormGroup>

                                            <FormGroup className="mb-15">
                                                <Button
                                                    color="primary"
                                                    className="btn-block text-white w-100"
                                                    variant="contained"
                                                    size="large"
                                                    type="submit">
                                                    <IntlMessages id="Reset_Password"></IntlMessages>
                        </Button>
                                            </FormGroup>

                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </QueueAnim>
        );
    }
}



export default ResetPassword;
