//Manage Tickets

import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import IconButton from '@material-ui/core/IconButton';

//API Services
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CustomFooter from 'Components/CustomFooter/CustomFooter';
import { Tooltip } from '@material-ui/core';
import { intlShape } from 'react-intl';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const IncidentPriority = {
   1: 'Urgent',
   2: 'High',
   3: 'Medium',
   4: 'Low',
}

const IncidentStatus = {
   1: 'Open',
   2: 'Pending',
   3: 'Process',
   4: 'Closed',
}

class ManageIncident extends Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      activeIndex: 0,
      Incidents: [],
      selectedDeleteID: 0,
      loading: true,
      logoBaseURL: '',
      sessionUser: {
         id: 0,
         UserRole: {
            id: 0,
            RoleId: 0
         }
      },
   }

   async componentDidMount() {
      await this.checkUserRole();
      this.setState({ loading: true });
      var Url = window.location.href.split("/").pop();
      var IncidentType = 0;
      if (Url.trim().toString() == 'manageincident') {
         IncidentType = 1;
      } else if (Url.trim().toString() == 'assignedincident') {
         IncidentType = 2;
      } else if (Url.trim().toString() == 'closedincident') {
         IncidentType = 3;
      }
      const logoBaseURL = await ApiService.ConfigAPIURL();
      await this.setState({ logoBaseURL });
      await this.GetIncident(IncidentType);
      this.setState({ loading: false });
   }

   checkUserRole() {
      ApiService.CheckUserRole(this.props);
      var sessionUser = SessionService.GetUserSession();
      this.setState({ sessionUser });
   }

   EditIncident(incidentid) {
      this.props.history.push('/app/editincident/' + cryptr.encrypt(incidentid));
   }

   handleChange(value) {
      this.setState({ activeIndex: value });
   }

   async GetIncident(incidenttype) {
      var IncidentsData = {
         Where: {
            IsActive: 1,
         }
      };

      if (this.state.sessionUser.id != 0 && (this.state.sessionUser.UserRole.RoleId == 1 || this.state.sessionUser.UserRole.RoleId == 4)) {
         IncidentsData.Where.CreatedBy = this.state.sessionUser.id;
      }

      if (incidenttype == 1) {
         IncidentsData.Where.Status = 1;
      } else if (incidenttype == 3) {
         IncidentsData.Where.Status = 4;
      }

      var Incidents = await ApiService.PostWhereAPI('execute/withoutnestedconditions/Incidents', IncidentsData);

      if (Incidents && Incidents.length > 0) {
         if (incidenttype == 2) {
            Incidents = Incidents.filter(x => x.AssignedUser != 0);
         }
      }

      this.setState({ Incidents: Incidents });
   }

   // on delete
   onDeleteIncidet(incidentid) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedDeleteID: cryptr.encrypt(incidentid) });
   }

   // delete
   async delete() {
      var Incideint = await ApiService.DeleteAPI('Incidents/' + this.state.selectedDeleteID);
      if (Incideint) {
         // this.props.history.push('app/')
      }
      this.setState({ selectedDeleteID: 0 });
      this.GetIncident();
      this.refs.deleteConfirmationDialog.close();
   }

   render() {
      const { activeIndex, loading } = this.state;


      const columns = [
         { name: "Incident_Code", options: { filter: false, sort: false, customHeadRender: (columnMeta, updateDirection) => (<th key={0}><IntlMessages id="Incident Code" /></th>) } },
         { name: "Subject", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Subject" /></th>) } },
         { name: "Priority", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Priority" /></th>) } },
         { name: "Status", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Status" /></th>) } },
         {
            name: "Actions",
            options: {
               filter: false,
               sort: false,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={4}><IntlMessages id="Actions" />
                  </th>
               )
            }
         }];
      const data = [
      ];
      const options = {
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,
         selectableRows: 'none',
         responsive: 'scrollFullHeight',
         textLabels: {
            body: {
               noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
            }
         },
         customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
            return (
               <CustomFooter
                  count={count}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  changeRowsPerPage={changeRowsPerPage}
                  changePage={changePage}
                  textLabels={textLabels} />
            );
         },
         onCellClick: (colData, cellMeta) => {
            if (cellMeta.colIndex != 4) {
               this.props.history.push('viewsecurityincident/' + cryptr.encrypt(this.state.Incidents[cellMeta.dataIndex].id));
            }
         }
      };

      return (
         <div className="data-table-wrapper">
            <PageTitleBar title={<IntlMessages id="Manage_Incidents" />} redirect='/app/dashboard' match={this.props.match} />
            <RctCollapsibleCard fullBlock>

               {/* <div className="row mrgnL20">
                  <div className="col-sm-12 col-md-12 col-xl-12">
                     <Link className="mt-10" to={{
                        pathname: '/app/supportticket',
                        state: { activeTab: 0 }
                     }}>
                        <MatButton variant="contained" style={{ float: 'right' }} color="primary" className="mb-10 text-white"><IntlMessages id="Add_Ticket" /></MatButton>
                     </Link>
                  </div>
               </div> */}

               {loading ? <RctSectionLoader /> : ''}
               <MUIDataTable style={{ overflowX: 'scroll' }}
                  data={this.state.Incidents.map(incd => {
                     return [
                        incd.IncidentCode,
                        incd.Subject,
                        IncidentPriority[incd.Priority],
                        IncidentStatus[incd.Status],
                        <div className="row actionbtns" style={{ justifyContent: 'flex-end' }}>
                           <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                              <IconButton className="text-primary" onClick={() => this.EditIncident(incd.id)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton></Tooltip>
                           <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                              <IconButton className="text-danger" onClick={() => this.onDeleteIncidet(incd.id)} aria-label="Add an alarm"><i className="zmdi zmdi-delete"></i></IconButton></Tooltip>
                        </div>
                     ]
                  })}
                  columns={columns}
                  options={options}
               />
            </RctCollapsibleCard>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               onConfirm={() => this.delete()}
            />
         </div>
      );
   }
}
export default ManageIncident;