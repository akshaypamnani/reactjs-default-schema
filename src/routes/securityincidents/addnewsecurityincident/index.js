//Create Client User

import React, { Component } from 'react';
import ReactTags from 'react-tag-autocomplete';
import { FormGroup, Label, Col, FormText } from 'reactstrap';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import { isEmail } from 'validator';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
import { FormControl, IconButton, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import Textarea from 'react-validation/build/textarea';
import Select from 'react-select';
import ReactQuill from 'react-quill';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};

const Filecheck = (value, props) => {
    if (value) {
        var fileExt = value.split('.').pop().toString().trim().toLowerCase();
        var validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];
        if (!validFileExtensions.includes(fileExt)) {
            return <span className="form-error is-visible">Invalid File Extension</span>;
        }
    }
};

const modules = {
    toolbar: [
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'font': [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
        ['link', 'image'],
        ['clean'],
        [{ 'align': [] }],
        ['code-block']
    ],
};
const formats = [
    'header',
    'font',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'align',
    'code-block'
];

class CreateNewSecurityIncident extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        Incident: {
            id: 0,
            IncidentCode: '',
            Subject: '',
            Description: '',
            IncidentType: 1,
            ReferenceId: '',
            Priority: 1,
            Status: 1
        },
        SelectedType: {
            value: 1, label: 'Issue'
        },
        IncidentTypes: [],
        // IncidentPriority: "1",
        SelectedPriority: {
            value: 1, label: 'Issue'
        },
        IncidentPriorities: [],
        VulnArguments: [],
        SelectedVuln: 0,
        Vulnerabilities: []
    }

    async componentDidMount() {
        this.checkUserRole();
        this.setState({ loading: true });
        const { id } = this.props.match.params;
        // await this.GetVulnerabilities();
        if (id) {
            this.GetIncident(id);
        } else {
            await this.GetLatestIncidentCode();
        }

        this.setState({
            IncidentPriority: [{
                value: 1, label: this.context.intl.formatMessage({ id: 'Urgent' })
            }, {
                value: 2, label: this.context.intl.formatMessage({ id: 'High' })
            }, {
                value: 3, label: this.context.intl.formatMessage({ id: 'Medium' })
            }, {
                value: 4, label: this.context.intl.formatMessage({ id: 'Low' })
            }],
            IncidentTypes: [{
                value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
            }]
        });

        this.setState({
            SelectedPriority: { value: 4, label: this.context.intl.formatMessage({ id: 'Low' }) }, SelectedType: {
                value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
            }
        })

        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetIncident(incidentid) 
    {
        var Incident = await ApiService.GetAPI('Incidents/' + incidentid);
        if (Incident) {
            this.setState({ IncidentID: Incident.id });
            this.setState({ Incident });
            
            this.setState({ SelectedType: this.state.IncidentTypes.filter(x => x.value == Incident.IncidentType), 
                SelectedPriority: this.state.IncidentPriority.filter(x => x.value == Incident.Priority)});

        } else {
            this.props.history.push('/app/managetickets');
        }
    }
    async GetLatestIncidentCode() {
        
        var LatestCode = await ApiService.GetWithoutmsgAPI('incidentsapi/GetLatestIncidentCode');

        if (LatestCode) {
            let Incident = this.state.Incident;
            Incident.IncidentCode = LatestCode.Code;
            this.setState({ Incident });
        }
    }

    handleChangeRadio = (e, key) => {
        console.log('handleChangeRadio', e.target.value);
        this.setState({ [key]: e.target.value });
    }

    handleChange = (event, key) => {
        let Incident = Object.assign({}, this.state.Incident);    //creating copy of object
        Incident[key] = event.target.value;                        //updating value
        this.setState({ Incident });
    }

    handleChangeEditor(value, key) {
        let Incident = this.state.Incident;
        Incident[key] = value;
        this.setState({ Incident });
    }

    handleChangeIncident = async SelectedType => {
        this.setState({ SelectedType });
    };

    handleChangeIncidentPriority = async SelectedPriority => {
        this.setState({ SelectedPriority });
    };

    handleChangeVuln = async SelectedVuln => {
        this.setState({ SelectedVuln });
    };

    async SaveIncident() {
        this.form.validateAll();
        console.log('_errors', this.form.getChildContext()._errors)
        if (this.form.getChildContext()._errors.length <= 0) {
            var Incident;
            var IncidentData = {
                id: this.state.Incident.id,
                IncidentCode: this.state.Incident.IncidentCode.toString().trim(),
                Subject: this.state.Incident.Subject.toString().trim(),
                Description: this.state.Incident.Description.toString().trim(),
                IncidentType: this.state.SelectedType.value,
                ReferenceId: this.state.SelectedVuln.value,
                Priority: this.state.SelectedPriority.value,
                Status: this.state.Incident.Status
            };

            if (IncidentData.id && IncidentData.id != 0) {
                Incident = await ApiService.PostWhereAPI('Incidents/' + cryptr.encrypt(this.state.IncidentID), IncidentData);
            } else {
                Incident = await ApiService.PutAPI('Incidents', IncidentData);
            }

            if (Incident) {
                this.props.history.push('/app/manageincident');
            }
        }
        this.setState({ loading: false });
    }

    render() {
        const { loading } = this.state;
        return (
            <div className="formelements-wrapper">
                <Form ref={c => { this.form = c }}>
                    <PageTitleBar title={<IntlMessages id="Add_new_incident/request_new_user" />} newtitle="newincident" redirect='/app/addsecurityincident' match={this.props.match} />
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-4 d-block">
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Incident_information' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Incident_number:" />}</Label>
                                            </FormControl>
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex clsticketcode">#{this.state.Incident.IncidentCode}</Label>                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            {/* <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Assign_ticket_to_vulnerability:' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Search_a_vulnerability" />}</Label>
                                                <Select
                                                    value={this.state.SelectedVuln}
                                                    onChange={this.handleChangeVuln}
                                                    options={this.state.Vulnerabilities}
                                                />
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div> */}
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'File_upload_section' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="If_needed,_please_add_your_attachments_(JPG/PNG_only!)" />}</Label>
                                                <Col sm={12}>
                                                    <Input type="file" accept="image/*" onChange={(e) => this.setState({ UploadFile: e.target.files })} validations={[required, Filecheck]} name="file" id="File-1" />
                                                    <FormText color="muted">
                                                    </FormText>
                                                </Col>
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-12 col-xl-8 d-block">
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Options_for_your_incident' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-5 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Select_a_category" />}</Label>
                                                <Select
                                                    value={this.state.SelectedType}
                                                    onChange={this.handleChangeIncident}
                                                    options={this.state.IncidentTypes}
                                                />
                                            </FormControl>
                                        </div>
                                        <div className="col-sm-12 col-md-12 col-xl-2 p-0 pb-15"></div>
                                        <div className="col-sm-12 col-md-12 col-xl-5 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Select_your_priority" />}</Label>
                                                <Select
                                                    value={this.state.SelectedPriority}
                                                    onChange={this.handleChangeIncidentPriority}
                                                    options={this.state.IncidentPriority}
                                                />
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Incident_fields' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Your_subject" />}</Label>
                                                <Input type="text" maxLength="250" className="form-control" value={this.state.Incident.Subject} validations={[required]} onChange={(e) => this.handleChange(e, 'Subject')} name="Subject" id="Subject" placeholder={this.context.intl.formatMessage({ id: 'Subject' })} />
                                            </FormControl>
                                        </div>
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Please_add_a_clear_description_here" />}</Label>
                                                <ReactQuill key={1} modules={modules} formats={formats} value={this.state.Incident.Description} onChange={(value) => { this.handleChangeEditor(value, 'Description') }} placeholder={this.context.intl.formatMessage({ id: 'Description' })} />
                                            </FormControl>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div style={{ float: 'right', width: '100%' }}>
                                            <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveIncident()} className="mr-10 mb-10 text-white"><IntlMessages id="Save_incident_request" /></MatButton>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            {/* <RctCollapsibleCard>
                            {loading ? <RctSectionLoader /> : ''}
                            <Form ref={c => { this.form = c }}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-8">
                                        <FormGroup>
                                            <Label for="subject"><IntlMessages id="Subject" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="250" className="form-control" value={this.state.Ticket.Subject} validations={[required]} onChange={(e) => this.handleChange(e, 'Subject')} name="Subject" id="Subject" placeholder={this.context.intl.formatMessage({ id: 'Subject' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="subject"><IntlMessages id="Ticket_Code" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="50" className="form-control clsticketcode" value={this.state.Ticket.TicketCode} disabled onChange={(e) => this.handleChange(e, 'TicketCode')} name="TicketCode" id="TicketCode" placeholder={this.context.intl.formatMessage({ id: 'Ticket Code' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-8">
                                        <FormGroup>
                                            <Label for="TicketType"><IntlMessages id="Ticket_Type" /></Label>
                                            <Select
                                                value={this.state.SelectedType}
                                                onChange={this.handleChangeTicket}
                                                options={this.state.TicketTypes}
                                            />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <FormControl component="fieldset" required>
                                                <Label for="priority"><IntlMessages id="Priority" /></Label>
                                                <RadioGroup row aria-label="role" value={this.state.TicketPriority} name="role" onChange={(e) => this.handleChangeRadio(e, 'TicketPriority')} >
                                                    <FormControlLabel value="1" control={<Radio />} label="Normal" />
                                                    <FormControlLabel value="2" control={<Radio />} label="Urgent" />
                                                    <FormControlLabel value="3" control={<Radio />} label="Critical" />
                                                </RadioGroup>
                                            </FormControl>
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                        <FormGroup>
                                            <Label for="Description"><IntlMessages id="Description" /></Label>
                                            <ReactQuill key={1} modules={modules} formats={formats} value={this.state.Ticket.Description} onChange={(value) => { this.handleChangeEditor(value, 'Description') }} placeholder={this.context.intl.formatMessage({ id: 'Description' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div style={{ float: 'right', width: '100%' }}>
                                        <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveTicket()} className="mr-10 mb-10 text-white"><IntlMessages id="Create/Update" /></MatButton>

                                        <MatButton variant="contained" style={{ float: 'right', color: '#5d92f4' }} onClick={() => this.props.history.push('/app/userclientmanagement')} className="btn-default mr-10 mb-10"><IntlMessages id="Cancel" /></MatButton>
                                    </div>
                                </div>
                            </Form>
                        </RctCollapsibleCard>
                     */}
                        </div>
                    </div>
                </Form>
            </div >
        );
    }
}
export default CreateNewSecurityIncident;