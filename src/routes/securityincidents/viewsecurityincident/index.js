//Create Client User

import React, { Component } from 'react';
import ReactTags from 'react-tag-autocomplete';
import { FormGroup, Label, Col, FormText } from 'reactstrap';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import { isEmail } from 'validator';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
import { FormControl, IconButton, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import Textarea from 'react-validation/build/textarea';
import Select from 'react-select';
import ReactQuill from 'react-quill';
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Checkbox, AppBar, Tabs, Tab, Typography } from '@material-ui/core';
import MUIDataTable from 'mui-datatables';
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import MuiTablePagination from "@material-ui/core/TablePagination";


let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};

const Filecheck = (value, props) => {
    if (value) {
        var fileExt = value.split('.').pop().toString().trim().toLowerCase();
        var validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];
        if (!validFileExtensions.includes(fileExt)) {
            return <span className="form-error is-visible">Invalid File Extension</span>;
        }
    }
};

const modules = {
    toolbar: [
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'font': [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
        ['link', 'image'],
        ['clean'],
        [{ 'align': [] }],
        ['code-block']
    ],
};
const formats = [
    'header',
    'font',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'align',
    'code-block'
];

class ViewSecurityIncident extends Component {

    static contextTypes = {
        intl: intlShape,
    };


    handleChangeCheckBox = name => (event, checked) => {
        this.setState({ [name]: checked });

        if (this.state.chkAcceptance && this.state.chkResponsibility) {
            this.setState({ IsAcceptButtonVisible: true });
        } else {
            this.setState({ IsAcceptButtonVisible: false });
        }
    };

    state = {
        activeIndex: 0,
        Incident: {
            id: 0,
            IncidentCode: '',
            Subject: '',
            Description: '',
            IncidentType: 1,
            ReferenceId: '',
            Priority: 1,
            Status: 1
        },
        SelectedType: {
            value: 1, label: 'Issue'
        },
        IncidentTypes: [],
        IncidentPriority: "1",
        SelectedPriority: {
            value: 1, label: 'Issue'
        },

        IncidentPriorities: [],
        VulnArguments: [],
        SelectedVuln: 0,
        Vulnerabilities: []
    }

    async componentDidMount() {
        this.checkUserRole();
        this.setState({ loading: true });
        const { id } = this.props.match.params;
        // await this.GetVulnerabilities();
        if (id) {
            this.GetIncident(id);
        } else {
            await this.GetLatestIncidentCode();
        }

        this.setState({
            IncidentPriorities: [{
                value: 1, label: this.context.intl.formatMessage({ id: 'Urgent' })
            }, {
                value: 2, label: this.context.intl.formatMessage({ id: 'High' })
            }, {
                value: 3, label: this.context.intl.formatMessage({ id: 'Medium' })
            }, {
                value: 4, label: this.context.intl.formatMessage({ id: 'Low' })
            }],
            IncidentTypes: [{
                value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
            }]
        });

        this.setState({
            SelectedPriority: { value: 4, label: this.context.intl.formatMessage({ id: 'Low' }) }, SelectedType: {
                value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
            }
        })

        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetVulnerabilities() {
        var Vulnerabilities = await ApiService.GetWithoutmsgAPI('Vulnerabilitiesapi/getAllVulnerabilities');

        var selectVulns = [];
        if (Vulnerabilities && Vulnerabilities.length > 0) {
            Vulnerabilities.forEach(vuln => {
                selectVulns.push({ label: vuln.Name, value: vuln.id });
            });
        }

        this.setState({ Vulnerabilities: selectVulns });
    }

    async GetIncident(incidentid) {
        var Incident = await ApiService.GetAPI('Incidents/' + incidentid);
        if (Incident) {
            this.setState({ IncidentID: Incident.id });
            this.setState({ Incident });

            this.setState({ SelectedType: this.state.IncidentTypes.filter(x => x.value == Incident.IncidentType), SelectedPriority: this.state.IncidentPriorities.filter(x => x.value == Incident.Priority)});

        } else {
            this.props.history.push('/app/manageincident');
        }
    }

    async GetLatestIncidentCode() {
        var LatestCode = await ApiService.GetWithoutmsgAPI('incidentsapi/GetLatestIncidentCode');

        if (LatestCode) {
            let Incident = this.state.Incident;
            Incident.IncidentCode = LatestCode.Code;
            this.setState({ Incident });
        }
    }

    handleChangeRadio = (e, key) => {
        console.log('handleChangeRadio', e.target.value);
        this.setState({ [key]: e.target.value });
    }

    handleChange = (event, key) => {
        let Incident = Object.assign({}, this.state.Incident);    //creating copy of object
        Incident[key] = event.target.value;                        //updating value
        this.setState({ Incident });
    }

    handleChangeEditor(value, key) {
        let Incident = this.state.Incident;
        Incident[key] = value;
        this.setState({ Incident });
    }

    handleChangeIncident = async SelectedType => {
        this.setState({ SelectedType });
    };

    handleChangeIncidentPriority = async SelectedPriority => {
        this.setState({ SelectedPriority });
    };

    handleChangeVuln = async SelectedVuln => {
        this.setState({ SelectedVuln });
    };

    async SaveIncident() {
        this.form.validateAll();
        console.log('_errors', this.form.getChildContext()._errors)
        if (this.form.getChildContext()._errors.length <= 0) {
            var Incident;
            var IncidentData = {
                id: this.state.Incident.id,
                IncidentCode: this.state.Incident.IncidentCode.toString().trim(),
                Subject: this.state.Incident.Subject.toString().trim(),
                Description: this.state.Incident.Description.toString().trim(),
                TicketType: this.state.SelectedType.value,
                ReferenceId: this.state.SelectedVuln.value,
                Priority: this.state.SelectedPriority.value,
                Status: this.state.Incident.Status
            };

            if (IncidentData.id && IncidentData.id != 0) {
                Incident = await ApiService.PostWhereAPI('Incidents/' + cryptr.encrypt(this.state.IncidentID), IncidentData);
            } else {
                Incident = await ApiService.PutAPI('Incident', IncidentData);
            }

            if (Incident) {
                this.props.history.push('/app/manageincident');
            }
        }
        this.setState({ loading: false });

        
    
    }

    render() {
        const { loading } = this.state;
        const columns = [
            // "View",
            {
               name: "Time",
               options: {
                  filter: false,
                  ccustomHeadRender: (columnMeta, updateDirection) => (
                     <th key={0} style={{ width: '20%' }} onClick={() => updateDirection(0)}><IntlMessages id="Time" />
                     </th>
                  )
               }
            }, {
               name: "Who",
               options: {
                  filter: true,
                  sort: true,
                  customHeadRender: (columnMeta, updateDirection) => (
                     <th key={1} style={{ width: '20%' }} onClick={() => updateDirection(1)}><IntlMessages id="Who" />
                     </th>
                  )
               }
            }, {
               name: "What",
               options: {
                  filter: true,
                  sort: true,
                  customHeadRender: (columnMeta, updateDirection) => (
                     <th key={2} style={{ width: '30%' }} onClick={() => updateDirection(2)}><IntlMessages id="What" />
                     </th>
                  )
               }
            }];
         const options = {
            filter: false,
            search: false,
            print: false,
            download: false,
            viewColumns: false,
            selectableRows: 'none',
            responsive: 'scrollFullHeight',
            textLabels: {
               body: {
                  noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
               }
            },
            rowsPerPage: 50,
            // serverSide: false,
            // onTableChange: (action, state) => setQuantitySubmitted(filteredQuantitySubmitted(state)),
            onCellClick: (colData, cellMeta) => {
               if (cellMeta.colIndex != 6) {
                  this.props.history.push({
                     pathname: '/app/vulnerability/' + cryptr.encrypt(this.state.Vulnerabilities[cellMeta.dataIndex].id),
                     state: { history: this.props.location }
                  })
               }
            },
            customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
               return (
                  <TableFooter>
                     <TableRow>
                        <TableCell style={footerStyle} colSpan={1000}>
                           <FormControlLabel className="m-0 clsHideacceptedonly" control={
                              <Checkbox color="primary" checked={this.state.HideAcceptedVuln} onChange={(e) => this.ChangeCheckAcceptedVuln(e)} value="" />
                           } label={this.context.intl.formatMessage({ id: 'Hide_Accepted_Only' })}
                           />
    
                           <MuiTablePagination
                              component="div"
                              count={count}
                              rowsPerPage={rowsPerPage}
                              page={page}
                              labelRowsPerPage={this.context.intl.formatMessage({ id: 'Rows_per_page:' })}
                              labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${this.context.intl.formatMessage({ id: 'of' })} ${count}`}
                              backIconButtonProps={{
                                 'aria-label': textLabels.previous,
                              }}
                              nextIconButtonProps={{
                                 'aria-label': textLabels.next,
                              }}
                              rowsPerPageOptions={[10, 50, 100]}
                              onChangePage={(_, newPage) => changePage(newPage)}
                              onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                           />
                        </TableCell>
                     </TableRow>
                  </TableFooter>
               );
            }
         };
         const { Ticket } = this.state;

      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };
        return (
            <div className="formelements-wrapper">
                <Form ref={c => { this.form = c }}>
                    <PageTitleBar title={<IntlMessages id="Security_Incidents" />} newtitle="newticket" redirect='/app/addsecurityincident' match={this.props.match} />
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-4 d-block">
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'General_incident_description' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">

                                                <Label component="legend"><span><strong><IntlMessages id="Start_of_incident" /> : </strong> 14:02:16 </span></Label>
                                                <Label component="legend"><span><strong><IntlMessages id="Date_of_start" /> : </strong> 24-01-2020 </span></Label>

                                            </FormControl>

                                            <FormControl component="fieldset">
                                                <span>
                                                    <IntlMessages id="Priority_of_the_incident" />
                                                </span>

                                                <Select
                                                    value={this.state.SelectedPriority}
                                                    onChange={this.handleChangeIncidentPriority}
                                                    options={this.state.IncidentPriorities}
                                                />

                                                <span>
                                                    <IntlMessages id="Name_of_incident" />
                                                </span>

                                                <Input type="text" className="form-control" value="Malware Injection into citrix" />

                                                <span>
                                                    <IntlMessages id="Mark_the_status_as" />
                                                </span>

                                                <Select
                                                    value={this.state.SelectedType}
                                                    options={this.state.IncidentTypes}
                                                /><span>

                                                    <IntlMessages id="People_assign_to_this_incident" />
                                                </span>

                                                <textarea className="form-control">

                                                </textarea>



                                            </FormControl>

                                        </div>
                                    </div>

                                </RctCollapsibleCard>
                            </div>
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Incident_information' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">

                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex clsticketcode"><IntlMessages id="Security_Incidents" /></Label>
                                            </FormControl>
                                            <FormControl component="fieldset">

                                                <Label component="legend"><span><strong><IntlMessages id="Incident_ref" /> : </strong> #414141 </span></Label>
                                                <Label component="legend"><span><strong><IntlMessages id="Category" /> : </strong> Malware </span></Label>
                                                <Label component="legend"><span><strong><IntlMessages id="Ticket_date:" />  </strong> 24-01-2020 </span></Label>
                                                <Label component="legend"><span><strong><IntlMessages id="Priority:" />  </strong> Urgent </span></Label>
                                                <Label component="legend"><span><strong><IntlMessages id="Manager" />  </strong> Emil Plecki </span></Label>

                                            </FormControl>

                                        </div>
                                    </div>

                                </RctCollapsibleCard>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-12 col-xl-8 d-block">
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={<IntlMessages id="Add_your_remark" />}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend"><IntlMessages id="Please_add_a_clear_description_here" /></Label>
                                                <ReactQuill key={1} modules={modules} formats={formats} onChange={(value) => { this.handleChangeEditor(value, 'Description') }} placeholder={this.context.intl.formatMessage({ id: 'Description' })} />
                                            </FormControl>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-7 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend"><IntlMessages id="If_needed,_please_add_your_attachments_(JPG/PNG_only!)" /></Label>
                                            </FormControl>
                                        </div>
                                        <div className="col-sm-12 col-md-12 col-xl-5 p-0 pb-15">
                                            <FormControlLabel className="col-sm-12" control={
                                                <Checkbox color="primary" onChange={this.handleChangeCheckBox('chkAll')} checked={this.state.chkAll} value="chkAll" />
                                            } label={this.context.intl.formatMessage({
                                                id: "Make_my_input_as_urgent"
                                            })}
                                            />
                                            <FormControlLabel className="col-sm-12" control={
                                                <Checkbox color="primary" onChange={this.handleChangeCheckBox('chkAll')} checked={this.state.chkAll} value="chkAll" />
                                            } label={this.context.intl.formatMessage({
                                                id: "I_don't_need_a_reply"
                                            })}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div style={{ float: 'right', width: '100%' }}>
                                            <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveIncident()} className="mr-10 mb-10 text-white"><IntlMessages id="Add_to_the_timeline" /></MatButton>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>

                            <div className="boxlayout">

                     <RctCollapsibleCard heading={<IntlMessages id="Ticket_history" />}>
                        <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">

                           <MUIDataTable className="w-100"
                              data={["Test", "Test", "Test"]}
                              columns={columns}
                              options={options}
                           />
                           </div>
                        </div>
                     </RctCollapsibleCard>
                  </div>

                        </div>
                    </div>
                </Form>
            </div >
        );
    }
}
export default ViewSecurityIncident;