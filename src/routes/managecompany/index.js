//Manage Company

import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Link } from 'react-router-dom';
import MatButton from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

//API Services
import * as ApiService from '../../actions/ApiServices';
import DeleteConfirmationDialog from '../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from '../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../components/CustomFooter/CustomFooter';
import { Tooltip } from '@material-ui/core';
import { intlShape } from 'react-intl';
import { socketConnect } from 'socket.io-react';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class ManageCompany extends Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      activeIndex: 0,
      Companies: [],
      selectedDeleteID: 0,
      loading: true,
      logoBaseURL: ''
   }

   async componentDidMount() {
      this.checkUserRole();
      // this.setState({ logoBaseURL: 'http://180.211.103.172:8093/api/' });
      this.setState({ loading: true });
      const logoBaseURL = await ApiService.ConfigAPIURL();
      await this.setState({ logoBaseURL });
      await this.GetCompanies();
      this.setState({ loading: false });
   }

   checkUserRole() {
      ApiService.CheckUserRole(this.props);
   }

   EditCompany(companyid) {
      this.props.history.push('/app/editcompany/' + cryptr.encrypt(companyid));
   }

   handleChange(value) {
      this.setState({ activeIndex: value });
   }

   async GetCompanies() {
      var Companies = await ApiService.GetAPI('Company');
      this.setState({ Companies: Companies });
   }

   // on delete
   onDeleteCompany(companyid) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedDeleteID: cryptr.encrypt(companyid) });
   }

   // delete
   async delete() {
      var users = await ApiService.DeleteAPI('Company/' + this.state.selectedDeleteID);
      if (users) {
         // this.props.history.push('app/')
      }
      this.setState({ selectedDeleteID: 0 });
      this.GetCompanies();
      this.refs.deleteConfirmationDialog.close();
   }

   render() {
      const { activeIndex, loading } = this.state;


      const columns = [
         { name: "Logo", options: { filter: false, sort: false, customHeadRender: (columnMeta, updateDirection) => (<th key={0}><IntlMessages id="Logo" /></th>) } },
         { name: "Company Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Company_Name" /></th>) } },
         { name: "Legel Entity", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Legal_Entity" /></th>) } },
         { name: "Jumb of Commerce", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Jumb_of_Commerc" /></th>) } },
         { name: "Email Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={4} onClick={() => updateDirection(4)}><IntlMessages id="Email_Adres" /></th>) } },
         { name: "IP Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={5} onClick={() => updateDirection(5)}><IntlMessages id="IP_Address" /></th>) } },
         { name: "Total Users", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={6} onClick={() => updateDirection(6)}><IntlMessages id="Total_Users" /></th>) } },
         {
            name: "Actions",
            options: {
               filter: false,
               sort: false,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={7}><IntlMessages id="Actions" />
                  </th>
               )
            }
         }];
      const data = [
      ];
      const options = {
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,
         selectableRows: 'none',
         responsive: 'scrollFullHeight',
         textLabels: {
            body: {
               noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
            }
         },
         customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
            return (
               <CustomFooter
                  count={count}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  changeRowsPerPage={changeRowsPerPage}
                  changePage={changePage}
                  textLabels={textLabels} />
            );
         },
         onCellClick: (colData, cellMeta) => {
            if (cellMeta.colIndex != 7) {
               this.props.history.push('editcompany/' + cryptr.encrypt(this.state.Companies[cellMeta.dataIndex].id));
            }
         }
      };

      return (
         <div className="data-table-wrapper">
            <PageTitleBar title={<IntlMessages id="Manage_Company" />} redirect='/app/managecompany' match={this.props.match} />
            <RctCollapsibleCard fullBlock>
               {loading ? <RctSectionLoader /> : ''}
               <MUIDataTable style={{ overflowX: 'scroll' }}
                  data={this.state.Companies.map(comp => {
                     return [
                        <img alt={'test'} className="roundimg" src={this.state.logoBaseURL + 'uploadapi/getlogoimage/' + (comp.Logo != '' ? comp.Logo : '1')}></img>,
                        comp.Name,
                        comp.LegalEntity,
                        comp.CommerceNo,
                        comp.ContactPersonEmail,
                        comp.IpAddress,
                        <div style={{ textAlign: 'right' }}>{comp.UserCompanies.length}</div>,
                        <div className="row actionbtns" style={{ justifyContent: 'flex-end', width: '95px' }}>
                           <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                              <IconButton className="text-primary" onClick={() => this.EditCompany(comp.id)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton></Tooltip>
                           <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                              <IconButton className="text-danger" onClick={() => this.onDeleteCompany(comp.id)} aria-label="Add an alarm"><i className="zmdi zmdi-delete"></i></IconButton></Tooltip>
                        </div>
                     ]
                  })}
                  columns={columns}
                  options={options}
               />
            </RctCollapsibleCard>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               // message="This Company might be associated with more than one Pentest. So are you sure want to delete this permanently ?"
               onConfirm={() => this.delete()}
            />
         </div>
      );
   }
}
export default socketConnect(ManageCompany);