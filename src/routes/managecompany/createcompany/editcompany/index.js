//Create Client User

import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import MUIDataTable from "mui-datatables";
import AssestDialog from '../../../../components/modalDialogs/assestDialog';
import SystemDialog from '../../../../components/modalDialogs/systemDialog';
import AssignedUserDialog from '../../../../components/modalDialogs/assignuserDialog';
import IconButton from '@material-ui/core/IconButton';
import * as ApiService from '../../../../actions/ApiServices';
import * as TranslationService from '../../../../actions/TranslationService';
import DeleteConfirmationDialog from '../../../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import CustomFooter from '../../../../components/CustomFooter/CustomFooter';
import { intlShape } from 'react-intl';
import { Tooltip } from '@material-ui/core';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class EditCompany extends Component {

   constructor(props) {
      super(props);
      console.log('CompanyId', props);
      if (this.props.Company && this.props.Company.id && this.props.Company.id != 0) {
         this.state = {
            changeprops: true,
            SelectedModel: null,
            CompanyID: 0,
            Company: {
               Name: '',
               UserCompanies: [],
               Assets: [],
               Systems: []
            },
            selectedDeleteID: 0,
            loading: false,
         }
         this.GetCompay(cryptr.encrypt(this.props.Company.id));
         // this.setState({ changeprops: true });
      }
   }

   static contextTypes = {
      intl: intlShape,
   };

   // state = {
   //    SelectedModel: null,
   //    CompanyID: 0,
   //    Company: {
   //       Name: '',
   //       UserCompanies: [],
   //       Assets: [],
   //       Systems: []
   //    },
   //    selectedDeleteID: 0,
   //    loading: false,
   // }

   async componentDidMount() {
      this.setState({ loading: true });
      console.log('CompanyId', this.props.Company);
      // const { Company } = this.props;
      // if (Company && Company.id && Company.id != 0) {
      //    await this.GetCompay(Company.id);
      // }
      this.setState({ loading: false });
   }

   async GetCompay(companyid) {
      console.log('getcompid', companyid);

      var Company = await ApiService.GetAPI('Company/' + companyid);
      if (Company) {
         this.setState({ CompanyID: Company.id });

         Company.Systems = Company.Systems.filter(x => x.IsActive == 1);
         Company.Assets = Company.Assets.filter(x => x.IsActive == 1);

         this.setState({ Company: Company });

         var NewComp = {
            id: Company.id,
            Name: Company.Name,
            LegalEntity: Company.LegalEntity,
            Logo: Company.Logo,
            CommerceNo: Company.CommerceNo ? Company.CommerceNo == 0 ? '' : Company.CommerceNo : '',
            ContactPersonEmail: Company.ContactPersonEmail,
            IpAddress: Company.IpAddress
         }
         this.setState({ NewCompany: NewComp });

      } else {
         this.props.history.push('/app/managecompany');
      }
   }

   shouldComponentUpdate(nextProps, nextState) {

      if (this.props.Company !== nextProps.Company) {
         this.setState({ changeprops: false });
         return false;
      } else {
         console.log('this.state.changeprops', this.state.changeprops);
         if (!this.state.changeprops) {
            return false;
         } else {
            return true;
         }
      }
      // return this.shallowCompare(this, nextProps, nextState);
   }

   shallowCompare(instance, nextProps, nextState) {
      // return ((instance.props === nextProps));
      return (
         this.shallowEqual(instance.props, nextProps)
      );
      // !this.shallowEqual(instance.state, nextState)

   }

   shallowEqual(objA, objB) {
      if (objA === objB) {
         return true;
      }

      if (typeof objA !== 'object' || objA === null ||
         typeof objB !== 'object' || objB === null) {
         return false;
      }

      var keysA = Object.keys(objA);
      var keysB = Object.keys(objB);

      if (keysA.length !== keysB.length) {
         return false;
      }

      // Test for A's keys different from B.
      var bHasOwnProperty = hasOwnProperty.bind(objB);
      for (var i = 0; i < keysA.length; i++) {
         if (!bHasOwnProperty(keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) {
            return false;
         }
      }

      return true;
   }


   GetTranslatedText(translatedData, field, value) {
      return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
   }

   OpenAssignedUser() {
      this.refs.assignedUserDialog.handleAddClickOpen(this.state.CompanyID);
   }

   OpenSystemDialog() {
      this.refs.systemDialog.handleAddClickOpen(this.state.CompanyID);
   }

   OpenAssetDialog() {
      this.refs.assestDialog.handleAddClickOpen(this.state.CompanyID);
   }

   EditassignedUser(companyid, userid, companyuserid) {
      this.refs.assignedUserDialog.handleEditClickOpen(companyid, userid, cryptr.encrypt(companyuserid));
   }

   EditSystem(id, companyid) {
      this.refs.systemDialog.handleEditClickOpen(cryptr.encrypt(id), companyid);
   }

   EditAsset(id, companyid) {
      this.refs.assestDialog.handleEditClickOpen(cryptr.encrypt(id), companyid);
   }

   // on delete
   onDeleteSystem(systemid) {
      this.refs.deletesystemConfirmationDialog.open();
      this.setState({ SelectedModel: 'System' });
      this.setState({ selectedDeleteID: cryptr.encrypt(systemid) });
   }

   onDeleteAsset(assetid) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ SelectedModel: 'Asset' });
      this.setState({ selectedDeleteID: cryptr.encrypt(assetid) });
   }

   onDeleteAssignedUser(userid) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ SelectedModel: 'UserCompany' });
      this.setState({ selectedDeleteID: cryptr.encrypt(userid) });
   }

   // delete
   async delete() {
      if (this.state.SelectedModel != null) {
         var users;
         if (this.state.SelectedModel == 'UserCompany') {
            users = await ApiService.HardDeleteAPI(this.state.SelectedModel + '/' + this.state.selectedDeleteID);
         } else {
            users = await ApiService.DeleteAPI(this.state.SelectedModel + '/' + this.state.selectedDeleteID);
         }

         if (users) {
            // this.props.history.push('app/')
         }
         this.setState({ selectedDeletedUserID: 0 });
      }
      this.GetCompay(cryptr.encrypt(this.state.CompanyID));
      this.refs.deleteConfirmationDialog.close();
      this.refs.deletesystemConfirmationDialog.close();
   }

   BackFromProps(id) {
      setTimeout(() => {
         this.GetCompay(id);
      }, 1500);
   }

   render() {
      const { loading } = this.state;
      const columns = [
         { name: "Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Name" /></th>) } },
         { name: "Email", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Email_Adres" /></th>) } }, {
            name: "Actions",
            options: {
               filter: false,
               sort: false,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={2}><IntlMessages id="Actions" />
                  </th>
               )
            }
         }, { name: "id", label: "id", options: { display: false } }];
      const columnsSystems = [
         { name: "Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Name" /></th>) } },
         { name: "Host Details", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Host_Details" /></th>) } },
         // { name: "Remarks", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Remarks" /></th>) } },
         {
            name: "Actions",
            options: {
               filter: false,
               sort: false,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={3}><IntlMessages id="Actions" />
                  </th>
               )
            }
         }];
      // const columnsAssets = ["Name", "Department", "System", "Hosts", "Remarks", "Actions"];
      const columnsAssets = [
         { name: "Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Name" /></th>) } },
         { name: "System", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Systems" /></th>) } },
         { name: "Hosts", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Hosts" /></th>) } },
         { name: "Remarks", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Remarks" /></th>) } },
         {
            name: "Actions",
            options: {
               filter: false,
               sort: false,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={4}><IntlMessages id="Actions" />
                  </th>
               )
            }
         }];
      const data = [];
      // const dataSystems = this.state.Systems;
      const options = {
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,
         selectableRows: 'none',
         responsive: 'scrollFullHeight',
         textLabels: {
            body: {
               noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
            }
         },
         customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
            return (
               <CustomFooter
                  count={count}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  changeRowsPerPage={changeRowsPerPage}
                  changePage={changePage}
                  textLabels={textLabels} />
            );
         },
         // serverSide: true
      };

      return (
         <div>
            <div className="row">
               <div className="col-sm-12 col-md-12 col-xl-12 CompanyTitle">
                  <RctCollapsibleCard fullBlock>

                     <div className="mt-10 mr-15" style={{ float: 'right' }}>
                        <Button variant="contained" className="btn-primary text-white btn-block" onClick={() => this.OpenAssignedUser()}><IntlMessages id="Assign_users" /></Button>
                        <AssignedUserDialog ref="assignedUserDialog" backFromProps={this.BackFromProps.bind(this)} />
                     </div>

                     <MUIDataTable
                        title={<IntlMessages id="Assign_user_to_this_company" />}
                        data={this.state.Company.UserCompanies.map((usr, key) => {
                           return [
                              usr.User != null ? (usr.User.FirstName + ' ' + usr.User.LastName) : '',
                              usr.User != null ? usr.User.Email : '',
                              <div className="row actionbtns" key={key} style={{ justifyContent: 'flex-end' }}>
                                 <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                                    <IconButton className="text-primary" onClick={() => this.EditassignedUser(usr.CompanyId, usr.UserId, usr.id)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton>
                                 </Tooltip>
                                 <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                                    <IconButton className="text-danger" onClick={() => this.onDeleteAssignedUser(usr.id)} aria-label="Add an alarm"><i className="zmdi zmdi-delete"></i></IconButton></Tooltip>
                              </div>,
                              usr.id
                           ]
                        })}
                        columns={columns}
                        options={options}
                     />
                  </RctCollapsibleCard>
               </div>
            </div>

            <div className="row">
               <div className="col-sm-12 col-md-12 col-xl-6 CompanyTitle">
                  <RctCollapsibleCard fullBlock>

                     <div className="mt-10 mr-15" style={{ float: 'right' }}>
                        <Button variant="contained" className="btn-primary text-white btn-block" onClick={() => this.OpenSystemDialog()}><IntlMessages id="Add_system" /></Button>
                        <SystemDialog ref="systemDialog" backFromProps={this.BackFromProps.bind(this)} />
                     </div>

                     <MUIDataTable
                        title={<IntlMessages id="Systems" />}
                        data={this.state.Company.Systems.map((sys, key) => {
                           return [
                              sys.Remarks,
                              sys.Name,
                              // sys.Remarks ? sys.Remarks.toString().length > 30 ? sys.Remarks.toString().substr(0, 30) + '...' : sys.Remarks.toString() : '--',
                              <div className="row actionbtns" key={key} style={{ justifyContent: 'flex-end', minWidth: '90px' }}>
                                 <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                                    <IconButton className="text-primary" onClick={() => this.EditSystem(sys.id, sys.CompanyId)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton></Tooltip>
                                 <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                                    <IconButton className="text-danger" onClick={() => this.onDeleteSystem(sys.id)} aria-label="Add an alarm"><i className="zmdi zmdi-delete"></i></IconButton></Tooltip>
                              </div>
                           ]
                        })}
                        columns={columnsSystems}
                        options={options}
                     />
                  </RctCollapsibleCard>
               </div>
               <div className="col-sm-12 col-md-12 col-xl-6 CompanyTitle">
                  <RctCollapsibleCard fullBlock>

                     <div className="mt-10 mr-15" style={{ float: 'right' }}>
                        <Button variant="contained" className="btn-primary text-white btn-block" onClick={() => this.OpenAssetDialog()}><IntlMessages id="Add_Assets" /></Button>
                        <AssestDialog ref="assestDialog" backFromProps={this.BackFromProps.bind(this)} />
                     </div>

                     <MUIDataTable
                        title={<IntlMessages id="Assets" />}
                        data={this.state.Company.Assets.map((asts, key) => {
                           return [
                              asts.Name,
                              (asts.System != null ? asts.System.Name : '--'),
                              (asts.Hosts.length > 0 ? (asts.Hosts[0].HostIP) + ' ...' : '--'),
                              asts.Remarks ? asts.Remarks.toString().length > 30 ? asts.Remarks.toString().substr(0, 30) + '...' : asts.Remarks.toString() : '--',
                              <div className="row actionbtns" key={key} style={{ justifyContent: 'flex-end', minWidth: '90px' }}>
                                 <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                                    <IconButton className="text-primary" onClick={() => this.EditAsset(asts.id, asts.CompanyId)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton></Tooltip>
                                 <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                                    <IconButton className="text-danger" onClick={() => this.onDeleteAsset(asts.id)} aria-label="Add an alarm"><i className="zmdi zmdi-delete"></i></IconButton></Tooltip>
                              </div>
                           ]
                        })}
                        columns={columnsAssets}
                        options={options}
                     />
                  </RctCollapsibleCard>
               </div>
            </div>

            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               onConfirm={() => this.delete()}
            />
            <DeleteConfirmationDialog
               ref="deletesystemConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               // message="This System might be associated with more than one Asset. So are you sure want to delete this permanently ?"
               onConfirm={() => this.delete()}
            />
         </div>
      );
   }
}
export default EditCompany;