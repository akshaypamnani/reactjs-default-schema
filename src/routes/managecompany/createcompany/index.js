//Create Client User

import React, { Component } from 'react';
import {
   // Form,
   FormGroup,
   Label,
   // Input,
   Col,
   FormText
} from 'reactstrap';
import Button from '@material-ui/core/Button';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import MUIDataTable from "mui-datatables";
import { Link } from 'react-router-dom'
import AssestDialog from '../../../components/modalDialogs/assestDialog';
import SystemDialog from '../../../components/modalDialogs/systemDialog';
import AssignedUserDialog from '../../../components/modalDialogs/assignuserDialog';
import IconButton from '@material-ui/core/IconButton';
import { isEmail } from 'validator';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Select from 'react-validation/build/select';

//API Services
import * as ApiService from '../../../actions/ApiServices';
import * as TranslationService from '../../../actions/TranslationService';
import DeleteConfirmationDialog from '../../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import { intlShape } from 'react-intl';
import EditCompany from './editcompany';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const email = (value) => {
   if (!isEmail(value)) {
      return <span className="form-error is-visible">{value} <IntlMessages id="is_invalid_email_address" /></span>;
   }
};

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

const Filecheck = (value, props) => {
   if (value) {
      var fileExt = value.split('.').pop().toString().trim().toLowerCase();
      var validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];
      if (!validFileExtensions.includes(fileExt)) {
         return <span className="form-error is-visible">Invalid File Extension</span>;
      }
   }
};

const IPCheck = (value, props) => {
   if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
   }
};

const OnlyNumCheck = (value, props) => {
   if (value && value.length > 0) {
      if (!(/^(0|[0-9][0-9]*)$/.test(value)) || (props.isCheckable && !props.checked)) {
         return <span className="form-error is-visible"><IntlMessages id="Invalid_Commerce_no" /></span>;
      }
   }
};

class CreateCompany extends Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      CompanyID: 0,
      Company: {
         Name: '',
         UserCompanies: [],
         Assets: [],
         Systems: []
      },
      NewCompany: {
         id: 0,
         Name: '',
         LegalEntity: '',
         Logo: '',
         CommerceNo: '',
         ContactPersonEmail: '',
         IpAddress: ''
      },
      loading: false,
      UploadFile: null
   }

   async componentDidMount() {
      this.checkUserRole();
      this.setState({ loading: true });
      const { id } = this.props.match.params;
      if (id) {
         await this.GetCompay(id);
      }
      this.setState({ loading: false });
   }
   
   checkUserRole() {
      ApiService.CheckUserRole(this.props);
  }

   async GetCompay(companyid) {
      var Company = await ApiService.GetAPI('Company/' + companyid);
      if (Company) {
         this.setState({ CompanyID: Company.id });

         Company.Systems = Company.Systems.filter(x => x.IsActive == 1);
         Company.Assets = Company.Assets.filter(x => x.IsActive == 1);

         this.setState({ Company: Company });

         var NewComp = {
            id: Company.id,
            Name: Company.Name,
            LegalEntity: Company.LegalEntity,
            Logo: Company.Logo,
            CommerceNo: Company.CommerceNo ? Company.CommerceNo == 0 ? '' : Company.CommerceNo : '',
            ContactPersonEmail: Company.ContactPersonEmail,
            IpAddress: Company.IpAddress
         }
         this.setState({ NewCompany: NewComp });

      } else {
         this.props.history.push('/app/managecompany');
      }
   }

   GetTranslatedText(translatedData, field, value) {
      return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
   }

   async SaveCompany() {
      this.form.validateAll();

      this.setState({ loading: true });
      if (this.form.getChildContext()._errors.length <= 0) {
         var Company;
         var Image = {};
         if (this.state.UploadFile) {
            let fd = new FormData();
            fd.append('file', this.state.UploadFile[0]);
            Image = await ApiService.UploadAPI(fd);
         } else {
            Image.fileName = this.state.NewCompany.Logo;
         }
         if (Image) {
            var NewCompany = this.state.NewCompany;

            var NewCompany = {
               id: this.state.NewCompany.id,
               Name: this.state.NewCompany.Name.toString().trim(),
               LegalEntity: this.state.NewCompany.LegalEntity.toString().trim(),
               CommerceNo: this.state.NewCompany.CommerceNo,
               ContactPersonEmail: this.state.NewCompany.ContactPersonEmail.toString().trim(),
               IpAddress: this.state.NewCompany.IpAddress
            }

            NewCompany.Logo = Image.fileName;
            if (NewCompany.id != 0) {
               Company = await ApiService.PostAPI('Company/' + cryptr.encrypt(NewCompany.id), NewCompany);
            } else {
               Company = await ApiService.PutAPI('Company', NewCompany);
            }
            var NewComp = {
               id: Company.id,
               Name: Company.Name,
               LegalEntity: Company.LegalEntity,
               Logo: Company.Logo,
               CommerceNo: Company.CommerceNo,
               ContactPersonEmail: Company.ContactPersonEmail,
               IpAddress: Company.IpAddress
            }
            this.setState({ NewCompany: NewComp });
            this.setState({ CompanyID: Company.id });
         }
         this.props.history.push('/app/managecompany');
      }
      this.setState({ loading: false });
   }

   handleChange = (event, key) => {
      let NewCompany = Object.assign({}, this.state.NewCompany);    //creating copy of object
      NewCompany[key] = event.target.value;                        //updating value
      this.setState({ NewCompany });
   }

   render() {
      const { loading } = this.state;

      return (
         <div className="formelements-wrapper">
            <PageTitleBar title={<IntlMessages id="Add/edit_Company" />} redirect='/app/managecompany' match={this.props.match} />
            <div className="row">
               <div className="col-sm-12 col-md-12 col-xl-12">
                  <RctCollapsibleCard>
                     {loading ? <RctSectionLoader /> : ''}
                     <Form ref={c => { this.form = c }}>
                        <div className="row">
                           <div className="col-sm-12 col-md-12 col-xl-6">
                              <FormGroup>
                                 <Label for="firstname"><IntlMessages id="Company_Name" />
                                    <span className="required">*</span>
                                 </Label>
                                 <Input className="form-control" type="text" maxLength="50" name="firstname" value={this.state.NewCompany.Name} validations={[required]} onChange={(e) => this.handleChange(e, 'Name')} id="firstname" placeholder={this.context.intl.formatMessage({ id: 'Company_Name' })} />
                              </FormGroup>
                           </div>
                           {this.state.NewCompany.id <= 0 ?
                              <div className="col-sm-12 col-md-12 col-xl-6">
                                 <FormGroup row>
                                    <Label for="File-1" ><IntlMessages id="Logo" />
                                       <span className="required">*</span>
                                    </Label>
                                    <Col sm={12}>
                                       <Input type="file" accept="image/*" onChange={(e) => this.setState({ UploadFile: e.target.files })} validations={[required, Filecheck]} name="file" id="File-1" />
                                       <FormText color="muted">
                                       </FormText>
                                    </Col>
                                 </FormGroup>
                              </div> : ''}
                           {this.state.NewCompany.id > 0 ?
                              <div className="col-sm-12 col-md-12 col-xl-6">
                                 <FormGroup row>
                                    <Label for="File-1" ><IntlMessages id="Logo" />
                                    </Label>
                                    <br />
                                    <Label className="col-sm-12" for="File-1" ><IntlMessages id="Logo" /> : {this.state.NewCompany.Logo}
                                    </Label>
                                    <Col sm={12}>
                                       <Input type="file" accept="image/*" onChange={(e) => this.setState({ UploadFile: e.target.files })} validations={[Filecheck]} name="file" id="File-1" />
                                       <FormText color="muted">
                                       </FormText>
                                    </Col>
                                 </FormGroup>
                              </div> : ''}
                        </div>

                        <div className="row">
                           <div className="col-sm-12 col-md-12 col-xl-6">
                              <FormGroup>
                                 <Label for="LegalEntity"><IntlMessages id="Legal_Entity" /></Label>
                                 <Input className="form-control" maxLength="50" type="text" value={this.state.NewCompany.LegalEntity} onChange={(e) => this.handleChange(e, 'LegalEntity')} name="LegalEntity" id="LegalEntity" placeholder={this.context.intl.formatMessage({ id: 'Legal_Entity' })} />
                              </FormGroup>
                           </div>
                           <div className="col-sm-12 col-md-12 col-xl-6">
                              <FormGroup>
                                 <Label for="jbCom"><IntlMessages id="Jumb_of_Commerc" /></Label>
                                 <Input className="form-control" maxLength="10" validations={[OnlyNumCheck]} type="text" value={this.state.NewCompany.CommerceNo} onChange={(e) => this.handleChange(e, 'CommerceNo')} name="CommerceNo" id="jbCom" placeholder={this.context.intl.formatMessage({ id: 'Jumb_of_Commerc' })} />
                              </FormGroup>
                           </div>
                        </div>

                        <div className="row">
                           <div className="col-sm-12 col-md-12 col-xl-6">
                              <FormGroup>
                                 <Label for="ContactPersonEmail"><IntlMessages id="Email_Adres" />
                                    <span className="required">*</span>
                                 </Label>
                                 <Input className="form-control" maxLength="150" type="text" value={this.state.NewCompany.ContactPersonEmail} validations={[required, email]} onChange={(e) => this.handleChange(e, 'ContactPersonEmail')} name="ContactPersonEmail" id="ContactPersonEmail" placeholder={this.context.intl.formatMessage({ id: 'Email_Adres' })} />
                              </FormGroup>
                           </div>


                           <div className="col-sm-12 col-md-12 col-xl-6">
                              <FormGroup>
                                 <Label for="IpAddress"><IntlMessages id="IP_Address" />
                                    <span className="required">*</span>
                                 </Label>
                                 <Input className="form-control" validations={[IPCheck]} type="text" value={this.state.NewCompany.IpAddress} onChange={(e) => this.handleChange(e, 'IpAddress')} name="IpAddress" id="IpAddress" placeholder={this.context.intl.formatMessage({ id: 'IP_Address' })} />
                              </FormGroup>
                           </div>
                        </div>

                        {this.state.NewCompany.id != 0 ?
                           <EditCompany Company={this.state.NewCompany} />
                           : ''}
                        <div className="row">
                           <div style={{ float: 'right', width: '100%' }}>
                              <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveCompany()} className="mr-10 mb-10 text-white"><IntlMessages id="Create/Update" /></MatButton>
                              <MatButton variant="contained" style={{ float: 'right', color: '#5d92f4' }} onClick={() => this.props.history.push('/app/managecompany')} className="btn-default mr-10 mb-10"><IntlMessages id="Cancel" /></MatButton>
                           </div>
                        </div>
                     </Form>
                  </RctCollapsibleCard>
               </div>
            </div>
            {/* <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               onConfirm={() => this.delete()}
            />
            <DeleteConfirmationDialog
               ref="deletesystemConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               // message="This System might be associated with more than one Asset. So are you sure want to delete this permanently ?"
               onConfirm={() => this.delete()}
            /> */}
         </div>
      );
   }
}
export default CreateCompany;