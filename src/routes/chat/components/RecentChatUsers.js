/**
 * Recent Chat Users
 */
import React, { Component } from 'react';
import List from '@material-ui/core/List';
import { connect } from 'react-redux';
import * as ApiService from '../../../actions/ApiServices';

// components
import UserListItem from './UserListItem';

// actions
import { chatWithSelectedUser, getRecentChatUsers, setRecentChatUsers } from 'Actions';

class RecentChatUsers extends Component {

    state = {
        Chatrooms: []
    }

    async componentWillMount() {
        await this.fetchRecentChatUsers();
    }

    /**
     * Fetch Recent User
     */
    async fetchRecentChatUsers() {

        // var ChatRoomData = {
        //     Where: {
        //         IsActive: 1
        //     }
        // }
        // var Chatrooms = await ApiService.PostWhereAPI('execute/withoutnestedconditions/Chatroom', ChatRoomData);
        // console.log('ChatRooms', Chatrooms);

        // if (Chatrooms && Chatrooms.length > 0) {
        //     Chatrooms.forEach(chtrm => {
        //         chtrm.isSelectedChat = false;
        //     });
        //     Chatrooms[0].isSelectedChat = true;
        // }
        // this.setState({ Chatrooms });
        // await this.props.setRecentChatUsers(Chatrooms);
        await this.props.getRecentChatUsers();

    }

    /**
     * Swicth Chat With User
     * @param {*object} user
     */
    switchChatWithUser(user) {
        console.log('user', user);
        this.props.chatWithSelectedUser(user);
    }

    render() {
        const { Chatrooms } = this.state;
        const { recentChatUsers, selectedUser } = this.props;
        if (recentChatUsers === null) {
            return (
                <div className="no-found-user-wrap">
                    <h4>No User Found</h4>
                </div>
            );
        }
        return (
            <List className="p-0 mb-0">
                {recentChatUsers.map((user, key) => (
                    <UserListItem
                        selectedUser={selectedUser}
                        user={user}
                        key={key}
                        onClickListItem={() => this.switchChatWithUser(user)}
                    />
                ))}
            </List>
        );
    }
}

const mapStateToProps = ({ chatAppReducer }) => {
    return chatAppReducer;
};

export default connect(mapStateToProps, {
    chatWithSelectedUser,
    getRecentChatUsers,
    setRecentChatUsers
})(RecentChatUsers);
