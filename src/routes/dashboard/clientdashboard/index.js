import React, { Component } from 'react'
import { Helmet } from "react-helmet";
import { Line } from 'react-chartjs-2';
import { Pie } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import { Scrollbars } from 'react-custom-scrollbars';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
let Datetime = require('react-datetime');
import * as TranslationService from '../../../actions/TranslationService';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

export default class ClientDashboard extends Component {

	static contextTypes = {
		intl: intlShape,
	};

	state = {
		activeIndex: 0,
		Companies: [],
		assets: [],
		PentestsList: [],
		Pentests: [],
		Company: "0",
		asset: "0",
		Pentest: "0",
		TrendsFilter: 1,
		selectedCompany: 0,
		selectedPentest: 0,
		selectedAsset: 0,
		Vulnerabilities: [],
		todate: "",//new Date(),
		fromdate: "",//new Date(),
		date: new Date(),
		IsDataLoaded: false,
		loading: false,
		CorrectRisk: 0,
		ExtremeRisk: 0,
		HighRisk: 0,
		MediumRisk: 0,
		LowRisk: 0,
		DonePentestCount: 0,
		DoneAssessmentCount: 0,
		DoneQuickAssessmentCount: 0,
		TrendsDates: [],
		TrendsLabels: [],
		TrendsFilter: 2,
		DashboardTrends: [],
		KnownTrends: [],
		UnknownTrends: [],
		ResolvedTrends: [],
		AcceptedTrends: [],
		CommonLabelPentestCheckList: [],
		CommonPentestCheckListCount: [],
		CommonLabelVulnerableSystems: [],
		CommonVulnerableSystemCount: [],
	}

	async componentDidMount() {
		this.checkUserRole();
		this.setState({ loading: true });
		await this.CheckPentests();
		// await this.GetDates();
		// await this.GetMonths();
		await this.GetDates(30);
		await this.GetCompanies();
		this.setState({ loading: false });
	}

	checkUserRole() {
		var userRole = SessionService.GetUserRole('userRole');
		if (userRole) {
			if (userRole.Name != "ClientUser") {
				console.log('ClientUser', userRole.Name);
				this.props.history.push('/session/login');
			} 
		}
	}

	async CheckPentests() {
		this.setState({ loading: true });
		var UserID = localStorage.getItem('user_id');
		if (UserID) {
			var CheckPentests = await ApiService.GetAPI('Dashboard/CheckPentests/' + cryptr.encrypt(UserID));
			if (CheckPentests.length > 0) {
			} else {
				this.props.history.push('/company/companyvulnerabilities');
			}
			this.setState({ loading: false });
		}
	}

	ChangeTrendsFilter = async (event) => {
		let TrendsFilter = event.target.value;
		await this.setState({ TrendsFilter });

		if (TrendsFilter == '2') {
			await this.GetDates(30);
		} else if (TrendsFilter == '3') {
			await this.GetDates(7);
		} else {
			await this.GetMonths()
		}
		await this.GetDashboardTrends();
	}

	GetDates(ndays) {
		var TrendsLabels = [];
		for (let day = 0; day < ndays; day++) {
			let date = new Date();
			date = moment(date).add(-day, 'days').format('DD-MMM');
			TrendsLabels.push(date);
		}
		TrendsLabels = TrendsLabels.reverse();
		this.setState({ TrendsLabels });
	}

	GetMonths() {
		var TrendsLabels = [];
		for (let month = 0; month < 12; month++) {
			let date = new Date();
			date = moment(date).add(-month, 'months').format('MMM-YY');
			TrendsLabels.push(date);
		}
		TrendsLabels = TrendsLabels.reverse();
		this.setState({ TrendsLabels });
	}

	async GetDashboardTrends() {
		this.setState({ loading: true });
		var TrendData = {
			currentdate: moment(new Date()).add(1, 'days').format('YYYY-MM-DD')
		};
		if (this.state.Company && this.state.Company != "0") {
			TrendData.companyid = this.state.Company;
		}
		if (this.state.Pentest && this.state.Pentest != "0") {
			TrendData.pentestid = this.state.Pentest;
		}
		if (this.state.TrendsFilter && this.state.TrendsFilter != "0") {
			TrendData.TrendsFilter = this.state.TrendsFilter;
		}

		var DashboardTrends = await ApiService.PostWhereAPI('Dashboard/dashboardtrends', TrendData);

		if (DashboardTrends) {
			await this.setState({ DashboardTrends });
			await this.GetTrendsData();
		}

		this.setState({ loading: false });
	}

	async GetCompanies() {
		this.setState({ loading: true });
		var UserID = localStorage.getItem("user_id");
		if (UserID) {
			var User = await ApiService.GetAPI('User/' + cryptr.encrypt(UserID));
			this.setState({ Companies: User.UserCompanies });
			if (User.UserCompanies.length > 0) {
				this.setState({ Company: User.UserCompanies[0].CompanyId });
				await this.GetPentests(User.UserCompanies[0].CompanyId);
				await this.GetAssets(User.UserCompanies[0].CompanyId);
				await this.GetDashboardData();
				await this.GetDashboardTrends();
			}
		} else {
			this.props.history.push('/session/login');
		}
		this.setState({ loading: false });
	}

	async GetPentests(companyid) {
		this.setState({ loading: true });
		var Pentests = [];
		if (companyid && parseInt(companyid) > 0) {
			// var PenTestData = {
			// 	Where: {
			// 		CompanyId: companyid,
			// 		IsActive: 1
			// 	}
			// }
			// Pentests = await ApiService.PostWhereAPI('execute/withoutnestedconditions/PenTest', PenTestData);

			Pentests = await ApiService.GetAPI('pentestapi/getpentestbycompany/' + cryptr.encrypt(companyid));
			Pentests = Pentests.filter(x => x.IsPublished == 1 && x.RecurrenceType != 3);

		} else {
			Pentests = await ApiService.GetAPI('PenTest');
		}
		this.setState({ Pentests: Pentests });
		this.setState({ loading: false });
	}

	async GetAssets(companyid) {
		this.setState({ loading: true });
		var Assets = [];
		if (companyid && companyid != 0) {
			var Assetdata = {
				Where: {
					CompanyId: companyid,
					IsActive: 1
				}
			}
			Assets = await ApiService.PostWhereAPI('execute/conditions/Asset', Assetdata);

		} else {
			Assets = await ApiService.GetAPI('Asset');
		}
		this.setState({ assets: Assets });
		this.setState({ loading: false });
	}

	handleChangedd = (event, key) => {
		this.setState({ [key]: event.target.value });
	}

	async GetDashboardData() {
		this.setState({ loading: true });
		var dashboarddata = {};
		if (this.state.Company && this.state.Company != "0") {
			dashboarddata.CompanyId = this.state.Company;
		}
		if (this.state.Pentest && this.state.Pentest != "0") {
			dashboarddata.PentestId = this.state.Pentest;
		}
		if (this.state.asset && this.state.asset != "0") {
			dashboarddata.AssetId = this.state.asset;
		}
		if ((this.state.fromdate && this.state.fromdate != "") && this.state.todate && this.state.todate != "") {
			var daterange = {
				from: this.state.fromdate,
				to: this.state.todate
			}
			dashboarddata.daterange = daterange;
		}
		var Dashboardpentest = await ApiService.PostWhereAPI('Dashboard/GetClientDashboardPentests', dashboarddata);
		if (Dashboardpentest) {

			if (Dashboardpentest.PenTests && Dashboardpentest.PenTests.length > 0) {
				Dashboardpentest.PenTests.forEach(pentest => {
					pentest.Vulnerabilities = pentest.Vulnerabilities.filter(x => x.IsActive == 1);
				});
			}

			if (Dashboardpentest.Vulnerabilities && Dashboardpentest.Vulnerabilities.length > 0) {
				Dashboardpentest.Vulnerabilities.forEach(vuln => {
					if (vuln.LanguageTranslations && vuln.LanguageTranslations.length > 0) {
						vuln.LanguageTranslations = vuln.LanguageTranslations.filter(x => x.PageRefId == 2);
					}
				});
			}

			var Vulnerabilities = Dashboardpentest.Vulnerabilities.sort((x, y) => x.Order - y.Order);

			// var Vulnerabilitiesarr1 = Vulnerabilities.filter(x => x.Risk != 1);
			// var Vulnerabilitiesarr2 = Vulnerabilities.filter(x => x.Risk == 1);

			// Vulnerabilities = [];
			// Vulnerabilities = [...Vulnerabilitiesarr1, ...Vulnerabilitiesarr2];

			this.setState({ PentestsList: Dashboardpentest.PenTests.filter(x => x.IsPublished == 1 && x.RecurrenceType != 3), Vulnerabilities: Vulnerabilities });
			// await this.GetTrendsData();
			await this.GetVulnerableSystems();
			// await this.GetVulnerabilitiesChart();
			if (Dashboardpentest.Vulnerabilities && Dashboardpentest.Vulnerabilities.length > 0) {
				var Correct = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 1).length
				var Extreme = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 2).length;
				var High = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 2).length;
				var Medium = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 3).length;
				var Low = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 4).length;

				this.setState({ CorrectRisk: Correct, ExtremeRisk: Extreme, HighRisk: High, MediumRisk: Medium, LowRisk: Low });
			} else {
				this.setState({ CorrectRisk: 0, ExtremeRisk: 0, HighRisk: 0, MediumRisk: 0, LowRisk: 0 });
			}

			if (Dashboardpentest.PenTests && Dashboardpentest.PenTests.length > 0) {
				var Pentests = Dashboardpentest.PenTests.filter(x => x.RecurrenceType == 1 && x.IsPublished == 1).length;
				var Assessments = Dashboardpentest.PenTests.filter(x => x.RecurrenceType == 2 && x.IsPublished == 1).length;
				var QuickAssessments = Dashboardpentest.PenTests.filter(x => x.RecurrenceType == 3 && x.IsPublished == 1).length;

				this.setState({ DonePentestCount: Pentests, DoneAssessmentCount: Assessments, DoneQuickAssessmentCount: QuickAssessments });
			}

		}
		this.setState({ loading: false });
	}

	GetTranslatedText(translatedData, field, value) {
		return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
	}

	GetTrendsData() {
		let KnownTrends = [];
		let UnknownTrends = [];
		let ResolvedTrends = [];
		let AcceptedTrends = [];

		this.state.TrendsLabels.forEach(data => {

			var KnownCount = 0;
			var UnknownCount = 0;
			var ResolvedCount = 0;
			var AcceptedCount = 0;

			var KnownData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 1);
			var UnknownData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 2);
			var ResolvedData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 3);
			var AcceptedData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 4);

			if (KnownData && KnownData.length > 0) {
				KnownCount = KnownData[0].total;
			}
			if (UnknownData && UnknownData.length > 0) {
				UnknownCount = UnknownData[0].total;
			}
			if (ResolvedData && ResolvedData.length > 0) {
				ResolvedCount = ResolvedData[0].total;
			}
			if (AcceptedData && AcceptedData.length > 0) {
				AcceptedCount = AcceptedData[0].total;
			}
			KnownTrends.push(KnownCount);
			UnknownTrends.push(UnknownCount);
			ResolvedTrends.push(ResolvedCount);
			AcceptedTrends.push(AcceptedCount);
		});

		if (KnownTrends.length > 0) {
			for (let i = 1; i <= (KnownTrends.length - 1); i++) {
				KnownTrends[i] = KnownTrends[i - 1] + KnownTrends[i];
			}
		}

		if (UnknownTrends.length > 0) {
			for (let i = 1; i <= (UnknownTrends.length - 1); i++) {
				UnknownTrends[i] = UnknownTrends[i - 1] + UnknownTrends[i];
			}
		}

		if (ResolvedTrends.length > 0) {
			for (let i = 1; i <= (ResolvedTrends.length - 1); i++) {
				ResolvedTrends[i] = ResolvedTrends[i - 1] + ResolvedTrends[i];
			}
		}

		if (AcceptedTrends.length > 0) {
			for (let i = 1; i <= (AcceptedTrends.length - 1); i++) {
				AcceptedTrends[i] = AcceptedTrends[i - 1] + AcceptedTrends[i];
			}
		}

		this.setState({ KnownTrends });
		this.setState({ UnknownTrends });
		this.setState({ ResolvedTrends });
		this.setState({ AcceptedTrends });
	}

	GetVulnerableSystems() {
		let Vulnerabilities = this.state.Vulnerabilities;
		let CommonLabelVulnerableSystems = [];
		let CommonLabelVulnerableSystemIds = [];
		let CommonVulnerableSystemCount = [];

		Vulnerabilities.forEach(vul => {
			vul.VulnerabilityAssets.forEach(vasset => {
				if (vasset.System) {
					CommonLabelVulnerableSystems.push(vasset.System);
					CommonLabelVulnerableSystemIds.push(vasset.System.id);
				}
			});
		});
		var systemids = [];
		var SystemTotalCount = [];
		CommonLabelVulnerableSystems.forEach(sys => {
			if (!systemids.includes(sys.id)) {

				var VulnerableSystemCounts = CommonLabelVulnerableSystemIds.filter(x => x == sys.id).length;

				let SystemCount = {
					system: sys.Name && sys.Name.toString().length > 0 && sys.Remarks ? sys.Remarks.toString().substring(0, 12) + ' - ' + sys.Name : sys.Remarks ? sys.Remarks.toString().substring(0, 28) : '--',
					sysid: sys.id,
					count: VulnerableSystemCounts
				}
				SystemTotalCount.push(SystemCount);
				systemids.push(sys.id);
			}
		});

		SystemTotalCount = SystemTotalCount.sort((x, y) => y.count - x.count);
		SystemTotalCount = SystemTotalCount.slice(0, 5);
		console.log('SystemTotalCount', SystemTotalCount);
		CommonLabelVulnerableSystems = SystemTotalCount.map(x => { return x.system });
		CommonVulnerableSystemCount = SystemTotalCount.map(x => { return x.count });

		console.log('CommonLabelVulnerableSystems', CommonLabelVulnerableSystems);
		console.log('CommonVulnerableSystemCount', CommonVulnerableSystemCount);

		this.setState({ CommonLabelVulnerableSystems });
		this.setState({ CommonVulnerableSystemCount });
	}

	GetVulnerabilitiesChart() {
		let Vulnerabilities = this.state.Vulnerabilities;
		let CommonLabelPentestCheckList = [];
		let CommonLabelPentestCheckListIds = [];
		let CommonPentestCheckListCount = [];

		Vulnerabilities.forEach(vul => {
			if (vul.PenTestCheckListStatus) {
				CommonLabelPentestCheckList.push(vul.PenTestCheckListStatus.PenTestChecklist.Title.substring(0, 20) + '...');
				CommonLabelPentestCheckListIds.push(vul.PenTestCheckListStatus.PenTestChecklist.id)
			}
		});

		CommonLabelPentestCheckList = [...new Set(CommonLabelPentestCheckList)];
		CommonLabelPentestCheckList = CommonLabelPentestCheckList.slice(0, 4);
		CommonLabelPentestCheckListIds = [...new Set(CommonLabelPentestCheckListIds)];
		CommonLabelPentestCheckListIds = CommonLabelPentestCheckListIds.slice(0, 4);

		CommonLabelPentestCheckListIds.forEach(id => {
			var PentestCheckListCount = Vulnerabilities.filter(function (x) {
				if (x.PenTestCheckListStatus && x.PenTestCheckListStatus.PenTestChecklist.id == id) {
					return x;
				}
			}).length;
			CommonPentestCheckListCount.push(PentestCheckListCount);
		});

		this.setState({ CommonLabelPentestCheckList });
		this.setState({ CommonPentestCheckListCount });
	}

	ChangeCompany = async (event) => {
		this.setState({ loading: true });
		let id = event.target.value;
		this.setState({ Company: event.target.value });
		await this.GetPentests(id);
		await this.GetAssets(id);
		setTimeout(async () => {
			await this.GetDashboardData();
			await this.GetDashboardTrends();
		}, 1500);
		this.setState({ loading: false });
	}

	ChangePentest = async (event) => {
		this.setState({ loading: true });
		let id = event.target.value;
		this.setState({ Pentest: id });
		setTimeout(async () => {
			await this.GetDashboardData();
			await this.GetDashboardTrends();
		}, 1500);
		this.setState({ loading: false });
	}

	ChangeAsset = async (event) => {
		this.setState({ loading: true });
		let id = event.target.value;
		this.setState({ asset: id });
		setTimeout(() => {
			this.GetDashboardData();
		}, 500);
		this.setState({ loading: false });
	}

	//onChangeDate = date => this.setState({ date })

	ChangeToDate = async (event) => {
		this.setState({ loading: true });
		this.setState({ todate: event._d }, function () {
			var tod = this.state.todate != "" ? new Date(this.state.todate) : null;
			var frmd = this.state.fromdate != "" ? new Date(this.state.fromdate) : null;
			if (frmd && tod) {
				setTimeout(async () => {
					await this.GetDashboardData();
				}, 1500);
			}
		});
		this.setState({ loading: false });
	}

	ChangeFromDate = async (event) => {
		this.setState({ loading: true });
		this.setState({ fromdate: event._d }, function () {
			var tod = this.state.todate != "" ? new Date(this.state.todate) : null;
			var frmd = this.state.fromdate != "" ? new Date(this.state.fromdate) : null;
			if (frmd && tod) {
				setTimeout(async () => {
					await this.GetDashboardData();
				}, 1500);
			}
		});
		this.setState({ loading: false });
	}

	GetRCEStatus(RiskStatus, ComplexityStatus, EffortStatus) {
		var Risk = 'H';
		var Complexity = 'H';
		var Effort = 'H';

		var RiskClass = 'highclr';
		var ComplexityClass = 'highclr';
		var EffortClass = 'highclr';

		Risk = (RiskStatus ? RiskStatus == 1 ? 'C' : RiskStatus == 2 ? 'H' : RiskStatus == 3 ? 'M' : RiskStatus == 4 ? 'L' : 'H' : 'H');
		Complexity = (ComplexityStatus ? ComplexityStatus == 1 ? 'C' : ComplexityStatus == 2 ? 'H' : ComplexityStatus == 3 ? 'M' : ComplexityStatus == 4 ? 'L' : 'H' : 'H');
		Effort = (EffortStatus ? EffortStatus == 1 ? 'C' : EffortStatus == 2 ? 'H' : EffortStatus == 3 ? 'M' : EffortStatus == 4 ? 'L' : 'H' : 'H');

		RiskClass = (RiskStatus ? RiskStatus == 1 ? 'correctclr' : RiskStatus == 2 ? 'highclr' : RiskStatus == 3 ? 'mediumclr' : RiskStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');

		ComplexityClass = (ComplexityStatus ? ComplexityStatus == 1 ? 'correctclr' : ComplexityStatus == 2 ? 'lowclr' : ComplexityStatus == 3 ? 'mediumclr' : ComplexityStatus == 4 ? 'highclr' : 'highclr' : 'highclr');

		EffortClass = (EffortStatus ? EffortStatus == 1 ? 'correctclr' : EffortStatus == 2 ? 'highclr' : EffortStatus == 3 ? 'mediumclr' : EffortStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');

		return <ul className="list-unstyled d-flex justify-content-between rcevalue mb-0"><li className={RiskClass}>{Risk}</li><li className={ComplexityClass}>{Complexity}</li><li className={EffortClass}>{Effort}</li></ul>;
	}

	RedirectPentest(pentestid) {
		this.props.history.push('/company/companyassessment/' + cryptr.encrypt(pentestid));
	}

	cutExtraSystems(system, assetsCount) {
		let returnstring = (system.Remarks ? system.Remarks : '') + (system.Name ? ' - ' + system.Name : '');

		if (returnstring.length > 35 || assetsCount > 1) {
			returnstring = returnstring.toString().substring(0, 32) + '...';
		}
		return returnstring;
	}

	cutExtraVulnName(Name) {
		let returnstring = Name;

		if (returnstring.length > 35) {
			returnstring = returnstring.toString().substring(0, 32) + '...';
		}
		return returnstring;
	}

	render() {
		const { loading, CorrectRisk, ExtremeRisk, HighRisk, MediumRisk, LowRisk, DonePentestCount, DoneAssessmentCount, DoneQuickAssessmentCount, TrendsDates, TrendsLabels, KnownTrends, UnknownTrends, ResolvedTrends, AcceptedTrends, CommonLabelPentestCheckList, CommonPentestCheckListCount, CommonLabelVulnerableSystems, CommonVulnerableSystemCount } = this.state;
		const data = {
			labels: TrendsLabels,
			datasets: [
				{
					label: this.context.intl.formatMessage({ id: 'Known' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.warning,
					borderColor: ChartConfig.color.warning,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.warning,
					pointBackgroundColor: ChartConfig.color.warning,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.warning,
					pointHoverBorderColor: ChartConfig.color.warning,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: KnownTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Unknown' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: '#ec5840',
					borderColor: '#ec5840',
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: '#ec5840',
					pointBackgroundColor: '#ec5840',
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: '#ec5840',
					pointHoverBorderColor: '#ec5840',
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: UnknownTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Solved' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.success,
					borderColor: ChartConfig.color.success,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.success,
					pointBackgroundColor: ChartConfig.color.success,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.success,
					pointHoverBorderColor: ChartConfig.color.success,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: ResolvedTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Accepted' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.primary,
					borderColor: ChartConfig.color.primary,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.primary,
					pointBackgroundColor: ChartConfig.color.primary,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.primary,
					pointHoverBorderColor: ChartConfig.color.primary,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: AcceptedTrends
				}
			]
		};

		const options = {
			legend: {
				labels: {
					fontColor: ChartConfig.legendFontColor
				}
			},
			scales: {
				xAxes: [{
					gridLines: {
						color: ChartConfig.chartGridColor
					},
					ticks: {
						fontColor: ChartConfig.axesColor
					}
				}],
				yAxes: [{
					gridLines: {
						color: ChartConfig.chartGridColor
					},
					ticks: {
						fontColor: ChartConfig.axesColor
					}
				}]
			}
		};

		const piedoneservicesdata = {
			labels: [
				this.context.intl.formatMessage({ id: 'Pentest' }),
				this.context.intl.formatMessage({ id: 'Assessment' }),
				this.context.intl.formatMessage({ id: 'Quickscan' })
			],
			datasets: [{
				data: [DonePentestCount, DoneAssessmentCount, DoneQuickAssessmentCount],
				backgroundColor: [
					'#eb144c',
					'#ff6900',
					'#cddc39'
				],
				hoverBackgroundColor: [
					'#eb144c',
					'#ff6900',
					'#cddc39'
				]
			}]
		};

		const pieoptions = {
			legend: {
				labels: {
					fontColor: ChartConfig.legendFontColor,
					'word-break': 'break-word',
					'white-space': 'normal',
					lineWidth: 5
				},
				position: "right",
				fullWidth: true,
				lineWidth: 5,
				legendTemplate: "<ul style=\" display: inline;\" className=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li style=\"float: left; clear: both; list-style: none;\"><div className=\"indicator_box\" style=\"background-color:<%=segments[i].fillColor%>; padding: 5px ; margin: 8px 10px 5px 10px; display: block; float: left; \"></div><span style=\"font-size: 1.250em; word-wrap: break-word;\"><%if(segments[i].label){%><%=segments[i].label%><%}%></span></li><%}%></ul>",
			},
		};

		const pieCommonVulnerabilities = {
			labels: CommonLabelVulnerableSystems,
			datasets: [{
				data: CommonVulnerableSystemCount,
				backgroundColor: [
					'#eb144c',
					'#ff6900',
					'#fcb900',
					'#cddc39',
					'#00d014'
				],
				hoverBackgroundColor: [
					'#eb144c',
					'#ff6900',
					'#fcb900',
					'#cddc39',
					'#00d014'
				]
			}]
		};

		const { match } = this.props;
		const yesterday = Datetime.moment(this.state.fromdate);
		const valid = function (current) {
			return current.isAfter(yesterday);
		};
		return (
			<div className="ecom-dashboard-wrapper">
				<Helmet>
					<title>ROOTDASH</title>
					<meta name="description" content="Reactify Ecommerce Dashboard" />
				</Helmet>
				<PageTitleBar title="Dashboard" redirect='/company/dashboard' match={match} />

				<div className="row rct-block rctPadding">
					{/* <div className="col-sm-6 col-md-6 col-xl-2">
						<div className="form-group">
							<FormControl fullWidth>
								<InputLabel htmlFor="Select-Company">Select Company</InputLabel>

								<Select value={this.state.Company}
									onChange={(e) => this.ChangeCompany(e)}
									input={<Input id="Select-Company" />}>
									{this.state.Companies.map(comp => (
										<MenuItem key={comp.Company.id} value={comp.Company.id}>{comp.Company.Name}</MenuItem>
									))}
								</Select>
							</FormControl>
						</div>
					</div> */}
					<div className="col-sm-6 col-md-6 col-xl-2">
						<div className="form-group">
							<FormControl fullWidth>
								<InputLabel htmlFor="Select-Pentests"><IntlMessages id="Select_Pentests" /></InputLabel>
								<NativeSelect style={{ fontSize: '12px' }} value={this.state.Pentest}
									onChange={(e) => this.ChangePentest(e)}
									input={<Input id="Select-Pentests" />}>
									<option key={0} value={0}>{this.context.intl.formatMessage({ id: 'All' })}</option>
									{this.state.Pentests.map(pt => (
										<option key={pt.id} value={pt.id}>{pt.Name}</option>
										// <MenuItem key={pt.id} value={pt.id}>{pt.Name}</MenuItem>
									))}
								</NativeSelect>
							</FormControl>
						</div>
					</div>
					<div className="col-sm-6 col-md-6 col-xl-2">
						<div className="form-group">
							<FormControl fullWidth>
								<InputLabel htmlFor="Select-Asset"><IntlMessages id="Select_Assets" /></InputLabel>
								<NativeSelect style={{ fontSize: '12px' }} value={this.state.asset}
									onChange={(e) => this.ChangeAsset(e)}
									input={<Input id="Select-Asset" />}>
									<option key={0} value={0}>{this.context.intl.formatMessage({ id: 'All' })}</option>
									{this.state.assets.map(ast => (
										<option key={ast.id} value={ast.id}>{ast.Name}</option>
										// <MenuItem key={ast.id} value={ast.id}>{ast.Name}</MenuItem>
									))}
								</NativeSelect>
							</FormControl>
						</div>
					</div>
					<div className="col-sm-6 col-md-6 col-xl-3"></div>
					<div className="col-sm-6 col-md-6 col-xl-2">
						<FormControl fullWidth className="mb-md-0 mb-3">
							<label className="labelDashboard"><IntlMessages id="From" /></label>
							<Datetime inputProps={{ readOnly: true }} value={this.state.fromdate} inputProps={{ placeholder: 'MM/DD/YYYY' }} dateFormat={true} timeFormat={false} onChange={(e) => this.ChangeFromDate(e)} />
						</FormControl>
					</div>
					<div className="col-sm-6 col-md-6 col-xl-2">
						<div className="form-group">
							<FormControl fullWidth>
								<label className="labelDashboard"><IntlMessages id="To" /></label>
								<Datetime inputProps={{ readOnly: true }} isValidDate={valid} value={this.state.todate} minDate={new Date(this.state.fromdate)} inputProps={{ placeholder: 'MM/DD/YYYY' }} dateFormat={true} timeFormat={false} onChange={(e) => this.ChangeToDate(e)} />
							</FormControl>
						</div>
					</div>
				</div>

				<div className="row dashbord-tile">
					<div className="col-sm-6 col-lg-3 mb-20">
						<div className="col-sm-12 float-left hidden-md-down" style={{ zIndex: '1', position: 'absolute', left: '15px', top: '10px' }}>
							<div className="row">
								<div className="col-4 featured-section-icon dashboardTabs" style={{ color: '#eb144c' }}>
									{/* <i className="zmdi zmdi-format-list-numbered float-left" style={{ padding: '5px 0px 7px 8px' }}></i> */}
									<img src={require('Assets/icons/extremeNumbers.svg')} style={{ width: '25px', display: 'block', margin: '8px auto' }}></img>
								</div>
								<div className="col-8 featured-section-icon" style={{ color: '#ffffff', padding: '0px 7px' }}>
									<h1 style={{ margin: '0 0 0 5px' }}>{HighRisk}</h1>
									<h5 style={{ fontWeight: '300', margin: '0 0 0 5px' }}><IntlMessages id="High" /></h5>
								</div>
							</div>
						</div>
						<img src={require('Assets/img/box_1.png')} className="mr-15" alt="site logo" />
					</div>
					<div className="col-sm-6 col-lg-3 mb-20">
						<div className="col-sm-12 float-left hidden-md-down" style={{ zIndex: '1', position: 'absolute', left: '15px', top: '10px' }}>
							<div className="row">
								<div className="col-4 featured-section-icon dashboardTabs" style={{ color: '#ff6900' }}>
									<img src={require('Assets/icons/HighNumbers.svg')} style={{ width: '25px', display: 'block', margin: '8px auto' }}></img>
								</div>
								<div className="col-8 featured-section-icon" style={{ color: '#ffffff', padding: '0px 7px' }}>
									<h1 style={{ margin: '0 0 0 5px' }}>{MediumRisk}</h1>
									<h5 style={{ fontWeight: '300', margin: '0 0 0 5px' }}><IntlMessages id="Medium" /></h5>
								</div>
							</div>
						</div>
						<img src={require('Assets/img/box_2.png')} className="" alt="site logo" />
					</div>
					<div className="col-sm-6 col-lg-3 mb-20">
						<div className="col-sm-12 float-left hidden-md-down" style={{ zIndex: '1', position: 'absolute', left: '15px', top: '10px' }}>
							<div className="row">
								<div className="col-4 featured-section-icon dashboardTabs" style={{ color: '#fcb900' }}>
									<img src={require('Assets/icons/mednum.svg')} style={{ width: '25px', display: 'block', margin: '8px auto' }}></img>
								</div>
								<div className="col-8 featured-section-icon" style={{ color: '#ffffff', padding: '0px 7px' }}>
									<h1 style={{ margin: '0 0 0 5px' }}>{LowRisk}</h1>
									<h5 style={{ fontWeight: '300', margin: '0 0 0 5px' }}><IntlMessages id="Low" /></h5>
								</div>
							</div>
						</div>
						<img src={require('Assets/img/box_3.png')} className="mr-15" alt="site logo" />
					</div>
					<div className="col-sm-6 col-lg-3 mb-20">
						<div className="col-sm-12 float-left hidden-md-down" style={{ zIndex: '1', position: 'absolute', left: '15px', top: '10px' }}>
							<div className="row">
								<div className="col-4 featured-section-icon dashboardTabs" style={{ color: '#00D014' }}>
									{/* <i className="zmdi zmdi-format-list-numbered float-left" style={{ padding: '5px 0px 7px 8px' }}></i> */}
									<img src={require('Assets/icons/correctNumbers.svg')} style={{ width: '25px', display: 'block', margin: '8px auto' }}></img>
								</div>
								<div className="col-8 featured-section-icon" style={{ color: '#ffffff', padding: '0px 7px' }}>
									<h1 style={{ margin: '0 0 0 5px' }}>{CorrectRisk}</h1>
									<h5 style={{ fontWeight: '300', margin: '0 0 0 5px' }}><IntlMessages id="Correct" /></h5>
								</div>
							</div>
						</div>
						<img src={require('Assets/img/box_5.png')} className="mr-15" alt="site logo" />
					</div>
				</div>

				<div className="row mt-10 dashbord-box d-flex flex-wrap">
					<div className="col-sm-12 col-md-12 col-lg-6 d-sm-full">
						<div className="row mx-15">
							<div className="col-sm-12 col-md-12 col-lg-12">
								<div className="dash-cards">
									<RctCollapsibleCard
										collapsible
										reloadable
										closeable
										fullBlock
										heading="Trends">
										<NativeSelect className="dayfilter" value={this.state.TrendsFilter}
											onChange={(e) => this.ChangeTrendsFilter(e)}
											id="Select-trends" >
											<option key={1} value={1}>{this.context.intl.formatMessage({ id: 'Year' })}</option>
											<option key={2} value={2}>{this.context.intl.formatMessage({ id: 'Month' })}</option>
											<option key={3} value={3}>{this.context.intl.formatMessage({ id: 'Week' })}</option>
										</NativeSelect>

										{loading ? <RctSectionLoader /> : ''}
										<Line data={data} options={options} />

									</RctCollapsibleCard>


								</div>
							</div>
							<div className="col-sm-12 col-md-12 col-lg-12">
								<div className="dash-cards mobile-table clslatestpentests">
									<RctCollapsibleCard
										heading={<IntlMessages id="Latest_Pentests" />}
										collapsible
										reloadable
										closeable
										fullBlock
									>
										{loading ? <RctSectionLoader /> : ''}
										<table className="table table-hover mb-0 dashboardtables">
											<thead>
												<tr>
													<th><IntlMessages id="Name" /></th>
													<th><IntlMessages id="Comp./Dep" /></th>
													<th><IntlMessages id="Date" /></th>
													<th><IntlMessages id="Vuln" /></th>
													{/* <th>Status</th> */}
												</tr>
											</thead>
											<tbody>
												{this.state.PentestsList.slice(0, 5).map(pt => (
													<tr onClick={() => this.RedirectPentest(pt.id)}>
														<td style={{ width: '30%' }}>{pt.Name}</td>
														<td>{pt.Company.Name}</td>
														<td>{pt.DateofTest != null ? moment(pt.DateofTest).format('DD-MM-YYYY') : ''}</td>
														<td>{pt.Vulnerabilities ? pt.Vulnerabilities.length : '0'}</td>
														{/* <td>{pt.IsPublished == 1 ? 'Published' : 'Not published'}</td> */}
													</tr>
												))}
											</tbody>
										</table>
										{/* <RecentOrders2Widget /> */}
									</RctCollapsibleCard>
								</div>
							</div>
						</div>
					</div>

					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-6 d-sm-full clscriticalissues mobile-table"
						heading={<IntlMessages id="Top_critical_issues" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						{loading ? <RctSectionLoader /> : ''}
						<a href="javascript:;" onClick={() => this.props.history.push('/company/companyvulnerabilities')} className="viewall-button"><IntlMessages id="View_all" /></a>
						<div class="table-responsive rct-scroll ">
							<Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={'100%'} autoHide>
								<div className="criticalissuesheight">
									<table className="table table-hover mb-0 dashboardtables">
										<thead>
											<tr>
												<th className="DashrceAlign"><IntlMessages id="R_C_E" /></th>
												<th><IntlMessages id="Present_on" /></th>
												<th><IntlMessages id="Name" /></th>
												<th><IntlMessages id="Date" /></th>
												<th><IntlMessages id="Status" /></th>
												{/* <th><IntlMessages id="Facing" /></th> */}
											</tr>
										</thead>
										<tbody>
											{this.state.Vulnerabilities.slice(0, 17).map(Vulner => (
												<tr onClick={() => this.props.history.push({
													pathname: '/company/vulnerability/' + cryptr.encrypt(Vulner.id),
													state: { history: this.props.location }
												})}>
													<td style={{ width: '8%' }}>{this.GetRCEStatus(Vulner.Risk, Vulner.Complexity, Vulner.Effort)}</td>
													<td style={{ width: '32%' }}>{(Vulner.VulnerabilityAssets.length > 0 ? this.cutExtraSystems(Vulner.VulnerabilityAssets[0].System, Vulner.VulnerabilityAssets.length) : '')}</td>
													<td style={{ width: '33%' }}>{this.cutExtraVulnName(this.GetTranslatedText(Vulner.LanguageTranslations, 'Name', Vulner.Name).toString())}</td>
													<td style={{ width: '15%' }}>{Vulner.CreatedAt != null ? moment(Vulner.CreatedAt).format('DD-MM-YYYY') : ''}</td>
													<td style={{ width: '12%' }}>{(Vulner.IssueType != null ? Vulner.IssueType == 1 ? <IntlMessages id="Known" /> : Vulner.IssueType == 2 ? <IntlMessages id="Unknown" /> : Vulner.IssueType == 3 ? <IntlMessages id="Solved" /> : Vulner.IssueType == 4 ? <IntlMessages id="Accepted" /> : '--' : '--')}</td>
													{/* <td>{Vulner.Facing}</td> */}
												</tr>
											))}
										</tbody>
									</table>
								</div>
							</Scrollbars>
						</div>
						{/* <RecentOrdersWidget /> */}
					</RctCollapsibleCard>
				</div>
				<div className="row mx-10">
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
						heading={<IntlMessages id="Mostly_done_services" />}
						collapsible
						reloadable
						closeable
					>
						{loading ? <RctSectionLoader /> : ''}
						<Pie data={piedoneservicesdata} options={pieoptions} />
					</RctCollapsibleCard>
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
						heading={<IntlMessages id="Most_Vulnerable_Systems" />}
						collapsible
						reloadable
						closeable
					>
						{loading ? <RctSectionLoader /> : ''}
						<Pie ref="piechart" data={pieCommonVulnerabilities} options={pieoptions} />
					</RctCollapsibleCard>
				</div>
				{/* <div className="row">
					<div dangerouslySetInnerHTML={{ __html: this.refs.piechart ? this.refs.piechart.chartInstance.generateLegend() : '' }} />
				</div> */}
			</div >
		)
	}
}
