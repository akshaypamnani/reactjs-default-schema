/**
 * Ecommerce Dashboard
 */

import React, { Component } from 'react'
import { Helmet } from "react-helmet";
import { Line } from 'react-chartjs-2';
import { Pie } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import { Link } from 'react-router';
import { Scrollbars } from 'react-custom-scrollbars';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
import * as TranslationService from '../../../actions/TranslationService';
import * as SessionService from '../../../actions/SessionService';
import { Label } from 'reactstrap';

let Datetime = require('react-datetime');
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

export default class TicketDashboard extends Component {

	static contextTypes = {
		intl: intlShape,
	};

	state = {
		activeIndex: 0,
		TrendsFilter: 2,
		date: new Date(),
		loading: false,
		sessionUser: {
			id: 0,
			FirstName: '',
			LastName: '',
			UserRole: {
				RoleId: 0
			}
		},
		TrendsDates: [],
		TrendsLabels: [],
		TicketTrends: [],
		PendingTrends: [],
		SolvedTrends: [],
		TicketPriorities: {
			Urgent: 0,
			High: 0,
			Medium: 0,
			Low: 0
		},
		TicketCategories: {
			Technical: 0,
			General: 0,
			Information: 0,
			NewUsers: 0
		},
		Tickets: [],
		OpenTickets: [],
	}

	static getInitialProps(args) {
		console.log('args.env:', args.env);
	}

	async componentWillMount() {

	}

	async componentDidMount() {
		await this.checkUserRole();
		await this.GetDates(30);
		if (this.props.location.state) {
			await this.GetTickets();
			await this.GetTicketsTrends();
		} else {
			await this.GetTickets();
			await this.GetTicketsTrends();
		}
	}

	checkUserRole() {
		ApiService.CheckUserRole(this.props);
		var sessionUser = SessionService.GetUserSession();
		this.setState({ sessionUser });
	}

	async GetTickets() {
		var TicketData = {
			Where: {
				IsActive: 1,
			}
		};

		if (this.state.sessionUser.id != 0 && (this.state.sessionUser.UserRole.RoleId == 1 || this.state.sessionUser.UserRole.RoleId == 4)) {
			TicketData.Where.CreatedBy = this.state.sessionUser.id;
		}

		var Tickets = await ApiService.PostWhereAPI('execute/withoutnestedconditions/Tickets', TicketData);

		var OpenTickets = Tickets.filter(x => x.Status == 1);

		let TicketPriorities = this.state.TicketPriorities;
		let TicketCategories = this.state.TicketCategories;

		TicketPriorities.Urgent = Tickets.filter(x => x.Priority == 1).length;
		TicketPriorities.High = Tickets.filter(x => x.Priority == 2).length;
		TicketPriorities.Medium = Tickets.filter(x => x.Priority == 3).length;
		TicketPriorities.Low = Tickets.filter(x => x.Priority == 4).length;

		TicketCategories.Technical = Tickets.filter(x => x.TicketType == 1).length;
		TicketCategories.General = Tickets.filter(x => x.TicketType == 2).length;
		TicketCategories.Information = Tickets.filter(x => x.TicketType == 3).length;
		TicketCategories.NewUsers = Tickets.filter(x => x.TicketType == 4).length;

		this.setState({ Tickets, OpenTickets, TicketPriorities, TicketCategories });
	}

	ChangeTrendsFilter = async (event) => {
		let TrendsFilter = event.target.value;
		await this.setState({ TrendsFilter });

		if (TrendsFilter == '2') {
			await this.GetDates(30);
		} else if (TrendsFilter == '3') {
			await this.GetDates(7);
		} else {
			await this.GetMonths()
		}
		await this.GetTicketsTrends();
	}

	GetDates(ndays) {
		var TrendsLabels = [];
		for (let day = 0; day < ndays; day++) {
			let date = new Date();
			date = moment(date).add(-day, 'days').format('DD-MMM');
			TrendsLabels.push(date);
		}
		TrendsLabels = TrendsLabels.reverse();
		this.setState({ TrendsLabels });
	}

	GetMonths() {
		var TrendsLabels = [];
		for (let month = 0; month < 12; month++) {
			let date = new Date();
			date = moment(date).add(-month, 'months').format('MMM-YY');
			TrendsLabels.push(date);
		}
		TrendsLabels = TrendsLabels.reverse();
		this.setState({ TrendsLabels });
	}

	async GetTicketsTrends() {
		this.setState({ loading: true });
		var TrendData = {
			currentdate: moment(new Date()).add(1, 'days').format('YYYY-MM-DD')
		};

		if (this.state.sessionUser.id != 0 && (this.state.sessionUser.UserRole.RoleId == 1 || this.state.sessionUser.UserRole.RoleId == 4)) {
			TrendData.userid = this.state.sessionUser.id;
		}

		if (this.state.TrendsFilter && this.state.TrendsFilter != "0") {
			TrendData.TrendsFilter = this.state.TrendsFilter;
		}
		TicketTrends
		var TicketTrends = await ApiService.PostWhereAPI('ticketsapi/TicketTrends', TrendData);
		if (TicketTrends) {
			await this.setState({ TicketTrends });
			await this.GetTrendsData();
		}

		this.setState({ loading: false });
	}

	GetTranslatedText(translatedData, field, value) {
		return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
	}

	GetTrendsData() {
		let PendingTrends = [];
		let SolvedTrends = [];

		this.state.TrendsLabels.forEach(data => {

			var PendingCount = 0;
			var SolvedCount = 0;

			var PendingData = this.state.TicketTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.Status == 2);
			var SolvedData = this.state.TicketTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.Status == 4);

			if (PendingData && PendingData.length > 0) {
				PendingCount = PendingData[0].total;
			}
			if (SolvedData && SolvedData.length > 0) {
				SolvedCount = SolvedData[0].total;
			}
			PendingTrends.push(PendingCount);
			SolvedTrends.push(SolvedCount);
		});

		if (PendingTrends.length > 0) {
			for (let i = 1; i <= (PendingTrends.length - 1); i++) {
				PendingTrends[i] = PendingTrends[i - 1] + PendingTrends[i];
			}
		}

		if (SolvedTrends.length > 0) {
			for (let i = 1; i <= (SolvedTrends.length - 1); i++) {
				SolvedTrends[i] = SolvedTrends[i - 1] + SolvedTrends[i];
			}
		}

		this.setState({ PendingTrends });
		this.setState({ SolvedTrends });
	}

	render() {
		const { loading, TicketPriorities, TicketCategories, TrendsDates, TrendsLabels, PendingTrends, SolvedTrends } = this.state;
		const data = {
			labels: TrendsLabels,
			datasets: [
				{
					label: this.context.intl.formatMessage({ id: 'Pending' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.warning,
					borderColor: ChartConfig.color.warning,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.warning,
					pointBackgroundColor: ChartConfig.color.warning,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.warning,
					pointHoverBorderColor: ChartConfig.color.warning,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: PendingTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Solved' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: '#ec5840',
					borderColor: '#ec5840',
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: '#ec5840',
					pointBackgroundColor: '#ec5840',
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: '#ec5840',
					pointHoverBorderColor: '#ec5840',
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: SolvedTrends
				}
			]
		};

		const options = {
			legend: {
				labels: {
					fontColor: ChartConfig.legendFontColor
				}
			},
			scales: {
				xAxes: [{
					gridLines: {
						color: ChartConfig.chartGridColor
					},
					ticks: {
						fontColor: ChartConfig.axesColor
					}
				}],
				yAxes: [{
					gridLines: {
						color: ChartConfig.chartGridColor
					},
					ticks: {
						fontColor: ChartConfig.axesColor
					}
				}]
			}
		};

		const { match } = this.props;

		const yesterday = Datetime.moment(this.state.fromdate);
		const valid = function (current) {
			return current.isAfter(yesterday);
		};

		return (
			<div className="ecom-dashboard-wrapper">
				<Helmet>
					<title>ROOTDASH</title>
					<meta name="description" content="Reactify Ecommerce Dashboard" />
				</Helmet>
				<PageTitleBar title={this.context.intl.formatMessage({ id: 'Ticket_dashboard_Admin' })} redirect='/app/dashboard' match={match} />

				<div className="row dashbord-box d-flex flex-wrap">
					<div className="col-sm-12 col-md-12 col-lg-6 d-sm-full">
						<div className="row mx-15">
							<div className="col-sm-12 col-md-6 col-lg-6">
								<div className="dash-cards mobile-table clsticketdashboard">
									<RctCollapsibleCard
										heading={<IntlMessages id="Ticket_Priorities" />}
										collapsible
										reloadable
										closeable
										fullBlock

									>
										{loading ? <RctSectionLoader /> : ''}
										<div className="row">
											<div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15 px-15">
												<FormControl component="fieldset">
													<Label component="legend" className="d-flex vulnclrs"><p className="highclr"></p> <span><strong className="px-15">{TicketPriorities.Urgent}  </strong> <IntlMessages id="Urgent" /></span></Label>
													<Label component="legend" className="d-flex vulnclrs"><p className="highclr"></p> <span><strong className="px-15">{TicketPriorities.High}  </strong> <IntlMessages id="High" /></span></Label>
													<Label component="legend" className="d-flex vulnclrs"><p className="mediumclr"></p> <span><strong className="px-15">{TicketPriorities.Medium}  </strong> <IntlMessages id="Medium" /></span></Label>
													<Label component="legend" className="d-flex vulnclrs"><p className="lowclr"></p> <span><strong className="px-15">{TicketPriorities.Low}  </strong> <IntlMessages id="Low" /></span></Label>
												</FormControl>
											</div>
										</div>
									</RctCollapsibleCard>
								</div>
							</div>
							<div className="col-sm-12 col-md-6 col-lg-6">
								<div className="dash-cards mobile-table clsticketdashboard">
									<RctCollapsibleCard
										heading={<IntlMessages id="Ticket_Categories" />}
										collapsible
										reloadable
										closeable
										fullBlock

									>
										{loading ? <RctSectionLoader /> : ''}
										<div className="row">
											<div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15 px-15">
												<FormControl component="fieldset">
													<Label component="legend" className="d-flex vulnclrs"><p className="highclr"></p> <span><strong className="px-15">{TicketCategories.Technical}  </strong> <IntlMessages id="Technical" /></span></Label>
													<Label component="legend" className="d-flex vulnclrs"><p className="highclr"></p> <span><strong className="px-15">{TicketCategories.General}  </strong> <IntlMessages id="General" /></span></Label>
													<Label component="legend" className="d-flex vulnclrs"><p className="mediumclr"></p> <span><strong className="px-15">{TicketCategories.Information}  </strong> <IntlMessages id="Information" /></span></Label>
													<Label component="legend" className="d-flex vulnclrs"><p className="lowclr"></p> <span><strong className="px-15">{TicketCategories.NewUsers}  </strong> <IntlMessages id="New_users" /></span></Label>
												</FormControl>
											</div>
										</div>
									</RctCollapsibleCard>
								</div>
							</div>
							<div className="col-sm-12 col-md-12 col-lg-12">
								<div className="dash-cards">
									<RctCollapsibleCard
										collapsible
										reloadable
										closeable
										fullBlock
										heading={this.context.intl.formatMessage({ id: 'Ticket_Trends' })}>
										<NativeSelect className="dayfilter" value={this.state.TrendsFilter}
											onChange={(e) => this.ChangeTrendsFilter(e)}
											id="Select-trends" >
											<option key={1} value={1}>{this.context.intl.formatMessage({ id: 'Year' })}</option>
											<option key={2} value={2}>{this.context.intl.formatMessage({ id: 'Month' })}</option>
											<option key={3} value={3}>{this.context.intl.formatMessage({ id: 'Week' })}</option>
										</NativeSelect>

										{loading ? <RctSectionLoader /> : ''}
										<Line data={data} options={options} />

									</RctCollapsibleCard>


								</div>
							</div>
						</div>
					</div>

					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-6 d-sm-full clscriticalissues mobile-table"
						heading={<IntlMessages id="Top_critical_issues" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						{loading ? <RctSectionLoader /> : ''}

						{/* <Link to="/app/prioritiesvulnerabilities"><a href="javascript:;" ><IntlMessages id="View_all" /></a></Link> */}
						<a href="javascript:;" className="viewall-button" onClick={() => this.props.history.push('/app/prioritiesvulnerabilities')} ><IntlMessages id="View All" /></a>
						<Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={'100%'} autoHide>
							<div className="criticalissuesheight">
								<table className="table table-hover mb-0 dashboardtables">
									<thead>
										<tr>
											<th><IntlMessages id="Priority" /></th>
											<th><IntlMessages id="Date" /></th>
											<th><IntlMessages id="Type_of_ticket" /></th>
											<th><IntlMessages id="Assigned_to" /></th>
											<th><IntlMessages id="Actions" /></th>
										</tr>
									</thead>
									<tbody>
										{this.state.OpenTickets.slice(0, 17).map(ticket => (
											<tr onClick={() => this.props.history.push('/app/vulnerability/' + cryptr.encrypt(ticket.id))}>
												<td style={{ width: '8%' }}>{ticket.Priority}</td>
												<td style={{ width: '32%' }}>{ticket.CreatedAt != null ? moment(ticket.CreatedAt).format('DD-MM-YYYY') : ''}</td>
												<td style={{ width: '33%' }}>{ticket.TicketType}</td>
												<td style={{ width: '15%' }}>{ticket.AssignedUser != null ? ticket.AssignedUser : ''}</td>
												<td style={{ width: '12%' }}>{''}</td>
											</tr>
										))}
									</tbody>
								</table>
							</div>
						</Scrollbars>
					</RctCollapsibleCard>
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-12 d-sm-full clscriticalissues mobile-table"
						heading={<IntlMessages id="Ticket_history" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						{loading ? <RctSectionLoader /> : ''}

						<a href="javascript:;" className="viewall-button" onClick={() => this.props.history.push('/app/prioritiesvulnerabilities')} ><IntlMessages id="View All" /></a>
						<Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={'100%'} autoHide>
							<div className="criticalissuesheight">
								<table className="table table-hover mb-0 dashboardtables">
									<thead>
										<tr>
											<th><IntlMessages id="Priority" /></th>
											<th><IntlMessages id="Opened_on" /></th>
											<th><IntlMessages id="Type_of_ticket" /></th>
											<th><IntlMessages id="Status" /></th>
											<th><IntlMessages id="Assigned_to" /></th>
											<th><IntlMessages id="Action" /></th>
										</tr>
									</thead>
									<tbody>
										{this.state.Tickets.slice(0, 17).map(ticket => (
											<tr onClick={() => this.props.history.push('/app/vulnerability/' + cryptr.encrypt(ticket.id))}>
												<td style={{ width: '8%' }}>{ticket.Priority}</td>
												<td style={{ width: '12%' }}>{ticket.CreatedAt != null ? moment(ticket.CreatedAt).format('DD-MM-YYYY') : ''}</td>
												<td style={{ width: '25%' }}>{ticket.TicketType}</td>
												<td style={{ width: '25%' }}>{ticket.Status}</td>
												<td style={{ width: '15%' }}>{ticket.AssignedUser != null ? ticket.AssignedUser : ''}</td>
												<td style={{ width: '12%' }}>{''}</td>
											</tr>
										))}
									</tbody>
								</table>

							</div>
						</Scrollbars>
						{/* <RecentOrdersWidget /> */}
					</RctCollapsibleCard>
				</div>
			</div >
		)
	}
}