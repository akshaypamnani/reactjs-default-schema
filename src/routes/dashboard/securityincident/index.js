/**
 * Security Incident
 */

import React, { Component } from 'react'
import { Helmet } from "react-helmet";
import { Line } from 'react-chartjs-2';
import { Pie } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import { Link } from 'react-router';
import { Scrollbars } from 'react-custom-scrollbars';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
import * as TranslationService from '../../../actions/TranslationService';
import * as SessionService from '../../../actions/SessionService';
import { Label } from 'reactstrap';
import MatButton from '@material-ui/core/Button';
let Datetime = require('react-datetime');
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

export default class SecurityIncidentDashboard extends Component {

	static contextTypes = {
		intl: intlShape,
	};

	state = {
		activeIndex: 0,
		Companies: [],
		assets: [],
		PentestsList: [],
		Pentests: [],
		Company: "0",
		asset: "0",
		Pentest: "0",
		TrendsFilter: 2,
		selectedCompany: 0,
		selectedPentest: 0,
		selectedAsset: 0,
		Vulnerabilities: [],
		todate: "",//new Date(),
		fromdate: "",//new Date(),
		date: new Date(),
		IsDataLoaded: false,
		loading: false,
		CorrectRisk: 0,
		ExtremeRisk: 0,
		HighRisk: 0,
		MediumRisk: 0,
		LowRisk: 0,
		DonePentestCount: 0,
		DoneAssessmentCount: 0,
		DoneQuickAssessmentCount: 0,
		TrendsDates: [],
		TrendsLabels: [],
		DashboardTrends: [],
		KnownTrends: [],
		UnknownTrends: [],
		ResolvedTrends: [],
		AcceptedTrends: [],
		CommonLabelPentestCheckList: [],
		CommonPentestCheckListCount: []
	}

	static getInitialProps(args) {
		console.log('args.env:', args.env);
	}

	async componentWillReceiveProps() {
		this.checkUserRole();
		var companyid = 0;

		setTimeout(async () => {
			var params = new URLSearchParams(this.props.location.search);
			var paramcompanyid = params.get('companyid');
			if (paramcompanyid) {
				companyid = paramcompanyid;
			} else if (this.props.location.state) {
				companyid = this.props.location.state.id;
			}
			if (companyid != 0) {
				this.setState({ Company: cryptr.decrypt(companyid) });
				this.setState({ loading: true });
				await this.GetPentests(cryptr.decrypt(companyid));
				await this.GetAssets(cryptr.decrypt(companyid));
				setTimeout(() => {
					this.GetDashboardData();
					this.GetDashboardTrends();
				}, 1000);
				this.setState({ loading: false });
			}
		}, 1000);
	}

	async componentDidMount() {
		this.checkUserRole();
		// await this.GetDates();
		// await this.GetMonths();
		await this.GetDates(30);
		await this.GetCompanies();
		if (this.props.location.state) {
			var companyid = this.props.location.state.id;
			this.setState({ Company: cryptr.decrypt(companyid), IsDataLoaded: true });
			await this.GetPentests(cryptr.decrypt(companyid));
			await this.GetAssets(cryptr.decrypt(companyid));
			setTimeout(() => {
				this.GetDashboardData();
				this.GetDashboardTrends();
			}, 1500);
		} else {
			this.GetDashboardData();
			this.GetDashboardTrends();
			// this.GetPentests(0);
			// this.GetAssets(0);
		}
		// this.TranslatedLabels();
	}

	checkUserRole() {
		var userRole = SessionService.GetUserRole('userRole');
		if (userRole) {
			if (userRole.Name == "ClientUser") {
				this.props.history.push('/company/clientdashboard');
			}
		}
	}


	TranslatedLabels = async () => {
		var LabelData = {
			Language: 'en-EN'
		}
		var Labels = await ApiService.PostWhereAPI('labelapi/TranslatedLabels', LabelData);
		// return Labels;
	}

	ChangeTrendsFilter = async (event) => {
		let TrendsFilter = event.target.value;
		await this.setState({ TrendsFilter });

		if (TrendsFilter == '2') {
			await this.GetDates(30);
		} else if (TrendsFilter == '3') {
			await this.GetDates(7);
		} else {
			await this.GetMonths()
		}
		await this.GetDashboardTrends();
	}

	GetDates(ndays) {
		var TrendsLabels = [];
		for (let day = 0; day < ndays; day++) {
			let date = new Date();
			date = moment(date).add(-day, 'days').format('DD-MMM');
			TrendsLabels.push(date);
		}
		TrendsLabels = TrendsLabels.reverse();
		this.setState({ TrendsLabels });
	}

	GetMonths() {
		var TrendsLabels = [];
		for (let month = 0; month < 12; month++) {
			let date = new Date();
			date = moment(date).add(-month, 'months').format('MMM-YY');
			TrendsLabels.push(date);
		}
		TrendsLabels = TrendsLabels.reverse();
		this.setState({ TrendsLabels });
	}

	async GetCompanies() {
		this.setState({ loading: true });
		var companies = await ApiService.GetAPI('Company');
		this.setState({ Companies: companies });
		this.setState({ loading: false });
	}

	async GetDashboardTrends() {
		this.setState({ loading: true });
		var TrendData = {
			currentdate: moment(new Date()).add(1, 'days').format('YYYY-MM-DD')
		};
		if (this.state.Company && this.state.Company != "0") {
			TrendData.companyid = this.state.Company;
		}
		if (this.state.Pentest && this.state.Pentest != "0") {
			TrendData.pentestid = this.state.Pentest;
		}
		if (this.state.TrendsFilter && this.state.TrendsFilter != "0") {
			TrendData.TrendsFilter = this.state.TrendsFilter;
		}

		var DashboardTrends = await ApiService.PostWhereAPI('Dashboard/dashboardtrends', TrendData);
		if (DashboardTrends) {
			await this.setState({ DashboardTrends });
			await this.GetTrendsData();
		}

		this.setState({ loading: false });
	}

	async GetPentests(companyid) {
		this.setState({ loading: true });
		var Pentests = [];
		if (companyid && parseInt(companyid) > 0) {

			Pentests = await ApiService.GetAPI('pentestapi/getpentestbycompany/' + cryptr.encrypt(companyid));
		} else {
			Pentests = await ApiService.GetAPI('PenTest');
		}
		this.setState({ Pentests: Pentests });
		this.setState({ loading: false });
	}

	async GetAssets(companyid) {
		this.setState({ loading: true });
		var Assets = [];
		if (companyid && companyid != 0) {
			var Assetdata = {
				Where: {
					CompanyId: companyid,
					IsActive: 1
				}
			}
			Assets = await ApiService.PostWhereAPI('execute/conditions/Asset', Assetdata);

		} else {
			Assets = await ApiService.GetAPI('Asset');
		}
		this.setState({ assets: Assets });
		this.setState({ loading: false });
	}

	handleChangedd = (event, key) => {
		this.setState({ [key]: event.target.value });
	}

	async GetDashboardData() {
		this.setState({ loading: true });
		var dashboarddata = {};
		if (this.state.Company && this.state.Company != "0") {
			dashboarddata.CompanyId = this.state.Company;
		}
		if (this.state.Pentest && this.state.Pentest != "0") {
			dashboarddata.PentestId = this.state.Pentest;
		}
		if (this.state.asset && this.state.asset != "0") {
			dashboarddata.AssetId = this.state.asset;
		}
		if ((this.state.fromdate && this.state.fromdate != "") && this.state.todate && this.state.todate != "") {
			var daterange = {
				from: this.state.fromdate,
				to: this.state.todate
			}
			dashboarddata.daterange = daterange;
		}
		var Dashboardpentest = await ApiService.PostWhereAPI('Dashboard/GetDashboardPentests', dashboarddata);
		if (Dashboardpentest) {

			if (Dashboardpentest.PenTests && Dashboardpentest.PenTests.length > 0) {
				Dashboardpentest.PenTests.forEach(pentest => {
					pentest.Vulnerabilities = pentest.Vulnerabilities.filter(x => x.IsActive == 1);
				});
			}

			if (Dashboardpentest.Vulnerabilities && Dashboardpentest.Vulnerabilities.length > 0) {
				Dashboardpentest.Vulnerabilities.forEach(vuln => {
					if (vuln.LanguageTranslations && vuln.LanguageTranslations.length > 0) {
						vuln.LanguageTranslations = vuln.LanguageTranslations.filter(x => x.PageRefId == 2);
					}
				});
			}

			var Vulnerabilities = Dashboardpentest.Vulnerabilities && Dashboardpentest.Vulnerabilities.length > 0 ? Dashboardpentest.Vulnerabilities.sort((x, y) => x.Order - y.Order) : [];

			// var Vulnerabilitiesarr1 = Vulnerabilities.filter(x => x.Risk != 1);
			// var Vulnerabilitiesarr2 = Vulnerabilities.filter(x => x.Risk == 1);

			// Vulnerabilities = [];
			// Vulnerabilities = [...Vulnerabilitiesarr1, ...Vulnerabilitiesarr2];

			await this.setState({ PentestsList: Dashboardpentest.PenTests, Vulnerabilities: Vulnerabilities });
			await this.GetVulnerabilitiesChart();
			if (Dashboardpentest.Vulnerabilities && Dashboardpentest.Vulnerabilities.length > 0) {
				var Correct = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 1).length;
				var Extreme = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 2).length;
				var High = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 2).length;
				var Medium = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 3).length;
				var Low = Dashboardpentest.Vulnerabilities.filter(x => x.Risk == 4).length;

				this.setState({ CorrectRisk: Correct, ExtremeRisk: Extreme, HighRisk: High, MediumRisk: Medium, LowRisk: Low });
			} else {
				this.setState({ CorrectRisk: 0, ExtremeRisk: 0, HighRisk: 0, MediumRisk: 0, LowRisk: 0 });
			}

			if (Dashboardpentest.PenTests && Dashboardpentest.PenTests.length > 0) {
				var Pentests = Dashboardpentest.PenTests.filter(x => x.RecurrenceType == 1).length;
				var Assessments = Dashboardpentest.PenTests.filter(x => x.RecurrenceType == 2).length;
				var QuickAssessments = Dashboardpentest.PenTests.filter(x => x.RecurrenceType == 3).length;

				this.setState({ DonePentestCount: Pentests, DoneAssessmentCount: Assessments, DoneQuickAssessmentCount: QuickAssessments });
			}

		}
		this.setState({ loading: false });
	}

	GetTranslatedText(translatedData, field, value) {
		return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
	}

	GetTrendsData() {
		let KnownTrends = [];
		let UnknownTrends = [];
		let ResolvedTrends = [];
		let AcceptedTrends = [];

		this.state.TrendsLabels.forEach(data => {

			var KnownCount = 0;
			var UnknownCount = 0;
			var ResolvedCount = 0;
			var AcceptedCount = 0;

			var KnownData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 1);
			var UnknownData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 2);
			var ResolvedData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 3);
			var AcceptedData = this.state.DashboardTrends.filter(x => x.DateFormat.toString().toLowerCase() == data.toString().toLowerCase() && x.IssueType == 4);

			if (KnownData && KnownData.length > 0) {
				KnownCount = KnownData[0].total;
			}
			if (UnknownData && UnknownData.length > 0) {
				UnknownCount = UnknownData[0].total;
			}
			if (ResolvedData && ResolvedData.length > 0) {
				ResolvedCount = ResolvedData[0].total;
			}
			if (AcceptedData && AcceptedData.length > 0) {
				AcceptedCount = AcceptedData[0].total;
			}
			KnownTrends.push(KnownCount);
			UnknownTrends.push(UnknownCount);
			ResolvedTrends.push(ResolvedCount);
			AcceptedTrends.push(AcceptedCount);
		});

		if (KnownTrends.length > 0) {
			for (let i = 1; i <= (KnownTrends.length - 1); i++) {
				KnownTrends[i] = KnownTrends[i - 1] + KnownTrends[i];
			}
		}

		if (UnknownTrends.length > 0) {
			for (let i = 1; i <= (UnknownTrends.length - 1); i++) {
				UnknownTrends[i] = UnknownTrends[i - 1] + UnknownTrends[i];
			}
		}

		if (ResolvedTrends.length > 0) {
			for (let i = 1; i <= (ResolvedTrends.length - 1); i++) {
				ResolvedTrends[i] = ResolvedTrends[i - 1] + ResolvedTrends[i];
			}
		}

		if (AcceptedTrends.length > 0) {
			for (let i = 1; i <= (AcceptedTrends.length - 1); i++) {
				AcceptedTrends[i] = AcceptedTrends[i - 1] + AcceptedTrends[i];
			}
		}

		this.setState({ KnownTrends });
		this.setState({ UnknownTrends });
		this.setState({ ResolvedTrends });
		this.setState({ AcceptedTrends });
	}

	GetVulnerabilitiesChart() {
		let Vulnerabilities = this.state.Vulnerabilities;
		let CommonLabelPentestCheckList = [];
		let CommonLabelPentestCheckListIds = [];
		let CommonPentestCheckListCount = [];

		Vulnerabilities.forEach(vul => {
			if (vul.PenTestCheckListStatus) {
				vul.PenTestCheckListStatus.PenTestChecklist.Title = this.context.intl.formatMessage({ id: vul.PenTestCheckListStatus.PenTestChecklist.Title });
				CommonLabelPentestCheckList.push(vul.PenTestCheckListStatus.PenTestChecklist.Title.substring(0, 20) + '...');
				CommonLabelPentestCheckListIds.push(vul.PenTestCheckListStatus.PenTestChecklist.id)
			}
		});

		CommonLabelPentestCheckList = [...new Set(CommonLabelPentestCheckList)];
		CommonLabelPentestCheckList = CommonLabelPentestCheckList.slice(0, 4);
		CommonLabelPentestCheckListIds = [...new Set(CommonLabelPentestCheckListIds)];
		CommonLabelPentestCheckListIds = CommonLabelPentestCheckListIds.slice(0, 4);
		CommonLabelPentestCheckListIds.forEach(id => {
			// var PentestCheckListCount = Vulnerabilities.filter(x => x.PenTestCheckListStatus.PenTestChecklist.id == id).length;
			var PentestCheckListCount = Vulnerabilities.filter(function (x) {
				if (x.PenTestCheckListStatus && x.PenTestCheckListStatus.PenTestChecklist.id == id) {
					return x;
				}
			}).length;
			CommonPentestCheckListCount.push(PentestCheckListCount);
		});

		this.setState({ CommonLabelPentestCheckList });
		this.setState({ CommonPentestCheckListCount });
	}

	ChangeCompany = async (event) => {
		this.setState({ loading: true });
		this.setState({ Pentest: '0' });
		this.setState({ asset: '0' });
		let id = event.target.value;
		this.setState({ Company: event.target.value });
		await this.GetPentests(id);
		await this.GetAssets(id);
		await this.GetDashboardData();
		await this.GetDashboardTrends();
		this.setState({ loading: false });
	}

	ChangePentest = async (event) => {
		this.setState({ loading: true });
		let id = event.target.value;
		this.setState({ Pentest: id });
		setTimeout(() => {
			this.GetDashboardData();
			this.GetDashboardTrends();
		}, 500);
		this.setState({ loading: false });
	}

	ChangeAsset = async (event) => {
		this.setState({ loading: true });
		let id = event.target.value;
		this.setState({ asset: id });
		setTimeout(() => {
			this.GetDashboardData();
		}, 500);
		this.setState({ loading: false });
	}

	//onChangeDate = date => this.setState({ date })

	ChangeToDate = (event) => {
		this.setState({ loading: true });
		this.setState({ todate: event._d }, function () {
			var tod = this.state.todate != "" ? new Date(this.state.todate) : null;
			var frmd = this.state.fromdate != "" ? new Date(this.state.fromdate) : null;
			if (frmd && tod) {
				setTimeout(() => {
					this.GetDashboardData();
				}, 1500);
			}
		});
		this.setState({ loading: false });
	}

	ChangeFromDate = (event) => {
		this.setState({ loading: true });
		this.setState({ fromdate: event._d }, function () {
			var tod = this.state.todate != "" ? new Date(this.state.todate) : null;
			var frmd = this.state.fromdate != "" ? new Date(this.state.fromdate) : null;
			if (frmd && tod) {
				setTimeout(() => {
					this.GetDashboardData();
				}, 1500);
			}
		});
		this.setState({ loading: false });
	}

	GetRCEStatus(RiskStatus, ComplexityStatus, EffortStatus) {
		var Risk = 'H';
		var Complexity = 'H';
		var Effort = 'H';

		var RiskClass = 'highclr';
		var ComplexityClass = 'highclr';
		var EffortClass = 'highclr';

		Risk = (RiskStatus ? RiskStatus == 1 ? 'C' : RiskStatus == 2 ? 'H' : RiskStatus == 3 ? 'M' : RiskStatus == 4 ? 'L' : 'H' : 'H');
		Complexity = (ComplexityStatus ? ComplexityStatus == 1 ? 'C' : ComplexityStatus == 2 ? 'H' : ComplexityStatus == 3 ? 'M' : ComplexityStatus == 4 ? 'L' : 'H' : 'H');
		Effort = (EffortStatus ? EffortStatus == 1 ? 'C' : EffortStatus == 2 ? 'H' : EffortStatus == 3 ? 'M' : EffortStatus == 4 ? 'L' : 'H' : 'H');

		RiskClass = (RiskStatus ? RiskStatus == 1 ? 'correctclr' : RiskStatus == 2 ? 'highclr' : RiskStatus == 3 ? 'mediumclr' : RiskStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');

		ComplexityClass = (ComplexityStatus ? ComplexityStatus == 1 ? 'correctclr' : ComplexityStatus == 2 ? 'lowclr' : ComplexityStatus == 3 ? 'mediumclr' : ComplexityStatus == 4 ? 'highclr' : 'highclr' : 'highclr');

		EffortClass = (EffortStatus ? EffortStatus == 1 ? 'correctclr' : EffortStatus == 2 ? 'highclr' : EffortStatus == 3 ? 'mediumclr' : EffortStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');

		return <ul className="list-unstyled d-flex justify-content-between rcevalue mb-0"><li className={RiskClass}>{Risk}</li><li className={ComplexityClass}>{Complexity}</li><li className={EffortClass}>{Effort}</li></ul>;
	}

	RedirectPentest(RecurrenceType, pentestid) {
		if (RecurrenceType == 1) {
			this.props.history.push('/app/pentest/' + cryptr.encrypt(pentestid));
		} else if (RecurrenceType == 2) {
			this.props.history.push('/app/assessment/' + cryptr.encrypt(pentestid));
		} else if (RecurrenceType == 3) {
			this.props.history.push('/app/quickassessment/' + cryptr.encrypt(pentestid));
		} else {
			this.props.history.push('/app/managepentest');
		}
	}

	cutExtraSystems(system, assetsCount) {
		let returnstring = (system.Remarks ? system.Remarks : '') + (system.Name ? ' - ' + system.Name : '');

		if (returnstring.length > 28 || assetsCount > 1) {
			returnstring = returnstring.toString().substring(0, 25) + '...';
		}
		return returnstring;
	}

	cutExtraVulnName(Name) {
		let returnstring = Name;

		if (returnstring.length > 28) {
			returnstring = returnstring.toString().substring(0, 25) + '...';
		}
		return returnstring;
	}

	render() {
		const { loading, CorrectRisk, ExtremeRisk, HighRisk, MediumRisk, LowRisk, DonePentestCount, DoneAssessmentCount, DoneQuickAssessmentCount, TrendsDates, TrendsLabels, KnownTrends, UnknownTrends, ResolvedTrends, AcceptedTrends, CommonLabelPentestCheckList, CommonPentestCheckListCount } = this.state;
		const data = {
			labels: TrendsLabels,
			datasets: [
				{
					label: this.context.intl.formatMessage({ id: 'Known' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.warning,
					borderColor: ChartConfig.color.warning,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.warning,
					pointBackgroundColor: ChartConfig.color.warning,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.warning,
					pointHoverBorderColor: ChartConfig.color.warning,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: KnownTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Unknown' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: '#ec5840',
					borderColor: '#ec5840',
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: '#ec5840',
					pointBackgroundColor: '#ec5840',
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: '#ec5840',
					pointHoverBorderColor: '#ec5840',
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: UnknownTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Solved' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.success,
					borderColor: ChartConfig.color.success,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.success,
					pointBackgroundColor: ChartConfig.color.success,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.success,
					pointHoverBorderColor: ChartConfig.color.success,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: ResolvedTrends
				},
				{
					label: this.context.intl.formatMessage({ id: 'Accepted' }),
					fill: false,
					lineTension: 0.1,
					backgroundColor: ChartConfig.color.primary,
					borderColor: ChartConfig.color.primary,
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					pointBorderColor: ChartConfig.color.primary,
					pointBackgroundColor: ChartConfig.color.primary,
					pointBorderWidth: 1,
					pointHoverRadius: 1,
					pointHoverBackgroundColor: ChartConfig.color.primary,
					pointHoverBorderColor: ChartConfig.color.primary,
					pointHoverBorderWidth: 0,
					pointRadius: 2,
					pointHitRadius: 2,
					data: AcceptedTrends
				}
			]
		};

		const options = {
			legend: {
				labels: {
					fontColor: ChartConfig.legendFontColor
				}
			},
			scales: {
				xAxes: [{
					gridLines: {
						color: ChartConfig.chartGridColor
					},
					ticks: {
						fontColor: ChartConfig.axesColor
					}
				}],
				yAxes: [{
					gridLines: {
						color: ChartConfig.chartGridColor
					},
					ticks: {
						fontColor: ChartConfig.axesColor
					}
				}]
			}
		};

		const piedoneservicesdata = {
			labels: [
				this.context.intl.formatMessage({ id: 'Pentest' }),
				this.context.intl.formatMessage({ id: 'Assessment' }),
				this.context.intl.formatMessage({ id: 'Quickscan' })
			],
			datasets: [{
				data: [DonePentestCount, DoneAssessmentCount, DoneQuickAssessmentCount],
				backgroundColor: [
					'#eb144c',
					'#ff6900',
					'#cddc39'
				],
				hoverBackgroundColor: [
					'#eb144c',
					'#ff6900',
					'#cddc39'
				]
			}]
		};

		const pieoptions = {
			legend: {
				labels: {
					fontColor: ChartConfig.legendFontColor,
					'word-break': 'break-word',
					'white-space': 'normal',
					lineWidth: 5
				},
				position: "right",
				fullWidth: true,
				lineWidth: 5,
				legendTemplate: "<ul style=\" display: inline;\" className=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li style=\"float: left; clear: both; list-style: none;\"><div className=\"indicator_box\" style=\"background-color:<%=segments[i].fillColor%>; padding: 5px ; margin: 8px 10px 5px 10px; display: block; float: left; \"></div><span style=\"font-size: 1.250em; word-wrap: break-word;\"><%if(segments[i].label){%><%=segments[i].label%><%}%></span></li><%}%></ul>",
				// display: false
			},
			// legendCallback: function(chart) { 
			// 	var text = []; 
			// 	text.push('<ul class="' + chart.id + '-legend">sadasd'); 
			// 	for (var i = 0; i < chart.data.datasets.length; i++) { 
			// 		text.push('<li><span style="background-color:' + 
			// 				   chart.data.datasets[i].backgroundColor + 
			// 				   '"></span>'); 
			// 		if (chart.data.datasets[i].label) { 
			// 			text.push(chart.data.datasets[i].label); 
			// 		} 
			// 		text.push('</li>'); 
			// 	} 
			// 	text.push('</ul>'); 
			// 	return text.join(''); 
			// }
		};

		const pieCommonVulnerabilities = {
			labels: CommonLabelPentestCheckList,
			datasets: [{
				data: CommonPentestCheckListCount,
				backgroundColor: [
					'#eb144c',
					'#ff6900',
					'#fcb900',
					'#cddc39'
				],
				hoverBackgroundColor: [
					'#eb144c',
					'#ff6900',
					'#fcb900',
					'#cddc39'
				]
			}]
		};

		const { match } = this.props;

		const yesterday = Datetime.moment(this.state.fromdate);
		const valid = function (current) {
			return current.isAfter(yesterday);
		};

		return (
			<div className="ecom-dashboard-wrapper">
				<Helmet>
					<title>ROOTDASH</title>
					<meta name="description" content="Reactify Security Incident Dashboard" />
				</Helmet>
				<PageTitleBar title={this.context.intl.formatMessage({ id: 'Security_Incidents' })} redirect='/app/securityincident' match={match} />

				<div className="row dashbord-box d-flex flex-wrap">
					<div className="col-12">
						<div className="row mx-15">
							<div className="col-lg-4">
								<div className="boxlayout">
									<RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Starting_a_new_security_incident' })}>
										<div className="row">
											<div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15 d-flex">
												<FormControl component="fieldset">
													<p>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
													<Label component="legend"><span><strong><IntlMessages id="Step" /> 1 : </strong> <IntlMessages id="Open_the_incident" /></span></Label>
													<Label component="legend"><span><strong><IntlMessages id="Step" /> 2 : </strong> <IntlMessages id="Give_the_right_context" /></span></Label>
													<Label component="legend"><span><strong><IntlMessages id="Step" /> 3 : </strong> <IntlMessages id="Assign_the_right_person" /></span></Label>
													<Label component="legend"><span><strong><IntlMessages id="Step" /> 4 : </strong> <IntlMessages id="Insert_every_step_you_take" /></span></Label>
													<Label component="legend"><span><strong><IntlMessages id="Step" /> 5 : </strong> <IntlMessages id="Close_when_incident_is_solved" /></span></Label>


													
												</FormControl>
												
											</div>
											<MatButton variant="contained" color="primary" className="mr-15 mb-10 text-white w-100" onClick={() => this.props.history.push('/app/addsecurityincident')}><IntlMessages id="Start_an_new_security_incident" /></MatButton>
											
										</div>
									</RctCollapsibleCard>
								</div>
							</div>
							<div className="col-lg-8">
								<div className="dash-cards mobile-table clsticketdashboard">
									<RctCollapsibleCard
										collapsible
										reloadable
										closeable
										fullBlock
										heading={this.context.intl.formatMessage({ id: 'Security_incidents_trends' })}>
										<NativeSelect className="dayfilter" value={this.state.TrendsFilter}
											onChange={(e) => this.ChangeTrendsFilter(e)}
											id="Select-trends" >
											<option key={1} value={1}>{this.context.intl.formatMessage({ id: 'Year' })}</option>
											<option key={2} value={2}>{this.context.intl.formatMessage({ id: 'Month' })}</option>
											<option key={3} value={3}>{this.context.intl.formatMessage({ id: 'Week' })}</option>
										</NativeSelect>

										{loading ? <RctSectionLoader /> : ''}
										<Line data={data} options={options} />

									</RctCollapsibleCard>
								</div>
							</div>


							<div className="col-sm-12 col-md-12 col-lg-12">
								<div className="dash-cards">

								<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-12 d-sm-full clscriticalissues mobile-table"
						heading={<IntlMessages id="Your_current_security_incident" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						{loading ? <RctSectionLoader /> : ''}

						<a href="javascript:;" className="viewall-button" onClick={() => this.props.history.push('/app/manageincident')} ><IntlMessages id="View All" /></a>
						<Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={'100%'} autoHide>
							<div className="criticalissuesheight">
								<table className="table table-hover mb-0 dashboardtables">
									<thead>
										<tr>
											<th><IntlMessages id="Priority" /></th>
											<th><IntlMessages id="Opened_on" /></th>
											<th><IntlMessages id="Type_of_incident" /></th>
											<th><IntlMessages id="Status" /></th>
											<th><IntlMessages id="Assigned_to" /></th>
											<th><IntlMessages id="Action" /></th>
										</tr>
									</thead>
									<tbody>
										{this.state.Vulnerabilities.slice(0, 17).map(Vulner => (
											<tr onClick={() => this.props.history.push('/app/vulnerability/' + cryptr.encrypt(Vulner.id))}>
												<td style={{ width: '8%' }}>{this.GetRCEStatus(Vulner.Risk, Vulner.Complexity, Vulner.Effort)}</td>
												<td style={{ width: '32%' }}>{(Vulner.VulnerabilityAssets.length > 0 ? this.cutExtraSystems(Vulner.VulnerabilityAssets[0].System, Vulner.VulnerabilityAssets.length) : '')}</td>
												<td style={{ width: '33%' }}>{this.cutExtraVulnName(this.GetTranslatedText(Vulner.LanguageTranslations, 'Name', Vulner.Name).toString())}</td>
												<td style={{ width: '15%' }}>{Vulner.CreatedAt != null ? moment(Vulner.CreatedAt).format('DD-MM-YYYY') : ''}</td>
												<td style={{ width: '12%' }}>{(Vulner.IssueType != null ? Vulner.IssueType == 1 ? <IntlMessages id="Known" /> : Vulner.IssueType == 2 ? <IntlMessages id="Unknown" /> : Vulner.IssueType == 3 ? <IntlMessages id="Solved" /> : Vulner.IssueType == 4 ? <IntlMessages id="Accepted" /> : '--' : '--')}</td>
											</tr>
										))}
									</tbody>
								</table>
							</div>
						</Scrollbars>
						{/* <RecentOrdersWidget /> */}
					</RctCollapsibleCard>

								</div>
							</div>


							<div className="col-sm-12 col-md-12 col-lg-12">
								<div className="dash-cards">

								<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-12 d-sm-full clscriticalissues mobile-table"
						heading={<IntlMessages id="All_security_incident" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						{loading ? <RctSectionLoader /> : ''}

						<a href="javascript:;" className="viewall-button" onClick={() => this.props.history.push('/app/manageincident')} ><IntlMessages id="View All" /></a>
						<Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={'100%'} autoHide>
							<div className="criticalissuesheight">
								<table className="table table-hover mb-0 dashboardtables">
									<thead>
										<tr>
											<th><IntlMessages id="Priority" /></th>
											<th><IntlMessages id="Opened_on" /></th>
											<th><IntlMessages id="Type_of_incident" /></th>
											<th><IntlMessages id="Status" /></th>
											<th><IntlMessages id="Assigned_to" /></th>
											<th><IntlMessages id="Action" /></th>
										</tr>
									</thead>
									<tbody>
										{this.state.Vulnerabilities.slice(0, 17).map(Vulner => (
											<tr onClick={() => this.props.history.push('/app/vulnerability/' + cryptr.encrypt(Vulner.id))}>
												<td style={{ width: '8%' }}>{this.GetRCEStatus(Vulner.Risk, Vulner.Complexity, Vulner.Effort)}</td>
												<td style={{ width: '32%' }}>{(Vulner.VulnerabilityAssets.length > 0 ? this.cutExtraSystems(Vulner.VulnerabilityAssets[0].System, Vulner.VulnerabilityAssets.length) : '')}</td>
												<td style={{ width: '33%' }}>{this.cutExtraVulnName(this.GetTranslatedText(Vulner.LanguageTranslations, 'Name', Vulner.Name).toString())}</td>
												<td style={{ width: '15%' }}>{Vulner.CreatedAt != null ? moment(Vulner.CreatedAt).format('DD-MM-YYYY') : ''}</td>
												<td style={{ width: '12%' }}>{(Vulner.IssueType != null ? Vulner.IssueType == 1 ? <IntlMessages id="Known" /> : Vulner.IssueType == 2 ? <IntlMessages id="Unknown" /> : Vulner.IssueType == 3 ? <IntlMessages id="Solved" /> : Vulner.IssueType == 4 ? <IntlMessages id="Accepted" /> : '--' : '--')}</td>
											</tr>
										))}
									</tbody>
								</table>
							</div>
						</Scrollbars>
						{/* <RecentOrdersWidget /> */}
					</RctCollapsibleCard>

								</div>
							</div>
						</div>
					</div>


				</div>
			</div >
		)
	}
}