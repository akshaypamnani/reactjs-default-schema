//Vulneribility

import React, { Component } from 'react';
import {
   Label,
   Col,
   FormText
} from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../actions/ApiServices';
import { intlShape } from 'react-intl';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import MUIDataTable from 'mui-datatables';
import CustomFooter from '../../components/CustomFooter/CustomFooter';
import RctSectionLoader from '../../components/RctSectionLoader/RctSectionLoader';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

const Filecheck = (value, props) => {
   if (value) {
      var fileExt = value.split('.').pop().toString().trim().toLowerCase();
      var validFileExtensions = ["xlsx"];
      if (!validFileExtensions.includes(fileExt)) {
         return <span className="form-error is-visible">Invalid File Extension</span>;
      }
   }
};

class BulkImport extends Component {
   static contextTypes = {
      intl: intlShape,
   };

   state = {
      loading: false,
      logoBaseURL: '',
      RedirectionURL: '/app/bulkimport',
      SampleFiles: {
         'Importing_System_and_Assets': 'Systemsassets.xlsx',
         'Importing_Companies': 'Company.xlsx',
         'Importing_Users': 'Users.xlsx',
         'Importing_Vulnerabilities': 'Vulnerabilities.xlsx',
         'Importing_combine_stuff': 'CombineAllimports.xlsx'
      },
      SelectedImportMode: 1,
      SelectedTitle: 'Importing_System_and_Assets',
      SelectedFile: 'Systemsassets.xlsx',
      UploadFile: null,
      BulkimpData: {
         importedcompany: [],
         importedsystem: [],
         importedasset: [],
         importeduser: [],
         importedVulnerabilities: [],
         rejectedcompany: [],
         rejectedsystem: [],
         rejectedasset: [],
         rejecteduser: [],
         rejectedVulnerabilities: []
      }
   }

   sigPad = {}

   async componentWillReceiveProps(nextProps) {
      const logoBaseURL = await ApiService.ConfigAPIURL();
      await this.setState({ logoBaseURL });
      if (nextProps.location !== this.props.location) {
      }
   }

   async componentDidMount() {
      this.setState({ loading: true });
      this.checkUserRole();
      this.setState({ loading: false });
   }

   checkUserRole() {
      ApiService.CheckUserRole(this.props);
   }

   ChangeFile(filename, modeid) {
      this.setState({ loading: true });
      var getFileName = this.state.SampleFiles[filename];
      if (getFileName) {
         this.setState({ SelectedFile: getFileName, SelectedImportMode: modeid, SelectedTitle: filename });
      }
      this.setState({ loading: false });
   }

   async DownloadFile() {
      this.setState({ loading: true });
      var Token = await ApiService.GetWithoutmsgAPI('userapi/getdownloadtoken');
      if (Token) {
         var URL = 'uploadapi/getsamplefiles';
         URL += '?filename=' + this.state.SelectedFile + '&token=' + Token;
         await ApiService.DownloadImage(URL, this.state.SelectedFile);
      }
      this.setState({ loading: false });
   }

   async UploadFile() {
      this.form.validateAll();

      this.setState({ loading: true });
      if (this.form.getChildContext()._errors.length <= 0) {
         var Company;
         if (this.state.UploadFile) {
            let fd = new FormData();
            fd.append('excelFile', this.state.UploadFile[0]);
            fd.append('importType', this.state.SelectedImportMode);
            var ExcelFileData = await ApiService.UploadExcelAPI(fd);

            if (ExcelFileData) {
               this.setState({ BulkimpData: ExcelFileData });
               console.log('excelfile', ExcelFileData);
            }
         }
      }
      this.setState({ loading: false });
   }

   render() {
      const { SelectedFile, SelectedTitle, BulkimpData, loading } = this.state;


      const columns = [
         { name: "column Name", options: { filter: false, sort: false, customHeadRender: (columnMeta, updateDirection) => (<th key={0}><IntlMessages id="column Name" /></th>) } },
         { name: "Issue", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Issue" /></th>) } }];
      const data = [
      ];
      const options = {
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,
         selectableRows: 'none',
         responsive: 'scrollFullHeight',
         textLabels: {
            body: {
               noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
            }
         },
         customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
            return (
               <CustomFooter
                  count={count}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  changeRowsPerPage={changeRowsPerPage}
                  changePage={changePage}
                  textLabels={textLabels} />
            );
         },
         onCellClick: (colData, cellMeta) => {
            if (cellMeta.colIndex != 7) {
               this.props.history.push('editcompany/' + cryptr.encrypt(this.state.Companies[cellMeta.dataIndex].id));
            }
         }
      };

      return (
         <div className="formelements-wrapper">
            {loading ? <RctSectionLoader /> : ''}
            <PageTitleBar title={'Import'} redirect={this.state.RedirectionURL} match={this.props.match} />
            <Form ref={c => { this.form = c }}>
               <div className="row">
                  <div className="col-sm-12 col-md-12 col-xl-4">
                     <div className="boxlayout">
                        <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'What_are_you_importing?' })}>
                           <div className="row">
                              <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                 <FormControl component="fieldset">
                                    <MatButton variant="contained" color="primary" onClick={() => this.ChangeFile('Importing_System_and_Assets', 1)} className="mr-10 mb-10 text-white w-100"><IntlMessages id="Systems_&_Assets" /></MatButton>
                                    <MatButton variant="contained" color="primary" onClick={() => this.ChangeFile('Importing_Companies', 2)} className="mr-10 mb-10 text-white w-100"><IntlMessages id="Companies" /></MatButton>
                                    <MatButton variant="contained" color="primary" onClick={() => this.ChangeFile('Importing_Users', 3)} className="mr-10 mb-10 text-white w-100"><IntlMessages id="Users" /></MatButton>
                                    <MatButton variant="contained" color="primary" onClick={() => this.ChangeFile('Importing_Vulnerabilities', 4)} className="mr-10 mb-10 text-white w-100"><IntlMessages id="Vulnerability" /></MatButton>
                                    <MatButton variant="contained" color="primary" onClick={() => this.ChangeFile('Importing_combine_stuff', 5)} className="mr-10 mb-10 text-white w-100"><IntlMessages id="I'm_an_expert:_combine_stuff" /></MatButton>
                                    <MatButton variant="contained" color="primary" className="mr-10 mb-10 text-white w-100"><IntlMessages id="Previous_logs" /></MatButton>
                                 </FormControl>
                              </div>
                           </div>
                        </RctCollapsibleCard>
                     </div>
                  </div>
                  <div className="col-sm-12 col-md-12 col-xl-8">
                     <div className="boxlayout">
                        <RctCollapsibleCard heading={<IntlMessages id={SelectedTitle} />}>
                           <div className="row">
                              <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                 <FormControl component="fieldset">
                                    <Label component="legend"><IntlMessages id="Please_use_the_sample_structure_as_below" /></Label>
                                    <p className="vulnAttachments" style={{ width: 'fit-content' }}>{SelectedFile}
                                       <a className="vulnAttachmentdwnld" onClick={() => this.DownloadFile()} href="javascript:;"><i class="zmdi zmdi-download"></i></a>
                                    </p>
                                 </FormControl>
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-sm-12 col-md-12 col-xl-7 p-0 pb-15">
                                 <FormControl component="fieldset">
                                    <Label component="legend"><IntlMessages id="Upload_here:" /></Label>
                                    <Col sm={12}>
                                       <Input type="file" accept=".xlsx" onChange={(e) => this.setState({ UploadFile: e.target.files })} validations={[required, Filecheck]} name="file" id="File-1" />
                                       <FormText color="muted">
                                       </FormText>
                                    </Col>
                                 </FormControl>
                              </div>
                           </div>
                           <div className="row">
                              <div style={{ float: 'right', width: '100%' }}>
                                 <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.UploadFile()} className="mr-10 mb-10 text-white"><IntlMessages id="Upload_now" /></MatButton>
                              </div>
                           </div>

                           {BulkimpData.rejectedcompany && BulkimpData.rejectedcompany.length > 0 ?
                              <div className="row pb-20">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <Label component="legend"><IntlMessages id="Rejected Companies:" /></Label>
                                    <MUIDataTable style={{ overflowX: 'scroll' }} className="w-100"
                                       data={BulkimpData.rejectedcompany.map(impdata => {
                                          return [
                                             impdata.columnName,
                                             impdata.message
                                          ]
                                       })}
                                       columns={columns}
                                       options={options}
                                    />
                                 </div>
                              </div>
                              :
                              ''}
                           {BulkimpData.rejectedsystem && BulkimpData.rejectedsystem.length > 0 ?
                              <div className="row pb-20">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <Label component="legend"><IntlMessages id="Rejected Systems:" /></Label>
                                    <MUIDataTable style={{ overflowX: 'scroll' }} className="w-100"
                                       data={BulkimpData.rejectedsystem.map(impdata => {
                                          return [
                                             impdata.columnName,
                                             impdata.message
                                          ]
                                       })}
                                       columns={columns}
                                       options={options}
                                    />
                                 </div>
                              </div>
                              :
                              ''}
                           {BulkimpData.rejectedasset && BulkimpData.rejectedasset.length > 0 ?
                              <div className="row pb-20">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <Label component="legend"><IntlMessages id="Rejected Assets:" /></Label>
                                    <MUIDataTable style={{ overflowX: 'scroll' }} className="w-100"
                                       data={BulkimpData.rejectedasset.map(impdata => {
                                          return [
                                             impdata.columnName,
                                             impdata.message
                                          ]
                                       })}
                                       columns={columns}
                                       options={options}
                                    />
                                 </div>
                              </div>
                              :
                              ''}
                           {BulkimpData.rejecteduser && BulkimpData.rejecteduser.length > 0 ?
                              <div className="row pb-20">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <Label component="legend"><IntlMessages id="Rejected Users:" /></Label>
                                    <MUIDataTable style={{ overflowX: 'scroll' }} className="w-100"
                                       data={BulkimpData.rejecteduser.map(impdata => {
                                          return [
                                             impdata.columnName,
                                             impdata.message
                                          ]
                                       })}
                                       columns={columns}
                                       options={options}
                                    />
                                 </div>
                              </div>
                              :
                              ''}
                           {BulkimpData.rejectedVulnerabilities && BulkimpData.rejectedVulnerabilities.length > 0 ?
                              <div className="row pb-20">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <Label component="legend"><IntlMessages id="Rejected Vulnerabilities:" /></Label>
                                    <MUIDataTable style={{ overflowX: 'scroll' }} className="w-100"
                                       data={BulkimpData.rejectedVulnerabilities.map(impdata => {
                                          return [
                                             impdata.columnName,
                                             impdata.message
                                          ]
                                       })}
                                       columns={columns}
                                       options={options}
                                    />
                                 </div>
                              </div>
                              :
                              ''}
                        </RctCollapsibleCard>
                     </div>
                  </div>
               </div>
            </Form>
         </div >
      );
   }
}
export default BulkImport;