//Notifications
import React, { Component, Fragment } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
//API Services
import * as ApiService from '../../actions/ApiServices';
import * as TranslationService from '../../actions/TranslationService';
import * as SessionService from '../../actions/SessionService'

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class RootSecNotifications extends Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      loading: false,
      NotificationPentestList: []
   }

   async componentDidMount() {
      this.setState({ loading: true });
      await this.GetNotificationPentestList();
      await this.UpdateReadNotifications();
      this.setState({ loading: false });
   }

   async UpdateReadNotifications() {
      this.setState({ NotificationPentestCount: 0 });
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var NotificationPentests = await ApiService.PostWhereAPI('Notificationapi/ReadNotifications/' + cryptr.encrypt(UserID));
      }
   }

   async GetNotificationPentestList() {
      this.setState({ loading: true });
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var NotificationPentests = await ApiService.GetAPI('Notificationapi/GetNotifications/' + cryptr.encrypt(UserID));

         if (NotificationPentests && NotificationPentests.length > 0) {
            NotificationPentests.forEach(notificationuser => {
               if (notificationuser && notificationuser.Notification && notificationuser.Notification.PenTestComment) {
                  if (notificationuser.Notification.PenTestComment.LanguageTranslations && notificationuser.Notification.PenTestComment.LanguageTranslations.length > 0) {
                     notificationuser.Notification.PenTestComment.LanguageTranslations = notificationuser.Notification.PenTestComment.LanguageTranslations.filter(x => x.PageRefId == 5);
                  }
               }
            });
         }

         this.setState({ NotificationPentestList: NotificationPentests.filter(x => x.Notification != null && x.Notification.PenTestComment != null && x.Notification.PenTestComment.Vulnerability != null) });
      }
      this.setState({ loading: false });
   }

   GetTranslatedText(translatedData, field, value) {
      return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
   }

   RedirectVulnerability(Vulnerid) {
      var userRole = SessionService.GetUserRole('userRole')
      if (userRole) {
         if (userRole.Name == "ClientUser") {
            this.props.history.push({
               pathname: '/company/vulnerability/' + cryptr.encrypt(Vulnerid),
               state: { history: this.props.location }
            });
         } else {
            this.props.history.push({
               pathname: '/app/vulnerability/' + cryptr.encrypt(Vulnerid),
               state: { history: this.props.location }
            });
         }
      }
   }

   RedirectPentest(pentestid, recurrencetype) {
      var userRole = SessionService.GetUserRole('userRole')
      if (userRole) {
         if (userRole.Name == "ClientUser") {
            this.props.history.push({
               pathname: '/company/companyassessment/' + cryptr.encrypt(pentestid),
               state: { history: this.props.location }
            });
         } else {
            if (recurrencetype == 2) {
               this.props.history.push({
                  pathname: '/app/assessment/' + cryptr.encrypt(pentestid),
                  state: { history: this.props.location }
               });
            } else if (recurrencetype == 3) {
               this.props.history.push({
                  pathname: '/app/quickassessment/' + cryptr.encrypt(pentestid),
                  state: { history: this.props.location }
               });
            } else {
               this.props.history.push({
                  pathname: '/app/pentest/' + cryptr.encrypt(pentestid),
                  state: { history: this.props.location }
               });
            }
         }
      }
   }

   render() {

      const { NotificationPentestList, loading } = this.state;
      return (
         <div className="data-table-wrapper">
            <PageTitleBar title={<IntlMessages id="Notifications" />} redirect='/app/notifications' match={this.props.match} />
            <RctCollapsibleCard fullBlock>
               <Fragment>
                  <Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={'100%'} autoHide>
                     <ul className="list-group new-customer-list">
                        {NotificationPentestList && NotificationPentestList.map((Notificationuser, key) => (
                           <li className="list-group-item d-flex justify-content-between" key={key}>
                              <div className="col-sm-12">
                                 <div className="media">
                                    {/* <div className="media-left mr-15">
                                       {key + 1}.
                                    </div> */}
                                    <div className="media-body">
                                       <span className="d-block fs-14"><strong>{Notificationuser.Notification.PenTestComment.User.FirstName}</strong> <IntlMessages id="is_commented" /> <strong>{this.GetTranslatedText(Notificationuser.Notification.PenTestComment.LanguageTranslations, 'Comment', Notificationuser.Notification.PenTestComment.Comment)}</strong> <IntlMessages id="On_Vulnerability" /> <a href="javascript:;" className="notificationliStrong" onClick={() => this.RedirectVulnerability(Notificationuser.Notification.PenTestComment.Vulnerability.id)}> <strong>({Notificationuser.Notification.PenTestComment.Vulnerability.Name})</strong></a> <IntlMessages id="of_pentest" /> <a href="javascript:;" className="notificationliStrong" onClick={() => this.RedirectPentest(Notificationuser.Notification.PenTest.id, Notificationuser.Notification.PenTest.RecurrenceType)}><strong>({Notificationuser.Notification.PenTest.Name})</strong></a></span>
                                       <span className="d-block fs-12 text-muted">{moment(Notificationuser.CreatedAt).format('DD-MM-YYYY HH:mm')}</span>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        ))}
                     </ul>
                     {loading ? <RctSectionLoader /> : ''}
                  </Scrollbars>
               </Fragment>
            </RctCollapsibleCard>
         </div>
      );
   }
}
export default RootSecNotifications;