//Manage Company

import React, { Component } from 'react';
import {
    // Button,
    //  Form,
    FormGroup,
    Label,
    //  Input,
    FormText,
    Col,
    FormFeedback
} from 'reactstrap';
import MatButton from '@material-ui/core/Button';
// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import * as ApiService from '../../actions/ApiServices';
import { isEmail } from 'validator';
import Button from 'react-validation/build/Button';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Select from 'react-validation/build/select';
import { Link } from 'react-router-dom';

import * as SessionService from '../../actions/SessionService';
import RctSectionLoader from '../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};

const email = (value) => {
    if (!isEmail(value)) {
        return <span className="form-error is-visible">{value} <IntlMessages id="is_invalid_email_address" /></span>;
    }
};

const lt = (value, props) => {
    // get the maxLength from component's props
    if (!value.toString().trim().length > props.maxLength) {
        // Return jsx
        return <span className="error">The value exceeded {props.maxLength} symbols.</span>
    }
};

const IPCheck = (value, props) => {
    if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
    }
};

const MobileCheck = (value, props) => {
    if (!(/^\d{10}$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Invalid_Mobile_number" /></span>;
    }
};
const IsPassword = (value, props) => {
    if (value && value.length > 0 && !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,16}$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Password_must_contain_at_least_one_uppercase_character,one_lowercase_character,one_digit,one_special_character_and_length_must_be_min_8_char,_max_16char" />  </span>;
    }
};
const isEqual = (value, props, components) => {
    const bothUsed = components.password[0].isUsed && components.confirm[0].isUsed;
    const bothChanged = components.password[0].isChanged && components.confirm[0].isChanged;

    if (bothChanged && bothUsed && components.password[0].value !== components.confirm[0].value) {
        return <span className="form-error is-visible"><IntlMessages id="Passwords_are_not_equal" /></span>;
    }
};


class AddProfile extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        User: {
            id: 0,
            FirstName: "",
            LastName: "",
            Email: "",
            MobileNo: "",
            IP: "",
            Password: "",
            UserCompanies: [],
            assigncmp: "",
            Company: 0,
            ConfirmPwd: '',
            UserRole: {
                RoleId: 0
            }
        },
        Role: {
            id: 0,
            UserId: "",
            RoleId: ""
        },
        Roletype: "2",
        companies: [],
        SelectedCompanies: [],
        UserID: 0,
        name: [],
        loading: false,
    }

    componentDidMount() {
        this.checkUserRole();
        var sessiondata = SessionService.GetUserSession();
        if (sessiondata.id) {
            this.setState({ UserID: sessiondata.id });
            this.GetUser(cryptr.encrypt(sessiondata.id));
        }
        // this.setState({ User:user });
    }

    checkUserRole() {
        var userRole = SessionService.GetUserRole('userRole');
        if (userRole) {
            if (userRole.Name == "ClientUser") {
                this.props.history.push('/company/clientprofile');
            }
        }
    }

    async GetUser(id) {
        this.setState({ loading: true });
        var user = await ApiService.GetAPI('User/' + id);
        user.ConfirmPwd = user.Password;

        var str = "";
        user.UserCompanies.forEach(cmp => {
            if (cmp.Company) {
                str = str + cmp.Company.Name + " , ";
            }

        });
        user.assigncmp = str.substring(0, str.length - 3);
        this.setState({ User: user });
        this.setState({ loading: false });
    }
    handleChange = (event, key) => {
        let User = Object.assign({}, this.state.User);    //creating copy of object
        User[key] = event.target.value;                        //updating value
        this.setState({ User });
    }

    handleChangepwd = (key) => {
        let User = Object.assign({}, this.state.User);    //creating copy of object
        User[key] = '';                        //updating value
        this.setState({ User });
    }

    // handleClick = () => {
    //     this.form.validateAll();
    // };
    async SaveRootSecUser() {
        this.setState({ loading: true });
        this.form.validateAll();
        if (this.state.User.Password.length > 0) {
            if (this.state.User.ConfirmPwd.length <= 0) {
                this.setState({ loading: false });
                return;
            } else if (this.state.User.ConfirmPwd != this.state.User.Password) {
                this.setState({ loading: false });
                return;
            }
        }
        if (this.form.getChildContext()._errors.length <= 0) {
            var User;
            var userData = {
                id: this.state.User.id,
                FirstName: this.state.User.FirstName.toString().trim(),
                LastName: this.state.User.LastName.toString().trim(),
                Email: this.state.User.Email.toString().trim(),
                MobileNo: this.state.User.MobileNo,
                IP: this.state.User.IP,
                Password: this.state.User.Password,
                ConfirmPwd: this.state.User.ConfirmPwd
            };
            // userData.compnies = [userData.Company];
            // userData.Roles = [this.state.Roletype];
            User = await ApiService.PostAPI('userapi/EditRootSecuser/   ', userData);
            if (User) {
                // this.props.history.push('/app/dashboard');
            }
        }
        this.setState({ loading: false });
    }

    render() {
        const { activeIndex, loading } = this.state;

        return (
            <div className="formelements-wrapper">
                <PageTitleBar title={<IntlMessages id="Profile" />} redirect='/app/profile' match={this.props.match} />
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-xl-12">
                        <RctCollapsibleCard>
                            {loading ? <RctSectionLoader /> : ''}
                            <Form ref={c => { this.form = c }}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="firstname"><IntlMessages id="First_name" /></Label>
                                            <Input type="text" maxLength="50" className="form-control" name="firstname" validations={[required]} value={this.state.User.FirstName}
                                                onChange={(e) => this.handleChange(e, 'FirstName')}
                                                id="firstname" placeholder={this.context.intl.formatMessage({ id: 'First_name' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="lastname"><IntlMessages id="Last_Name" /></Label>
                                            <Input type="text" maxLength="50" className="form-control" name="lastname" validations={[required]} value={this.state.User.LastName}
                                                onChange={(e) => this.handleChange(e, 'LastName')}
                                                id="lastname" placeholder={this.context.intl.formatMessage({ id: 'Last_Name' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="mobileno"><IntlMessages id="Mobile_nummer" /></Label>
                                            <Input type="text" maxLength="10" name="mobileno" className="form-control" validations={[required, MobileCheck]} value={this.state.User.MobileNo}
                                                onChange={(e) => this.handleChange(e, 'MobileNo')}
                                                id="mobileno" placeholder={this.context.intl.formatMessage({ id: 'Mobile_nummer' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="useremail"><IntlMessages id="Emailaddress" /></Label>
                                            <Input type="email" maxLength="150" disabled="disabled" name="useremail" className="form-control" validations={[required, email]} value={this.state.User.Email}
                                                onChange={(e) => this.handleChange(e, 'Email')}
                                                id="useremail" placeholder={this.context.intl.formatMessage({ id: 'Emailaddress' })} />
                                        </FormGroup>

                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="ip"><IntlMessages id="IP_Address" /><span className="required">*</span></Label>
                                            <Input type="text" name="ipaddress" validations={[IPCheck]} className="form-control" value={this.state.User.IP}
                                                onChange={(e) => this.handleChange(e, 'IP')}
                                                id="ipaddress" placeholder={this.context.intl.formatMessage({ id: 'IP_Address' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">

                                        <FormGroup>
                                            <Label for="role"><IntlMessages id="Role" /></Label>
                                            <Label className="form-control">{this.state.User.UserRole.RoleId == 1 ? "User" : this.state.User.UserRole.RoleId == 2 ? "Employee" : this.state.User.UserRole.RoleId == 3 ? "Super Admin" : ''}</Label>
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="assignedcomp"><IntlMessages id="Company_Assigned_to" /></Label>
                                            <Label className="form-control">{this.state.User.assigncmp}</Label>
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="dept">department</Label>
                                            <Input type="text" className="form-control" value={"Department"} disabled="disabled" name="dept" id="dept" placeholder={this.context.intl.formatMessage({ id: 'Department' })} />
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="Password-3"><IntlMessages id="Password" />
                                            </Label>
                                            <Input type="password" autocomplete="new-password" onPaste={() => setTimeout(() => {
                                                this.handleChangepwd('Password');
                                            }, 100)} className="form-control"
                                                onChange={(e) => this.handleChange(e, 'Password')}
                                                value={this.state.User.Password} name="password" id="Password-3" placeholder={this.context.intl.formatMessage({ id: 'Password' })} validations={[IsPassword, isEqual]} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="cnfPassword-3"><IntlMessages id="Confirm_Password" />
                                            </Label>
                                            <Input type="password" autocomplete="new-password" onPaste={() => setTimeout(() => {
                                                this.handleChangepwd('ConfirmPwd');
                                            }, 100)} value={this.state.User.ConfirmPwd} className="form-control" name="confirm" id="cnfPassword-3" placeholder={this.context.intl.formatMessage({ id: 'Confirm_Password' })} validations={[isEqual]} />
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row">
                                    <div style={{ float: 'right', width: '100%' }}>
                                        <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveRootSecUser()} className="mr-10 mb-10 text-white"><IntlMessages id="Create/Update" /></MatButton>

                                        <MatButton variant="contained" style={{ float: 'right', color: '#5d92f4' }} onClick={() => this.props.history.push('/app/dashboard')} className="btn-default mr-10 mb-10"><IntlMessages id="Cancel" /></MatButton>
                                    </div>
                                </div>
                            </Form>
                        </RctCollapsibleCard>
                    </div>
                </div>
            </div>
        );
    }
}
export default AddProfile;