//Manage Company

import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Link } from 'react-router-dom'
import MatButton from '@material-ui/core/Button';

//API Services
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService';
import IconButton from '@material-ui/core/IconButton';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import { FormControl, InputLabel, NativeSelect, FormControlLabel, Checkbox } from '@material-ui/core';
import { intlShape } from 'react-intl';
import * as TranslationService from '../../../actions/TranslationService';
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import MuiTablePagination from "@material-ui/core/TablePagination";

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

function TabContainer({ children }) {
      return (
            <Typography component="div" style={{ paddingTop: 8 * 3 }}>
                  {children}
            </Typography>
      );
}

class AssessmentsVulnerabilities extends Component {

      static contextTypes = {
            intl: intlShape,
      };

      state = {
            activeIndex: 0,
            Companies: [],
            selectedDeleteID: 0,
            loading: true,
            logoBaseURL: '',
            Vulnerabilities: [],
            FilteredVulnerabilities: [],
            ShowAcceptedVuln: false,
            CompanyId: 0,
            Systems: [],
            Assets: [],
            SystemId: '0',
            AssetId: '0'
      }

      async componentDidMount() {
            this.checkUserRole();
            // this.setState({ logoBaseURL: 'http://180.211.103.172:8093/api/' });
            this.setState({ loading: true });
            const logoBaseURL = await ApiService.ConfigAPIURL();
            await this.setState({ logoBaseURL });
            await this.GetCompanies();
            await this.GetVulnerabilities();
            this.setState({ loading: false });
      }

      checkUserRole() {
            var userRole = SessionService.GetUserRole('userRole');
            if (userRole) {
                  if (userRole.Name != "ClientUser") {
                        this.props.history.push('/app/dashboard');
                  }
            }
      }

      async ChangeCheckAcceptedVuln(event) {
            await this.setState({ ShowAcceptedVuln: event.target.checked });
            var Vulnerabilities = [];
            if (this.state.ShowAcceptedVuln) {
                  Vulnerabilities = this.state.FilteredVulnerabilities.filter(x => x.IssueType != 4);
            } else {
                  Vulnerabilities = this.state.FilteredVulnerabilities;
            }
            this.setState({ Vulnerabilities });
      }

      async GetCompanies() {
            this.setState({ loading: true });
            var UserID = localStorage.getItem("user_id");
            if (UserID) {
                  var User = await ApiService.GetAPI('User/' + cryptr.encrypt(UserID));
                  this.setState({ Companies: User.UserCompanies });
                  if (User.UserCompanies.length > 0) {
                        this.setState({ CompanyId: User.UserCompanies[0].CompanyId });
                        await this.GetSystems(User.UserCompanies[0].CompanyId);
                        await this.GetAssets(null);
                  }
            } else {
                  this.props.history.push('/session/login');
            }
            this.setState({ loading: false });
      }

      async GetSystems(companyid) {
            var SystemData = {
                  Where: {
                        CompanyId: companyid,
                        IsActive: 1
                  }
            };

            var Systems = await ApiService.PostWhereAPI('/execute/conditions/System', SystemData);
            if (Systems) {
                  this.setState({ Systems });
            }
      }

      async GetAssets(systemid) {
            var AssetData = {
                  Where: {
                        CompanyId: this.state.CompanyId,
                        SystemId: systemid,
                        IsActive: 1
                  }
            };

            var Assets = await ApiService.PostWhereAPI('/execute/conditions/Asset', AssetData);
            if (Assets) {
                  this.setState({ Assets });
            }
      }

      async GetVulnerabilities() {
            var UserID = localStorage.getItem("user_id");

            if (UserID) {
                  var dashboarddata = {
                        UserId: UserID
                  };

                  var Dashboardpentest = await ApiService.PostWhereAPI('Dashboard/GetPentestByUserID', dashboarddata);

                  if (Dashboardpentest.Vulnerabilities && Dashboardpentest.Vulnerabilities.length > 0) {
                        Dashboardpentest.Vulnerabilities.forEach(vuln => {
                              if (vuln.LanguageTranslations && vuln.LanguageTranslations.length > 0) {
                                    vuln.LanguageTranslations = vuln.LanguageTranslations.filter(x => x.PageRefId == 2);
                              }
                        });

                        var Vulnerabilities = Dashboardpentest.Vulnerabilities.sort((x, y) => x.Order - y.Order);

                        // var Vulnerabilitiesarr1 = Vulnerabilities.filter(x => x.Risk != 1);
                        // var Vulnerabilitiesarr2 = Vulnerabilities.filter(x => x.Risk == 1);

                        // Vulnerabilities = [];
                        // Vulnerabilities = [...Vulnerabilitiesarr1, ...Vulnerabilitiesarr2];

                        this.setState({ Vulnerabilities: Vulnerabilities });
                        this.setState({ FilteredVulnerabilities: Vulnerabilities });
                  }
            }
      }

      GetTranslatedText(translatedData, field, value) {
            return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
      }

      GetRCEStatus(RiskStatus, ComplexityStatus, EffortStatus) {
            var Risk = 'H';
            var Complexity = 'H';
            var Effort = 'H';

            var RiskClass = 'highclr';
            var ComplexityClass = 'highclr';
            var EffortClass = 'highclr';

            Risk = (RiskStatus ? RiskStatus == 1 ? 'C' : RiskStatus == 2 ? 'H' : RiskStatus == 3 ? 'M' : RiskStatus == 4 ? 'L' : 'H' : 'H');
            Complexity = (ComplexityStatus ? ComplexityStatus == 1 ? 'C' : ComplexityStatus == 2 ? 'H' : ComplexityStatus == 3 ? 'M' : ComplexityStatus == 4 ? 'L' : 'H' : 'H');
            Effort = (EffortStatus ? EffortStatus == 1 ? 'C' : EffortStatus == 2 ? 'H' : EffortStatus == 3 ? 'M' : EffortStatus == 4 ? 'L' : 'H' : 'H');

            RiskClass = (RiskStatus ? RiskStatus == 1 ? 'correctclr' : RiskStatus == 2 ? 'highclr' : RiskStatus == 3 ? 'mediumclr' : RiskStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');

            ComplexityClass = (ComplexityStatus ? ComplexityStatus == 1 ? 'correctclr' : ComplexityStatus == 2 ? 'lowclr' : ComplexityStatus == 3 ? 'mediumclr' : ComplexityStatus == 4 ? 'highclr' : 'highclr' : 'highclr');

            EffortClass = (EffortStatus ? EffortStatus == 1 ? 'correctclr' : EffortStatus == 2 ? 'highclr' : EffortStatus == 3 ? 'mediumclr' : EffortStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');

            return <ul className="list-unstyled d-flex justify-content-between rcevalue mb-0"><li className={RiskClass}>{Risk}</li><li className={ComplexityClass}>{Complexity}</li><li className={EffortClass}>{Effort}</li></ul>;
      }

      handleChangedd = async (event, key) => {
            this.setState({ loading: true });
            var valueChanged = event.target.value;
            await this.setState({ [key]: event.target.value });
            var filterddata = [];
            var systemid = this.state.SystemId;
            var assetid = this.state.AssetId;
            if (key == "SystemId") {
                  systemid = valueChanged;
                  if (parseInt(systemid) == 0) {
                        await this.GetAssets(null);
                  } else {
                        await this.GetAssets(systemid);
                  }
            }
            if (key == "AssetId") {
                  assetid = valueChanged;
            }
            filterddata = this.state.Vulnerabilities.filter(function (vuln) {
                  if (parseInt(systemid) != 0 && (parseInt(assetid) != 0)) {
                        var vulnerabilityAssets = vuln.VulnerabilityAssets.filter(x => x.Asset != null && x.Asset.id == parseInt(assetid));
                        if (vulnerabilityAssets.length > 0) {
                              return vuln;
                        }
                  } else if (parseInt(systemid) == 0 && (parseInt(assetid) != 0)) {
                        var vulnerabilityAssets = vuln.VulnerabilityAssets.filter(x => x.Asset != null && x.Asset.id == parseInt(assetid));
                        if (vulnerabilityAssets.length > 0) {
                              return vuln;
                        }
                  } else if (parseInt(systemid) != 0 && (parseInt(assetid) == 0)) {
                        var vulnerabilityAssets = vuln.VulnerabilityAssets.filter(x => x.System.id == parseInt(systemid));
                        if (vulnerabilityAssets.length > 0) {
                              return vuln;
                        }
                  } else {
                        return vuln;
                  }
            });
            this.setState({ Vulnerabilities: filterddata });
            this.setState({ loading: false });
      }

      cutExtraSystems(system, assetsCount) {
            let returnstring = (system.Remarks ? system.Remarks : '') + (system.Name ? ' - ' + system.Name : '');

            if (returnstring.length > 40 || assetsCount > 1) {
                  returnstring = returnstring.toString().substring(0, 37) + '...';
            }
            return returnstring;
      }

      render() {
            const { activeIndex, loading } = this.state;

            const footerStyle = {
                  display: 'flex',
                  justifyContent: 'flex-end',
                  padding: '0px 24px 0px 24px'
            };

            const columns = [{
                  name: "R C E",
                  options: {
                        filter: false,
                        customHeadRender: (columnMeta, updateDirection) => (
                              <th key={0} style={{ width: '10%' }} className="rceAlign">
                                    {columnMeta.name}
                              </th>
                        )
                  }
            }, {
                  name: "Present on Systems",
                  options: {
                        filter: true,
                        sort: true,
                        customHeadRender: (columnMeta, updateDirection) => (
                              <th key={1} style={{ width: '20%' }} onClick={() => updateDirection(1)}><IntlMessages id="Present_on_systems" />
                              </th>
                        )
                  }
            }, {
                  name: "Name",
                  options: {
                        filter: true,
                        sort: true,
                        customHeadRender: (columnMeta, updateDirection) => (
                              <th key={2} style={{ width: '40%' }} onClick={() => updateDirection(1)}><IntlMessages id="Name" />
                              </th>
                        )
                  }
            }, {
                  name: "Date",
                  options: {
                        filter: true,
                        sort: true,
                        customHeadRender: (columnMeta, updateDirection) => (
                              <th key={3} style={{ width: '10%' }} onClick={() => updateDirection(1)}>
                                    <IntlMessages id="Date" />
                              </th>
                        )
                  }
            }, {
                  name: "Status",
                  options: {
                        filter: true,
                        sort: true,
                        customHeadRender: (columnMeta, updateDirection) => (
                              <th key={4} style={{ width: '10%' }} onClick={() => updateDirection(1)}><IntlMessages id="Status" />
                              </th>
                        )
                  }
            },
            {
                  name: "Facing",
                  options: {
                        filter: true,
                        sort: false,
                        customHeadRender: (columnMeta, updateDirection) => (
                              <th key={5} style={{ width: '10%' }} onClick={() => updateDirection(1)}><IntlMessages id="Facing" />
                              </th>
                        )
                  }
            }];
            const options = {
                  filter: false,
                  search: false,
                  print: false,
                  download: false,
                  viewColumns: false,
                  selectableRows: 'none',
                  responsive: 'scrollFullHeight',
                  textLabels: {
                        body: {
                              noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                        }
                  },
                  serverSide: false,
                  rowsPerPage: 50,
                  onCellClick: (colData, cellMeta) => {
                        this.props.history.push({
                              pathname: '/company/vulnerability/' + cryptr.encrypt(this.state.Vulnerabilities[cellMeta.dataIndex].id),
                              state: { history: this.props.location }
                        });
                  },
                  customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                        return (
                              <TableFooter>
                                    <TableRow>
                                          <TableCell style={footerStyle} colSpan={1000}>
                                                <FormControlLabel className="m-0 clsHideacceptedonly" control={
                                                      <Checkbox color="primary" checked={this.state.ShowAcceptedVuln} onChange={(e) => this.ChangeCheckAcceptedVuln(e)} value="" />
                                                } label={this.context.intl.formatMessage({ id: 'Hide_Accepted_Only' })}
                                                />

                                                <MuiTablePagination
                                                      component="div"
                                                      count={count}
                                                      rowsPerPage={rowsPerPage}
                                                      page={page}
                                                      labelRowsPerPage={this.context.intl.formatMessage({ id: 'Rows_per_page:' })}
                                                      labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${this.context.intl.formatMessage({ id: 'of' })} ${count}`}
                                                      backIconButtonProps={{
                                                            'aria-label': textLabels.previous,
                                                      }}
                                                      nextIconButtonProps={{
                                                            'aria-label': textLabels.next,
                                                      }}
                                                      rowsPerPageOptions={[10, 50, 100]}
                                                      onChangePage={(_, newPage) => changePage(newPage)}
                                                      onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                                                />
                                          </TableCell>
                                    </TableRow>
                              </TableFooter>
                        );
                  }
            };

            return (
                  <div className="data-table-wrapper" >
                        <PageTitleBar title={<IntlMessages id="Company_Vulnerabilities" />} redirect='/company/companyvulnerabilities' match={this.props.match} />
                        <RctCollapsibleCard fullBlock>
                              <div className="row">
                                    <div className="col-sm-6 col-md-6 col-xl-3" style={{ margin: '10px 10px 0px 10px' }}>
                                          <div className="form-group">
                                                <FormControl fullWidth>
                                                      <InputLabel htmlFor="age-simple"><IntlMessages id="Select_Systems" /></InputLabel>
                                                      <NativeSelect value={this.state.SystemId} onChange={(e) => this.handleChangedd(e, 'SystemId')}
                                                            id="Select-System" >
                                                            <option value={0}>{this.context.intl.formatMessage({ id: 'All' })}</option>
                                                            {this.state.Systems.map(sys => (
                                                                  <option key={sys.id} value={sys.id}>{sys.Remarks ? sys.Remarks.toString() + (sys.Name && sys.Name.toString().length > 0 ? ' - ' + sys.Name : '') : ''}</option>
                                                            ))}
                                                      </NativeSelect>
                                                </FormControl>
                                          </div>
                                    </div>
                                    <div className="col-sm-6 col-md-6 col-xl-3" style={{ margin: '10px 10px 0px 10px' }}>
                                          <div className="form-group">
                                                <FormControl fullWidth>
                                                      <InputLabel htmlFor="Status-simple"><IntlMessages id="Select_Assets" /></InputLabel>
                                                      <NativeSelect value={this.state.AssetId} onChange={(e) => this.handleChangedd(e, 'AssetId')}
                                                            id="Select-Asset" >
                                                            <option value={0}>{this.context.intl.formatMessage({ id: 'All' })}</option>
                                                            {this.state.Assets.map(ast => (
                                                                  <option key={ast.id} value={ast.id}>{ast.Name}</option>
                                                            ))}
                                                      </NativeSelect>
                                                </FormControl>
                                          </div>
                                    </div>
                              </div>
                              {loading ? <RctSectionLoader /> : ''}
                              <MUIDataTable

                                    // title={"Employee list"}
                                    data={this.state.Vulnerabilities.map(vulnerability => {
                                          return [
                                                this.GetRCEStatus(vulnerability.Risk, vulnerability.Complexity, vulnerability.Effort),
                                                vulnerability.VulnerabilityAssets.length > 0 ? this.cutExtraSystems(vulnerability.VulnerabilityAssets[0].System, vulnerability.VulnerabilityAssets.length) : '',
                                                this.GetTranslatedText(vulnerability.LanguageTranslations, 'Name', vulnerability.Name),
                                                (vulnerability.CreatedAt != null ? moment(vulnerability.CreatedAt).format('DD-MM-YYYY') : ''),
                                                (vulnerability.IssueType != null ? vulnerability.IssueType == 1 ? <IntlMessages id="Known" /> : vulnerability.IssueType == 2 ? <IntlMessages id="Unknown" /> : vulnerability.IssueType == 3 ? <IntlMessages id="Solved" /> : vulnerability.IssueType == 4 ? <IntlMessages id="Accepted" /> : '--' : '--'),
                                                <div className="text-right pr-2">{vulnerability.Facing != null ? vulnerability.Facing == 1 ? <IntlMessages id="Public" /> : vulnerability.Facing == 2 ? <IntlMessages id="External" /> : vulnerability.Facing == 3 ? <IntlMessages id="Internal" /> : '--' : '--'}</div>
                                                // vulnerability.UserName
                                          ]
                                    })}
                                    columns={columns}
                                    options={options}
                              />
                        </RctCollapsibleCard>
                  </div >
            );
      }
}
export default AssessmentsVulnerabilities;