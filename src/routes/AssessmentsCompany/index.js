//Manage Company

import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Link } from 'react-router-dom'
import MatButton from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

// intl messages
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';

//API Services
import * as SessionService from '../../actions/SessionService';
import * as ApiService from '../../actions/ApiServices';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DeleteConfirmationDialog from '../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from '../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../components/CustomFooter/CustomFooter';
import AssessmentDialog from '../../components/modalDialogs/assessmentDialog';
import { NativeSelect } from '@material-ui/core';
import { intlShape } from 'react-intl';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

function TabContainer({ children }) {
      return (
            <Typography component="div" style={{ paddingTop: 8 * 3 }}>
                  {children}
            </Typography>
      );
}

class AssessmentsCompany extends Component {

      static contextTypes = {
            intl: intlShape,
      };

      state = {
            activeIndex: 0,
            filteredpentests: [],
            Pentests: [],
            Company: 0,
            Companies: [],
            Status: 0,
            selectedDeletePentest: 0,
            loading: true
      }

      async componentDidMount() {
            this.checkUserRole();
            this.setState({ loading: true });
            await this.GetPentests();
            await this.GetCompanies();
            this.setState({ loading: false });
      }

      checkUserRole() {
            var userRole = SessionService.GetUserRole('userRole');
            if (userRole) {
                  if (userRole.Name != "ClientUser") {
                        this.props.history.push('/app/dashboard');
                  }
            }
      }

      async GetPentests() {
            // var pentestdata = {
            //       Where: {
            //             RecurrenceType: [1, 2],
            //             IsActive: 1
            //       }
            // }
            // var Pentests = await ApiService.PostWhereAPI('execute/conditions/PenTest', pentestdata);
            // this.setState({ Pentests: Pentests });
            // this.setState({ filteredpentests: Pentests });
            var UserID = localStorage.getItem("user_id");

            if (UserID) {
                  var dashboarddata = {
                        UserId: UserID
                  };

                  var Dashboardpentest = await ApiService.PostWhereAPI('Dashboard/GetPentestByUserID', dashboarddata);
                  if (Dashboardpentest) {

                        if (Dashboardpentest.PenTests && Dashboardpentest.PenTests.length > 0) {
                              Dashboardpentest.PenTests.forEach(pentest => {
                                    pentest.Vulnerabilities = pentest.Vulnerabilities.filter(x => x.IsActive == 1);
                              });
                        }

                        this.setState({ Pentests: Dashboardpentest.PenTests, filteredpentests: Dashboardpentest.PenTests });
                  }
            }
      }

      handleChange(value) {
            this.setState({ activeIndex: value });
            //  this.setState({ [event.target.name]: event.target.value });
      }
      handleChangedd = (event, key) => {
            this.setState({ loading: true });
            this.setState({ [key]: event.target.value });
            var filterddata = [];
            let companyid = this.state.Company;
            let statusid = this.state.Status;
            if (key == "Company") {
                  companyid = event.target.value;
            }
            // if (key == "Status") {
            //       statusid = event.target.value;
            // }

            filterddata = this.state.Pentests.filter(function (pntst) {
                  if (companyid == 0) {
                        return pntst;
                  }
                  else {
                        return pntst.CompanyId == companyid;
                  }
            });
            this.setState({ filteredpentests: filterddata });
            this.setState({ loading: false });
      }

      async GetCompanies() {
            var companies = await ApiService.GetAPI('Company');
            // var test1 = Companies.map(usrcmp => {
            //     return Companies.Name;
            // });            
            this.setState({ Companies: companies });
      }

      EditPentest(pentestid) {
            this.refs.assessmentDialog.handleEditClickOpen(cryptr.encrypt(pentestid));
      }

      onDelete(pentestid) {
            this.refs.deleteConfirmationDialog.open();
            this.setState({ selectedDeletePentest: cryptr.encrypt(pentestid) });
      }

      // delete
      async delete() {
            if (this.state.selectedDeletePentest != 0) {
                  var Pentest = await ApiService.DeleteAPI('PenTest/' + this.state.selectedDeletePentest);
                  if (Pentest) {
                        // this.props.history.push('app/')
                  }
                  this.setState({ selectedDeletePentest: 0 });
                  this.refs.deleteConfirmationDialog.close();
                  this.GetPentests();
            }
      }

      ViewPentest(pentestid) {
            this.props.history.push('companyassessment/' + cryptr.encrypt(pentestid));
      }

      OpenPentestDialog() {
            this.refs.assessmentDialog.handleAddClickOpen();
      }

      BackFromProps() {
            setTimeout(async () => {
                  this.setState({ loading: true });
                  await this.GetPentests();
                  await this.GetCompanies();
                  this.setState({ loading: false });
            }, 1500);
      }

      render() {

            const { activeIndex, loading } = this.state;
            // const columns = ["Name", "Company", "No. of Vulneribilitie", "Status", "Actions"];
            const columns = [
                  // { name: "View", label: "View", options: { filter: false, sort: false } },
                  { name: "id", label: "id", options: { display: false } },
                  { name: "Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Name" /></th>) } },
                  { name: "Company", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Company" /></th>) } },
                  { name: "Date", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Date" /></th>) } },
                  { name: "No. of Vulnerability", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={4} onClick={() => updateDirection(4)}><IntlMessages id="No._of_vulnerability" /></th>) } },
                  // { name: "Status", label: "Status", options: { filter: true, sort: true } },
                  // { name: "Actions", label: "Actions" }
            ]
            const options = {
                  filter: false,
                  search: false,
                  print: false,
                  download: false,
                  viewColumns: false,
                  selectableRows: 'none',
                  responsive: 'scrollFullHeight',
                  textLabels: {
                        body: {
                              noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                        }
                  },
                  customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                        return (
                              <CustomFooter
                                    count={count}
                                    page={page}
                                    rowsPerPage={rowsPerPage}
                                    changeRowsPerPage={changeRowsPerPage}
                                    changePage={changePage}
                                    textLabels={textLabels} />
                        );
                  },
                  onCellClick: (colData, cellMeta) => {
                        this.props.history.push('companyassessment/' + cryptr.encrypt(this.state.filteredpentests[cellMeta.dataIndex].id));
                  }
            };

            return (
                  <div className="data-table-wrapper">
                        <PageTitleBar title={<IntlMessages id="Assessment" />} redirect='/company/companyassessments' match={this.props.match} />
                        <RctCollapsibleCard fullBlock>

                              {loading ? <RctSectionLoader /> : ''}
                              <MUIDataTable
                                    data={this.state.filteredpentests.map((pentst, key) => {
                                          return [
                                                pentst.id,
                                                pentst.Name,
                                                pentst.Company.Name,
                                                pentst.DateofTest != null ? moment(pentst.DateofTest).format('DD-MM-YYYY') : '',
                                                <div className="text-right pr-15">{pentst.Vulnerabilities.length}</div>
                                                // (pentst.IsPublished != 1 ? 'Not Published' : 'Published')
                                          ]
                                    })}
                                    columns={columns}
                                    options={options}
                              />
                        </RctCollapsibleCard>
                  </div>
            );
      }
}
export default AssessmentsCompany;