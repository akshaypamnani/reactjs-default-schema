//Create Client User

import React, { Component } from 'react';
import { FormGroup, Label } from 'reactstrap';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService';
import { isEmail } from 'validator';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';

let Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};

const email = (value) => {
    if (!isEmail(value)) {
        return <span className="form-error is-visible">{value} <IntlMessages id="is_invalid_email_address" /></span>;
    }
};

const IPCheck = (value, props) => {
    if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
    }
};

const MobileCheck = (value, props) => {
    if (!(/^\d{10}$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Invalid_Mobile_number" /></span>;
    }
};

class RequestNewClientUser extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        User: {
            id: 0,
            FirstName: "",
            LastName: "",
            Email: "",
            MobileNo: "",
            IP: "",
            JobDescription: ""
        },
        UserID: 0,
        loading: false,
        sessionUser: {
            id: 0,
            UserCompanies: [{ id: 0 }]
        },
    }

    async componentDidMount() {
        // this.checkUserRole();
        this.setState({ loading: true });
        const { id } = this.props.match.params;
        if (id) {
            this.setState({ UserID: id });
            await this.GetUser(id);
        }
        var sessionUser = SessionService.GetUserSession();
        if (sessionUser) {
            this.setState({ sessionUser });
        }
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    handleClick = () => {
        this.form.validateAll();
    };

    async GetUser(id) {
        var user = await ApiService.GetAPI('RequestNewUser/' + id);
        console.log('users', user);
        this.setState({ User: user });
    }

    handleChange = (event, key) => {
        let User = Object.assign({}, this.state.User);    //creating copy of object
        User[key] = event.target.value;                        //updating value
        this.setState({ User });
    }

    async checkDuplicateEmail() {
        var UserData = {
            email: this.state.User.Email.toString().trim(),
            IsAccepted: false
        }
        var checkDuplicateEmail = await ApiService.PostWhereAPI('userapi/checkDuplicateEmailAddress', UserData);

        console.log('checkDuplicateEmail', checkDuplicateEmail);
        if (checkDuplicateEmail && checkDuplicateEmail.IsDuplicate) {
            return true;
        } else {
            return false;
        }
    }

    async SaveClientUser() {
        this.form.validateAll();

        this.setState({ loading: true });
        if (this.form.getChildContext()._errors.length <= 0) {

            if (await this.checkDuplicateEmail()) {
                this.setState({ loading: false });
                return;
            }

            var User;
            var userData = {
                id: this.state.User.id,
                FirstName: this.state.User.FirstName.toString().trim(),
                LastName: this.state.User.LastName.toString().trim(),
                Email: this.state.User.Email.toString().trim(),
                MobileNo: this.state.User.MobileNo,
                IP: this.state.User.IP,
                JobDescription: this.state.User.JobDescription,
                RequestedBy: this.state.sessionUser.id,
                CompanyId: this.state.sessionUser.UserCompanies[0].CompanyId
            };

            if (userData.id != 0) {
                User = await ApiService.PostAPI('RequestNewUser/' + cryptr.encrypt(userData.id), userData);
            } else {
                User = await ApiService.PutAPI('RequestNewUser/', userData);
            }
            if (User) {
                this.props.history.push('/app/requestedusers');
            }
        }
        this.setState({ loading: false });
    }

    render() {
        const { loading } = this.state;
        return (
            <div className="formelements-wrapper">
                <PageTitleBar title={<IntlMessages id="Add/edit_requested_new_user" />} newtitle="clientuser" redirect='/app/requestedusers' match={this.props.match} />
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-xl-12">
                        <RctCollapsibleCard>
                            {loading ? <RctSectionLoader /> : ''}
                            <Form ref={c => { this.form = c }}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="firstname"><IntlMessages id="First_name" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="50" className="form-control" value={this.state.User.FirstName} validations={[required]} onChange={(e) => this.handleChange(e, 'FirstName')} name="firstname" id="firstname" placeholder={this.context.intl.formatMessage({ id: 'First_name' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="lastname"><IntlMessages id="Last_Name" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="50" className="form-control" value={this.state.User.LastName} validations={[required]} onChange={(e) => this.handleChange(e, 'LastName')} name="lastname" id="lastname" placeholder={this.context.intl.formatMessage({ id: 'Last_Name' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="MobileNo"><IntlMessages id="Mobile_nummer" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input className="form-control" maxLength="10" type="text" validations={[required, MobileCheck]} value={this.state.User.MobileNo} onChange={(e) => this.handleChange(e, 'MobileNo')} name="email" id="Email-3" placeholder={this.context.intl.formatMessage({ id: 'Mobile_nummer' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="usersemail"><IntlMessages id="Emailaddress" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input className="form-control" autocomplete="false" maxLength="150" type="text" validations={[required, email]} value={this.state.User.Email} onChange={(e) => this.handleChange(e, 'Email')} name="usersemail" id="usersemail" placeholder={this.context.intl.formatMessage({ id: 'Emailaddress' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="ip"><IntlMessages id="IP_Address" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input className="form-control" validations={[required, IPCheck]} type="text" name="ip" value={this.state.User.IP} onChange={(e) => this.handleChange(e, 'IP')} id="ip" placeholder={this.context.intl.formatMessage({ id: 'IP_Address' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="jobdescription"><IntlMessages id="Job_Description" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input className="form-control" autocomplete="false" maxLength="150" type="text" validations={[required]} value={this.state.User.JobDescription} onChange={(e) => this.handleChange(e, 'JobDescription')} name="jobdescription" id="jobdescription" placeholder={this.context.intl.formatMessage({ id: 'Job description' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div style={{ float: 'right', width: '100%' }}>
                                        <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveClientUser()} className="mr-10 mb-10 text-white"><IntlMessages id="Create/Update" /></MatButton>

                                        <MatButton variant="contained" style={{ float: 'right', color: '#5d92f4' }} onClick={() => this.props.history.push('/app/requestedusers')} className="btn-default mr-10 mb-10"><IntlMessages id="Cancel" /></MatButton>
                                    </div>
                                </div>
                            </Form>
                        </RctCollapsibleCard>
                    </div>
                </div>
            </div >
        );
    }
}
export default RequestNewClientUser;