//User Client Management

import React, { Component } from 'react';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RootClient from './rootsecEmps';
import ClientUsers from './clientusers';

// rct section loader
import RctSectionLoader from '../../components/RctSectionLoader/RctSectionLoader';

function TabContainer({ children }) {
   return (
      <Typography component="div" style={{ paddingTop: 8 * 3 }}>
         {children}
      </Typography>
   );
}

class UserClientManagement extends Component {

   state = {
      activeIndex: 0,
      loading : true
   }

   componentDidMount() {
      this.setState({loading : true });
      var Url = window.location.href.split("/").pop();
      if (Url.trim().toString() == 'userclientmanagement') {
         this.handleChange(1);
      }
      this.setState({loading : false });
   }

   handleChange(value) {
      this.setState({ activeIndex: value });
   }

   render() {
      const { activeIndex, loading } = this.state;
      if (loading) {
         return <RctSectionLoader />;
      }
      return (
         <div className="image-cropper-wrap">
            <PageTitleBar title={<IntlMessages id="User_Management" />} match={this.props.match} redirect='/app/userclientmanagement' />
            {/* <FixedTab /> */}
            <RctCollapsibleCard fullBlock>
               <Tabs style={{display: 'none'}}
                  value={activeIndex}
                  onChange={(e, value) => this.handleChange(value)}
                  textColor="primary"
                  indicatorColor="primary">
                  <Tab label={<IntlMessages id="Rootsec_Users" />} />
                  <Tab label={<IntlMessages id="Client's_users" />} />
               </Tabs>
               <SwipeableViews
                  axis={'x'}
                  index={this.state.activeIndex}
                  onChangeIndex={(index) => this.handleChange(index)}>
                  <TabContainer>
                     <RootClient />
                  </TabContainer>
                  <TabContainer>
                     <ClientUsers />
                  </TabContainer>
               </SwipeableViews>
            </RctCollapsibleCard>
         </div>
      );
   }
}
export default UserClientManagement;