//Create Rootsec employee

import React, { Component } from 'react';
import ReactTags from 'react-tag-autocomplete';
import {
    // Form,
    FormGroup,
    Label,
    FormFeedback
} from 'reactstrap';
// import { Input } from 'reactstrap';
import Input from 'react-validation/build/input';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import * as ApiService from '../../../actions/ApiServices';
import FormControl from '@material-ui/core/FormControl';
import Select from 'react-select';
import Form from 'react-validation/build/form';
import { isEmail } from 'validator';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
import { IconButton } from '@material-ui/core';
import update from 'react-addons-update';
import { NotificationManager } from 'react-notifications';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};

const email = (value) => {
    if (!isEmail(value)) {
        return <span className="form-error is-visible">{value} <IntlMessages id="is_invalid_email_address" /></span>;
    }
};

const lt = (value, props) => {
    // get the maxLength from component's props
    if (!value.toString().trim().length > props.maxLength) {
        // Return jsx
        return <span className="error">The value exceeded {props.maxLength} symbols.</span>
    }
};

const isEqual = (value, props, components) => {
    const bothUsed = components.password[0].isUsed && components.confirm[0].isUsed;
    const bothChanged = components.password[0].isChanged && components.confirm[0].isChanged;

    if (bothChanged && bothUsed && components.password[0].value !== components.confirm[0].value) {
        return <span className="form-error is-visible"><IntlMessages id="Passwords_are_not_equal" /></span>;
    }
};

const IsEdit = (value) => {
    return <span className="form-error is-visible">Edit</span>;
}

const IPCheck = (value, props) => {
    if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
    }
};

const MobileCheck = (value, props) => {
    if (!(/^\d{10}$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Invalid_Mobile_number" /></span>;
    }
};
const IsPassword = (value, props) => {
    if (value && value.length > 0 && !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,16}$/.test(value)) || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Password_must_contain_at_least_one_uppercase_character,one_lowercase_character,one_digit,one_special_character_and_length_must_be_min_8_char,_max_16char" />  </span>;
    }
};
class CreateRootsecEmp extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        User: {
            id: 0,
            FirstName: "",
            LastName: "",
            Email: "",
            MobileNo: "",
            IP: "",
            JobDescription: "",
            Password: "",
            UserCompanies: [],
            Company: 0,
            Companies: [],
            ConfirmPwd: ''
        },
        Role: {
            id: 0,
            UserId: "",
            RoleId: ""
        },
        Roletype: "2",
        companies: [],
        newCompanies: [],
        SelectedCompanies: [],
        UserID: 0,
        name: [],
        loading: false,
        IsMultiselectInValid: false,
        tags: [],
        suggestions: [
            { id: 3, name: "Bananas" },
            { id: 4, name: "Mangos" },
            { id: 5, name: "Lemons" },
            { id: 6, name: "Apricots", disabled: true }
        ],
        DeletedCompany: [],
        UserIps: [],
        OldUserIps: [],
        DeletedIps: []
    }

    async componentDidMount() {
        this.checkUserRole();
        this.setState({ loading: true });
        await this.GetCompanies();
        const { id } = this.props.match.params;
        if (id) {
            this.setState({ UserID: id });
            await this.GetUser(id);
        }
        await this.AddHost();
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    ValidationMultiselect() {
        if (this.state.tags.length <= 0) {
            this.setState({ IsMultiselectInValid: true });
        } else {
            this.setState({ IsMultiselectInValid: false });
        }
        return this.state.IsMultiselectInValid;
    }

    async GetCompanies() {
        var Companies = await ApiService.GetAPI('Company');

        var autoCompanies = [];
        var CompaniesSuggesions = [];
        if (Companies && Companies.length > 0) {
            Companies.forEach(comp => {
                autoCompanies.push({ id: comp.id, name: comp.Name });

                var newcomp = {
                    id: comp.id,
                    name: comp.Name,
                    value: comp.id,
                    label: comp.Name
                }
                CompaniesSuggesions.push(newcomp);
            });
        }
        this.setState({ companies: autoCompanies, newCompanies: CompaniesSuggesions });
    }

    async GetUser(id) {
        var user = await ApiService.GetAPI('User/' + id);
        if (user) {

            user.ConfirmPwd = user.Password;
            this.setState({ User: user });
            var comps = [];
            var newcomps = [];
            user.UserCompanies.forEach(comp => {
                var tag = this.state.companies.filter((x) => x.id == comp.Company.id)[0];
                if (tag) {
                    tag.usercompid = comp.id;
                    tag.disabled = true;
                }
                comps.push({ id: comp.Company.id, name: comp.Company.Name, disabled: true, usercompid: comp.id });

                newcomps.push({ id: comp.Company.id, name: comp.Company.Name, value: comp.Company.id, label: comp.Company.Name, disabled: true, VcompId: comp.id });
            });
            // this.setState({ tags: comps });
            this.setState({ tags: newcomps });
            if (this.state.User.UserRole) {
                this.setState({ Roletype: this.state.User.UserRole.RoleId.toString() })
            }
            this.setState({ UserIps: user.UserIps, OldUserIps: user.UserIps });
        }

    }

    handleChangeRadio = (e, key) => {
        this.setState({ [key]: e.target.value });
    }

    handleChange = (event, key) => {
        let User = Object.assign({}, this.state.User);    //creating copy of object
        User[key] = event.target.value;                        //updating value
        this.setState({ User });
    }

    handleChangepwd = (key) => {
        let User = Object.assign({}, this.state.User);    //creating copy of object
        User[key] = '';                        //updating value
        this.setState({ User });
    }

    handleChangeMultiselect = (event) => {                      //updating value
        this.setState({ SelectedCompanies: event.target.value });
        // setTimeout(() => {
        //     this.ValidationMultiselect();
        // }, 1000);
    }

    async SaveRootSecUser() {
        this.form.validateAll();

        if (this.state.User.Password.length > 0) {
            if (this.state.User.ConfirmPwd.length <= 0) {
                this.setState({ loading: false });
                return;
            } else if (this.state.User.ConfirmPwd != this.state.User.Password) {
                this.setState({ loading: false });
                return;
            }
        }
        this.setState({ loading: true });
        var companies = this.state.tags.map((comp) => (
            comp.id
        ));
        if (this.form.getChildContext()._errors.length <= 0) {
            var User;
            var userData = {
                id: this.state.User.id,
                FirstName: this.state.User.FirstName.toString().trim(),
                LastName: this.state.User.LastName.toString().trim(),
                Email: this.state.User.Email.toString().trim(),
                MobileNo: this.state.User.MobileNo,
                IP: this.state.User.IP,
                JobDescription: this.state.User.JobDescription ? this.state.User.JobDescription.toString() : '',
                Password: this.state.User.Password,
                ConfirmPwd: this.state.User.ConfirmPwd
            };
            userData.compnies = companies;
            userData.Roles = [this.state.Roletype];
            userData.UserIps = this.state.UserIps;

            if (userData.UserIps && userData.UserIps.length > 0) {
                var oldUserIps = this.state.OldUserIps;

                var arrUserIps = userData.UserIps.map((userip) => {
                    return userip.IP;
                });

                var duplicateIps = [];
                duplicateIps = this.find_duplicate_in_array(arrUserIps);

                if (duplicateIps.length > 0) {
                    var strduplicateIps = duplicateIps.join(",");
                    NotificationManager.error('Duplicate Ips ' + strduplicateIps + ' not allowed!');
                    this.setState({ loading: false });
                    return;
                }

                var NewUserIps = [];

                if (oldUserIps && oldUserIps.length > 0) {
                    var arrOldUserIps = oldUserIps.map((userip) => {
                        return userip.IP;
                    });

                    if (arrUserIps && arrUserIps.length > 0) {
                        arrUserIps.forEach(ip => {
                            if (!arrOldUserIps.includes(ip)) {
                                NewUserIps.push(ip);
                            }
                        });
                    }
                }

                if (NewUserIps && NewUserIps.length > 0) {

                    var checkUserIPData = {
                        UserIPs: NewUserIps
                    }

                    var checkUserIP = await ApiService.PostWhereAPI('userapi/checkDuplicateUserIp', checkUserIPData);

                    if (checkUserIP && checkUserIP.IsDuplicate) {
                        this.setState({ loading: false });
                        return;
                    }
                }
            }

            if (userData.UserIps && userData.UserIps.length > 1) {
                userData.UserIps.splice((userData.UserIps.length - 1), 1);
            } else {
                userData.UserIps = [];
            }

            if (userData.id != 0) {
                User = await ApiService.PostAPI('userapi/EditRootSecuser/', userData);
            } else {
                User = await ApiService.PutAPI('userapi/AddRootSecuser/', userData);
            }
            if (User) {
                await this.DeleteHosts();
                if (this.state.DeletedCompany && this.state.DeletedCompany.length > 0) {
                    this.state.DeletedCompany.forEach(async (tag) => {
                        if (companies && companies.length > 0) {
                            if (!companies.includes(tag.id)) {
                                var comp = await ApiService.HardDeleteAPI('UserCompany/' + cryptr.encrypt(tag.VcompId));
                            }
                        } else {
                            var comp = await ApiService.HardDeleteAPI('UserCompany/' + cryptr.encrypt(tag.VcompId));
                        }
                    });
                }
                this.props.history.push('/app/userrootsecmanagement');
            }
        }
        this.setState({ loading: false });
    }

    find_duplicate_in_array(arra1) {
        var object = {};
        var result = [];

        arra1.forEach(function (item) {
            if (!object[item])
                object[item] = 0;
            object[item] += 1;
        })

        for (var prop in object) {
            if (object[prop] >= 2) {
                result.push(prop);
            }
        }

        return result;

    }

    handleValidate(tag) {
        tag.disabled = true;
        return tag;
    }

    async handleDelete(i) {
        var tag = this.state.tags[i];

        if (tag) {
            tag.disabled = false;
            var newtag = this.state.companies.filter((x) => x.id == tag.id)[0];
            if (newtag) {
                newtag.disabled = false;
            }
            const tags = this.state.tags.slice(0)
            tags.splice(i, 1)
            this.setState({ tags })
            if (tag.usercompid) {
                this.state.DeletedCompany.push(tag);
            }
            this.setState({ loading: false });
        }
    }

    handleAddition(tag) {
        const tags = [].concat(this.state.tags, tag)
        this.setState({ tags })
        // setTimeout(() => {
        //     this.ValidationMultiselect();
        // }, 1000);
    }

    async RemoveHost(index) {
        await this.setState({ loading: true });
        let UserIps = this.state.UserIps;
        if (UserIps[index].id > 0) {
            const DeletedIps = this.state.DeletedIps;
            await DeletedIps.push(UserIps[index]);
            await this.setState({ DeletedIps });
        }
        UserIps.splice(index, 1);
        await this.setState({ UserIps: UserIps });
        await this.setState({ loading: false });
    }

    async AddHost() {
        let UserIps = this.state.UserIps;
        let newIP = {
            id: 0,
            IP: '0.0.0.0'
        }
        UserIps.push(newIP);
        await this.setState({ UserIps: UserIps });
    }

    async DeleteHosts() {
        if (this.state.DeletedIps && this.state.DeletedIps.length > 0) {
            this.state.DeletedIps.forEach(async (host) => {
                var host = await ApiService.HardDeleteAPI('UserIps/' + cryptr.encrypt(host.id));
            });
        }
    }

    handleChange2(value, data, key) {
        let selectedHostIndex = this.state.UserIps.indexOf(data);
        let newIP = update(this.state, {
            UserIps: {
                [selectedHostIndex]: {
                    [key]: { $set: value }
                }
            }
        });
        this.setState({ UserIps: newIP.UserIps });
    }

    handleChangeSystemSelect = async (tags) => {
        let newtags = this.state.tags;
        let differencetag = newtags.filter(x => !tags.includes(x));

        if (differencetag && differencetag.length > 0) {
            if (differencetag[0].VcompId)
                this.state.DeletedCompany.push(differencetag[0]);
        }

        await this.setState({ tags });
        // const SystemTagIds = this.state.tags.map((x) => { return x.id });
        // await this.setState({ SystemTagIds });
    };

    render() {
        const { loading } = this.state;
        return (
            <div className="formelements-wrapper">
                <PageTitleBar title={<IntlMessages id="Add/Edit_rootsec_user" />} redirect='/app/userrootsecmanagement' match={this.props.match} />
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-xl-12">
                        <RctCollapsibleCard>
                            {loading ? <RctSectionLoader /> : ''}
                            <Form ref={c => { this.form = c }}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="firstname"><IntlMessages id="First_name" />
                                                <span className="required">*</span>
                                            </Label>
                                            {/* invalid={this.state.User.FirstName.length <= 0} */}
                                            <Input type="text" maxLength="50" className="form-control" name="firstname" validations={[required]} value={this.state.User.FirstName}
                                                onChange={(e) => this.handleChange(e, 'FirstName')}
                                                id="firstname" placeholder={this.context.intl.formatMessage({ id: 'First_name' })} />
                                            {/* <FormFeedback invalid={this.state.User.FirstName.length <= 0}>Sweet! that name is available</FormFeedback> */}
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="lastname"><IntlMessages id="Last_Name" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="50" className="form-control" name="lastname" validations={[required]} value={this.state.User.LastName}
                                                onChange={(e) => this.handleChange(e, 'LastName')}
                                                id="lastname" placeholder={this.context.intl.formatMessage({ id: 'Last_Name' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="mobileno"><IntlMessages id="Mobile_nummer" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="10" name="mobileno" className="form-control" validations={[required, MobileCheck]} value={this.state.User.MobileNo}
                                                onChange={(e) => this.handleChange(e, 'MobileNo')}
                                                id="mobileno" placeholder={this.context.intl.formatMessage({ id: 'Mobile_nummer' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="useremail"><IntlMessages id="Emailaddress" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="email" disabled={this.state.User.id > 0} maxLength="150" name="useremail" className="form-control" validations={[required, email]} value={this.state.User.Email}
                                                onChange={(e) => this.handleChange(e, 'Email')}
                                                id="useremail" placeholder={this.context.intl.formatMessage({ id: 'Emailaddress' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="ip"><IntlMessages id="IP_Address" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" name="ip" validations={[required, IPCheck]} className="form-control" value={this.state.User.IP}
                                                onChange={(e) => this.handleChange(e, 'IP')}
                                                id="ip" placeholder={this.context.intl.formatMessage({ id: 'IP_Address' })} />
                                        </FormGroup>
                                    </div>

                                    {(this.state.User.Email != 'E.pilecki@rootsec.nl' && this.state.User.Email != 'mail@thomasneurink.com') ?
                                        <div className="col-sm-12 col-md-12 col-xl-4">
                                            <FormGroup>
                                                <FormControl component="fieldset" required>
                                                    <Label for="role"><IntlMessages id="Role" /></Label>
                                                    <RadioGroup row aria-label="role" value={this.state.Roletype} name="role" onChange={(e) => this.handleChangeRadio(e, 'Roletype')} >
                                                        <FormControlLabel value="2" control={<Radio />} label="Employee" />
                                                        <FormControlLabel value="3" control={<Radio />} label="Super admin" />
                                                    </RadioGroup>
                                                </FormControl>
                                            </FormGroup>
                                        </div> : ''}
                                </div>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup className={this.state.IsMultiselectInValid ? 'rootsecmultiselect errorautocomplete' : 'rootsecmultiselect'}>
                                            <FormControl fullWidth>
                                                <Label for="assignedcomp"><IntlMessages id="Company_Assigned_to" /></Label>
                                                <Select
                                                    isMulti
                                                    value={this.state.tags}
                                                    isClearable
                                                    isSearchable
                                                    options={this.state.newCompanies}
                                                    onChange={this.handleChangeSystemSelect}
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                />
                                                {/* <ReactTags class="form-control"
                                                    tags={this.state.tags}
                                                    handleValidate={this.handleValidate.bind(this)}
                                                    suggestions={this.state.companies}
                                                    handleDelete={this.handleDelete.bind(this)}
                                                    handleAddition={this.handleAddition.bind(this)} /> */}
                                                {this.state.IsMultiselectInValid ? <span className="form-error is-visible"><IntlMessages id="Required" /></span> : ''}
                                            </FormControl>
                                        </FormGroup>
                                    </div>
                                    {!this.props.match.params.id ?
                                        <div className="col-sm-12 col-md-12 col-xl-4">
                                            <FormGroup>
                                                <Label for="Password-3"><IntlMessages id="Password" />
                                                    <span className="required">*</span>
                                                </Label>
                                                <Input type="password" onPaste={() => setTimeout(() => {
                                                    this.handleChangepwd('Password');
                                                }, 100)} autocomplete="new-password" className="form-control"
                                                    onChange={(e) => this.handleChange(e, 'Password')}
                                                    value={this.state.User.Password} name="password" id="Password-3" placeholder={this.context.intl.formatMessage({ id: 'Password' })} validations={[required, IsPassword, isEqual]} />
                                            </FormGroup>
                                        </div>
                                        :
                                        <div className="col-sm-12 col-md-12 col-xl-4">
                                            <FormGroup>
                                                <Label for="Password-3"><IntlMessages id="Password" />
                                                </Label>
                                                <Input type="password" onPaste={() => setTimeout(() => {
                                                    this.handleChangepwd('Password');
                                                }, 100)} autocomplete="new-password" className="form-control"
                                                    onChange={(e) => this.handleChange(e, 'Password')}
                                                    value={this.state.User.Password} name="password" id="Password-3" placeholder={this.context.intl.formatMessage({ id: 'Password' })} validations={[IsPassword, isEqual]} />
                                            </FormGroup>
                                        </div>
                                    }
                                    {!this.props.match.params.id ?
                                        <div className="col-sm-12 col-md-12 col-xl-4">
                                            <FormGroup>
                                                <Label for="confirm"><IntlMessages id="Confirm_Password" />
                                                    <span className="required">*</span>
                                                </Label>
                                                <Input type="password" onPaste={() => setTimeout(() => {
                                                    this.handleChangepwd('ConfirmPwd');
                                                }, 100)} value={this.state.User.ConfirmPwd} onChange={(e) => this.handleChange(e, 'ConfirmPwd')} className="form-control" name="confirm" id="confirm" placeholder={this.context.intl.formatMessage({ id: 'Confirm_Password' })} validations={[required, isEqual]} />
                                            </FormGroup>
                                        </div>
                                        :
                                        <div className="col-sm-12 col-md-12 col-xl-4">
                                            <FormGroup>
                                                <Label for="cnfPassword-3"><IntlMessages id="Confirm_Password" />
                                                </Label>
                                                <Input type="password" onPaste={() => setTimeout(() => {
                                                    this.handleChangepwd('ConfirmPwd');
                                                }, 100)} value={this.state.User.ConfirmPwd} onChange={(e) => this.handleChange(e, 'ConfirmPwd')} className="form-control" name="confirm" id="cnfPassword-3" placeholder={this.context.intl.formatMessage({ id: 'Confirm_Password' })} validations={[isEqual]} />
                                            </FormGroup>
                                        </div>
                                    }
                                </div>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="jobdescription"><IntlMessages id="Job Description" /></Label>
                                            <Input className="form-control" autocomplete="false" maxLength="150" type="text" value={this.state.User.JobDescription} onChange={(e) => this.handleChange(e, 'JobDescription')} name="jobdescription" id="jobdescription" placeholder={this.context.intl.formatMessage({ id: 'Job description' })} />
                                        </FormGroup>
                                    </div>
                                </div>
                                <div style={{ minHeight: '200px', maxHeight: '300px', overflow: 'auto' }}>
                                    <div className="row">
                                        <Label className="pl-12">Host Ips</Label>
                                        {this.state.UserIps ? this.state.UserIps.map((host, key) => (
                                            <div className="col-sm-12 d-flex flex-wrap p-0 ipcontainer">
                                                <div className="col-sm-4 col-md-4 col-12">
                                                    <FormGroup style={{ marginBottom: '5px' }}>
                                                        <Input type="text" validations={[required, IPCheck]} className="form-control" key={key} value={host.IP} onChange={(e) => this.handleChange2(e.target.value, host, 'IP')} name="Host" id="Host" placeholder={this.context.intl.formatMessage({ id: 'Host' })} />
                                                    </FormGroup>
                                                </div>
                                                {key == (this.state.UserIps.length - 1) ? <div className="col-sm-2 col-md-2 col-12">
                                                    <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                                        <IconButton className="text-primary" onClick={() => this.AddHost()} aria-label="plus"><i className="zmdi zmdi-plus"></i></IconButton>
                                                    </FormGroup>
                                                </div> : ''}
                                                {key != (this.state.UserIps.length - 1) ? <div className="col-sm-2 col-md-2 col-12" style={{ paddingRight: '1px' }}>
                                                    <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                                        <IconButton className="text-danger" onClick={() => this.RemoveHost(key)} aria-label="plus"><i className="zmdi zmdi-minus"></i></IconButton>
                                                    </FormGroup>
                                                </div> : ''}
                                            </div>
                                        )) : ''}
                                    </div>
                                </div>
                                <div className="row">
                                    <div style={{ float: 'right', width: '100%' }}>
                                        <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveRootSecUser()} className="mr-10 mb-10 text-white"><IntlMessages id="Create/Update" /></MatButton>

                                        <MatButton variant="contained" style={{ float: 'right', color: '#5d92f4' }} onClick={() => this.props.history.push('/app/userrootsecmanagement')} className="btn-default mr-10 mb-10"><IntlMessages id="Cancel" /></MatButton>
                                    </div>
                                </div>
                            </Form>
                        </RctCollapsibleCard>
                    </div>
                </div>
            </div>
        );
    }
}
export default CreateRootsecEmp;