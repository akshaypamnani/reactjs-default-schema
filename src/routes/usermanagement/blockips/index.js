import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import * as ApiService from '../../../actions/ApiServices';
import DeleteConfirmationDialog from '../../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import { TextField, Tooltip, Checkbox, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import IntlMessages from 'Util/IntlMessages';
import MatButton from '@material-ui/core/Button';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class ManageblockIps extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        UserIps: [],
        FilteredUserIps: [],
        SelectedUserIps: [],
        loading: true,
        searchText: '',
        ChkSelectAll: false,
        Isindeterminate: false,
        IPType: '1'
    }
    async componentDidMount() {
        this.checkUserRole();
        this.setState({ loading: true });
        await this.GetBlockIPs();
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetBlockIPs() {
        var UserIpsData = {
            Where: {
                IsIPblocked: this.state.IPType,
                IsActive: 1
            }
        }
        var UserIps = await ApiService.PostWhereAPI('userapi/getUserIps', UserIpsData);

        UserIps.forEach(uip => {
            uip.Selectedrow = false;
        });

        this.setState({ UserIps, FilteredUserIps: UserIps, ChkSelectAll: false, Isindeterminate: false });
    }

    // GetBlockedwhiteListIPs() {
    //     var UserIps = this.state.UserIps;
    //     UserIps = UserIps.filter(x => x.IsIPblocked == parseInt(this.state.IPType));
    //     this.setState({ UserIps });
    // }

    handleChange = name => (event, checked) => {
        this.setState({ [name]: checked });
    };

    handleChangerowcheck = async (user, event) => {
        let UserIps = this.state.UserIps;
        var userIP = UserIps.filter(x => x.id == user.id)[0];
        if (userIP) {
            userIP.Selectedrow = event.target.checked;
        }
        await this.setState({ UserIps });

        var CheckIsindeterminate = UserIps.filter(x => x.Selectedrow == false);
        if (CheckIsindeterminate && CheckIsindeterminate.length > 0) {
            await this.setState({ Isindeterminate: true });
        } else {
            await this.setState({ Isindeterminate: false, ChkSelectAll: true });
        }

        var CheckDeselect = UserIps.filter(x => x.Selectedrow == true);
        if (CheckDeselect && CheckDeselect.length <= 0) {
            await this.setState({ ChkSelectAll: false, Isindeterminate: false });
        }
        await this.setState({ SelectedUserIps: CheckDeselect })
    };

    async SelectAllRows(event) {
        var ischecked = event.target.checked;
        let UserIps = this.state.UserIps;
        UserIps.forEach(userip => {
            userip.Selectedrow = ischecked;
        });

        if (ischecked) {
            let SelectedUserIps = UserIps;
            await this.setState({ SelectedUserIps });
        }

        await this.setState({ ChkSelectAll: ischecked, UserIps, Isindeterminate: false });
    }

    async SearchOnChange(event) {
        this.setState({ loading: true });
        await this.setState({ searchText: event.target.value });
        if (this.state.searchText.length > 0) {
            let UserIps = [];
            this.state.FilteredUserIps.forEach((user) => {
                if ((user.IP).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return UserIps.push(user);
                }
            });
            await this.setState({ UserIps: UserIps });
        } else {
            await this.setState({ UserIps: this.state.FilteredUserIps });
        }
        this.setState({ loading: false });
    }

    handleChangeRadio = async (e, key) => {
        await this.setState({ [key]: e.target.value });
        await this.GetBlockIPs();
    }

    async BlockWhiteSelectedIPs(IpType) {
        let SelectedUserIps = this.state.SelectedUserIps;
        var Ids = SelectedUserIps.map((x) => (
            x.id
        ));

        var IpsData = {
            SelectedAll: this.state.ChkSelectAll,
            Ids: Ids,
            IsIPblocked: IpType
        }

        var resIp = await ApiService.PostWhereAPI('userapi/WhiteBlockIps', IpsData);
        if (resIp) {
            this.GetBlockIPs();
        }

    }

    render() {
        const { activeIndex, loading, Isindeterminate } = this.state;

        // const columns = ["Fullname", "Username", "Email Address", "IP Address", "Role", "Company Assigned To", "Actions"];
        const columns = [
            {
                name: "Select All",
                options: {
                    filter: false,
                    sort: false,
                    customHeadRender: (columnMeta, updateDirection) => (
                        <th key={0}>
                            <Checkbox color="primary" checked={this.state.ChkSelectAll} onChange={(e) => this.SelectAllRows(e)} value="" indeterminate={Isindeterminate} /><IntlMessages id="Select_All" />
                        </th>
                    )
                }
            },
            { name: "IP Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="IP_Address" /></th>) } },
            { name: "UserName", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Name" /></th>) } },
            { name: "Company", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Company" /></th>) } },
            { name: "Is Ip Blocked ?", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={4} onClick={() => updateDirection(4)}><IntlMessages id="Is_IP_blocked" /></th>) } }
        ];

        const data = this.state.UserIps;
        const options = {
            filter: true,
            // search: false,
            print: false,
            download: false,
            viewColumns: false,
            selectableRows: 'none',
            responsive: 'scrollFullHeight',
            textLabels: {
                body: {
                    noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                }
            },
            customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                return (
                    <CustomFooter
                        count={count}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        changeRowsPerPage={changeRowsPerPage}
                        changePage={changePage}
                        textLabels={textLabels} />
                );
            },
            searchText: this.state.searchText,
            customSearch: (searchQuery, currentRow, columns) => {
                let isFound = false;
                currentRow.forEach(col => {
                    if (col.toString().indexOf(searchQuery) >= 0) {
                        isFound = true;
                    }
                });
                return isFound;
            }
        };

        return (
            <div className="data-table-wrapper">
                <PageTitleBar title={<IntlMessages id="Manage_Block_Ips" />} redirect='/app/manageblockips' match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="row">
                        <div className="mb-15 col-sm-5" style={{ padding: '0px 16px' }}>
                            <TextField id="Search" fullWidth label={<IntlMessages id="Search" />} value={this.state.searchText} onChange={(e) => this.SearchOnChange(e)} />
                        </div>
                        <div className="col-sm-7">
                            <RadioGroup row aria-label="role" value={this.state.IPType} name="role" onChange={(e) => this.handleChangeRadio(e, 'IPType')} >
                                <FormControlLabel value="1" control={<Radio />} label={<IntlMessages id="Blocked" />} />
                                <FormControlLabel value="0" control={<Radio />} label={<IntlMessages id="Not_Blocked" />} />
                            </RadioGroup>
                        </div>
                    </div>
                    {loading ? <RctSectionLoader /> : ''}
                    <MUIDataTable
                        data={this.state.UserIps.map(user => {
                            return [
                                <div className="row actionbtns">
                                    <Checkbox color="primary" checked={user.Selectedrow} onChange={(e) => this.handleChangerowcheck(user, e)} value="" />
                                </div>,
                                user.IP,
                                (user.User ? user.User.FirstName + ' ' + user.User.LastName : '--'),
                                (user.User && user.User.UserCompanies.length > 0 ? user.User.UserCompanies[0].Company ? user.User.UserCompanies[0].Company.Name : '--' : '--'),
                                <div className="text-right pr-2">{(user.IsIPblocked == 1 ? 'Yes' : 'No')}</div>
                            ]
                        })}
                        columns={columns}
                        options={options}
                    />
                    {this.state.ChkSelectAll || this.state.Isindeterminate ?
                        <div className="row pt-2">
                            <div style={{ float: 'right', width: '100%' }}>
                                {this.state.IPType == '1' ?
                                    <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.BlockWhiteSelectedIPs(false)} className="mr-10 mb-10 text-white"><IntlMessages id="Whitelist_Selected_Ips" /></MatButton>
                                    :
                                    <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.BlockWhiteSelectedIPs(true)} className="mr-10 mb-10 text-white"><IntlMessages id="Block_Selected_Ips" /></MatButton>
                                }
                            </div>
                        </div>
                        : ''}
                </RctCollapsibleCard>
            </div>
        );
    }
}
export default ManageblockIps;