//Client users

import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import MatButton from '@material-ui/core/Button';
import { Link, withRouter } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import * as ApiService from '../../../actions/ApiServices';
import DeleteConfirmationDialog from '../../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import { TextField, Tooltip } from '@material-ui/core';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class ClientUsers extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        Users: [],
        FilteredUsers: [],
        selectedDeletedUserID: 0,
        loading: true,
        searchText: ''
    }
    async componentDidMount() {
        this.setState({ loading: true });
        await this.GetUsers();
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetUsers() {
        var users = await ApiService.GetAPI('userapi/getClientuser');
        var users1 = users;
        this.setState({ Users: users1 });
        this.setState({ FilteredUsers: users1 });
    }

    handleChange(value) {
        this.setState({ activeIndex: value });
    }

    onDelete(userID) {
        this.refs.deleteConfirmationDialog.open();
        this.setState({ selectedDeletedUserID: userID });
    }

    // delete
    async delete() {
        if (this.state.selectedDeletedUserID != 0) {
            var users = await ApiService.DeleteAPI('User/' + this.state.selectedDeletedUserID);
            if (users) {
                var userIps = await ApiService.HardDeleteAPI('userapi/deleteuserips/' + this.state.selectedDeletedUserID);
            }
            this.setState({ selectedDeletedUserID: 0 });
            this.refs.deleteConfirmationDialog.close();
            this.GetUsers();
        }
    }

    async SearchOnChange(event) {
        this.setState({ loading: true });
        await this.setState({ searchText: event.target.value });
        if (this.state.searchText.length > 0) {
            let Users = [];
            this.state.FilteredUsers.forEach((user) => {
                if ((user.FirstName + " " + user.LastName).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return Users.push(user);
                } else if (user.Email.toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return Users.push(user);
                } else if (user.IP.toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return Users.push(user);
                } else if (user.UserRole && user.UserRole.Role.Name.toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return Users.push(user);
                } else if (user.UserCompanies.length > 0) {
                    user.UserCompanies.forEach(Ucomp => {
                        if (Ucomp.Company && Ucomp.Company.Name.toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                            return Users.push(user);
                        }
                    });
                }
            });
            await this.setState({ Users: Users });
        } else {
            await this.setState({ Users: this.state.FilteredUsers });
        }
        this.setState({ loading: false });
    }

    render() {
        const { activeIndex, loading } = this.state;

        // const columns = ["Fullname", "Username", "Email Address", "IP Address", "Role", "Company Assigned To", "Actions"];
        const columns = [
            { name: "Full Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Full_name" /></th>) } },
            { name: "Email Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Emailaddres" /></th>) } },
            { name: "IP Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="IP_Address" /></th>) } },
            { name: "Role", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Role" /></th>) } },
            { name: "Company Assigned To", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={4} onClick={() => updateDirection(4)}><IntlMessages id="Company_assigned_to" /></th>) } },
            {
                name: "Actions",
                options: {
                    filter: false,
                    sort: false,
                    customHeadRender: (columnMeta, updateDirection) => (
                        <th key={5}><IntlMessages id="Actions" />
                        </th>
                    )
                }
            }];

        const data = this.state.Users;
        const options = {
            filter: true,
            // search: false,
            print: false,
            download: false,
            viewColumns: false,
            selectableRows: 'none',
            responsive: 'scrollFullHeight',
            textLabels: {
                body: {
                    noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                }
            },
            customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                return (
                    <CustomFooter
                        count={count}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        changeRowsPerPage={changeRowsPerPage}
                        changePage={changePage}
                        textLabels={textLabels} />
                );
            },
            searchText: this.state.searchText,
            customSearch: (searchQuery, currentRow, columns) => {
                let isFound = false;
                currentRow.forEach(col => {
                    if (col.toString().indexOf(searchQuery) >= 0) {
                        isFound = true;
                    }
                });
                return isFound;
            },
            onCellClick: (colData, cellMeta) => {
                if (cellMeta.colIndex != 5) {
                    this.props.history.push('editClientUser/' + cryptr.encrypt(this.state.Users[cellMeta.dataIndex].id));
                }
            }
        };
        return (
            <RctCollapsibleCard fullBlock>
                <div className="row">
                    <div className="mb-15 col-sm-5" style={{ padding: '0px 16px' }}>
                        <TextField id="Search" fullWidth label={<IntlMessages id="Search" />} value={this.state.searchText} onChange={(e) => this.SearchOnChange(e)} />
                    </div>
                    <div className="mb-15 col-sm-7 text-md-right">
                        <Link to={{
                            pathname: '/app/clientuser',
                            state: { activeTab: 0 }
                        }}>
                            <MatButton variant="contained" color="primary" className="mb-10 mt-10 text-white"><IntlMessages id="Add_user" /></MatButton>
                        </Link>
                    </div>
                </div>
                {loading ? <RctSectionLoader /> : ''}
                <MUIDataTable
                    // title={"Employee list"}
                    // data={data}
                    data={this.state.Users.map(user => {
                        return [
                            user.FirstName + " " + user.LastName,
                            user.Email,
                            user.IP,
                            (user.UserRole != null ? user.UserRole.Role.Name : '--'),
                            //   user.UserRole.Role.Name,
                            (user.UserCompanies.length > 0 ? (user.UserCompanies.map((usrcmp) => (
                                usrcmp.Company != null ? usrcmp.Company.Name : ''
                            )).toString()) : '--'),
                            <div className="row actionbtns" style={{ justifyContent: 'flex-end' }}>
                                <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                                    <Link className="mt-10" style={{ float: 'right' }} to={{
                                        pathname: '/app/editClientUser/' + cryptr.encrypt(user.id),
                                        state: { activeTab: 0 }
                                    }}>
                                        <IconButton className="text-primary" aria-label="Edit"><i className="zmdi zmdi-edit"></i></IconButton>
                                    </Link>
                                </Tooltip>
                                <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                                    {/* <IconButton className="text-info"  aria-label="Edit" onClick={() => this.EditClientuser(user.id)}><i className="zmdi zmdi-edit"></i></IconButton> */}
                                    <IconButton className="text-danger" onClick={() => this.onDelete(cryptr.encrypt(user.id))} aria-label="Delete"><i className="zmdi zmdi-delete"></i></IconButton></Tooltip>
                            </div>,

                        ]
                    })}
                    columns={columns}
                    options={options}
                />

                <DeleteConfirmationDialog
                    ref="deleteConfirmationDialog"
                    title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
                    message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
                    onConfirm={() => this.delete()}
                />
            </RctCollapsibleCard>
        );
    }
}
export default withRouter(ClientUsers);