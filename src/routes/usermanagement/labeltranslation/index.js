import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import { Input } from 'reactstrap';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import * as ApiService from '../../../actions/ApiServices';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import { TextField } from '@material-ui/core';
import IntlMessages from 'Util/IntlMessages';
import MatButton from '@material-ui/core/Button';
import update from 'react-addons-update';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class LabelTranslation extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        LabelsTranslations: [],
        FilteredLabelsTranslations: [],
        loading: true,
        searchText: '',
        NewLabel: ''
    }
    async componentDidMount() {
        this.checkUserRole();
        this.setState({ loading: true });
        await this.GetLabelsTranslations();
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async componentWillMount() {
        this.setState({ loading: true });
        await this.GetLabelsTranslations();
        this.setState({ loading: false });
    }

    async GetLabelsTranslations() {
        var LabelsTranslationsData = {
            Where: {
                IsActive: 1
            }
        }
        var LabelsTranslations = await ApiService.PostWhereAPI('execute/withoutnestedconditions/LabelsTranslations', LabelsTranslationsData);

        LabelsTranslations.forEach(label => {
            label.IsEdit = 0;
        });

        LabelsTranslations.sort(function (a, b) { return b.id - a.id });

        this.setState({ LabelsTranslations, FilteredLabelsTranslations: LabelsTranslations });
    }

    handleChangeText(value, key) {
        this.setState({ [key]: value });
    }

    async handleChange(value, data, key) {
        let selectedHostIndex = this.state.LabelsTranslations.indexOf(data);
        let newLabelsTranslations = this.state.LabelsTranslations;
        newLabelsTranslations[selectedHostIndex][key] = value;
        newLabelsTranslations[selectedHostIndex]['IsEdit'] = 1;
        this.setState({ LabelsTranslations: newLabelsTranslations });
    };

    async SaveNewLabels() {
        if (this.state.NewLabel.length > 1) {

            var TranslatedLabelsData = {
                Labels: [this.state.NewLabel]
            }

            var TranslatedLabels = await ApiService.PostAPI('labelapi/AddLabels', TranslatedLabelsData)

            if (TranslatedLabels && TranslatedLabels.length > 0) {
                this.setState({ NewLabel: '' });
                this.GetLabelsTranslations();
            }
        }

    }

    async SaveBulkLabels() {
        var EditedLabels = this.state.LabelsTranslations.filter(x => x.IsEdit == 1);

        if (EditedLabels && EditedLabels.length > 0) {
            var TranslatedLabelsData = {
                Translations: EditedLabels
            }

            var TranslatedLabels = await ApiService.PostWhereAPI('labelapi/EditBulkLabels', TranslatedLabelsData)

            if (TranslatedLabels && TranslatedLabels.length > 0) {
                this.GetLabelsTranslations();
            }
        }
    }

    async SearchOnChange(event) {
        this.setState({ loading: true });
        await this.setState({ searchText: event.target.value });
        if (this.state.searchText.length > 0) {
            let LabelsTranslations = [];
            this.state.FilteredLabelsTranslations.forEach((label) => {
                if ((label.DefaultKey).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return LabelsTranslations.push(label);
                }
                // else if ((label.TitleEN).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                //     return LabelsTranslations.push(label);
                // } else if ((label.TitleNL).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                //     return LabelsTranslations.push(label);
                // }
            });

            this.setState({ LabelsTranslations });
        } else {
            await this.setState({ LabelsTranslations: this.state.FilteredLabelsTranslations });
        }
        this.setState({ loading: false });
    }

    render() {
        const { activeIndex, loading } = this.state;

        const columns = [
            { name: "Key", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Key" /></th>) } },
            { name: "Label (English)", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}>English</th>) } },
            { name: "Label (Dutch)", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}>Dutch</th>) } }
        ];

        const data = this.state.UserIps;
        const options = {
            filter: true,
            // search: false,
            print: false,
            download: false,
            viewColumns: false,
            selectableRows: 'none',
            responsive: 'scrollFullHeight',
            textLabels: {
                body: {
                    noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                }
            },
            customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                return (
                    <CustomFooter
                        count={count}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        changeRowsPerPage={changeRowsPerPage}
                        changePage={changePage}
                        textLabels={textLabels} />
                );
            },
            searchable: true,
            searchText: this.state.searchText,
            customSearch: (searchQuery, currentRow, columns) => {
                let isFound = false;
                currentRow.forEach(col => {
                    if (col.toString().indexOf(searchQuery) >= 0) {
                        isFound = true;
                    }
                });
                return isFound;
            }
        };

        return (
            <div className="data-table-wrapper">
                <PageTitleBar title={<IntlMessages id="Manage_Labels" />} redirect='/app/manageblockips' match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="row mrgnL20">
                        {/* <div className="mb-15 col-sm-12 col-md-5 col-xl-5" style={{ padding: '0px 16px' }}>
                            <TextField id="Search" fullWidth label={<IntlMessages id="Search" />} value={this.state.searchText} onChange={(e) => this.SearchOnChange(e)} />
                        </div> */}
                        <div className="mb-15 col-sm-12 col-md-5 col-xl-5" style={{ padding: '0px 16px' }}>
                            <TextField id="NewLabel" fullWidth label={<IntlMessages id="Add_New_Label" />} value={this.state.NewLabel} onChange={(e) => this.handleChangeText(e.target.value, 'NewLabel')} />
                        </div>

                        <div className="mb-15 col-sm-12 col-md-2 col-xl-2 mt-10">
                            <MatButton variant="contained" style={{ width: 'fit-content' }} color="primary" onClick={() => this.SaveNewLabels()} className="mr-15 mb-10 text-white"><IntlMessages id="Add" /></MatButton>
                        </div>

                        <div className="mb-15 col-sm-12 col-md-5 col-xl-5 mt-10" style={{ textAlign: '-webkit-right' }}>
                            <MatButton variant="contained" style={{ width: 'fit-content' }} color="primary" onClick={() => this.SaveBulkLabels()} className="mr-15 mb-10 text-white"><IntlMessages id="Update" /></MatButton>
                        </div>
                    </div>
                    {loading ? <RctSectionLoader /> : ''}
                    <MUIDataTable
                        data={this.state.LabelsTranslations.map(label => {
                            return [
                                // label.DefaultKey,
                                <Input type="text" className="form-control" value={label.DefaultKey} onChange={(e) => this.handleChange(e.target.value, label, 'DefaultKey')} name="DefaultKey" id="DefaultKey" />,
                                <Input type="text" className="form-control" value={label.TitleEN} onChange={(e) => this.handleChange(e.target.value, label, 'TitleEN')} name="TitleEN" id="TitleEN" />,
                                <Input type="text" className="form-control" value={label.TitleNL} onChange={(e) => this.handleChange(e.target.value, label, 'TitleNL')} name="TitleNL" id="TitleNL" />
                            ]
                        })}
                        columns={columns}
                        options={options}
                    />
                </RctCollapsibleCard>
            </div>
        );
    }
}
export default LabelTranslation;