import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import { TextField, Tooltip, IconButton } from '@material-ui/core';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
import DeleteConfirmationDialog from '../../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { Link, withRouter } from 'react-router-dom';
import MatButton from '@material-ui/core/Button';

let Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');

class CompanyTeamUsers extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        TeamUsers: [],
        FilteredTeamUsers: [],
        loading: true,
        searchText: '',
        ChkSelectAll: false,
        Isindeterminate: false,
        selectedDeleteUser: 0,
        Roletype: "1",
        sessionUser: {
            id: 0,
            UserRole: {
                id: 0,
                RoleId: 0
            }
        },
    }
    async componentDidMount() {
        // this.checkUserRole();
        this.setState({ loading: true });
        var sessionUser = SessionService.GetUserSession();
        if (sessionUser) {
            await this.setState({ sessionUser });
        }
        await this.GetTeamUserlist();
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetTeamUserlist() {
        var TeamUsers = await ApiService.GetWithoutmsgAPI('userapi/getusercompanywise');

        this.setState({ TeamUsers, FilteredTeamUsers: TeamUsers });
    }

    async SearchOnChange(event) {
        this.setState({ loading: true });
        await this.setState({ searchText: event.target.value });
        if (this.state.searchText.length > 0) {
            let TeamUsers = [];
            this.state.FilteredTeamUsers.forEach((user) => {
                if ((user.FirstName + ' ' + user.LastName).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return TeamUsers.push(user);
                } else if ((user.Email).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return TeamUsers.push(user);
                }
            });
            await this.setState({ TeamUsers: TeamUsers });
        } else {
            await this.setState({ TeamUsers: this.state.FilteredTeamUsers });
        }
        this.setState({ loading: false });
    }

    EditTeamUser(teamuserid) {
        this.props.history.push({
            pathname: '/app/editteamuser/' + cryptr.encrypt(teamuserid)
        });
    }

    onTeamUsers(teamuserid) {
        this.refs.deleteConfirmationDialog.open();
        this.setState({ selectedDeleteUser: cryptr.encrypt(teamuserid) });
    }

    // delete
    async delete() {
        // if (this.state.selectedDeleteUser != 0) {
        //     var TeamNewUser = await ApiService.DeleteAPI('TeamNewUser/' + this.state.selectedDeleteUser);
        //     if (TeamNewUser) {
        //         // this.props.history.push('app/')
        //     }
        //     this.setState({ selectedDeleteUser: 0 });
        //     this.refs.deleteConfirmationDialog.close();
        //     this.GetTeamUserlist();
        // }
    }

    render() {
        const { activeIndex, loading, Isindeterminate, sessionUser } = this.state;
        const columns = [
            { name: "Full Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Full_name" /></th>) } },
            { name: "Email Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Emailaddres" /></th>) } },
            { name: "Job Description", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Job Description" /></th>) } },
            {
                name: "Actions",
                options: {
                    filter: false,
                    sort: false,
                    customHeadRender: (columnMeta, updateDirection) => (
                        <th key={3}><IntlMessages id="Actions" />
                        </th>
                    )
                }
            }];
        const options = {
            filter: true,
            // search: false,
            print: false,
            download: false,
            viewColumns: false,
            selectableRows: 'none',
            responsive: 'scrollFullHeight',
            textLabels: {
                body: {
                    noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                }
            },
            customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                return (
                    <CustomFooter
                        count={count}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        changeRowsPerPage={changeRowsPerPage}
                        changePage={changePage}
                        textLabels={textLabels} />
                );
            }
        };

        return (
            <div className="data-table-wrapper">
                <PageTitleBar title={<IntlMessages id="Team Users" />} redirect='/app/teamusers' match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="row">
                        <div className="mb-15 col-sm-5" style={{ padding: '0px 16px' }}>
                            <TextField id="Search" fullWidth label={<IntlMessages id="Search" />} value={this.state.searchText} onChange={(e) => this.SearchOnChange(e)} />
                        </div>
                    </div>
                    {loading ? <RctSectionLoader /> : ''}
                    <MUIDataTable
                        data={this.state.TeamUsers.map(user => {
                            return [
                                user.FirstName + " " + user.LastName,
                                user.Email,
                                user.JobDescription,
                                <div className="row actionbtns" style={{ justifyContent: 'flex-end' }}>
                                    <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                                        <IconButton className="text-primary" onClick={() => this.EditRequestedUser(user.id)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton>
                                    </Tooltip>
                                    <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                                        <IconButton className="text-danger" onClick={() => this.onRequestedUser(user.id)} aria-label="Delete"><i className="zmdi zmdi-delete"></i></IconButton>
                                    </Tooltip>
                                </div>
                            ]
                        })}
                        columns={columns}
                        options={options}
                    />
                    <DeleteConfirmationDialog
                        ref="deleteConfirmationDialog"
                        title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
                        message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
                        onConfirm={() => this.delete()}
                    />
                </RctCollapsibleCard>
            </div>
        );
    }
}
export default CompanyTeamUsers;