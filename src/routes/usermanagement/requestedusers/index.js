import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import CustomFooter from '../../../components/CustomFooter/CustomFooter';
import { TextField, Tooltip, IconButton } from '@material-ui/core';
import IntlMessages from 'Util/IntlMessages';
import { intlShape } from 'react-intl';
import DeleteConfirmationDialog from '../../../components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { Link, withRouter } from 'react-router-dom';
import MatButton from '@material-ui/core/Button';

let Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');

class RequestedUsers extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        RequestedUsers: [],
        FilteredRequestedUsers: [],
        loading: true,
        searchText: '',
        ChkSelectAll: false,
        Isindeterminate: false,
        selectedDeleteUser: 0,
        Roletype: "1",
        sessionUser: {
            id: 0,
            UserRole: {
                id: 0,
                RoleId: 0
            }
        },
    }
    async componentDidMount() {
        // this.checkUserRole();
        this.setState({ loading: true });
        var sessionUser = SessionService.GetUserSession();
        if (sessionUser) {
            await this.setState({ sessionUser });
        }
        await this.GetRequestedUserlist();
        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetRequestedUserlist() {
        var RequestedUserData = {
            Where: {
                IsActive: 1
            }
        };

        if (this.state.sessionUser.UserRole.RoleId == 4) {
            RequestedUserData.Where.RequestedBy = this.state.sessionUser.id;
        }

        var RequestedUsers = await ApiService.PostWhereAPI('execute/withoutnestedconditions/RequestNewUser', RequestedUserData);

        this.setState({ RequestedUsers, FilteredRequestedUsers: RequestedUsers });
    }

    async SearchOnChange(event) {
        this.setState({ loading: true });
        await this.setState({ searchText: event.target.value });
        if (this.state.searchText.length > 0) {
            let RequestedUsers = [];
            this.state.FilteredRequestedUsers.forEach((user) => {
                if ((user.IP).toString().toLowerCase().includes(this.state.searchText.toString().toLowerCase())) {
                    return RequestedUsers.push(user);
                }
            });
            await this.setState({ RequestedUsers: RequestedUsers });
        } else {
            await this.setState({ RequestedUsers: this.state.FilteredRequestedUsers });
        }
        this.setState({ loading: false });
    }

    EditRequestedUser(requesteduserid) {
        this.props.history.push({
            pathname: '/app/editrequestnewuser/' + cryptr.encrypt(requesteduserid)
        });
    }

    onRequestedUser(requesteduserid) {
        this.refs.deleteConfirmationDialog.open();
        this.setState({ selectedDeleteUser: cryptr.encrypt(requesteduserid) });
    }

    // delete
    async delete() {
        if (this.state.selectedDeleteUser != 0) {
            var RequestNewUser = await ApiService.DeleteAPI('RequestNewUser/' + this.state.selectedDeleteUser);
            if (RequestNewUser) {
                // this.props.history.push('app/')
            }
            this.setState({ selectedDeleteUser: 0 });
            this.refs.deleteConfirmationDialog.close();
            this.GetRequestedUserlist();
        }
    }

    async checkDuplicateEmail(requestuser) {
        var UserData = {
            email: requestuser.Email.toString().trim(),
            IsAccepted: true
        }
        var checkDuplicateEmail = await ApiService.PostWhereAPI('userapi/checkDuplicateEmailAddress', UserData);

        console.log('checkDuplicateEmail', checkDuplicateEmail);
        if (checkDuplicateEmail && checkDuplicateEmail.IsDuplicate) {
            return true;
        } else {
            return false;
        }
    }

    async AcceptRejectUser(AcceptReject, requestuser) {
        this.setState({ loding: true });

        if (AcceptReject == 1) {
            if (await this.checkDuplicateEmail(requestuser)) {
                return;
            }
        }

        var UserData = {
            IsAccepted: AcceptReject
        }
        var RequestedUserStatus = await ApiService.PostAPI('RequestNewUser/' + cryptr.encrypt(requestuser.id), UserData);
        if (RequestedUserStatus) {
            if (RequestedUserStatus.IsAccepted == 1) {
                await this.InsertnewUser(requestuser);
            } else {
                this.GetRequestedUserlist();
            }
        }
        this.setState({ loding: false });
    }

    async InsertnewUser(requestuser) {
        this.setState({ loading: true });
        var userData = {
            FirstName: requestuser.FirstName.toString().trim(),
            LastName: requestuser.LastName.toString().trim(),
            Email: requestuser.Email.toString().trim(),
            MobileNo: requestuser.MobileNo,
            IP: requestuser.IP,
            Password: 'Login12*',
            ConfirmPwd: 'Login12*'
        };
        userData.compnies = [requestuser.CompanyId];

        userData.Roles = [this.state.Roletype];
        var User = await ApiService.PutWithoutmsgAPI('userapi/AddRootSecuser/', userData);
        if (User) {
            await this.GetRequestedUserlist();
        }
        this.setState({ loading: false });
    }

    render() {
        const { activeIndex, loading, Isindeterminate, sessionUser } = this.state;
        const columns = [
            { name: "Company_Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={0} onClick={() => updateDirection(0)}><IntlMessages id="Company_Name Name" /></th>) } },
            { name: "Full_Name", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={1} onClick={() => updateDirection(1)}><IntlMessages id="Full_name" /></th>) } },
            { name: "Email Address", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={2} onClick={() => updateDirection(2)}><IntlMessages id="Emailaddres" /></th>) } },
            { name: "Job_Description", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={3} onClick={() => updateDirection(3)}><IntlMessages id="Job_Description" /></th>) } },
            { name: "Requested_By", options: { filter: true, sort: true, customHeadRender: (columnMeta, updateDirection) => (<th key={4} onClick={() => updateDirection(4)}><IntlMessages id="Requested_By" /></th>) } },
            {
                name: "Actions",
                options: {
                    filter: false,
                    sort: false,
                    customHeadRender: (columnMeta, updateDirection) => (
                        <th key={5}><IntlMessages id="Actions" />
                        </th>
                    )
                }
            }];
        const options = {
            filter: true,
            // search: false,
            print: false,
            download: false,
            viewColumns: false,
            selectableRows: 'none',
            responsive: 'scrollFullHeight',
            textLabels: {
                body: {
                    noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
                }
            },
            customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
                return (
                    <CustomFooter
                        count={count}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        changeRowsPerPage={changeRowsPerPage}
                        changePage={changePage}
                        textLabels={textLabels} />
                );
            }
        };

        return (
            <div className="data-table-wrapper">
                <PageTitleBar title={<IntlMessages id="Requested_Users" />} redirect='/app/requestedusers' match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="row">
                        <div className="mb-15 col-sm-5" style={{ padding: '0px 16px' }}>
                            <TextField id="Search" fullWidth label={<IntlMessages id="Search" />} value={this.state.searchText} onChange={(e) => this.SearchOnChange(e)} />
                        </div>
                    </div>
                    {loading ? <RctSectionLoader /> : ''}
                    <MUIDataTable
                        data={this.state.RequestedUsers.map(user => {
                            return [
                                user.Company.Name,
                                user.FirstName + " " + user.LastName,
                                user.Email,
                                user.JobDescription,
                                user.User.FirstName + " " + user.User.LastName,
                                <div>
                                    {user.IsAccepted == 0 ?
                                        <div>
                                            {sessionUser.UserRole.RoleId == 2 ?
                                                <div className="row" style={{ justifyContent: 'flex-end' }}>
                                                    <MatButton variant="contained" onClick={() => this.AcceptRejectUser(1, user)} className="btn-success m-10"><IntlMessages id="Accept" /></MatButton>
                                                    <MatButton variant="contained" onClick={() => this.AcceptRejectUser(2, user)} className="btn-danger m-10"><IntlMessages id="Reject" /></MatButton>
                                                </div>
                                                : sessionUser.UserRole.RoleId == 4 ?
                                                    <div className="row actionbtns" style={{ justifyContent: 'flex-end' }}>
                                                        <Tooltip id="tooltip-icon" title={<IntlMessages id="edit" />}>
                                                            <IconButton className="text-primary" onClick={() => this.EditRequestedUser(user.id)} aria-label="Delete"><i className="zmdi zmdi-edit"></i></IconButton>
                                                        </Tooltip>
                                                        <Tooltip id="tooltip-icon" title={<IntlMessages id="delete" />}>
                                                            <IconButton className="text-danger" onClick={() => this.onRequestedUser(user.id)} aria-label="Delete"><i className="zmdi zmdi-delete"></i></IconButton>
                                                        </Tooltip>
                                                    </div>
                                                    : ''}
                                        </div>
                                        : user.IsAccepted == 1 ? <div className="text-right pr-2"><IntlMessages id="Accepted" /></div> : <div className="text-right pr-2"><IntlMessages id="Rejected" /></div>}
                                </div>
                            ]
                        })}
                        columns={columns}
                        options={options}
                    />
                    <DeleteConfirmationDialog
                        ref="deleteConfirmationDialog"
                        title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
                        message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
                        onConfirm={() => this.delete()}
                    />
                </RctCollapsibleCard>
            </div>
        );
    }
}
export default RequestedUsers;