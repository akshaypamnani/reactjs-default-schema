//Create Client User

import React, { Component } from 'react';
import ReactTags from 'react-tag-autocomplete';
import { FormGroup, Label, Col, FormText } from 'reactstrap';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import * as ApiService from '../../../actions/ApiServices';
import { isEmail } from 'validator';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
import { FormControl, IconButton, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import Textarea from 'react-validation/build/textarea';
import Select from 'react-select';
import ReactQuill from 'react-quill';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
    if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
        return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
    }
};

const Filecheck = (value, props) => {
    if (value) {
        var fileExt = value.split('.').pop().toString().trim().toLowerCase();
        var validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];
        if (!validFileExtensions.includes(fileExt)) {
            return <span className="form-error is-visible">Invalid File Extension</span>;
        }
    }
};

const modules = {
    toolbar: [
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'font': [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
        ['link', 'image'],
        ['clean'],
        [{ 'align': [] }],
        ['code-block']
    ],
};
const formats = [
    'header',
    'font',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'align',
    'code-block'
];

class CreateNewTicket extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        activeIndex: 0,
        Ticket: {
            id: 0,
            TicketCode: '',
            Subject: '',
            Description: '',
            TicketType: 1,
            ReferenceId: '',
            Priority: 1,
            Status: 1
        },
        SelectedType: {
            value: 1, label: 'Issue'
        },
        TicketTypes: [],
        TicketPriority: "1",
        SelectedPriority: {
            value: 1, label: 'Urgent'
        },
        TicketPriorities: [],
        VulnArguments: [],
        SelectedVuln: 0,
        Vulnerabilities: []
    }

    async componentDidMount() {
        this.checkUserRole();
        this.setState({ loading: true });
        const { id } = this.props.match.params;
        await this.GetVulnerabilities();
        if (id) {
            this.GetTicket(id);
        } else {
            await this.GetLatestTicketCode();
        }

        this.setState({
            TicketPriorities: [{
                value: 1, label: this.context.intl.formatMessage({ id: 'Urgent' })
            }, {
                value: 2, label: this.context.intl.formatMessage({ id: 'High' })
            }, {
                value: 3, label: this.context.intl.formatMessage({ id: 'Medium' })
            }, {
                value: 4, label: this.context.intl.formatMessage({ id: 'Low' })
            }],
            TicketTypes: [{
                value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
            }]
        });

        this.setState({
            SelectedPriority: { value: 4, label: this.context.intl.formatMessage({ id: 'Low' }) }, SelectedType: {
                value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
            }
        })

        this.setState({ loading: false });
    }

    checkUserRole() {
        ApiService.CheckUserRole(this.props);
    }

    async GetVulnerabilities() {
        var Vulnerabilities = await ApiService.GetWithoutmsgAPI('Vulnerabilitiesapi/getAllVulnerabilities');

        var selectVulns = [];
        if (Vulnerabilities && Vulnerabilities.length > 0) {
            Vulnerabilities.forEach(vuln => {
                selectVulns.push({ label: vuln.Name, value: vuln.id });
            });
        }

        this.setState({ Vulnerabilities: selectVulns });
    }

    async GetTicket(ticketid) {
        var Ticket = await ApiService.GetAPI('Tickets/' + ticketid);
        if (Ticket) {
            this.setState({ TicketID: Ticket.id });
            this.setState({ Ticket });

            this.setState({ SelectedType: this.state.TicketTypes.filter(x => x.value == Ticket.TicketType), SelectedPriority: this.state.TicketPriorities.filter(x => x.value == Ticket.Priority), SelectedVuln: this.state.Vulnerabilities.filter(x => x.value == Ticket.ReferenceId) });

        } else {
            this.props.history.push('/app/managetickets');
        }
    }

    async GetLatestTicketCode() {
        var LatestCode = await ApiService.GetWithoutmsgAPI('ticketsapi/GetLatestTicketCode');

        if (LatestCode) {
            let Ticket = this.state.Ticket;
            Ticket.TicketCode = LatestCode.Code;
            this.setState({ Ticket });
        }
    }

    handleChangeRadio = (e, key) => {
        console.log('handleChangeRadio', e.target.value);
        this.setState({ [key]: e.target.value });
    }

    handleChange = (event, key) => {
        let Ticket = Object.assign({}, this.state.Ticket);    //creating copy of object
        Ticket[key] = event.target.value;                        //updating value
        this.setState({ Ticket });
    }

    handleChangeEditor(value, key) {
        let Ticket = this.state.Ticket;
        Ticket[key] = value;
        this.setState({ Ticket });
    }

    handleChangeTicket = async SelectedType => {
        this.setState({ SelectedType });
    };

    handleChangeTicketPriority = async SelectedPriority => {
        this.setState({ SelectedPriority });
    };

    handleChangeVuln = async SelectedVuln => {
        this.setState({ SelectedVuln });
    };

    async SaveTicket() {
        this.form.validateAll();
        console.log('_errors', this.form.getChildContext()._errors)
        if (this.form.getChildContext()._errors.length <= 0) {
            var Ticket;
            var TicketData = {
                id: this.state.Ticket.id,
                TicketCode: this.state.Ticket.TicketCode.toString().trim(),
                Subject: this.state.Ticket.Subject.toString().trim(),
                Description: this.state.Ticket.Description.toString().trim(),
                TicketType: this.state.SelectedType.value,
                ReferenceId: this.state.SelectedVuln.value,
                Priority: this.state.SelectedPriority.value,
                Status: this.state.Ticket.Status
            };

            if (TicketData.id && TicketData.id != 0) {
                Ticket = await ApiService.PostWhereAPI('Tickets/' + cryptr.encrypt(this.state.TicketID), TicketData);
            } else {
                Ticket = await ApiService.PutAPI('Tickets', TicketData);
            }

            if (Ticket) {
                this.props.history.push('/app/managetickets');
            }
        }
        this.setState({ loading: false });
    }

    render() {
        const { loading } = this.state;
        return (
            <div className="formelements-wrapper">
                <Form ref={c => { this.form = c }}>
                    <PageTitleBar title={<IntlMessages id="Add_new_ticket/request_new_user" />} newtitle="newticket" redirect='/app/dashboard' match={this.props.match} />
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-4 d-block">
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Ticket_information' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Ticket_number:" />}</Label>
                                            </FormControl>
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex clsticketcode">#{this.state.Ticket.TicketCode}</Label>
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Assign_ticket_to_vulnerability:' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Search_a_vulnerability" />}</Label>
                                                <Select
                                                    value={this.state.SelectedVuln}
                                                    onChange={this.handleChangeVuln}
                                                    options={this.state.Vulnerabilities}
                                                />
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'File_upload_section' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="If_needed,_please_add_your_attachments_(JPG/PNG_only!)" />}</Label>
                                                <Col sm={12}>
                                                    <Input type="file" accept="image/*" onChange={(e) => this.setState({ UploadFile: e.target.files })} validations={[required, Filecheck]} name="file" id="File-1" />
                                                    <FormText color="muted">
                                                    </FormText>
                                                </Col>
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-12 col-xl-8 d-block">
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Options_for_your_ticket' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-5 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Select_a_category" />}</Label>
                                                <Select
                                                    value={this.state.SelectedType}
                                                    onChange={this.handleChangeTicket}
                                                    options={this.state.TicketTypes}
                                                />
                                            </FormControl>
                                        </div>
                                        <div className="col-sm-12 col-md-12 col-xl-2 p-0 pb-15"></div>
                                        <div className="col-sm-12 col-md-12 col-xl-5 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Select_your_priority" />}</Label>
                                                <Select
                                                    value={this.state.SelectedPriority}
                                                    onChange={this.handleChangeTicketPriority}
                                                    options={this.state.TicketPriorities}
                                                />
                                            </FormControl>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            <div className="boxlayout">
                                <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Ticket_fields' })}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Your_subject" />}</Label>
                                                <Input type="text" maxLength="250" className="form-control" value={this.state.Ticket.Subject} validations={[required]} onChange={(e) => this.handleChange(e, 'Subject')} name="Subject" id="Subject" placeholder={this.context.intl.formatMessage({ id: 'Subject' })} />
                                            </FormControl>
                                        </div>
                                        <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                            <FormControl component="fieldset">
                                                <Label component="legend" className="d-flex">{<IntlMessages id="Please_add_a_clear_description_here" />}</Label>
                                                <ReactQuill key={1} modules={modules} formats={formats} value={this.state.Ticket.Description} onChange={(value) => { this.handleChangeEditor(value, 'Description') }} placeholder={this.context.intl.formatMessage({ id: 'Description' })} />
                                            </FormControl>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div style={{ float: 'right', width: '100%' }}>
                                            <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveTicket()} className="mr-10 mb-10 text-white"><IntlMessages id="Send_your_support_request" /></MatButton>
                                        </div>
                                    </div>
                                </RctCollapsibleCard>
                            </div>
                            {/* <RctCollapsibleCard>
                            {loading ? <RctSectionLoader /> : ''}
                            <Form ref={c => { this.form = c }}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-8">
                                        <FormGroup>
                                            <Label for="subject"><IntlMessages id="Subject" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="250" className="form-control" value={this.state.Ticket.Subject} validations={[required]} onChange={(e) => this.handleChange(e, 'Subject')} name="Subject" id="Subject" placeholder={this.context.intl.formatMessage({ id: 'Subject' })} />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <Label for="subject"><IntlMessages id="Ticket_Code" />
                                                <span className="required">*</span>
                                            </Label>
                                            <Input type="text" maxLength="50" className="form-control clsticketcode" value={this.state.Ticket.TicketCode} disabled onChange={(e) => this.handleChange(e, 'TicketCode')} name="TicketCode" id="TicketCode" placeholder={this.context.intl.formatMessage({ id: 'Ticket Code' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-8">
                                        <FormGroup>
                                            <Label for="TicketType"><IntlMessages id="Ticket_Type" /></Label>
                                            <Select
                                                value={this.state.SelectedType}
                                                onChange={this.handleChangeTicket}
                                                options={this.state.TicketTypes}
                                            />
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-4">
                                        <FormGroup>
                                            <FormControl component="fieldset" required>
                                                <Label for="priority"><IntlMessages id="Priority" /></Label>
                                                <RadioGroup row aria-label="role" value={this.state.TicketPriority} name="role" onChange={(e) => this.handleChangeRadio(e, 'TicketPriority')} >
                                                    <FormControlLabel value="1" control={<Radio />} label="Normal" />
                                                    <FormControlLabel value="2" control={<Radio />} label="Urgent" />
                                                    <FormControlLabel value="3" control={<Radio />} label="Critical" />
                                                </RadioGroup>
                                            </FormControl>
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                        <FormGroup>
                                            <Label for="Description"><IntlMessages id="Description" /></Label>
                                            <ReactQuill key={1} modules={modules} formats={formats} value={this.state.Ticket.Description} onChange={(value) => { this.handleChangeEditor(value, 'Description') }} placeholder={this.context.intl.formatMessage({ id: 'Description' })} />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div style={{ float: 'right', width: '100%' }}>
                                        <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.SaveTicket()} className="mr-10 mb-10 text-white"><IntlMessages id="Create/Update" /></MatButton>

                                        <MatButton variant="contained" style={{ float: 'right', color: '#5d92f4' }} onClick={() => this.props.history.push('/app/userclientmanagement')} className="btn-default mr-10 mb-10"><IntlMessages id="Cancel" /></MatButton>
                                    </div>
                                </div>
                            </Form>
                        </RctCollapsibleCard>
                     */}
                        </div>
                    </div>
                </Form>
            </div >
        );
    }
}
export default CreateNewTicket;