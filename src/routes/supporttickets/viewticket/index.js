//Vulneribility

import React, { Component } from 'react';
import {
   FormGroup,
   Label,
   Col,
   FormText
} from 'reactstrap';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import MatButton from '@material-ui/core/Button';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Checkbox, Typography } from '@material-ui/core';
import * as ApiService from '../../../actions/ApiServices';
import * as SessionService from '../../../actions/SessionService'
import { intlShape } from 'react-intl';
import Select from 'react-select';
import ReactQuill from 'react-quill';
import MUIDataTable from 'mui-datatables';
import moment from 'moment';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

const Filecheck = (value, props) => {
   if (value) {
      var fileExt = value.split('.').pop().toString().trim().toLowerCase();
      var validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];
      if (!validFileExtensions.includes(fileExt)) {
         return <span className="form-error is-visible">Invalid File Extension</span>;
      }
   }
};

const EnumPriority = {
   1: 'Urgent',
   2: 'High',
   3: 'Medium',
   4: 'Low'
}

const modules = {
   toolbar: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ 'font': [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
      ['link', 'image'],
      ['clean'],
      [{ 'align': [] }],
      ['code-block']
   ],
};

const formats = [
   'header',
   'font',
   'bold', 'italic', 'underline', 'strike', 'blockquote',
   'list', 'bullet', 'indent',
   'link', 'image', 'align',
   'code-block'
];

function TabContainer({ children }) {
   return (
      <Typography component="div" style={{ padding: 8 * 3 }}>
         {children}
      </Typography>
   );
}

class ViewTicket extends Component {
   static contextTypes = {
      intl: intlShape,
   };

   state = {
      logoBaseURL: '',
      RedirectionURL: '/app/managepentest',
      Ticket: {
         TicketCode: 'TCK00000000'
      },
      sessionUser: {
         id: 0,
         FirstName: '',
         LastName: '',
         UserRole: {
            RoleId: 0
         }
      },
      chkResponsibility: false,
      IsAcceptButtonVisible: false,
      TicketStatus: [{
         label: 'Open', value: 1
      }, {
         label: 'Pending', value: 2
      }, {
         label: 'Process', value: 3
      }, {
         label: 'Closed', value: 4
      }],
      SelectedStatus: {
         label: 'Open', value: 1
      },
      Remark: {
         Comment: '',
         IsUrgentInput: false,
         IsDonotneedReply: false,
         CommentDate: '',
         CommentorId: 'Admin',
      },
      TicketDiscussions: [],
      SelectedPriority: {
         value: 1, label: 'Urgent'
      },
      TicketPriorities: [],
      SelectedUser: {},
      Users: []
   }

   sigPad = {}

   async componentWillReceiveProps(nextProps) {
      const logoBaseURL = await ApiService.ConfigAPIURL();
      await this.setState({ logoBaseURL });
      if (nextProps.location !== this.props.location) {
      }
   }

   async componentDidMount() {
      this.checkUserRole();
      this.setState({ loading: true });
      const { id } = this.props.match.params;
      // await this.GetVulnerabilities();

      await this.setState({
         TicketPriorities: [{
            value: 1, label: this.context.intl.formatMessage({ id: 'Urgent' })
         }, {
            value: 2, label: this.context.intl.formatMessage({ id: 'High' })
         }, {
            value: 3, label: this.context.intl.formatMessage({ id: 'Medium' })
         }, {
            value: 4, label: this.context.intl.formatMessage({ id: 'Low' })
         }]
      });
      await this.setState({
         SelectedPriority: { value: 1, label: this.context.intl.formatMessage({ id: 'Low' }) }, SelectedType: {
            value: 1, label: this.context.intl.formatMessage({ id: 'Technical_support' })
         }
      })

      if (id) {
         await this.GetUsers();
         await this.GetTicket(id);
      }
   }

   checkUserRole() {
      ApiService.CheckUserRole(this.props);
      var sessionUser = SessionService.GetUserSession();
      this.setState({ sessionUser });
   }

   async GetUsers() {
      var Users = await ApiService.GetAPI('userapi/getClientuser');

      if (Users && Users.length > 0) {
         var UserList = [];
         Users.forEach(user => {
            UserList.push({ label: user.FirstName + ' ' + user.LastName, value: user.id });
         });
         this.setState({ Users: UserList });
      }
   }

   async GetTicket(ticketid) {
      var Ticket = await ApiService.GetAPI('Tickets/' + ticketid);
      if (Ticket) {

         var Ticketpriority = this.state.TicketPriorities.filter(x => x.value == Ticket.Priority)[0];
         var TicketStatus = this.state.TicketStatus.filter(x => x.value == Ticket.Status)[0];
         var TicketAssignedUser = this.state.Users.filter(x => x.value == Ticket.AssignedUser)[0];

         console.log('Ticketpriority', TicketAssignedUser);
         if (Ticketpriority) {
            this.setState({ SelectedPriority: Ticketpriority });
         }

         if (TicketAssignedUser) {
            this.setState({ SelectedUser: TicketAssignedUser });
         }

         if (TicketStatus) {
            this.setState({ SelectedStatus: TicketStatus });
         }

         this.setState({ Ticket, TicketID: Ticket.id });

         this.GetTicketDiscussion(Ticket.id);
      } else {
         this.props.history.push('/app/managetickets');
      }
   }

   async GetTicketDiscussion(ticketid) {
      var TicketDiscussionData = {
         Where: {
            TicketId: ticketid,
            IsActive: true
         }
      }
      var TicketDiscussions = await ApiService.PostWhereAPI('execute/withoutnestedconditions/TicketDiscussions', TicketDiscussionData);
      if (TicketDiscussions && TicketDiscussions.length > 0) {

         TicketDiscussions.forEach(tdiscuss => {
            tdiscuss.CommentDate = moment(tdiscuss.CommentDate).format('DD-MM-YYYY HH:mm:ss');
         });
         this.setState({ TicketDiscussions });
      }
   }

   async DownloadImage(FileName) {
      var Token = await ApiService.GetWithoutmsgAPI('userapi/getdownloadtoken');
      if (Token) {
         var URL = 'uploadapi/getimage';
         URL += '?filename=' + FileName + '&token=' + Token;
         await ApiService.DownloadImage(URL, FileName);
      }
   }

   handleChangeTicketPriority = async SelectedPriority => {
      this.setState({ SelectedPriority });
      var TicketData = this.state.Ticket;
      TicketData.Priority = SelectedPriority.value;

      var Ticket = await ApiService.PostAPI('Tickets/' + cryptr.encrypt(this.state.TicketID), TicketData);
   };

   handleChangeAssignedUser = async SelectedUser => {
      this.setState({ SelectedUser });
      var TicketData = this.state.Ticket;
      TicketData.AssignedUser = SelectedUser.value;

      var Ticket = await ApiService.PostAPI('Tickets/' + cryptr.encrypt(this.state.TicketID), TicketData);
   };

   handleChangeCheckBox = name => (event, checked) => {
      let Remark = Object.assign({}, this.state.Remark);    //creating copy of object
      Remark[name] = checked;                        //updating value
      this.setState({ Remark });
   };

   handleChangeTicketStatus = async SelectedStatus => {
      this.setState({ SelectedStatus });
      var TicketData = this.state.Ticket;
      TicketData.Status = SelectedStatus.value;

      var Ticket = await ApiService.PostAPI('Tickets/' + cryptr.encrypt(this.state.TicketID), TicketData);
   };

   handleChange = (event, key) => {
      let Remark = Object.assign({}, this.state.Remark);    //creating copy of object
      Remark[key] = event.target.value;                        //updating value
      this.setState({ Remark });
   }

   handleChangeEditor(value, key) {
      let Remark = this.state.Remark;
      Remark[key] = value;
      this.setState({ Remark });
   }

   async AddNewRemark() {
      let Remark = this.state.Remark;

      Remark.TicketId = this.state.TicketID;
      Remark.CommentDate = new Date();
      Remark.CommentorId = this.state.sessionUser.id != 0 ? this.state.sessionUser.id : 1;

      var SavedRemark = ApiService.PutWithoutmsgAPI('TicketDiscussions', Remark);

      if (SavedRemark) {
         await this.GetTicketDiscussion(this.state.TicketID);
         this.setState({
            Remark: {
               Comment: '',
               IsUrgentInput: false,
               IsDonotneedReply: false,
               CommentDate: '',
               CommentorId: 'Admin',
            }
         });
      }
   }

   createMarkup(value) {
      return { __html: value };
   }

   render() {
      const { Ticket, Remark, TicketDiscussions } = this.state;

      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };

      const columns = [
         // "View",
         {
            name: "Time",
            options: {
               filter: false,
               ccustomHeadRender: (columnMeta, updateDirection) => (
                  <th key={0} style={{ width: '20%' }} onClick={() => updateDirection(0)}><IntlMessages id="Time" />
                  </th>
               )
            }
         }, {
            name: "Who",
            options: {
               filter: true,
               sort: true,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={1} style={{ width: '20%' }} onClick={() => updateDirection(1)}><IntlMessages id="Who" />
                  </th>
               )
            }
         }, {
            name: "What",
            options: {
               filter: true,
               sort: true,
               customHeadRender: (columnMeta, updateDirection) => (
                  <th key={2} style={{ width: '30%' }} onClick={() => updateDirection(2)}><IntlMessages id="What" />
                  </th>
               )
            }
         }];
      const options = {
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,
         selectableRows: 'none',
         responsive: 'scrollFullHeight',
         textLabels: {
            body: {
               noMatch: this.context.intl.formatMessage({ id: 'Sorry_no_matching_records_found' }),
            }
         },
         rowsPerPage: 50,
         // serverSide: false,
         // onTableChange: (action, state) => setQuantitySubmitted(filteredQuantitySubmitted(state)),
         onCellClick: (colData, cellMeta) => {
            if (cellMeta.colIndex != 6) {
               this.props.history.push({
                  pathname: '/app/vulnerability/' + cryptr.encrypt(this.state.Vulnerabilities[cellMeta.dataIndex].id),
                  state: { history: this.props.location }
               })
            }
         }
      };

      return (
         <div className="formelements-wrapper">
            <Form ref={c => { this.form = c }}>
               <PageTitleBar title={'Ticket #' + Ticket.TicketCode} redirect={this.state.RedirectionURL} match={this.props.match} />
               <div className="row">
                  <div className="col-sm-12 col-md-12 col-xl-4 d-block">
                     {this.state.sessionUser.UserRole.RoleId == 2 ?
                        <div className="boxlayout">
                           <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'General_ticket_information' })}>
                              <div className="row">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15 d-flex">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><span><strong><IntlMessages id="Start_of_incident:" /></strong> {moment(Ticket.CreatedAt).format('HH:mm:ss')}</span></Label>
                                       <Label component="legend"><span><strong><IntlMessages id="Date_of_start:" /></strong> {moment(Ticket.CreatedAt).format('DD-MM-YYYY')}</span></Label>
                                    </FormControl>
                                 </div>
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><IntlMessages id="Priority_of_ticket" /></Label>
                                       <Select
                                          value={this.state.SelectedPriority}
                                          onChange={this.handleChangeTicketPriority}
                                          options={this.state.TicketPriorities}
                                       />
                                    </FormControl>
                                 </div>
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><IntlMessages id="Name_of_ticket" /></Label>
                                       <Input type="text" maxLength="250" className="form-control" value={Ticket.Subject} onChange={(e) => this.handleChange(e, 'Subject')} name="Subject" id="Subject" disabled placeholder={this.context.intl.formatMessage({ id: 'Subject' })} />
                                    </FormControl>
                                 </div>
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><IntlMessages id="Mark_the_status_as" /></Label>
                                       <Select
                                          value={this.state.SelectedStatus}
                                          onChange={this.handleChangeTicketStatus}
                                          options={this.state.TicketStatus}
                                       />
                                    </FormControl>
                                 </div>
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><IntlMessages id="People_assigned_to_this_ticket" /></Label>
                                       <Select
                                          value={this.state.SelectedUser}
                                          onChange={this.handleChangeAssignedUser}
                                          options={this.state.Users}
                                       />
                                    </FormControl>
                                 </div>
                              </div>
                           </RctCollapsibleCard>
                        </div>
                        :
                        <div className="boxlayout">
                           <RctCollapsibleCard heading={this.context.intl.formatMessage({ id: 'Ticket_information' })}>
                              <div className="row">
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <FormControl component="fieldset">
                                       <Label component="legend" className="d-flex clsticketcode">{Ticket.Subject}</Label>
                                    </FormControl>
                                 </div>
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15 d-flex">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><span><strong><IntlMessages id="Ticket_ref:" /></strong> #{Ticket.TicketCode}</span></Label>
                                       <Label component="legend"><span><strong><IntlMessages id="Category:" /></strong> Technical support</span></Label>
                                       <Label component="legend"><span><strong><IntlMessages id="Ticket_date:" /></strong> {moment(Ticket.CreatedAt).format('DD-MM-YYYY')}</span></Label>
                                       <Label component="legend"><span><strong><IntlMessages id="Priority:" /></strong> {EnumPriority[Ticket.Priority]}</span></Label>
                                       <Label component="legend"><span><strong><IntlMessages id="Agent:" /></strong> Akshay</span></Label>
                                    </FormControl>
                                 </div>
                                 <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                    <FormControl component="fieldset">
                                       <Label component="legend"><IntlMessages id="Change_the_ticket_status:" /></Label>
                                       <Select
                                          value={this.state.SelectedStatus}
                                          onChange={this.handleChangeTicketStatus}
                                          options={this.state.TicketStatus}
                                       />
                                    </FormControl>
                                 </div>
                              </div>
                           </RctCollapsibleCard>
                        </div>
                     }
                  </div>
                  <div className="col-sm-12 col-md-12 col-xl-8">
                     <div className="boxlayout">
                        <RctCollapsibleCard heading={<IntlMessages id="Add_your_remark" />}>
                           <div className="row">
                              <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                 <FormControl component="fieldset">
                                    <Label component="legend"><IntlMessages id="Please_add_a_clear_description_here" /></Label>
                                    <ReactQuill key={1} modules={modules} formats={formats} onChange={(value) => { this.handleChangeEditor(value, 'Comment') }} placeholder={this.context.intl.formatMessage({ id: 'Comment' })} />
                                 </FormControl>
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-sm-12 col-md-12 col-xl-7 p-0 pb-15">
                                 <FormControl component="fieldset">
                                    <Label component="legend"><IntlMessages id="If_needed,_please_add_your_attachments_(JPG/PNG_only!)" /></Label>
                                    <Col sm={12}>
                                       <Input type="file" accept="image/*" onChange={(e) => this.setState({ UploadFile: e.target.files })} name="file" id="File-1" />
                                    </Col>
                                 </FormControl>
                              </div>
                              <div className="col-sm-12 col-md-12 col-xl-5 p-0 pb-15">
                                 <FormControlLabel className="col-sm-12" control={
                                    <Checkbox color="primary" onChange={this.handleChangeCheckBox('IsUrgentInput')} checked={Remark.ischkUrgent} value="ischkUrgent" />
                                 } label={this.context.intl.formatMessage({
                                    id: "Make_my_input_as_urgent"
                                 })}
                                 />
                                 <FormControlLabel className="col-sm-12" control={
                                    <Checkbox color="primary" onChange={this.handleChangeCheckBox('IsDonotneedReply')} checked={Remark.dontneedrply} value="dontneedrply" />
                                 } label={this.context.intl.formatMessage({
                                    id: "I_don't_need_a_reply"
                                 })}
                                 />
                              </div>
                           </div>
                           <div className="row">
                              <div style={{ float: 'right', width: '100%' }}>
                                 <MatButton variant="contained" style={{ float: 'right' }} color="primary" onClick={() => this.AddNewRemark()} className="mr-10 mb-10 text-white"><IntlMessages id="Add_to_the_timeline" /></MatButton>
                              </div>
                           </div>
                        </RctCollapsibleCard>
                     </div>
                     <div className="boxlayout">
                        <RctCollapsibleCard heading={<IntlMessages id="Ticket_history" />}>
                           <div className="row">
                              <div className="col-sm-12 col-md-12 col-xl-12 p-0 pb-15">
                                 <MUIDataTable className="w-100"
                                    data={TicketDiscussions.map(rmrk => {
                                       return [
                                          rmrk.CommentDate,
                                          rmrk.CommentorId,
                                          <div dangerouslySetInnerHTML={this.createMarkup(rmrk.Comment)}></div>
                                       ]
                                    })}
                                    columns={columns}
                                    options={options}
                                 />
                              </div>
                           </div>
                        </RctCollapsibleCard>
                     </div>
                  </div>
               </div>
            </Form>
         </div >
      );
   }
}
export default ViewTicket;