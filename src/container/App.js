/**
 * App.js Layout Start Here
 */
import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import { connect } from 'react-redux';
import RctThemeProvider from './RctThemeProvider';
import HorizontalLayout from './HorizontalLayout';
import RctDefaultLayout from './DefaultLayout';
import { socketConnect } from 'socket.io-react';

import $ from 'jquery';

// async components
import {
	AsyncSessionLoginComponent,
	AsyncSession2FaLoginComponent,
	AsyncSessionRegisterComponent,
	AsyncSessionResetPasswordComponent,
	AsyncSessionLockScreenComponent,
	AsyncSessionForgotPasswordComponent,
	AsyncSessionPage404Component,
	AsyncSessionPage500Component,
	AsyncSessionPage401Component,
	AsyncTermsConditionComponent
} from 'Components/AsyncComponent/AsyncComponent';

//Auth0
import Auth from '../Auth/Auth';

// callback component
import Callback from "Components/Callback/Callback";

import * as SessionService from '../actions/SessionService';
import * as ApiService from '../actions/ApiServices';
import { ReactSlackChat } from 'react-slack-chat';
import {
	miniSidebarAction,
	darkModeAction
} from 'Actions';
import { Launcher } from 'react-chat-window';

//Auth0 Handle Authentication
const auth = new Auth();

const handleAuthentication = ({ location }) => {
	if (/access_token|id_token|error/.test(location.hash)) {
		auth.handleAuthentication();
	}
}

/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: Component, ...rest, authUser }) =>
	<Route
		{...rest}
		render={props =>
			authUser
				? <Component {...props} />
				: <Redirect
					to={{
						pathname: '/session/login',
						state: { from: props.location }
					}}
				/>}
	/>;

class App extends Component {

	state = {
		SlackChannels: [],
		IsChannelSync: false,
		messageList: [],
		IsAuthorized: false,
		IsSessionPage: false,
		ChatRoomId: 0,
		UserId: 0,
		UserName: 'RootSec'
	}

	async componentWillReceiveProps() {
		await this.checkSessionPages();
		await this.CheckUserAndUserRole();
		// await this.GetChatRoomMessages();
	}

	checkSessionPages() {
		var SessionPages = location.hash.toString().substring(2);
		SessionPages = SessionPages.split('/');
		console.log('location.pathname', SessionPages);
		if (SessionPages[0] == 'session') {
			this.setState({ IsSessionPage: true });
		} else {
			this.setState({ IsSessionPage: false });
		}
	}

	async componentDidMount() {
		console.log('location.pathname', location.pathname);
		await this.CheckUserAndUserRole();

		this.props.socket.on('Loadmessage', data => {
			console.log('message', data);
			var text = data.message;
			this.setState({
				messageList: [...this.state.messageList, {
					author: 'them',
					type: 'text',
					data: { text },
					userid: 1,
					ChatroomId: 2
				}]
			});
		});

		await this.AuthenticateIPAddress();
		// await this.GetChatRoomMessages();
		// console.log(this.props.socket.on('this'));
	}

	CheckUserAndUserRole() {
		var AuthorizedToken = SessionService.GetAuthTokenSession();

		if (AuthorizedToken && AuthorizedToken.length > 0) {
			this.setState({ IsAuthorized: true });
		} else {
			this.setState({ IsAuthorized: false });
		}
	}

	async GetChatRoomMessages() {

		var User = SessionService.GetUserSession();
		var UserId = User.id;
		var UserName = User.FirstName + ' ' + User.LastName;
		console.log('UserId', User.id);
		this.setState({ UserId, UserName });
		if (UserId) {
			var ChatRoomData = {
				Where: {
					CreatedBy: UserId,
					IsActive: 1
				}
			}
			var Chatrooms = await ApiService.PostWhereAPI('execute/withoutnestedconditions/Chatroom', ChatRoomData);
			console.log('Chatrooms', Chatrooms);
			if (Chatrooms && Chatrooms.length > 0) {
				this.setState({ ChatRoomId: Chatrooms[0].id });
			} else {

				var ChatRoomData = {
					RoomCode: UserName,
					ChatType: 1,
					CreatedBy: UserId
				}

				var chatRoom = await ApiService.PostWhereAPI('chatapi/AddChatRoom', ChatRoomData);

				if (chatRoom) {
					this.setState({ ChatRoomId: chatRoom.id });
				}
			}

			var MessageData = {
				Where: {
					ChatroomId: this.state.ChatRoomId,
					IsActive: 1
				}
			}

			var newMessageList = await ApiService.PostWhereAPI('execute/withoutnestedconditions/ChatMessages', MessageData);

			if (newMessageList && newMessageList.length > 0) {
				var messageList = [];
				newMessageList.forEach(msg => {
					let text = msg.Message;
					messageList.push({
						author: msg.SendBy == UserId ? 'me' : 'them',
						type: msg.MessageType == 1 ? 'text' : 'file',
						data: { text },
						userid: msg.SendBy,
						ChatroomId: this.state.ChatRoomId
					});
				});

				this.setState({ messageList });
			}

		}
	}

	_onMessageWasSent(message) {

		var newmsg = {
			author: message.author,
			type: message.type,
			data: message.data,
			userid: this.state.UserId,
			ChatroomId: this.state.ChatRoomId
		}
		this.props.socket.emit('Sendmessage', 'Test', newmsg);
		this.setState({
			messageList: [...this.state.messageList, newmsg]
		});
		console.log('testtt');
	}

	async _onFilesSelected(filelist) {
		console.log('filelist', filelist);
		var Test = [];

		for (var i = 0; i <= filelist.length - 1; i++) {
			console.log('i', i);
			Test.push({
				author: 'me',
				type: 'file',
				data: {
					url: 'somefile.mp3',
					fileName: filelist[i].name
				}
			});
		}

		await this.setState({
			messageList: [...this.state.messageList, ...Test]
		});
	}

	_sendMessage(text) {
		if (text.length > 0) {
			this.setState({
				messageList: [...this.state.messageList, {
					author: 'them',
					type: 'text',
					data: { text }
				}]
			});
		}
	}

	darkModeHanlder() {
		var DarkMode = localStorage.getItem('dark-mode');
		if (DarkMode && parseInt(DarkMode) == 1) {
			$('body').addClass('dark-mode');
			this.props.darkModeAction(true);
		} else {
			$('body').removeClass('dark-mode');
			this.props.darkModeAction(false);
		}
	}

	async GetChannels() {
		var slackToken = 'xoxb-722783103460-723279329632-SVpPQaUX3i9VXpSnd7A5FB1j';
		var SlackChannels = await ApiService.GetWithSlackAPI(slackToken);
		this.setState({ SlackChannels, IsChannelSync: true });
	}

	async AuthenticateIPAddress() {
		var AuthenticateIP = await ApiService.AuthGetAPI('IPCheck');
		if (AuthenticateIP && !AuthenticateIP.Authentication) {
			this.props.history.push('/session/401');
		}
	}

	render() {
		const { SlackChannels, IsChannelSync, IsAuthorized, IsSessionPage } = this.state;
		const { location, match, user } = this.props;
		var UserRole = SessionService.GetUserRole();
		this.darkModeHanlder();
		if (!IsChannelSync) {
			// this.GetChannels();
		}
		if (location.pathname === '/') {
			if (!UserRole) {
				return (<Redirect to={'/session/login'} />);
			} else if (UserRole.Name == "ClientUser") {
				return (<Redirect to={'/company/clientdashboard'} />);
			} else {
				return (<Redirect to={'/app/dashboard'} />);
			}

			// if (user === null) {
			// 	return (<Redirect to={'/session/login'} />);
			// } else {
			// 	return (<Redirect to={'/app/dashboard'} />);
			// }
		}

		if (UserRole && UserRole.Name == "ClientUser") {
			var pathname = location.pathname.split("/");
			console.log('pathname', pathname);
			if (pathname && pathname.length > 0) {
				if (pathname[1].toString().trim().toLowerCase() == "app") {
					// return (<Redirect to={'/session/login'} />);
					this.props.history.push('/session/login');
				}
			}
		}

		// const getClientID = this.state.ip || this.getIP();
		const getClientAvatar = `https://robohash.org/123`;

		const slackChatProps = {
			botName: 'Akshay',
			// channels: [
			// 	{
			// 		name: "on-errors-and-vulnerabilities",
			// 		icon: "https://image.flaticon.com/icons/svg/141/141021.svg"
			// 	},
			// 	{
			// 		name: "random",
			// 		icon: "https://image.flaticon.com/icons/svg/141/141021.svg"
			// 	}
			// ],
			channels: SlackChannels,
			apiToken: "eG94Yi03MjI3ODMxMDM0NjAtNzIzMjc5MzI5NjMyLVNWcFBRYVVYM2k5VlhwU25kN0E1RkIxag==",
			helpText: "RootDash Slack",
			themeColor: "#856090",
			debugMode: true,
			userImage: getClientAvatar,
			hooks: [
				{
					/* My Custom Hook */
					id: "getSystemInfo",
					action: () => "MY SYSTEM INFO!"
				},
				{
					id: 'showHelpWizard',
					action: () => {
						return dispatch(showHelpWizard(true))
							.then(data).fail(uILogger.error('FAILED'))
					}
				},
				{
					id: "getScreenshot",
					action: () => "$=>@Slackbot:getScreenshot"
				}
			]
		};

		return (
			<RctThemeProvider>
				<NotificationContainer />
				<InitialPath
					path={`${match.url}app`}
					authUser={SessionService.GetUserSession()}
					component={RctDefaultLayout}
				/>
				<Route path="/company" component={HorizontalLayout} />
				<Route path="/session/login" component={AsyncSessionLoginComponent} />
				<Route path="/session/register" component={AsyncSessionRegisterComponent} />
				<Route path="/session/resetpassword" component={AsyncSessionResetPasswordComponent} />
				<Route path="/session/lock-screen" component={AsyncSessionLockScreenComponent} />
				<Route
					path="/session/forgot-password"
					component={AsyncSessionForgotPasswordComponent}
				/>
				<Route path="/session/404" component={AsyncSessionPage404Component} />
				<Route path="/session/500" component={AsyncSessionPage500Component} />
				<Route path="/session/401" component={AsyncSessionPage401Component} />
				<Route path="/terms-condition" component={AsyncTermsConditionComponent} />
				<Route path="/callback" render={(props) => {
					handleAuthentication(props);
					return <Callback {...props} />
				}} />

				{/* <ReactSlackChat {...slackChatProps} /> */}
				{/* {!IsSessionPage && IsAuthorized ?
					<Launcher
						agentProfile={{
							teamName: 'ROOTDASH HELP DESK',
							imageUrl: require('Assets/img/appLogo.png')
						}}
						onMessageWasSent={this._onMessageWasSent.bind(this)}
						messageList={this.state.messageList}
						onFilesSelected={this._onFilesSelected.bind(this)}
						showEmoji
					/> : ''} */}
			</RctThemeProvider>
		);
	}
}

// map state to props
const mapStateToProps = ({ settings }) => {
	return settings;
};

export default connect(mapStateToProps, {
	darkModeAction
})(socketConnect(App));
