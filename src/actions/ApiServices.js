import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import * as SessionService from './SessionService';
import { intlShape } from 'react-intl';
var fileDownload = require('js-file-download');


var AuthURL = 'http://192.168.1.167:9090/Auth/';
var APIURL = 'http://192.168.1.167:9090/api/';
// var AuthURL = 'http://180.211.103.170:9090/Auth/';
// var APIURL = 'http://180.211.103.170:9090/api/';
// var AuthURL = 'http://167.71.75.141:3000/Auth/';
// var APIURL = 'http://167.71.75.141:3000/api/';
// var AuthURL = 'https://api.rootdash.nl/Auth/';
// var APIURL = 'https://api.rootdash.nl/api/';

function handleErrorObservable(error) {
   console.log('error', error);
   // NotificationManager.error(error.message);
   localStorage.removeItem('dark-mode');
   // window.location.replace('/rootsec/web/#/session/login');
   // setTimeout(() => {
   //    window.location.reload(true);
   // }, 1500);
   var response = error.message || error;
   let responseJson = { is_error: true, message: response };
   return responseJson;
}

function RootsecHeaders() {
   let AuthToken = SessionService.GetAuthTokenSession();
   var lang = 'nl';
   lang = localStorage.getItem('langlocale');

   if (!lang) {
      lang = 'nl';
   }

   if (AuthToken) {
      let headers = {
         'Content-Type': 'application/json',
         'Authorization': 'Bearer ' + AuthToken,
         'lang': lang
      }
      return headers;
   } else {
      return null;
   }
}

export const CheckUserRole = (props) => {
   var userRole = SessionService.GetUserRole('userRole');
   if (userRole) {
      if (userRole.Name == "ClientUser") {
         props.history.push('/company/clientdashboard');
      }
   }
}

function RootsecDownloadHeaders() {
   let AuthToken = SessionService.GetAuthTokenSession();
   if (AuthToken) {
      let headers = {
         'responseType': 'blob',
         // 'Content-Disposition': 'attachment;filename=report.pdf',
         'Content-Type': 'application/pdf',
         'Authorization': 'Bearer ' + AuthToken
      }
      return headers;
   } else {
      return null;
   }
}

export const ConfigAPIURL = async () => {
   return APIURL;
}

export const GetAPI = async (URL, props) => {
   try {
      const response = await axios.get(APIURL + URL, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const GetWithOnlyURLAPI = async (URL) => {
   try {
      const response = await axios.get(URL, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const GetWithSlackAPI = async (token) => {
   try {
      const response = await axios.get('https://slack.com/api/channels.list?token=' + token, { headers: RootsecHeaders() });
      if (response.data.ok) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.channels;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const GetWithoutmsgAPI = async (URL) => {
   try {
      const response = await axios.get(APIURL + URL, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         // NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const AuthLoginPostAPI = async (URL, Data) => {
   try {
      const response = await axios.post(AuthURL + URL, Data);
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
         return response.data;
      }
      else {
         NotificationManager.error(response.data.message);
         return response.data;
      }
   }
   catch (error) {
      NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const AuthPostAPI = async (URL, Data) => {
   try {
      const response = await axios.post(AuthURL + URL, Data);
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
         return response.data;
      }
      else {
         // NotificationManager.error(response.data.message);
         return response.data;
      }
   }
   catch (error) {
      NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const AuthGetAPI = async (URL) => {
   try {
      const response = await axios.get(AuthURL + URL, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const AuthWithoutmsgPostAPI = async (URL, Data) => {
   try {
      const response = await axios.post(AuthURL + URL, Data);
      if (response.data.status == 1) {
         //   NotificationManager.success(response.data.message);
         return response.data.data;
      }
      else {
         NotificationManager.error(response.data.message);
         return response.data.data;
      }
   }
   catch (error) {
      NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const DownloadAPI = async (URL, ID) => {
   try {
      // const response = await axios.get(APIURL + URL, { headers: RootsecDownloadHeaders() });
      const url = APIURL + URL;
      const link = document.createElement('a');
      link.href = url;
      // link.target = '_blank';
      link.setAttribute('download', 'Penetratierapport_' + ID + '.pdf');
      document.body.appendChild(link);
      link.click();
      // fileDownload(response.data, 'Penetratierapport_90.pdf');
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const DownloadImage = async (URL, filename) => {
   try {
      const url = APIURL + URL;
      const link = document.createElement('a');
      link.href = url;
      // link.target = '_blank';
      link.setAttribute('download', filename);
      document.body.appendChild(link);
      link.click();
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const PostWhereAPI = async (URL, Data) => {
   try {
      const response = await axios.post(APIURL + URL, Data, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const PostAPI = async (URL, Data) => {
   try {
      const response = await axios.post(APIURL + URL, Data, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const UploadExcelAPI = async (Data) => {
   try {
      const response = await axios.post(APIURL + 'bulkimportapi/importbulkdata', Data, {
         headers: {
            'Content-Type': 'multipart/form-data'
         }
      });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      // handleErrorObservable(error);
   }
}

export const UploadAPI = async (Data) => {
   try {
      const response = await axios.post(APIURL + 'uploadapi/imageupload', Data, {
         headers: {
            'Content-Type': 'multipart/form-data'
         }
      });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      // handleErrorObservable(error);
   }
}

export const SignatureUploadAPI = async (Data) => {
   try {
      const response = await axios.post(APIURL + 'uploadapi/signatureupload', Data, {
         headers: {
            'Content-Type': 'multipart/form-data'
         }
      });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      // handleErrorObservable(error);
   }
}

export const PutWithoutmsgAPI = async (URL, Data) => {
   try {
      const response = await axios.put(APIURL + URL, Data, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const PutAPI = async (URL, Data) => {
   try {
      const response = await axios.put(APIURL + URL, Data, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response.data.data;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const DeleteAPI = async (URL) => {
   try {
      const response = await axios.delete(APIURL + 'Soft/' + URL, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         NotificationManager.success(response.data.message);
      }
      else {
         NotificationManager.error(response.data.message);
      }
      return response;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}

export const HardDeleteAPI = async (URL) => {
   try {
      const response = await axios.delete(APIURL + URL, { headers: RootsecHeaders() });
      if (response.data.status == 1) {
         // NotificationManager.success(response.data.message);
      }
      else {
         // NotificationManager.error(response.data.message);
      }
      return response;
   }
   catch (error) {
      // NotificationManager.error(error.message);
      handleErrorObservable(error);
   }
}