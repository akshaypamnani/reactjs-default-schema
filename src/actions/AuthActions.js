import * as ApiService from './ApiServices';

export const signinUser = (user, history) => async () => {
      var LoginUser = await ApiService.AuthLoginPostAPI('login', user);
      return LoginUser;
}

export const Get2FACode = (user, history) => async () => {
      var TwoFA = await ApiService.AuthPostAPI('GenerateTwoFA', user);
      return TwoFA;
}

export const signinUserInFirebase = (user, history) => (dispatch) => {
      // dispatch({ type: LOGIN_USER });
      // firebase.auth()
      //       .signInWithEmailAndPassword(user.email, user.password)
      //       .then((user) => {
      //             localStorage.setItem("user_id", "user-id");
      //             dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
      //             history.push('/');
      //             NotificationManager.success('User Login Successfully!');
      //       })
      //       .catch((error) => {
      //             dispatch({ type: LOGIN_USER_FAILURE });
      //             NotificationManager.error(error.message);
      //       });
}

// export const signinUserInFirebaseCompany = (user, history) => (dispatch) => {
//       dispatch({ type: LOGIN_USER });
//       firebase.auth()
//             .signInWithEmailAndPassword(user.email, user.password)
//             .then((user) => {
//                   localStorage.setItem("user_id", "user-id");
//                   dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//                   history.push('/horizontal/dashboard');
//                   NotificationManager.success('User Login Successfully!');
//             })
//             .catch((error) => {
//                   dispatch({ type: LOGIN_USER_FAILURE });
//                   NotificationManager.error(error.message);
//             });
// }