import * as ApiService from './ApiServices';
/**
 * Translation Actions
 */
export const SaveTranslations = async (LanguageTranslations, FieldsArray, pagerefid, pagerefname, referenceid, IdforUpdate, langLocale) => {
   let Translations = [];
   if (LanguageTranslations && LanguageTranslations.length > 0) {
      Translations = LanguageTranslations;
   }

   if (FieldsArray && FieldsArray.length > 0) {
      FieldsArray.forEach(field => {
         var objfield = Translations.filter(x => x.DefaultKey == field.columnName)[0];
         if (!objfield) {
            Translations.push({
               PageRefId: pagerefid,
               PageRefName: pagerefname,
               ReferenceId: referenceid,
               DefaultKey: field.columnName,
               Title: field.Title
            });
         } else {
            objfield.Title = field.Title;
         }
      });
   }

   var LanguageTranslationsData = {
      Translations: Translations,
      IsUpdated: IdforUpdate,
      lang: langLocale
   }

   var LanguageTranslations = await ApiService.PostWhereAPI('languageapi/AddTranslations', LanguageTranslationsData);

}

export const CreateUpdatechangeLanguage = (objModel, FieldsArray, langLocale) => {
   if (objModel.LanguageTranslations && objModel.LanguageTranslations.length > 0) {
      FieldsArray.forEach(field => {
         var objfield = objModel.LanguageTranslations.filter(x => x.DefaultKey == field)[0];

         if (objfield) {
            if (langLocale == 'nl-NL') {
               objModel[field] = objfield.TitleNL && objfield.TitleNL.length > 0 ? objfield.TitleNL : objModel[field];
            } else {
               objModel[field] = objfield.TitleEN && objfield.TitleEN.length > 0 ? objfield.TitleEN : objModel[field];
            }
         }
      });
   }
   return objModel;
}

export const GetTranslatedText = (translatedData, field, value, langLocale) => {
   let TranslatedValue = value;
   if (translatedData && translatedData.length > 0) {
      var TranslatedArr = translatedData.filter(x => x.DefaultKey == field);
      if (TranslatedArr && TranslatedArr.length > 0) {
         if (langLocale == 'nl-NL') {
            TranslatedValue = TranslatedArr[0].TitleNL && TranslatedArr[0].TitleNL.length > 0 ? TranslatedArr[0].TitleNL : TranslatedValue;
         } else {
            TranslatedValue = TranslatedArr[0].TitleEN && TranslatedArr[0].TitleEN.length > 0 ? TranslatedArr[0].TitleEN : TranslatedValue;
         }
      }
   }
   return TranslatedValue;
}

