/**
* Main App
*/
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, HashRouter } from 'react-router-dom';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import './lib/reactifyCss';
import App from './container/App';
import { configureStore } from './store';
import io from 'socket.io-client';
import { SocketProvider } from 'socket.io-react';

var options = {
	rememberUpgrade: true,
	// transports: ['websocket'],
	secure: true,
	rejectUnauthorized: false
}

const socket = io.connect('http://192.168.1.167:9090/', options);
socket.on('message', msg => console.log(msg));

const MainApp = () => (
	<SocketProvider socket={socket}>
		<Provider store={configureStore()}>
			<MuiPickersUtilsProvider utils={MomentUtils}>
				<HashRouter>
					<Switch>
						<Route path="/" component={App} />
					</Switch>
				</HashRouter>
			</MuiPickersUtilsProvider>
		</Provider>
	</SocketProvider>
);

export default MainApp;
