/**
 * App Header
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import screenfull from 'screenfull';
import Tooltip from '@material-ui/core/Tooltip';
import MenuIcon from '@material-ui/icons/Menu';
import { withRouter } from 'react-router-dom';
import $ from 'jquery';
import * as ApiService from '../../actions/ApiServices';

// actions
import { collapsedSidebarAction } from 'Actions';
import { logoutUserFromFirebase } from 'Actions';

import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Button from '@material-ui/core/Button';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import * as SessionService from '../../actions/SessionService';
import { Badge } from '@material-ui/core';
import LanguageProvider from './LanguageProvider';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');
const requestIp = require('request-ip');

const ipMiddleware = function (req, res, next) {
   const clientIp = requestIp.getClientIp(req);
   return clientIp;
};

class Header extends Component {

   state = {
      customizer: false,
      isMobileSearchFormVisible: false,
      UserRole: null,
      NotificationPentestCount: 0,
      IsDarkMode: false,
      IsShowDashboard: true
   }

   async componentDidMount() {

      // var addScript = document.createElement('script');
      // addScript.setAttribute('src', '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit');
      // document.body.appendChild(addScript);
      // window.googleTranslateElementInit = this.googleTranslateElementInit.bind(this);

      await this.CheckPentests();
      this.Getdarkmode();
      var userRole = SessionService.GetUserRole('userRole');
      if (userRole) {
         await this.setState({ UserRole: userRole });
         // await this.GetCurrentIp();
         // if (userRole.Name == "ClientUser") {
         //    await this.GetNotificationPentestList();
         // }
         await this.GetNotificationPentestList();
      }
   }

   googleTranslateElementInit() {
      //alert("test2")
      /* eslint-disable no-new */
      new window.google.translate.TranslateElement({ pageLanguage: 'en', layout: window.google.translate.TranslateElement.FloatPosition.TOP_LEFT }, 'google_translate_element')
   }

   changeLang(langcode) {
      window.googleTranslateElementInit = new window.google.translate.TranslateElement({ pageLanguage: langcode, layout: window.google.translate.TranslateElement.FloatPosition.TOP_LEFT }, 'google_translate_element');
   }

   Getdarkmode() {
      var DarkMode = localStorage.getItem('dark-mode');
      if (DarkMode && parseInt(DarkMode) == 1) {
         this.setState({ IsDarkMode: true });
      } else {
         this.setState({ IsDarkMode: false });
      }
   }

   async GetCurrentIp() {
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var CurrentIp = await ApiService.GetWithOnlyURLAPI('http://smart-ip.net/geoip-json');
      }
   }

   async CheckPentests() {
      this.setState({ loading: true });
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var CheckPentests = await ApiService.GetAPI('Dashboard/CheckPentests/' + cryptr.encrypt(UserID));

         if (CheckPentests.length > 0) {
            await this.setState({ IsShowDashboard: true });
         } else {
            await this.setState({ IsShowDashboard: false });
         }

         this.setState({ loading: false });
      }
   }

   async GetNotificationPentestList() {
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var NotificationPentests = await ApiService.GetAPI('Notificationapi/GetNotifications/' + cryptr.encrypt(UserID));
         if (NotificationPentests && NotificationPentests.length > 0) {
            NotificationPentests = NotificationPentests.filter(x => x.IsRead == 0);
         }
         this.setState({ NotificationPentestCount: NotificationPentests.length });
         this.setState({ loading: false });
      }
   }

   async UpdateReadNotifications() {
      this.setState({ NotificationPentestCount: 0 });
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var NotificationPentests = await ApiService.PostWhereAPI('Notificationapi/ReadNotifications/' + cryptr.encrypt(UserID));
      }
   }

   // function to change the state of collapsed sidebar
   onToggleNavCollapsed = (event) => {
      const val = !this.props.navCollapsed;
      this.props.collapsedSidebarAction(val);
   }

   // open dashboard overlay
   openDashboardOverlay() {
      $('.dashboard-overlay').toggleClass('d-none');
      $('.dashboard-overlay').toggleClass('show');
      if ($('.dashboard-overlay').hasClass('show')) {
         $('body').css('overflow', 'hidden');
      } else {
         $('body').css('overflow', '');
      }
   }

   // close dashboard overlay
   closeDashboardOverlay() {
      $('.dashboard-overlay').removeClass('show');
      $('.dashboard-overlay').addClass('d-none');
      $('body').css('overflow', '');
   }

   // toggle screen full
   toggleScreenFull() {
      screenfull.toggle();
   }

   // mobile search form
   openMobileSearchForm() {
      this.setState({ isMobileSearchFormVisible: true });
   }

   async logoutUser() {
      var UserLogout = await ApiService.GetWithoutmsgAPI('userapi/logout');
      localStorage.removeItem('dark-mode');
      this.props.history.push('/session/login');
   }

   GotoProfile() {
      if (this.state.UserRole) {
         if (this.state.UserRole.Name == "ClientUser") {
            this.props.history.push('/company/clientprofile');
         } else {
            this.props.history.push('/app/profile');
         }
      } else {
         this.props.history.push('/session/login');
      }
   }

   GotoNotifications() {
      if (this.state.UserRole) {
         this.UpdateReadNotifications();
         if (this.state.UserRole.Name == "ClientUser") {
            this.props.history.push('/company/notifications');
         } else {
            this.props.history.push('/app/notifications');
         }
      } else {
         this.props.history.push('/session/login');
      }
   }
   GotoDashBoard() {
      if (this.state.UserRole) {
         if (this.state.UserRole.Name == "ClientUser") {
            this.props.history.push('/company/clientdashboard');
         } else {
            this.props.history.push('/app/dashboard');
         }
      } else {
         this.props.history.push('/session/login');
      }
   }
   GotoCompany() {
      this.props.history.push('/app/managecompany');
   }
   GotoPentest() {
      this.props.history.push('/app/managepentest');
   }
   GotoCompanyPentest() {
      this.props.history.push('/company/companyassessments');
   }
   GotoCompanyVulnerabilities() {
      this.props.history.push('/company/companyvulnerabilities');
   }

   render() {
      const { isMobileSearchFormVisible, NotificationPentestCount, IsDarkMode, IsShowDashboard } = this.state;
      $('body').click(function () {
         $('.dashboard-overlay').removeClass('show');
         $('.dashboard-overlay').addClass('d-none');
         $('body').css('overflow', '');
      });
      const { horizontalMenu, agencyMenu, location, } = this.props;
      return (
         <div>
            {/* <div id="google_translate_element"></div> */}
            <AppBar position="static" className="rct-header cl-header">
               <Toolbar className="d-flex flex-wrap flex-sm-nowrap justify-content-between w-100 pl-0">
                  <div className="d-flex flex-wrap align-items-center">
                     {(horizontalMenu || agencyMenu) &&
                        <div className="site-logo">
                           {/* <Link to="/" className="logo-mini">
                           <img src={require('Assets/img/appLogo.png')} className="mr-15" alt="site logo" width="35" height="35" />
                        </Link> */}
                           {IsDarkMode ?
                              <Link to="/" className="logo-normal">
                                 <img src={require('Assets/img/appLogoText.png')} className="img-fluid" alt="site-logo" width="110" />
                              </Link>
                              :
                              <Link to="/" className="logo-normal">
                                 <img src={require('Assets/img/appLogoTextBlack.png')} className="img-fluid" alt="site-logo" width="110" />
                              </Link>
                           }

                        </div>
                     }

                     {(horizontalMenu || agencyMenu) &&
                        <ul className="list-inline mb-0 navbar-left clsclientDB">
                           {IsShowDashboard ?
                              <li className="list-inline-item">
                                 <Button color="inherit" mini="true" aria-label="Menu" onClick={() => this.GotoDashBoard()} className="humburger appheader" style={{ display: 'unset' }}>
                                    <IntlMessages id="Dashboard" />
                                 </Button>
                              </li>
                              : ''}
                           <li className="list-inline-item">
                              <Button color="inherit" mini="true" aria-label="Menu" onClick={() => this.GotoCompanyVulnerabilities()} className="humburger appheader" style={{ display: 'unset' }}>
                                 <IntlMessages id="All_Vulnerabilities" />
                              </Button>
                           </li>
                           <li className="list-inline-item">
                              <Button color="inherit" mini="true" onClick={() => this.GotoCompanyPentest()} aria-label="Menu" className="humburger appheader" style={{ display: 'unset' }}>
                                 <IntlMessages id="Assessments" />
                              </Button>
                           </li>
                        </ul>
                     }
                     <ul className="list-inline mb-0 navbar-left">
                        <li className="list-inline-item" onClick={(e) => this.onToggleNavCollapsed(e)}>
                           <IconButton color="inherit" mini="true" aria-label="Menu" className="humburger p-0">
                              <MenuIcon />
                           </IconButton>
                        </li>
                        <li className="list-inline-item">
                           <Button color="inherit" mini="true" aria-label="Menu" onClick={() => this.GotoDashBoard()} className="humburger appheader">
                              <IntlMessages id="Dashboard" />
                           </Button>
                        </li>
                        <li className="list-inline-item">
                           <Button color="inherit" mini="true" onClick={() => this.GotoCompany()} aria-label="Menu" className="humburger appheader">
                              <IntlMessages id="Companies" />
                           </Button>
                        </li>
                        <li className="list-inline-item">
                           <Button color="inherit" mini="true" aria-label="Menu" onClick={() => this.GotoPentest()} className="humburger appheader">
                              <IntlMessages id="Pentests" />
                           </Button>
                        </li>
                     </ul>
                  </div>
                  <div className="ml-auto">
                     <ul className="w-auto ml-auto list-inline navbar-right mb-0 mt-2">
                        {/* <Notifications /> */}

                        <LanguageProvider />
                        <a href="javascript:void(0)" onClick={() => this.GotoNotifications()}>
                           <IconButton aria-label="bell" className="rmvHover" style={{ paddingTop: '5px' }}>
                              <i className="zmdi zmdi-notifications-active"></i>
                              {NotificationPentestCount != 0 ? <Badge color="danger" className="badge-xs badge-top-right rct-notify">{NotificationPentestCount}</Badge> : ''}
                           </IconButton>
                        </a>
                        <UncontrolledDropdown nav className="list-inline-item vr-super">
                           <DropdownToggle nav className="text-black">
                              <span className="mr-10">
                                 <img src={require('Assets/avatars/user.svg')} alt="user profile" className="img-fluid rounded-circle" width="25" />
                              </span>
                              {/* Lucile Beck */}
                           </DropdownToggle>
                           <DropdownMenu style={{ marginLeft: '-60px' }}>
                              <DropdownItem>
                                 <a href="javascript:void(0)" onClick={() => this.GotoProfile()}>
                                    <i className="zmdi zmdi-account text-primary mr-3"></i>
                                    <IntlMessages id="Profile" />
                                 </a>
                              </DropdownItem>
                              <DropdownItem>
                                 <a href="javascript:void(0)" onClick={() => this.logoutUser()}>
                                    <i className="zmdi zmdi-power text-danger mr-3"></i>
                                    <IntlMessages id="Log_out" />
                                 </a>
                              </DropdownItem>
                           </DropdownMenu>
                        </UncontrolledDropdown>
                     </ul>
                  </div>
                  <Drawer
                     anchor={'right'}
                     open={this.state.customizer}
                     onClose={() => this.setState({ customizer: false })}
                  >
                  </Drawer>
               </Toolbar>
               {/* <DashboardOverlay
               onClose={() => this.closeDashboardOverlay()}
            /> */}
            </AppBar>
         </div>
      );
   }
}

// map state to props
const mapStateToProps = ({ settings }) => {
   return settings;
};

export default withRouter(connect(mapStateToProps, {
   logoutUserFromFirebase,
   collapsedSidebarAction
})(Header));
