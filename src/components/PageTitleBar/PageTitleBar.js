/**
 * Page Title Bar Component
 * Used To Display Page Title & Breadcrumbs
 */
import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';

// intl messages
import IntlMessages from 'Util/IntlMessages';

let Pagetitles = {
    "Rootsec Employee" : '/app/userrootsecmanagement',
    "clientuser" : '/app/userclientmanagement',
    "Company" : '/app/managecompany',
    "Pentest" : '/app/managepentest',
}

// get display string
const getDisplayString = (sub) => {
    const arr = sub.split("-");
    if (sub != ":id") {
        if (arr.length > 1) {
            return <IntlMessages id={`breadcrumb_${arr[0].charAt(0) + arr[0].slice(1) + arr[1].charAt(0).toUpperCase() + arr[1].slice(1)}`} />
        } else {
            return <IntlMessages id={`breadcrumb_${sub.charAt(0) + sub.slice(1)}`} />
        }
    }
};

const GetToRedirect = (title) => {
   return Pagetitles[title];
}

// get url string
const getUrlString = (path, sub, index) => {
    if (index === 0) {
        return '/';
    } else {
        return path.split(sub)[0] + sub;
    }
};

const PageTitleBar = ({ title, match, enableBreadCrumb, redirect }) => {
    const path = match.path.substr(1);
    const subPath = path.split('/');
    return (
        <div className="page-title d-flex flex-md-nowrap flex-wrap justify-content-between align-items-center">
            {title &&
                <div className="page-title-wrap" onClick={() => window.history.back()}>
                    <i className="ti-angle-left"></i>
                    <h2 className="">{title}</h2>
                </div>
            }
            {enableBreadCrumb &&
                <Breadcrumb className="mb-md-0 mb-3 tour-step-7 breadcrumbs" tag="nav">
                    {subPath.map((sub, index) => {
                        
                        return <BreadcrumbItem active={subPath.length === index + 1}
                            tag={subPath.length === index + 1 ? "span" : Link} key={index}
                            to={redirect}
                            >{getDisplayString(sub)}</BreadcrumbItem>
                    }
                    )}
                </Breadcrumb>
            }
        </div>
    )
};

// default props value
PageTitleBar.defaultProps = {
    enableBreadCrumb: true
}

export default PageTitleBar;
