// horizontal nav links
export default {
   category1: [
      {
         "menu_title": "Dashboard",
         "menu_icon": "zmdi zmdi-view-dashboard",
         "path": "#/company/clientdashboard",
         "child_routes": null
      },
      {
         "menu_title": "Pentests",
         "menu_icon": "zmdi zmdi-email",
         "path": "#/company/companyassessments",
         "child_routes": null
      },
      {
         "menu_title": "Vulnerabilities",
         "menu_icon": "zmdi zmdi-email",
         "path": "#/company/companyvulnerabilities",
         "child_routes": null
      }
   ]
}
