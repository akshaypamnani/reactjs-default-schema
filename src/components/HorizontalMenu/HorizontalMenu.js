/**
 * Horizontal Menu
 */
import React, { Component } from 'react';

import IntlMessages from 'Util/IntlMessages';

import navLinks from './NavLinks';

import NavMenuItem from './NavMenuItem';

class HorizontalMenu extends Component {
    render() {
        return (
            <div className="horizontal-menu">
                <ul className="list-unstyled nav">
                    {navLinks.category1.map((menu) => (
                        <li className="nav-item">
                            <a href={menu.path} className="nav-link">
                                <i className={menu.menu_icon}></i>
                                <span className="menu-title"><IntlMessages id={menu.menu_title} /></span>
                            </a>
                        </li>
                    ))}
                </ul>
                {/* <ul className="list-unstyled nav">
                    {navLinks.category2.map((menu) => (
                        <li className="nav-item">
                            <a href={menu.path} className="nav-link">
                                <i className={menu.menu_icon}></i>
                                <span className="menu-title"><IntlMessages id={menu.menu_title} /></span>
                            </a>
                        </li>
                    ))}
                </ul> */}
            </div>
        );
    }
}

export default HorizontalMenu;
