// Date Time Picker
import React, { Component } from 'react';
import { DateTimePicker } from 'material-ui-pickers';

export default class DefaultDateAndTimePickers extends Component {

	state = {
		selectedDate: new Date(),
	};

	handleDateChange = (date) => {
		this.setState({ selectedDate: date });
	};

	render() {
		const { selectedDate } = this.state;
		return (
			<div className="rct-picker">
				<DateTimePicker
				onChange={this.handleChange}
    showTimeSelect
    timeFormat="HH:mm"
    timeIntervals={15}
    dateFormat="MMMM d, yyyy h:mm aa"
    timeCaption="time"

				/>
			</div>
		)
	}
}
