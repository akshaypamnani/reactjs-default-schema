/**
 * Nav Menu Item
 */
import React, { Fragment, Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Collapse from '@material-ui/core/Collapse';
import { Link, NavLink } from 'react-router-dom';
import classNames from 'classnames';

// intl messages
import IntlMessages from 'Util/IntlMessages';

//API Services
import * as ApiService from '../../actions/ApiServices';
// import { Link } from '@material-ui/core';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

class HomeNavItem extends Component {

	state = {
		subMenuOpen: false,
		Top5Companies: [],
		logoBaseURL: ''
	}

	/**
	 * On Toggle Collapse Menu
	 */
	onToggleCollapseMenu() {
		this.setState({ subMenuOpen: !this.state.subMenuOpen });
	}

	async componentDidMount() {
		this.setState({ loading: true });
		const logoBaseURL = await ApiService.ConfigAPIURL();
		await this.setState({ logoBaseURL });
		await this.GetTop5Companies();
		this.setState({ loading: false });
	}

	async GetTop5Companies() {
		var Top5CompaniesData = {
			Where: {
				IsActive: 1
			}
		}
		var Top5Companies = await ApiService.GetAPI('Dashboard/TopFiveCompanies');
		// var Top5Companies = await ApiService.PostWhereAPI('execute/conditions/Company', Top5CompaniesData);
		Top5Companies = Top5Companies.slice(0, 5);
		this.setState({ Top5Companies: Top5Companies });
	}

	render() {
		const { menu, onToggleMenu } = this.props;
		const { subMenuOpen, Top5Companies } = this.state;
		if (menu.menu_title == 'Dashboard' && menu.child_routes != null) {
			return (
				<Fragment>
					<ListItem button component="li" onClick={onToggleMenu} className={`list-item ${classNames({ 'item-active': menu.open })}`}>
						<ListItemIcon className="menu-icon">
							{/* <i className={menu.menu_icon}></i> */}
							<img src={menu.menu_icon} style={{
								top: '-1px',
								left: '-2px',
								position: 'relative'
							}}></img>
						</ListItemIcon>
						<span className="menu" style={{ position: 'relative', left: '-2px' }}>
							<IntlMessages id={menu.menu_title} />
						</span>
					</ListItem>
					<Collapse in={menu.open} timeout="auto" className="sub-menu">
						<List className="list-unstyled py-0">
							{Top5Companies.map((company, index) => {
								return (
									<ListItem button component="li" key={index}>
										<Link className="d-flex" activeClassName="item-active" to={{
											pathname: '/app/dashboard',
											state: { id: cryptr.encrypt(company.id) },
											search: '?companyid=' + cryptr.encrypt(company.id)
										}}>

											<ListItemIcon className="menu-icon">
												<img src={require('Assets/icons/building.svg')} style={{
													top: '-1px',
													left: '-2px',
													position: 'relative'
												}}></img>
											</ListItemIcon>
											<span className="menu" style={{ margin: '6px' }}>
												{/* <IntlMessages id={company.Name} /> */}
												{company.Name}
											</span>
										</Link>
									</ListItem>
								);
							})}
						</List>
					</Collapse>
				</Fragment>
			)
		}
		return (
			<ListItem button component="li">
				<NavLink activeClassName="item-active" to={menu.path}>
					<ListItemIcon className="menu-icon">
						{/* <i className={menu.menu_icon}></i> */}
						<img src={menu.menu_icon} style={{
							top: '3px',
							position: 'relative'
						}}></img>
					</ListItemIcon>
					<span className="menu">
						<IntlMessages id={menu.menu_title} />
					</span>
				</NavLink>
			</ListItem>
		);
	}
}

export default HomeNavItem;
