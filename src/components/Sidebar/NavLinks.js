// sidebar nav links
export default {
   category1: [{
      "menu_title": "Dashboard",
      "menu_icon": require('Assets/icons/dashboard.svg'),
      "path": "/app/dashboard",
      "child_routes": [{
         "menu_title": "General_Overview",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "Vulnerabilities",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "Security_Incidents",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/securityincident"
      }, {
         "menu_title": "Security_Findings",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "User_Analytics",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }]
   }],
   category2: [{
      "menu_title": "Support_Tickets",
      "menu_icon": require('Assets/icons/search.svg'),
      "path": "/app/managetickets",
      "child_routes": [{
         "menu_title": "Ticket_overview",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/ticketdashboard"
      }, {
         "menu_title": "Add_New_Tickets",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/supportticket"
      }, {
         "menu_title": "Open_Tickets",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/opentickets"
      }, {
         "menu_title": "Assigned_Tickets",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/assignedtickets"
      }, {
         "menu_title": "Closed_Tickets",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/closedtickets"
      }, {
         "menu_title": "User_Requests",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/requestedusers"
      }]
   },
   {
      "menu_title": "Security_News",
      "menu_icon": require('Assets/icons/search.svg'),
      "path": "/app/dashboard",
      "child_routes": [{
         "menu_title": "All_News",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "Categories",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "Add_New_Item",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }]
   },
   {
      "menu_title": "Knowledgebase",
      "menu_icon": require('Assets/icons/report.svg'),
      "path": "/app/managepentest",
      "child_routes": [{
         "menu_title": "All_Items",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "Categories",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }, {
         "menu_title": "Add_New_Item",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/dashboard"
      }]
   }],
   category3: [
      {
         "menu_title": "Quickscan_Assessment",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/managequickscan",
         "child_routes": [{
            "menu_title": "Quickscan_Overview",
            "menu_icon": require('Assets/icons/search.svg'),
            "path": "/app/managequickscan"
         }, {
            "menu_title": "All_Quickscans",
            "menu_icon": require('Assets/icons/search.svg'),
            "path": "/app/managequickscan"
         }, {
            "menu_title": "Add_New_Quickscan",
            "menu_icon": require('Assets/icons/search.svg'),
            "path": "/app/managequickscan"
         }]
      },
      {
         "menu_title": "Pentests",
         "menu_icon": require('Assets/icons/report.svg'),
         "path": "/app/managepentest",
         "child_routes": [{
            "menu_title": "Pentest_Overview",
            "menu_icon": require('Assets/icons/search.svg'),
            "path": "/app/managepentest"
         }, {
            "menu_title": "All_Pentests",
            "menu_icon": require('Assets/icons/search.svg'),
            "path": "/app/managepentest"
         }, {
            "menu_title": "Add_New_Pentest",
            "menu_icon": require('Assets/icons/search.svg'),
            "path": "/app/managepentest"
         }]
      },
   ],
   category4: [{
      "menu_title": "Company_Management",
      "menu_icon": require('Assets/icons/user.svg'),
      "path": "/app/managecompany",
      "child_routes": [{
         "menu_title": "All_Companies",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/managecompany"
      }, {
         "menu_title": "Add_New_Company",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/company"
      }]
   }, {
      "menu_title": "User_Management",
      "menu_icon": require('Assets/icons/user.svg'),
      // "path": "/app/userrootsecmanagement",
      "child_routes": [{
         "menu_title": "All_Users",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/userclientmanagement"
      }, {
         "menu_title": "Add_New_User",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/clientuser"
      }]
   }],
   category5: [{
      "menu_title": "Rootsec_Employees",
      "menu_icon": require('Assets/icons/user.svg'),
      "path": "/app/userrootsecmanagement",
      "child_routes": null
   },
   {
      "menu_title": "IP_Blacklisting",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/manageblockips",
      "child_routes": null
   },
   {
      "menu_title": "Logo_&_Analytics",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/manageblockips",
      "child_routes": null
   },
   {
      "menu_title": "Bulk_import_data",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/bulkimport",
      "child_routes": [{
         "menu_title": "Import_Vulnerabilities",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/bulkimport"
      }, {
         "menu_title": "Import_Systems",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/bulkimport"
      }, {
         "menu_title": "Import_Users",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/bulkimport"
      }, {
         "menu_title": "Import_Companies",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/bulkimport"
      }]
   },
   {
      "menu_title": "Privacy_Settings",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/manageblockips",
      "child_routes": null
   },
   {
      "menu_title": "Terms_&_Conditions",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/manageblockips",
      "child_routes": null
   }],
   category6: [{
      "menu_title": "General_Overview",
      "menu_icon": require('Assets/icons/search.svg'),
      "path": "/app/dashboard",
      "child_routes": null
   }, {
      "menu_title": "Vulnerability_Management",
      "menu_icon": require('Assets/icons/search.svg'),
      "path": "/app/vulnerabilitydashboard",
      "child_routes": null,
   }, {
      "menu_title": "Security_Incidents",
      "menu_icon": require('Assets/icons/search.svg'),
      "child_routes": [{
         "menu_title": "All_Security_Incidents",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/manageincident"
      }, {
         "menu_title": "New_Security_Incident",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/addsecurityincident"
      }]
   }, {
      "menu_title": "Security_Findings",
      "menu_icon": require('Assets/icons/search.svg'),
      "path": "/app/dashboard"
   }, {
      "menu_title": "Team_Management",
      "menu_icon": require('Assets/icons/search.svg'),
      "path": "/app/teamusers",
      "child_routes": [{
         "menu_title": "View_your_team",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/teamusers"
      }, {
         "menu_title": "Request_new_users",
         "menu_icon": require('Assets/icons/search.svg'),
         "path": "/app/requestnewuser"
      }]
   }],
   category7: [{
      "menu_title": "All_Pentests",
      "menu_icon": require('Assets/icons/user.svg'),
      "path": "/app/clientpentests",
      "child_routes": null
   },
   {
      "menu_title": "All_Assessments",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/clientassessments",
      "child_routes": null
   }],
   category8: [{
      "menu_title": "Security_News",
      "menu_icon": require('Assets/icons/user.svg'),
      "path": "/app/userrootsecmanagement",
      "child_routes": null
   }, {
      "menu_title": "Knowledgebase",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/manageblockips",
      "child_routes": null
   }, {
      "menu_title": "Frequantly Asked",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/manageblockips",
      "child_routes": null
   }],
   category9: [{
      "menu_title": "Your_Support_Tickets",
      "menu_icon": require('Assets/icons/user.svg'),
      "path": "/app/managetickets",
      "child_routes": null
   }, {
      "menu_title": "Create_A_New_Ticket",
      "menu_icon": require('Assets/icons/multipleusers.svg'),
      "path": "/app/supportticket",
      "child_routes": null
   }],
}
