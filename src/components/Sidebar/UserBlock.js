/**
 * User Block Component
 */
import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Badge } from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import { withStyles } from '@material-ui/core/styles';

// redux action
import { logoutUserFromFirebase } from 'Actions';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import AppConfig from 'Constants/AppConfig';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import $ from 'jquery';

// redux actions
import {
	miniSidebarAction,
	darkModeAction
} from 'Actions';
import color from '@material-ui/core/colors/deepOrange';
import { intlShape } from 'react-intl';

class UserBlock extends Component {

	static contextTypes = {
		intl: intlShape,
	};

	state = {
		userDropdownMenu: false,
		isSupportModal: false,
		darkmodeLabel: 'Donkere modus'
	}

	componentDidMount() {
		console.log('darkmode', this.context.intl.locale);
	}

	componentWillReceiveProps() {
		console.log('darkmode', this.context.intl.locale);
		if (this.context.intl.locale == 'en-US') {
			this.setState({darkmodeLabel: 'Dark mode'});
		} else {
			this.setState({darkmodeLabel: 'Donkere modus'});
		}
	}

	miniSidebarHanlder(isTrue) {
		if (isTrue) {
			$('body').addClass('mini-sidebar');
		} else {
			$('body').removeClass('mini-sidebar');
		}
		setTimeout(() => {
			this.props.miniSidebarAction(isTrue);
		}, 100)
	}

	darkModeHanlder(isTrue) {
		if (isTrue) {
			$('body').addClass('dark-mode');
			localStorage.setItem('dark-mode', 1);
		} else {
			$('body').removeClass('dark-mode');
			localStorage.setItem('dark-mode', 0);
		}
		this.props.darkModeAction(isTrue);
	}

	/**
	 * Logout User
	 */
	logoutUser() {
		this.props.logoutUserFromFirebase();
	}

	/**
	 * Toggle User Dropdown Menu
	 */
	toggleUserDropdownMenu() {
		this.setState({ userDropdownMenu: !this.state.userDropdownMenu });
	}

	/**
	 * Open Support Modal
	 */
	openSupportModal() {
		this.setState({ isSupportModal: true });
	}

	/**
	 * On Close Support Page
	 */
	onCloseSupportPage() {
		this.setState({ isSupportModal: false });
	}

	/**
	 * On Submit Support Page
	 */
	onSubmitSupport() {
		this.setState({ isSupportModal: false });
		NotificationManager.success('Message has been sent successfully!');
	}

	render() {
		const {
			miniSidebar,
			darkMode
		} = this.props;

		const styles = {
			color: '#000000'
		}
		return (
			<div className="top-sidebar">
				<div className="sidebar-user-block">
					<Dropdown
						isOpen={this.state.userDropdownMenu}
						// toggle={() => this.toggleUserDropdownMenu()}
						className="rct-dropdown"
					>
						<DropdownToggle
							tag="div"
							className="d-flex align-items-center"
						>
							{/* <div className="user-profile">
							<i className="zmdi zmdi-settings"></i>
							</div> */}
							<div className="user-info">
								<FormControlLabel style={{ position: 'relative', left: '-13px' }}
									label={this.state.darkmodeLabel}
									control={
										<Switch
											checked={darkMode}
											onChange={(e) => this.darkModeHanlder(e.target.checked)}
											className="switch-btn"
										/>
									}
									className="m-0"
								/>
								{/* <span className="user-name ml-4">Settings</span> */}
								{/* <i className="zmdi zmdi-chevron-down dropdown-icon mx-4"></i> */}
							</div>
						</DropdownToggle>
						<DropdownMenu>
							<ul className="list-unstyled mb-0">
								{/* <li className="p-15 border-bottom user-profile-top bg-primary rounded-top">
									<p className="text-white mb-0 fs-14">Lucile Beck</p>
									<span className="text-white fs-14">info@example.com</span>
								</li>
								<li>
									<Link to={{
										pathname: '/app/profile',
										state: { activeTab: 0 }
									}}>
										<i className="zmdi zmdi-account text-primary mr-3"></i>
										<IntlMessages id="widgets.profile" />
									</Link>
								</li> */}
								<li>
									<FormControlLabel
										label={this.state.darkmodeLabel}
										control={
											<Switch
												checked={darkMode}
												onChange={(e) => this.darkModeHanlder(e.target.checked)}
												className="switch-btn"
											/>
										}
										className="m-0"
									/>
								</li>
								<li>
									<FormControlLabel
										control={
											<Switch
												// disabled={navCollapsed}
												checked={miniSidebar}
												onChange={(e) => this.miniSidebarHanlder(e.target.checked)}
												className="switch-btn"
											/>
										}
										label={<Typography className="text-danger">{<IntlMessages id="Mini_Sidebar" />}</Typography>}
										className="m-0 text-danger"
									/>
								</li>
								{/* <li className="border-top">
									<a href="javascript:void(0)" onClick={() => this.logoutUser()}>
										<i className="zmdi zmdi-power text-danger mr-3"></i>
										<IntlMessages id="widgets.logOut" />
									</a>
								</li> */}
							</ul>
						</DropdownMenu>
					</Dropdown>
				</div>
				{/* <SupportPage
					isOpen={this.state.isSupportModal}
					onCloseSupportPage={() => this.onCloseSupportPage()}
					onSubmit={() => this.onSubmitSupport()}
				/> */}
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ settings }) => {
	return settings;
}

export default connect(mapStateToProps, {
	logoutUserFromFirebase,
	miniSidebarAction,
	darkModeAction,
})(UserBlock);
