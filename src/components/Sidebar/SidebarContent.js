/**
 * Sidebar Content
 */
import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import IntlMessages from 'Util/IntlMessages';
import NavMenuItem from './NavMenuItem';
import { onToggleMenu } from 'Actions';
import HomeNavItem from './HomeNavItem';
import * as SessionService from '../../actions/SessionService';

class SidebarContent extends Component {

	state = {
		userRole: {
			Name: 'SuperAdmin'
		}
	}
	toggleMenu(menu, stateCategory) {
		let data = {
			menu,
			stateCategory
		}
		this.props.onToggleMenu(data);
	}

	async componentDidMount() {
		var userRole = await SessionService.GetUserRole('userRole');
		console.log('userRole', userRole);
		await this.setState({ userRole });
	}

	componentWillReceiveProps() {
	}

	render() {
		const { sidebarMenus } = this.props.sidebar;
		const { userRole } = this.state;
		return (
			<div className="rct-sidebar-nav">
				{(userRole && (userRole.Name == 'RootSecEmployee' || userRole.Name == 'SuperAdmin')) ?
					<nav className="navigation">
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							// subheader={
							// 	<ListSubheader className="side-title" component="li">
							// 		<IntlMessages id="Workbench" />
							// 	</ListSubheader>}
						>
							{/* {sidebarMenus.category1.map((menu, key) => (
							<HomeNavItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category1')}
							/>
						))} */}
							{sidebarMenus.category1.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category1')}
								/>
							))}
						</List>
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							subheader={<ListSubheader className="side-title" component="li">
								<IntlMessages id="Communication" />
							</ListSubheader>}
						>
							{sidebarMenus.category2.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category2')}
								/>
							))}
						</List>
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							subheader={<ListSubheader className="side-title" component="li">
								<IntlMessages id="Services" />
							</ListSubheader>}
						>
							{sidebarMenus.category3.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category3')}
								/>
							))}
						</List>
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							subheader={<ListSubheader className="side-title" component="li">
								<IntlMessages id="Client_Management" />
							</ListSubheader>}
						>
							{sidebarMenus.category4.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category4')}
								/>
							))}
						</List>
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							subheader={<ListSubheader className="side-title" component="li">
								<IntlMessages id="Rootdash_Settings" />
							</ListSubheader>}
						>
							{sidebarMenus.category5.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category5')}
								/>
							))}
						</List>
					</nav>
					: (userRole && userRole.Name == 'SecurityManager') ?
						<nav className="navigation">
							<List
								className="rct-mainMenu p-0 m-0 list-unstyled"
							>
								{sidebarMenus.category6.map((menu, key) => (
									<NavMenuItem
										menu={menu}
										key={key}
										onToggleMenu={() => this.toggleMenu(menu, 'category6')}
									/>
								))}
							</List>
							<List
								className="rct-mainMenu p-0 m-0 list-unstyled"
								subheader={<ListSubheader className="side-title" component="li">
									<IntlMessages id="Pentesting_&_Scans" />
								</ListSubheader>}
							>
								{sidebarMenus.category7.map((menu, key) => (
									<NavMenuItem
										menu={menu}
										key={key}
										onToggleMenu={() => this.toggleMenu(menu, 'category7')}
									/>
								))}
							</List>
							<List
								className="rct-mainMenu p-0 m-0 list-unstyled"
								subheader={<ListSubheader className="side-title" component="li">
									<IntlMessages id="Knowledge_&_FAQ" />
								</ListSubheader>}
							>
								{sidebarMenus.category8.map((menu, key) => (
									<NavMenuItem
										menu={menu}
										key={key}
										onToggleMenu={() => this.toggleMenu(menu, 'category8')}
									/>
								))}
							</List>
							<List
								className="rct-mainMenu p-0 m-0 list-unstyled"
								subheader={<ListSubheader className="side-title" component="li">
									<IntlMessages id="Get_support_from_Rootsec" />
								</ListSubheader>}
							>
								{sidebarMenus.category9.map((menu, key) => (
									<NavMenuItem
										menu={menu}
										key={key}
										onToggleMenu={() => this.toggleMenu(menu, 'category9')}
									/>
								))}
							</List>
						</nav>
						: ''}
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ sidebar, settings }) => {
	return { sidebar, settings };
};

export default withRouter(connect(mapStateToProps, {
	onToggleMenu
})(SidebarContent));
