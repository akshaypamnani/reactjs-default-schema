/**
 * Rct Horizontal Menu Layout
 */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import { Alert } from 'reactstrap';

// Components
import Header from 'Components/Header/Header';
import Footer from 'Components/Footer/Footer';
import HorizontalMenu from 'Components/HorizontalMenu/HorizontalMenu';
import ThemeOptions from 'Components/ThemeOptions/ThemeOptions';
import { intlShape } from 'react-intl';

class RctHorizontalLayout extends Component {

    static contextTypes = {
        intl: intlShape,
    };

    state = {
        visibleNote: false,
        currentLocal: 'nl-NL'
    }

    componentWillReceiveProps() {
        if (this.context.intl.locale != this.state.currentLocal) {
            if (this.context.intl.locale == 'en-US') {
                this.setState({ visibleNote: true, currentLocal: 'en-US' });
            } else {
                this.setState({ visibleNote: false, currentLocal: 'nl-NL' });
            }
        }
    }

    renderPage() {
        const { pathname } = this.props.location;
        const { children, match } = this.props;
        if (pathname === `${match.url}/chat` || pathname.startsWith(`${match.url}/mail`) || pathname === `${match.url}/todo`) {
            return (
                <div className="rct-page-content p-0">
                    {children}
                </div>
            );
        }
        return (
            <Scrollbars
                className="rct-scroll"
                autoHide
                autoHideDuration={100}
                style={{ height: 'calc(100vh - 64px)' }}
            >
                <div className="rct-page-content container">
                    {children}
                    {/* <Footer /> */}
                </div>
            </Scrollbars>
        );
    }

    onDismiss(key) {
        this.setState({ [key]: false });
    }

    render() {
        return (
            <div className="app-horizontal collapsed-sidebar">
                <div className="app-container">
                    <div className="rct-page-wrapper">
                        <div className="rct-app-content">
                            <div className="app-header">
                                <Header horizontalMenu />
                            </div>
                            <div className="rct-page">
                                <HorizontalMenu />
                                <div className="container">
                                    <Alert className="clsEnglishalert m-2" color="primary" isOpen={this.state.visibleNote} toggle={() => this.onDismiss('visibleNote')}>
                                        <strong className="mr-20">Important Notice:</strong>This translation is automatically generated with machine learning. Be aware that the translation of some details might not be 100% accurate.</Alert>
                                </div>
                                {this.renderPage()}
                            </div>
                            <ThemeOptions />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(RctHorizontalLayout);
