/**
 * Recent Orders
 */
import React, { Component } from 'react';
import IntlMessages from 'Util/IntlMessages';

// api
import api from 'Api';

class RecentOrders extends Component {

	state = {
		recentOrders: null
	}

	componentDidMount() {
		// this.getRecentOrders();
	}

	// recent orders
	getRecentOrders() {
		api.get('recentOrders.js')
			.then((response) => {
				this.setState({ recentOrders: response.data });
			})
			.catch(error => {
				// error hanlding
			})
	}

	GetRCEStatus(RiskStatus, ComplexityStatus, EffortStatus) {
		var Risk = 'H';
		var Complexity = 'H';
		var Effort = 'H';
  
		var RiskClass = 'highclr';
		var ComplexityClass = 'highclr';
		var EffortClass = 'highclr';
  
		Risk = (RiskStatus ? RiskStatus == 1 ? 'E' : RiskStatus == 2 ? 'H' : RiskStatus == 3 ? 'M' : RiskStatus == 4 ? 'L' : 'H' : 'H');
		Complexity = (ComplexityStatus ? ComplexityStatus == 1 ? 'H' : ComplexityStatus == 2 ? 'M' : ComplexityStatus == 3 ? 'L' : 'H' : 'H');
		Effort = (EffortStatus ? EffortStatus == 1 ? 'H' : EffortStatus == 2 ? 'M' : EffortStatus == 3 ? 'L' : 'H' : 'H');
  
		RiskClass = (RiskStatus ? RiskStatus == 1 ? 'extremeclr' : RiskStatus == 2 ? 'highclr' : RiskStatus == 3 ? 'mediumclr' : RiskStatus == 4 ? 'lowclr' : 'highclr' : 'highclr');
  
		ComplexityClass = (ComplexityStatus ? ComplexityStatus == 1 ? 'highclr' : ComplexityStatus == 2 ? 'mediumclr' : ComplexityStatus == 3 ? 'lowclr' : 'highclr' : 'highclr');
  
		EffortClass = (EffortStatus ? EffortStatus == 1 ? 'highclr' : EffortStatus == 2 ? 'mediumclr' : EffortStatus == 3 ? 'lowclr' : 'highclr' : 'highclr');
  
		return <ul className="list-unstyled d-flex justify-content-between rcevalue mb-0"><li className={RiskClass}>{Risk}</li><li className={ComplexityClass}>{Complexity}</li><li className={EffortClass}>{Effort}</li></ul>;
	 }

	render() {
		const { recentOrders } = this.state;
		return (
			<div className="table-responsive">
				<table className="table table-hover mb-0">
					<thead>
						<tr>
							<th><IntlMessages id="dashboard.RCE" /></th>
							<th><IntlMessages id="dashboard.Presenton" /></th>
							<th><IntlMessages id="dashboard.Name" /></th>
							<th><IntlMessages id="dashboard.Date" /></th>
							<th><IntlMessages id="dashboard.Status" /></th>
							<th><IntlMessages id="dashboard.Facing" /></th>
						</tr>
					</thead>
					<tbody>
						{recentOrders && recentOrders.map((order, key) => (
							<tr key={key}>
								<td>H H H</td>
								<td>10.62.1.90</td>
								<td>Vulnerability name</td>
								<td>31/03/2019</td>
								<td>Inprogress</td>
								<td>Public</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		);
	}
}

export default RecentOrders;
