/**
 * Recent Orders
 */
import React, { Component } from 'react';

// api
import api from 'Api';

class RecentOrders2 extends Component {

	state = {
		recentOrders: null
	}

	componentDidMount() {
		this.getRecentOrders();
	}

	// recent orders
	getRecentOrders() {
		api.get('recentOrders.js')
			.then((response) => {
				this.setState({ recentOrders: response.data });
			})
			.catch(error => {
				// error hanlding
			})
	}

	render() {
		const { recentOrders } = this.state;
		return (
			<div className="table-responsive">
				<table className="table table-hover mb-0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Com./Dep.</th>
							<th>Systems</th>
							<th>Assets</th>
							<th>Valn.</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						{recentOrders && recentOrders.map((order, key) => (
							<tr key={key}>
								<td>Pentest name</td>
								<td>RootSec</td>
								<td>System name</td>
								<td>Assets name</td>
								<td>12</td>
								<td>Inprogress</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		);
	}
}

export default RecentOrders2;
