import React from "react";
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import MuiTablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";
import { intlShape } from "react-intl";

const defaultFooterStyles = {
};

class CustomFooter extends React.Component {

    static contextTypes = {
        intl: intlShape,
    };

    handleRowChange = event => {
        this.props.changeRowsPerPage(event.target.value);
    };

    handlePageChange = (_, page) => {
        this.props.changePage(page);
    };

    render() {
        const { count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels } = this.props;

        const footerStyle = {
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '0px 24px 0px 24px'
        };

        return (
            <TableFooter>
                <TableRow>
                    <TableCell style={footerStyle} colSpan={1000}>
                        {/* <FormControlLabel className="m-0 clsHideacceptedonly" control={
                            <Checkbox color="primary" checked={this.state.HideAcceptedVuln} onChange={(e) => this.ChangeCheckAcceptedVuln(e)} value="" />
                        } label={this.context.intl.formatMessage({ id: 'Hide_Accepted_Only' })}
                        /> */}

                        <MuiTablePagination
                            component="div"
                            count={count}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            labelRowsPerPage={this.context.intl.formatMessage({ id: 'Rows_per_page:' })}
                            labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${this.context.intl.formatMessage({ id: 'of' })} ${count}`}
                            backIconButtonProps={{
                                'aria-label': textLabels.previous,
                            }}
                            nextIconButtonProps={{
                                'aria-label': textLabels.next,
                            }}
                            rowsPerPageOptions={[10, 50, 100]}
                            onChangePage={(_, newPage) => changePage(newPage)}
                            onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                        />
                    </TableCell>
                </TableRow>
            </TableFooter>
        );
    }

}

export default withStyles(defaultFooterStyles, { name: "CustomFooter" })(CustomFooter);