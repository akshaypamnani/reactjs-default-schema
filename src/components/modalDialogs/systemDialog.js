/**
 * System Dialog
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import MatButton from '@material-ui/core/Button';
import { FormGroup, Label } from 'reactstrap';
import Input from 'react-validation/build/input';
import Form from 'react-validation/build/form';
import Select from 'react-validation/build/select';
import Textarea from 'react-validation/build/textarea';
import { IconButton } from '@material-ui/core';
import update from 'react-addons-update';
import IntlMessages from 'Util/IntlMessages';

//API Services
import * as ApiService from '../../actions/ApiServices';
import RctSectionLoader from '../RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

const IPCheck = (value, props) => {
   if (value && value.toString().trim().length > 0 && !(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
   }
};

export default class SystemDialog extends React.Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      open: false,
      System: {
         id: 0,
         Name: '',
      },
      Hosts: [],
      SystemId: 0,
      CompanyId: 0,
      CompanySystemId: 0,
      loading: false,
      DeletedHosts: []
   };

   componentDidMount() {
   }

   handleAddClickOpen = (companyid) => {
      this.AddHost();
      this.setState({ open: true });
      this.setState({ CompanyId: companyid });
   };

   handleEditClickOpen = (id, companyid) => {
      this.setState({ open: true });
      this.setState({ CompanyId: companyid, SystemId: id });
      this.GetSystem(id);
      // this.setState({ Index: index });
   };

   async GetSystem(id) {
      this.setState({ loading: true });
      var System = await ApiService.GetAPI('System/' + id);
      this.setState({ System: System });

      if (System.Hosts && System.Hosts.length > 0) {
         this.setState({ Hosts: System.Hosts });
      }
      this.AddHost();
      this.setState({ loading: false });
   }

   handleChange = (event, key) => {
      let System = this.state.System;
      System[key] = event.target.value;
      this.setState({ System: System });
   }

   handleClickOpen = () => {
      this.setState({ open: true });
   };

   handleClose = () => {
      this.setState({ open: false, CompanyId: 0, System: {}, Hosts: [], SystemId: 0, CompanySystemId: 0 });
   };

   async RemoveHost(index) {
      this.setState({ loading: true });
      let Hosts = this.state.Hosts;
      if (Hosts[index].id > 0) {
         const DeletedHosts = this.state.DeletedHosts;
         await DeletedHosts.push(Hosts[index]);
         await this.setState({ DeletedHosts });
      }
      Hosts.splice(index, 1);
      setTimeout(async () => {
         await this.setState({ Hosts: Hosts });
         this.setState({ loading: false });
      }, 500);
   }

   AddHost() {
      let Hosts = this.state.Hosts;
      let newHost = {
         id: 0,
         PortNo: 80,
         Protocol: "tcp",
         ProtocolType: "smtp",
         HostIP: '0.0.0.0'
      }
      Hosts.push(newHost);
      this.setState({ Hosts: Hosts });
   }

   handleChange2(value, data, key) {
      let selectedHostIndex = this.state.Hosts.indexOf(data);
      let newHost = update(this.state, {
         Hosts: {
            [selectedHostIndex]: {
               [key]: { $set: value }
            }
         }
      });
      this.setState({ Hosts: newHost.Hosts });
   }

   async SaveSystem() {
      this.form.validateAll();
      this.setState({ loading: true });
      if (this.form.getChildContext()._errors.length <= 0) {
         var System;
         var systemdata = {
            id: this.state.System.id,
            Name: this.state.System.Name ? this.state.System.Name.toString().trim() : '',
            Remarks: this.state.System.Remarks ? this.state.System.Remarks.toString().trim() : '',
            CompanyId: this.state.CompanyId,
            Hosts: this.state.Hosts
         };

         if (systemdata.Hosts.length <= 1) {
            systemdata.Hosts = [];
         } else if (systemdata.Hosts[systemdata.Hosts.length - 1].id == 0) {
            systemdata.Hosts.splice((systemdata.Hosts.length - 1), 1);
         }

         System = await ApiService.PutAPI('systemapi/AddOrEditSystem', systemdata);
         if (System) {
            let CompanyId = this.state.CompanyId;

            if (this.state.DeletedHosts && this.state.DeletedHosts.length > 0) {
               this.state.DeletedHosts.forEach(async (host) => {
                  var host = await ApiService.HardDeleteAPI('Host/' + cryptr.encrypt(host.id));
               });
            }

            await this.setState({ open: false });
            setTimeout(() => {
               this.setState({ CompanyId: 0, System: {}, Hosts: [], SystemId: 0, CompanySystemId: 0 });
            }, 500);
            this.props.backFromProps(cryptr.encrypt(CompanyId));
         }
      }
      this.setState({ loading: false });
   }

   render() {
      const { loading } = this.state;
      return (
         <div>
            {/* <Button variant="contained" className="btn-primary text-white btn-block" onClick={this.handleClickOpen}>Add System</Button> */}
            <Dialog maxWidth="sm" fullWidth="true" open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
               <DialogTitle id="form-dialog-title" className="modaltl"><IntlMessages id="Add/Edit_System" /></DialogTitle>
               <DialogContent className="modalbd">
                  <DialogContentText>
                     <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-12 p-0">
                           <RctCollapsibleCard>
                              {loading ? <RctSectionLoader /> : ''}
                              <Form ref={c => { this.form = c }}>
                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="assignedcomp"><IntlMessages id="Name" />
                                             <span className="required">*</span></Label>
                                          <Input className="form-control" validations={[required]} value={this.state.System.Remarks} onChange={(e) => this.handleChange(e, 'Remarks')} name="assignedcomp" id="assignedcomp" placeholder={this.context.intl.formatMessage({ id: 'Name' })} />
                                       </FormGroup>
                                    </div>
                                 </div>
                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="firstname"><IntlMessages id="System_IP" />
                                          </Label>
                                          <Input type="text" maxLength="50" validations={[IPCheck]} className="form-control" value={this.state.System.Name} onChange={(e) => this.handleChange(e, 'Name')} name="systemip" id="systemip" placeholder={this.context.intl.formatMessage({ id: 'System IP' })} />
                                       </FormGroup>
                                    </div>
                                 </div>
                                 {/* <div className="row row-12">
                                    <Label className="col-sm-1 col-md-1 col-xl-1" for="assignedcomp"><IntlMessages id="Port" /></Label>
                                    <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"></Label>
                                    <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"></Label>
                                    <Label className="col-sm-1 col-md-1 col-xl-1" for="assignedcomp"><IntlMessages id="Host" /></Label>
                                 </div> */}
                                 {/* {this.state.Hosts.map((host, key) => (
                                    <div className="row ipcontainer">
                                       <div className="col-sm-2 col-md-2 col-12">
                                          <FormGroup style={{ marginBottom: '5px' }}>
                                             <Input type="number" key={key} validations={[required]} value={host.PortNo} onChange={(e) => this.handleChange2(e.target.value, host, 'PortNo')} className="form-control" name="assignedcomp" id="assignedcomp" placeholder={this.context.intl.formatMessage({ id: 'Port' })} />
                                          </FormGroup>
                                       </div>
                                       <div className="col-sm-2 col-md-2 col-12">
                                          <FormGroup style={{ marginBottom: '5px' }}>
                                             <Select className="form-control" key={key} value={host.Protocol} onChange={(e) => this.handleChange2(e.target.value, host, 'Protocol')} name="protocol" id="Select-2">
                                                <option value="tcp">tcp</option>
                                             </Select>
                                          </FormGroup>
                                       </div>
                                       <div className="col-sm-2 col-md-2 col-12">
                                          <FormGroup style={{ marginBottom: '5px' }}>
                                             <Select className="form-control" key={key} value={host.ProtocolType} onChange={(e) => this.handleChange2(e.target.value, host, 'ProtocolType')} name="protocoltype" id="Select-2">
                                                <option value="smtp">smtp</option>
                                                <option value="www">www</option>
                                                <option value="mssql">mssql</option>
                                             </Select>
                                          </FormGroup>
                                       </div>
                                       <div className="col-sm-3 col-md-3 col-12">
                                          <FormGroup style={{ marginBottom: '5px' }}>
                                             <Input type="text" validations={[required, IPCheck]} className="form-control" key={key} value={host.HostIP} onChange={(e) => this.handleChange2(e.target.value, host, 'HostIP')} name="Host" id="Host" placeholder={this.context.intl.formatMessage({ id: 'Host' })} />
                                          </FormGroup>
                                       </div>
                                       {key == (this.state.Hosts.length - 1) ? <div className="col-sm-2 col-md-2 col-xl-2" style={{ paddingRight: '1px' }}>
                                          <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                             <IconButton className="text-primary" onClick={() => this.AddHost()} aria-label="plus"><i className="zmdi zmdi-plus"></i></IconButton>
                                          </FormGroup>
                                       </div> : ''}
                                       {key != (this.state.Hosts.length - 1) ? <div className="col-sm-2 col-md-2 col-xl-2" style={{ paddingRight: '1px' }}>
                                          <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                             <IconButton className="text-danger" onClick={() => this.RemoveHost(key)} aria-label="plus"><i className="zmdi zmdi-minus"></i></IconButton>
                                          </FormGroup>
                                       </div> : ''}
                                    </div>
                                 ))} */}

                              </Form>
                           </RctCollapsibleCard>
                        </div>
                     </div>
                  </DialogContentText>
               </DialogContent>
               <DialogActions>
                  <Button variant="contained" onClick={this.handleClose} style={{ color: '#5d92f4' }} className="btn-default modalcancel">
                     <IntlMessages id="Cancel" />
                  </Button>
                  <Button variant="contained" onClick={() => this.SaveSystem()} className="btn-primary text-white modalsave">
                     <IntlMessages id="Create/Update" />
                  </Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }
}
