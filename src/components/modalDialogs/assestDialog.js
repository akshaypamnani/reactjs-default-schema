/**
 * Assest Dialog
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { FormGroup, Label } from 'reactstrap';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from 'react-validation/build/input';
import Form from 'react-validation/build/form';
import Select from 'react-validation/build/select';
import Textarea from 'react-validation/build/textarea';
import { IconButton, Checkbox } from '@material-ui/core';
import update from 'react-addons-update';
import { intlShape } from "react-intl";
import * as ApiService from '../../actions/ApiServices';
import * as TranslationService from '../../actions/TranslationService';
import RctSectionLoader from '../RctSectionLoader/RctSectionLoader';
import IntlMessages from 'Util/IntlMessages';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

const IPCheck = (value, props) => {
   if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
   }
};

export default class AssestDialog extends React.Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      open: false,
      Asset: {
         id: 0,
         SystemId: null
      },
      Hosts: [],
      AssetId: 0,
      CompanyId: 0,
      CompanyAssetId: 0,
      Systems: [],
      loading: false,
      DeletedHosts: [],
      ckUrl: true,
      ckIPrange: false,
      ckSoftware: false
   };

   async GetSystems(companyid) {
      var Systemdata = {
         Where: {
            CompanyId: companyid,
            IsActive: 1
         }
      }
      var Systems = await ApiService.PostWhereAPI('execute/conditions/System', Systemdata);
      this.setState({ Systems: Systems });
   }

   async componentDidMount() {
   }

   handleAddClickOpen = async (companyid) => {
      this.setState({ loading: true });
      this.setState({ open: true });
      this.setState({ CompanyId: companyid });
      this.setState({ ckUrl: true, ckIPrange: false, ckSoftware: false });
      await this.GetSystems(companyid);
      this.AddHost();
      this.setState({ loading: false });
   };

   handleEditClickOpen = async (id, companyid) => {
      this.setState({ loading: true });
      this.setState({ open: true });
      this.setState({ CompanyId: companyid, AssetId: id });
      this.setState({ ckUrl: false, ckIPrange: false, ckSoftware: false });
      await this.GetSystems(companyid);
      await this.GetAsset(id);
      this.setState({ loading: false });
      // this.setState({ Index: index });
   };

   async GetAsset(id) {
      this.setState({ loading: true });
      var Asset = await ApiService.GetAPI('Asset/' + id);
      this.setState({ Asset });

      if (Asset.Hosts && Asset.Hosts.length > 0) {
         this.state.Hosts = Asset.Hosts;
      }
      if (Asset.Urls && Asset.Urls.length > 0) {
         this.setState({ ckUrl: true });
      }
      if (Asset.IpRange && Asset.IpRange.length > 0) {
         this.setState({ ckIPrange: true });
      }
      if (Asset.Software && Asset.Software.length > 0) {
         this.setState({ ckSoftware: true });
      }

      await this.AddHost();
      this.setState({ loading: false });
   }

   handleClickOpen = () => {
      this.setState({ open: true });
   };

   handleClose = () => {
      this.setState({ open: false, CompanyId: 0, Asset: {}, Hosts: [], AssetId: 0 });
   };

   async RemoveHost(index) {
      this.setState({ loading: true });
      let Hosts = this.state.Hosts;
      if (Hosts[index].id > 0) {
         const DeletedHosts = this.state.DeletedHosts;
         await DeletedHosts.push(Hosts[index]);
         await this.setState({ DeletedHosts });
      }
      Hosts.splice(index, 1);
      setTimeout(async () => {
         await this.setState({ Hosts: Hosts });
         this.setState({ loading: false });
      }, 500);
   }

   AddHost() {
      let Hosts = this.state.Hosts;
      let newHost = {
         id: 0,
         PortNo: 80,
         Protocol: "tcp",
         ProtocolType: "smtp",
         HostIP: '0.0.0.0'
      }
      Hosts.push(newHost);
      this.setState({ Hosts: Hosts });
   }

   handleChange = (event, key) => {
      let Asset = this.state.Asset;
      Asset[key] = event.target.value;
      this.setState({ Asset: Asset });
   }

   handleChange2(value, data, key) {
      let selectedHostIndex = this.state.Hosts.indexOf(data);
      let newHost = update(this.state, {
         Hosts: {
            [selectedHostIndex]: {
               [key]: { $set: value }
            }
         }
      });
      this.setState({ Hosts: newHost.Hosts });
   }

   async SaveAssets() {
      this.form.validateAll();

      if (this.form.getChildContext()._errors.length <= 0) {
         var Asset;
         var assetdata = {
            id: this.state.Asset.id,
            Name: this.state.Asset.Name.toString().trim(),
            Urls: this.state.Asset.Urls ? this.state.Asset.Urls.toString().trim() : '',
            IpRange: this.state.Asset.IpRange ? this.state.Asset.IpRange.toString().trim() : '',
            Software: this.state.Asset.Software ? this.state.Asset.Software.toString().trim() : '',
            Remarks: this.state.Asset.Remarks ? this.state.Asset.Remarks.toString().trim() : '',
            CompanyId: this.state.CompanyId,
            SystemId: this.state.Asset.SystemId == '0' ? null : this.state.Asset.SystemId,
            Hosts: this.state.Hosts
         };

         if (assetdata.Hosts.length <= 1) {
            assetdata.Hosts = [];
         } else if (assetdata.Hosts[assetdata.Hosts.length - 1].id == 0) {
            assetdata.Hosts.splice((assetdata.Hosts.length - 1), 1);
         }

         Asset = await ApiService.PutAPI('Assetapi/AddOrEditAsset', assetdata);
         if (Asset) {
            let CompanyId = this.state.CompanyId;
            if (this.state.DeletedHosts && this.state.DeletedHosts.length > 0) {
               this.state.DeletedHosts.forEach(async (host) => {
                  var host = await ApiService.HardDeleteAPI('Host/' + cryptr.encrypt(host.id));
               });
            }
            await this.setState({ open: false });
            this.setState({ CompanyId: 0, Asset: {}, Hosts: [], AssetId: 0 });
            this.props.backFromProps(cryptr.encrypt(CompanyId));
         }
      }
   }

   handleChangeCheckBox = name => (event, checked) => {
      this.setState({ [name]: checked });
   };

   render() {
      const { loading } = this.state;
      const { classes } = this.props;
      return (
         <div>
            {/* <Button variant="contained" className="btn-primary text-white btn-block" onClick={this.handleClickOpen}>Add Assets</Button> */}
            <Dialog maxWidth="md" fullWidth="true" open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
               <DialogTitle id="form-dialog-title" className="modaltl"><IntlMessages id="Add/edit_Asset" /></DialogTitle>
               <DialogContent className="modalbd">
                  <DialogContentText>
                     <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-12 p-0">
                           <RctCollapsibleCard>
                              {loading ? <RctSectionLoader /> : ''}
                              <Form ref={c => { this.form = c }}>
                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="firstname"><IntlMessages id="Name" />
                                             <span className="required">*</span>
                                          </Label>
                                          <Input type="text" maxLength="50" validations={[required]} className="form-control" value={this.state.Asset.Name} onChange={(e) => this.handleChange(e, 'Name')} name="assetname" id="assetname" placeholder={this.context.intl.formatMessage({ id: 'Name' })} />
                                       </FormGroup>
                                    </div>
                                 </div>
                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="firstname"><IntlMessages id="Assign_to_system_(optional)" /></Label>
                                          <Select className="form-control" value={this.state.Asset.SystemId} name="usrselect" onChange={(e) => this.handleChange(e, 'SystemId')} id="Select-2">
                                             <option value={0}>{this.context.intl.formatMessage({ id: 'None' })}</option>
                                             {this.state.Systems.map(syst => (
                                                <option value={syst.id}>{syst.Remarks + (syst.Name && syst.Name.toString().length > 0 ? ' - ' + syst.Name : '')}</option>
                                             ))}
                                          </Select>
                                       </FormGroup>
                                    </div>
                                 </div>
                                 <div className="row row-12">
                                    <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"><IntlMessages id="Add_Assets" /></Label>
                                 </div>
                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                       <FormGroup>
                                          <div className="row">

                                             <FormControlLabel className="col-sm-4" control={
                                                <Checkbox color="primary" onChange={this.handleChangeCheckBox('ckUrl')} checked={this.state.ckUrl} value="URL" />
                                             } label={<IntlMessages id="URL" />}
                                             />
                                             <FormControlLabel className="col-sm-4" control={
                                                <Checkbox color="primary" checked={this.state.ckIPrange} onChange={this.handleChangeCheckBox('ckIPrange')} value="IP range" />
                                             } label={<IntlMessages id="IP_Range" />}
                                             />
                                             <FormControlLabel className="col-sm-3" control={
                                                <Checkbox color="primary" checked={this.state.ckSoftware} onChange={this.handleChangeCheckBox('ckSoftware')} value="Software" />
                                             } label={<IntlMessages id="Software" />}
                                             />
                                          </div>
                                          <div className="row row-12">
                                             <div className="col-sm-6 col-md-6 col-xl-4">
                                                <FormGroup>
                                                   <Textarea disabled={!this.state.ckUrl} className="form-control" value={this.state.Asset.Urls} onChange={(e) => this.handleChange(e, 'Urls')} name="Urls" id="idURLs" placeholder={this.context.intl.formatMessage({ id: 'URL' })}></Textarea>
                                                </FormGroup>
                                             </div>
                                             <div className="col-sm-6 col-md-6 col-xl-4">
                                                <FormGroup>
                                                   <Textarea disabled={!this.state.ckIPrange} className="form-control" value={this.state.Asset.IpRange} onChange={(e) => this.handleChange(e, 'IpRange')} name="IpRange" id="idIpRange" placeholder={this.context.intl.formatMessage({ id: 'IP_Range' })}></Textarea>
                                                </FormGroup>
                                             </div>
                                             <div className="col-sm-6 col-md-6 col-xl-4">
                                                <FormGroup>
                                                   <Textarea disabled={!this.state.ckSoftware} className="form-control" value={this.state.Asset.Software} onChange={(e) => this.handleChange(e, 'Software')} name="Software" id="idSoftware" placeholder={this.context.intl.formatMessage({ id: 'Software' })}></Textarea>
                                                </FormGroup>
                                             </div>
                                          </div>
                                       </FormGroup>
                                    </div>
                                 </div>
                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-7">
                                       <FormGroup>
                                          {/* <Label for="checkbox2">Add Assets</Label> */}
                                          <div className="row">
                                             <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"><IntlMessages id="Port" /></Label>
                                             <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"></Label>
                                             <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"></Label>
                                             <Label className="col-sm-1 col-md-1 col-xl-1" for="assignedcomp"><IntlMessages id="Host" /></Label>
                                          </div>
                                          {this.state.Hosts.map((host, key) => (
                                             <div className="row ipcontainer">
                                                <div className="col-sm-2 col-md-2 col-12">
                                                   <FormGroup style={{ marginBottom: '5px' }}>
                                                      <Input type="number" key={key} validations={[required]} value={host.PortNo} onChange={(e) => this.handleChange2(e.target.value, host, 'PortNo')} className="form-control" name="assignedcomp" id="assignedcomp" placeholder={this.context.intl.formatMessage({ id: 'Port' })} />
                                                   </FormGroup>
                                                </div>
                                                <div className="col-sm-2 col-md-2 col-12">
                                                   <FormGroup style={{ marginBottom: '5px' }}>
                                                      <Input type="text" className="form-control" key={key} onChange={(e) => this.handleChange2(e.target.value, host, 'Protocol')} name="Protocol" id="Protocol" placeholder={host.Protocol && host.Protocol.toString().length > 0 ? host.Protocol : this.context.intl.formatMessage({ id: 'tcp' })} />
                                                      {/* <Select className="form-control" key={key} value={host.Protocol} onChange={(e) => this.handleChange2(e.target.value, host, 'Protocol')} name="protocol" id="Select-2">
                                                         <option value="tcp">tcp</option>
                                                      </Select> */}
                                                   </FormGroup>
                                                </div>
                                                <div className="col-sm-2 col-md-2 col-12">
                                                   <FormGroup style={{ marginBottom: '5px' }}>
                                                      <Input type="text" className="form-control" key={key} onChange={(e) => this.handleChange2(e.target.value, host, 'ProtocolType')} name="ProtocolType" id="ProtocolType" placeholder={host.ProtocolType && host.ProtocolType.toString().length > 0 ? host.ProtocolType : this.context.intl.formatMessage({ id: 'Host' })} />
                                                      {/* <Select className="form-control" key={key} value={host.ProtocolType} onChange={(e) => this.handleChange2(e.target.value, host, 'ProtocolType')} name="protocoltype" id="Select-2">
                                                         <option value="smtp">smtp</option>
                                                         <option value="www">www</option>
                                                         <option value="mssql">mssql</option>
                                                      </Select> */}
                                                   </FormGroup>
                                                </div>
                                                <div className="col-sm-3 col-md-3 col-12">
                                                   <FormGroup style={{ marginBottom: '5px' }}>
                                                      <Input type="text" validations={[required, IPCheck]} className="form-control" key={key} value={host.HostIP} onChange={(e) => this.handleChange2(e.target.value, host, 'HostIP')} name="Host" id="Host" placeholder={this.context.intl.formatMessage({ id: 'Host' })} />
                                                   </FormGroup>
                                                </div>
                                                {key == (this.state.Hosts.length - 1) ? <div className="col-sm-2 col-md-2 col-xl-2" style={{ paddingRight: '1px' }}>
                                                   <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                                      <IconButton className="text-primary" onClick={() => this.AddHost()} aria-label="plus"><i className="zmdi zmdi-plus"></i></IconButton>
                                                      {/* <IconButton className="text-danger" onClick={() => this.AddHost()} aria-label="plus"><i className="zmdi zmdi-minus"></i></IconButton> */}
                                                   </FormGroup>
                                                </div> : ''}
                                                {key != (this.state.Hosts.length - 1) ? <div className="col-sm-2 col-md-2 col-xl-2" style={{ paddingRight: '1px' }}>
                                                   <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                                      <IconButton className="text-danger" onClick={() => this.RemoveHost(key)} aria-label="plus"><i className="zmdi zmdi-minus"></i></IconButton>
                                                   </FormGroup>
                                                </div> : ''}
                                             </div>
                                          ))}
                                       </FormGroup>
                                    </div>
                                 </div>
                                 <br />

                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="assignedcomp"><IntlMessages id="Add/edit_Asset" /><IntlMessages id="Remarks" /></Label>
                                          <Textarea className="form-control" value={this.state.Asset.Remarks} onChange={(e) => this.handleChange(e, 'Remarks')} name="assignedcomp" id="assignedcomp" placeholder={this.context.intl.formatMessage({ id: 'Remarks' })}></Textarea>
                                       </FormGroup>
                                    </div>
                                 </div>
                              </Form>
                           </RctCollapsibleCard>
                        </div>
                     </div>
                  </DialogContentText>
               </DialogContent>
               <DialogActions>
                  <Button variant="contained" onClick={this.handleClose} style={{ color: '#5d92f4' }} className="btn-default modalcancel">
                     <IntlMessages id="Cancel" />
                  </Button>
                  <Button variant="contained" onClick={() => this.SaveAssets()} className="btn-primary text-white modalsave">
                     <IntlMessages id="Create/Update" />
                  </Button>
               </DialogActions>
            </Dialog>
         </div >
      );
   }
}