/**
 * Vulnerabilities Dialog
 */
import React from 'react';
import ReactTags from 'react-tag-autocomplete';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { FormGroup, Label, FormText, Col } from 'reactstrap';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Input from 'react-validation/build/input';
import Form from 'react-validation/build/form';
// import Select from 'react-validation/build/select';
import Textarea from 'react-validation/build/textarea';
import { IconButton, Tooltip, NativeSelect, InputLabel } from '@material-ui/core';
import update from 'react-addons-update';
import Select from 'react-select';
import IntlMessages from 'Util/IntlMessages';
//API Services
import * as ApiService from '../../actions/ApiServices';
import RctSectionLoader from '../RctSectionLoader/RctSectionLoader';
import DeleteConfirmationDialog from '../DeleteConfirmationDialog/DeleteConfirmationDialog';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

const IPCheck = (value, props) => {
   if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)) || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Invalid_IP_address" /></span>;
   }
};

const OnlyNumCheck = (value, props) => {
   if (!(/^(0|[0-9][0-9]*)$/.test(value)) || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible">Invalid Port No</span>;
   }
};

export default class VulnerabilitiesDialog extends React.Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      open: false,
      Vulnerability: {
         id: 0,
         Name: '',
         Facing: 1,
         Risk: 4,
         Complexity: 3,
         Effort: 3,
         IssueType: 2
      },
      Hosts: [],
      PentestCheckStatusId: 0,
      PentestId: 0,
      VulnerabilityId: 0,
      loading: false,
      tags: [],
      Systems: [],
      Opportunities: [],
      Damages: [],
      VulnerabilitySystems: [],
      IsMultiselectInValid: false,
      UploadFile: [],
      selectedDeleteID: 0,
      DeletedSystems: [],
      DeletedAssets: [],
      DeletedHosts: [],
      PentestCheckListTitle: '',
      SystemTagIds: [],
      VulnerabilityAssets: [],
      Assets: [],
      AssetTags: [],
      Hostoptions1: [{ label: 'tcp', value: 'tcp' }],
      Hostoptions2: [{ label: 'smtp', value: 'smtp' }, { label: 'www', value: 'www' }, { label: 'mssql', value: 'mssql' }],
      inputValue: '',
   };

   async componentDidMount() {
   }

   async GetOpportunities() {
      var Opportunities = await ApiService.GetAPI('Opportunities');
      this.setState({ Opportunities: Opportunities });
   }

   async GetDamages() {
      var Damages = await ApiService.GetAPI('Damages');
      this.setState({ Damages: Damages });
   }

   async GetSystems(companyid) {
      this.setState({ loading: true });
      var systemData = {
         Where: {
            CompanyId: companyid,
            IsActive: 1
         }
      }
      var Systems = await ApiService.PostWhereAPI('execute/conditions/System', systemData);
      this.setState({ VulnerabilitySystems: Systems });
      var SystemSuggesions = [];
      if (Systems.length > 0) {
         Systems.forEach(sys => {
            var newSys = {
               id: sys.id,
               name: sys.Name,
               value: sys.id,
               label: sys.Name
            }
            SystemSuggesions.push(newSys);
         });
      }
      this.setState({ Systems: SystemSuggesions });
      // this.GetAssetsBySystems();
      this.setState({ loading: false });
   }

   async GetAssetsBySystems() {
      this.setState({ loading: true });
      var AssetData = {
         Where: {
            SystemId: this.state.SystemTagIds,
            IsActive: 1
         }
      }
      const VulnerabilityAssets = await ApiService.PostWhereAPI('execute/conditions/Asset', AssetData);
      this.setState({ VulnerabilityAssets });
      var AssetSuggesions = [];
      if (VulnerabilityAssets.length > 0) {
         VulnerabilityAssets.forEach(asset => {
            var newAsset = {
               id: asset.id,
               SystemId: asset.SystemId,
               name: asset.Name,
               value: asset.id,
               label: asset.Name
            }
            AssetSuggesions.push(newAsset);
         });
      }

      const AssetTags = this.state.AssetTags;
      if (AssetTags.length > 0) {
         var atagids = AssetTags.map((x) => { return x.id });
         var aSuggids = AssetSuggesions.map((x) => { return x.id });

         AssetTags.forEach((atag, key) => {
            if (!aSuggids.includes(atag.id)) {
               AssetTags.splice(key, 1);
            }
         });

         if (AssetSuggesions.length > 0) {
            AssetSuggesions.forEach(asset => {
               if (atagids.includes(asset.id)) {
                  asset.disabled = true;
               }
            });
         }
      }
      this.setState({ Assets: AssetSuggesions, AssetTags });
      this.setState({ loading: false });
   }

   handleAddClickOpen = async (pentestcheckstatusid, pentestid, companyid, pentestCheckListTitle) => {
      this.setState({ open: true });
      await this.setState({ PentestCheckStatusId: pentestcheckstatusid, PentestId: pentestid, CompanyId: companyid, PentestCheckListTitle: pentestCheckListTitle });
      this.setState({ loading: true });
      await this.GetSystems(companyid);
      await this.GetOpportunities();
      await this.GetDamages();
      await this.GetPentestcheckliststatus(pentestcheckstatusid);
      this.setState({ loading: false });
   };

   handleEditClickOpen = async (id, pentestid, companyid) => {
      this.setState({ loading: true });
      this.setState({ open: true });
      await this.setState({ VulnerabilityId: id, PentestId: pentestid, CompanyId: companyid });
      await this.GetSystems(companyid);
      await this.GetOpportunities();
      await this.GetDamages();
      await this.GetVulnerability(id);
      this.setState({ loading: false });
   };

   async GetPentestcheckliststatus(id) {
      this.setState({ loading: true });
      if (id) {
         var VulnerabilitiesData = {
            Where: {
               PenTestChecklistStatusId: id,
               IsActive: 1
            }
         }
         var Vulnerabilities = await ApiService.PostWhereAPI('execute/conditions/Vulnerabilities', VulnerabilitiesData);
         if (Vulnerabilities && Vulnerabilities.length > 0) {
            this.SetVulnerabilityData(Vulnerabilities[0]);
         } else {
            let Vulnerability = this.state.Vulnerability;
            Vulnerability.Name = this.state.PentestCheckListTitle;
            this.setState({ Vulnerability });
         }
      }
      this.AddHost();
      this.setState({ loading: false });
   }

   async SetVulnerabilityData(Vulnerability) {
      this.setState({ Vulnerability: Vulnerability });
      this.setState({ PentestCheckStatusId: Vulnerability.PenTestCheckListStatusId });
      if (Vulnerability.VulnerabilityHosts && Vulnerability.VulnerabilityHosts.length > 0) {
         const Hosts = this.state.Hosts;
         Vulnerability.VulnerabilityHosts.forEach(Vhost => {
            Vhost.Host.Protocol = { value: Vhost.Host.Protocol, label: Vhost.Host.Protocol };
            Vhost.Host.ProtocolType = { value: Vhost.Host.ProtocolType, label: Vhost.Host.ProtocolType };
            Hosts.push(Vhost.Host);
         });
         this.setState({ Hosts });
      }
      var systems = [];
      var assets = [];
      const SystemTagIds = this.state.SystemTagIds;
      Vulnerability.VulnerabilityAssets.forEach(ast => {
         var tag = this.state.Systems.filter((x) => x.id == ast.System.id)[0];
         var Astag = this.state.Systems.filter((x) => x.id == ast.AssetId.id)[0];
         if (tag) {
            tag.disabled = true;
         }
         systems.push({ id: ast.System.id, name: ast.System.Name, value: ast.System.id, label: ast.System.Name, disabled: true, VsysId: ast.id });

         if (Astag) {
            Astag.disabled = true;
         }
         if (ast.Asset) {
            assets.push({ id: ast.Asset.id, name: ast.Asset.Name, value: ast.Asset.id, label: ast.Asset.Name, disabled: true, VsysId: ast.id });
         }
         SystemTagIds.push(ast.System.id);
      });
      await this.setState({ tags: systems, AssetTags: assets, SystemTagIds });
      await this.GetAssetsBySystems();
      // await this.setState({ SystemTagIds: [] });
   }

   async GetVulnerability(id) {
      this.setState({ loading: true });
      var Vulnerability = await ApiService.GetWithoutmsgAPI('Vulnerabilities/' + id);
      if (Vulnerability) {
         this.SetVulnerabilityData(Vulnerability);
      }
      this.AddHost();
      this.setState({ loading: false });
   }

   handleClickOpen = () => {
      this.setState({ open: true });
   };

   handleClose = () => {
      this.setState({ open: false });
      this.setState({
         Hosts: [],
         PentestCheckStatusId: 0,
         PentestId: 0,
         VulnerabilityId: 0,
         loading: false,
         tags: [],
         IsMultiselectInValid: false,
         UploadFile: [],
         selectedDeleteID: 0,
         DeletedSystems: [],
         DeletedAssets: [],
         DeletedHosts: [],
         PentestCheckListTitle: '',
         SystemTagIds: [],
         VulnerabilityAssets: [],
         Assets: [],
         AssetTags: []
      });
      const Vulnerability = {
         id: 0,
         Facing: 1,
         Risk: 4,
         Complexity: 3,
         Effort: 3,
         IssueType: 2
      };
      this.setState({ Vulnerability });
   };

   handleChange = (event, key) => {
      let Vulnerability = this.state.Vulnerability;
      Vulnerability[key] = event.target.value;
      this.setState({ Vulnerability: Vulnerability });
   }

   handleChangeRadio = (event, key) => {
      let Vulnerability = this.state.Vulnerability;
      Vulnerability[key] = parseInt(event.target.value);
      this.setState({ Vulnerability: Vulnerability });
   }

   async RemoveHost(index) {
      this.setState({ loading: true });
      let Hosts = this.state.Hosts;
      if (Hosts[index].id > 0) {
         const DeletedHosts = this.state.DeletedHosts;
         await DeletedHosts.push(Hosts[index]);
         await this.setState({ DeletedHosts });
      }
      Hosts.splice(index, 1);
      setTimeout(async () => {
         await this.setState({ Hosts: Hosts });
         this.setState({ loading: false });
      }, 500);
   }

   AddHost() {
      let Hosts = this.state.Hosts;
      let newHost = {
         id: 0,
         PortNo: 80,
         Protocol: { value: 'tcp', label: 'tcp' },
         ProtocolType: { value: 'smtp', label: 'smtp' },
         HostIP: '0.0.0.0'
      }
      Hosts.push(newHost);
      this.setState({ Hosts: Hosts });
   }

   handleChange2(value, data, key) {
      let selectedHostIndex = this.state.Hosts.indexOf(data);
      let newHost = update(this.state, {
         Hosts: {
            [selectedHostIndex]: {
               [key]: { $set: value }
            }
         }
      });
      this.setState({ Hosts: newHost.Hosts });
   }

   handleChangeHostProtocols(value, data, key) {
      let selectedHostIndex = this.state.Hosts.indexOf(data);
      let newHost = update(this.state, {
         Hosts: {
            [selectedHostIndex]: {
               [key]: { $set: { value: value, label: value } }
            }
         }
      });
      this.setState({ Hosts: newHost.Hosts });
   }

   async SaveVulnerability() {
      this.form.validateAll();

      if (this.ValidationMultiselect()) {
         return;
      }
      var UserID = localStorage.getItem('user_id');
      if (this.form.getChildContext()._errors.length <= 0) {

         this.setState({ loading: true });
         // var Vulnerability;
         var VulnerabilityData = {
            id: this.state.Vulnerability.id,
            PenTestChecklistStatusId: this.state.PentestCheckStatusId,
            PenTestId: cryptr.decrypt(this.state.PentestId),
            Name: this.state.Vulnerability.Name.toString().trim(),
            Facing: this.state.Vulnerability.Facing,
            Risk: this.state.Vulnerability.Risk,
            Complexity: this.state.Vulnerability.Complexity,
            Effort: this.state.Vulnerability.Effort,
            SystemsDesc: this.state.Vulnerability.SystemsDesc ? this.state.Vulnerability.SystemsDesc.toString().trim() : '',
            RiskDesc: this.state.Vulnerability.RiskDesc ? this.state.Vulnerability.RiskDesc.toString().trim() : '',
            IssueType: this.state.Vulnerability.IssueType,
            ModifiedBy: UserID,
            CreatedAt: new Date(),
            ModifiedAt: new Date(),
            Hosts: this.state.Hosts
         };
         if (this.state.Vulnerability.id <= 0) {
            VulnerabilityData.CreatedBy = UserID;
         }
         var systems = [];
         var systemIds = [];
         if (this.state.tags && this.state.tags.length > 0) {

            this.state.tags.forEach(syst => {
               systems.push({ SystemId: syst.id });
               systemIds.push(syst.id);
            });
         }

         var assets = [];
         var assetsystemIds = [];
         if (this.state.AssetTags && this.state.AssetTags.length > 0) {

            this.state.AssetTags.forEach(ast => {
               assetsystemIds.push(ast.SystemId);
               assets.push({ AssetId: ast.id, SystemId: ast.SystemId })
            });
         }

         systemIds.forEach(sysid => {
            if (!assetsystemIds.includes(sysid)) {
               assets.push({ AssetId: 0, SystemId: sysid })
            }
         });

         VulnerabilityData.Assets = assets;

         if (VulnerabilityData.Hosts.length <= 1) {
            VulnerabilityData.Hosts = [];
         } else if (VulnerabilityData.Hosts[VulnerabilityData.Hosts.length - 1].id == 0) {
            VulnerabilityData.Hosts.splice((VulnerabilityData.Hosts.length - 1), 1);
         }

         if (VulnerabilityData.Hosts.length >= 0) {
            VulnerabilityData.Hosts.forEach(host => {
               host.Protocol = host.Protocol.value;
               host.ProtocolType = host.ProtocolType.value;
            });
         }

         var resVulnerability = await ApiService.PutAPI('Vulnerabilitiesapi/AddOrEditVulnerabilities', VulnerabilityData);
         if (resVulnerability) {
            // await this.DeleteSystems();
            await this.DeleteAssets();
            await this.DeleteHosts();

            if (this.state.UploadFile && this.state.UploadFile.length > 0) {

               for (let i = 0; i <= this.state.UploadFile.length - 1; i++) {
                  let fd = new FormData();
                  fd.append('file', this.state.UploadFile[i]);
                  let Image = await ApiService.UploadAPI(fd);
                  if (Image) {
                     var VulnerabilityAttchData = {
                        VulnerabilityId: resVulnerability.id,
                        FileName: Image.fileName
                     }
                     var VulnerabilityAttachment = await ApiService.PutWithoutmsgAPI('VlunerabilityAttachments', VulnerabilityAttchData);
                  }
               }
            }

            let PentestId = this.state.PentestId;
            this.setState({
               open: false
            });
            this.setState({
               Hosts: [],
               PentestCheckStatusId: 0,
               PentestId: 0,
               VulnerabilityId: 0,
               loading: false,
               tags: [],
               IsMultiselectInValid: false,
               UploadFile: [],
               selectedDeleteID: 0,
               DeletedSystems: [],
               DeletedAssets: [],
               DeletedHosts: [],
               PentestCheckListTitle: '',
               SystemTagIds: [],
               VulnerabilityAssets: [],
               Assets: [],
               AssetTags: []
            });
            const Vulnerability = {
               id: 0,
               Facing: 1,
               Risk: 4,
               Complexity: 3,
               Effort: 3,
               IssueType: 2
            };
            this.setState({ Vulnerability });
            this.props.backFromProps(PentestId);
         }
      }
      this.setState({ loading: false });
   }

   async DeleteSystems() {
      if (this.state.DeletedSystems && this.state.DeletedSystems.length > 0) {

         this.state.DeletedSystems.forEach(async (tag) => {
            if (this.state.tags && this.state.tags.length > 0) {
               var system = this.state.tags.filter(x => x.id == tag.id)[0];
               if (!system) {
                  var syst = await ApiService.HardDeleteAPI('VulnerabilitySystem/' + cryptr.encrypt(tag.VsysId));
               }
            } else {
               var syst = await ApiService.HardDeleteAPI('VulnerabilitySystem/' + cryptr.encrypt(tag.VsysId));
            }
         });
      }
   }

   async DeleteAssets() {
      if (this.state.DeletedAssets && this.state.DeletedAssets.length > 0) {

         this.state.DeletedAssets.forEach(async (tag) => {
            if (this.state.AssetTags && this.state.AssetTags.length > 0) {
               var asset = this.state.AssetTags.filter(x => x.id == tag.id)[0];
               if (!asset) {
                  var ast = await ApiService.HardDeleteAPI('VulnerabilityAsset/' + cryptr.encrypt(tag.VsysId));
               }
            } else {
               var ast = await ApiService.HardDeleteAPI('VulnerabilityAsset/' + cryptr.encrypt(tag.VsysId));
            }
         });
      }
   }

   async DeleteHosts() {
      if (this.state.DeletedHosts && this.state.DeletedHosts.length > 0) {
         this.state.DeletedHosts.forEach(async (host) => {
            var host = await ApiService.HardDeleteAPI('Host/' + cryptr.encrypt(host.id));
         });
      }
   }

   handleValidate(tag) {
      tag.disabled = true;
      return tag;
   }

   ValidationMultiselect() {
      if (this.state.tags.length <= 0) {
         this.setState({ AssetTags: [], VulnerabilityAssets: [], Assets: [], IsMultiselectInValid: true });
         return true;
      } else {
         this.setState({ IsMultiselectInValid: false });
         return false;
      }
   }

   // on delete
   onDeleteAttachment(id) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedDeleteID: id });
   }

   // delete
   async delete() {
      var VulnerabilityAttachment = await ApiService.HardDeleteAPI('VlunerabilityAttachments/' + this.state.selectedDeleteID);
      if (VulnerabilityAttachment) {
         // this.props.history.push('app/')
      }
      this.setState({ selectedDeleteID: 0 });
      this.GetVulnerability(this.state.VulnerabilityId);
      this.refs.deleteConfirmationDialog.close();
   }

   async handleAddition(tag) {
      const tags = [].concat(this.state.tags, tag);
      const SystemTagIds = [].concat(this.state.SystemTagIds, tag.id);
      await this.setState({ tags });
      await this.setState({ SystemTagIds });
      await this.ValidationMultiselect();
      await this.GetAssetsBySystems();
   }

   async handleAssetsAddition(tag) {
      const AssetTags = [].concat(this.state.AssetTags, tag);
      await this.setState({ AssetTags });
      setTimeout(() => {
         this.ValidationMultiselect();

         var asset = this.state.VulnerabilityAssets.filter((x) => x.id == tag.id)[0];
         if (asset.Hosts.length > 0) {
            const Hosts = [].concat(asset.Hosts, this.state.Hosts);
            this.setState({ Hosts });
         }
      }, 1000);
   }

   async handleDelete(i) {
      var tag = this.state.tags[i];

      if (tag) {
         tag.disabled = false;
         var newtag = this.state.Systems.filter((x) => x.id == tag.id)[0];
         if (newtag) {
            newtag.disabled = false;
         }
         const tags = this.state.tags.slice(0)
         tags.splice(i, 1)
         this.setState({ tags });
         if (tag.VsysId) {
            this.state.DeletedAssets.push(tag);
         }
         setTimeout(() => {
            this.ValidationMultiselect();
         }, 500);

         const SystemTagIds = this.state.SystemTagIds;
         const index = SystemTagIds.indexOf(tag.id);

         if (index !== -1) {
            SystemTagIds.splice(index, 1);
         }
         await this.setState({ SystemTagIds });
         await this.GetAssetsBySystems();
         this.setState({ loading: false });
      }
   }

   async handleAssetsDelete(i) {
      var tag = this.state.AssetTags[i];

      if (tag) {
         tag.disabled = false;
         var newtag = this.state.Assets.filter((x) => x.id == tag.id)[0];
         if (newtag) {
            newtag.disabled = false;
         }
         const AssetTags = this.state.AssetTags.slice(0)
         AssetTags.splice(i, 1)
         this.setState({ AssetTags });
         if (tag.VsysId) {
            this.state.DeletedAssets.push(tag);
         }
         setTimeout(() => {
            this.ValidationMultiselect();
         }, 500);
         this.setState({ loading: false });
      }
   }

   handleChangeSystemSelect = async (tags) => {

      let newtags = this.state.tags;
      let differencetag = newtags.filter(x => !tags.includes(x));

      if (differencetag && differencetag.length > 0) {
         if (differencetag[0].VsysId)
            this.state.DeletedAssets.push(differencetag[0]);
      }

      await this.setState({ tags });
      const SystemTagIds = this.state.tags.map((x) => { return x.id });
      await this.setState({ SystemTagIds });
      await this.ValidationMultiselect();
      await this.GetAssetsBySystems();
   };

   handleChangeAssetSelect = async (AssetTags) => {
      this.setState({ loading: true });
      let newtags = this.state.AssetTags;
      let differencetag = newtags.filter(x => !AssetTags.includes(x));

      if (differencetag && differencetag.length > 0) {
         if (differencetag[0].VsysId)
            this.state.DeletedAssets.push(differencetag[0]);
      }

      await this.setState({ AssetTags });
      await this.ValidationMultiselect();
      var AllAssetHosts = [];
      AssetTags.forEach(asttag => {
         let asset = this.state.VulnerabilityAssets.filter((x) => x.id == asttag.id)[0];
         if (asset && asset.Hosts.length > 0) {
            asset.Hosts.forEach(host => {
               let newhost = {
                  id: host.id,
                  PortNo: host.PortNo,
                  Protocol: { value: host.Protocol, label: host.Protocol },
                  ProtocolType: { value: host.ProtocolType, label: host.ProtocolType },
                  HostIP: host.HostIP,
                  AssetId: host.AssetId
               };
               AllAssetHosts.push(newhost);
            });
         }
      });
      if (AllAssetHosts && AllAssetHosts.length > 0) {
         const NewHosts = await this.state.Hosts.filter(x => x.id == 0);
         let Hosts = [...AllAssetHosts, ...NewHosts];
         await this.setState({ Hosts });
      }
      this.setState({ loading: false });
   };

   render() {
      const { loading, inputValue } = this.state;
      const { classes } = this.props;
      return (
         <div>
            {/* <Button variant="contained" className="btn-primary text-white btn-block" onClick={this.handleClickOpen}>Vulnerabilities</Button> */}
            <Dialog maxWidth="md" fullWidth="true" open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
               <DialogTitle id="form-dialog-title" className="modaltl">Add Vulnerabilities</DialogTitle>
               <DialogContent className="modalbd">
                  <DialogContentText>
                     <div className="row">
                        <div className="col-sm-12 col-md-12 col-12 p-0">
                           <RctCollapsibleCard>
                              {loading ? <RctSectionLoader /> : ''}
                              <Form ref={c => { this.form = c }}>
                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                       <FormGroup>
                                          <Label for="firstname"><IntlMessages id="Name" />
                                             <span className="required">*</span></Label>
                                          <Input type="text" className="form-control" validations={[required]} value={this.state.Vulnerability.Name} onChange={(e) => this.handleChange(e, 'Name')} name="Name" id="Name" placeholder={this.context.intl.formatMessage({ id: 'Name' })} />
                                       </FormGroup>
                                    </div>
                                 </div>

                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup className={this.state.IsMultiselectInValid ? 'rootsecmultiselect errorautocomplete' : 'rootsecmultiselect'}>
                                          <Label for="assignedcomp"><IntlMessages id="vulnerability.PresentonSystems" />
                                             <span className="required">*</span></Label>
                                          <Select
                                             isMulti
                                             value={this.state.tags}
                                             isClearable
                                             isSearchable
                                             options={this.state.Systems}
                                             onChange={this.handleChangeSystemSelect}
                                             className="basic-multi-select"
                                             classNamePrefix="select"
                                          />
                                          {this.state.IsMultiselectInValid ? <span className="form-error is-visible"><IntlMessages id="Required" /></span> : ''}
                                       </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup className={this.state.IsMultiselectInValid ? 'rootsecmultiselect errorautocomplete' : 'rootsecmultiselect'}>
                                          <Label for="assignedcomp">Assign Vulnerabilities to asset</Label>
                                          <Select
                                             isMulti
                                             name="color"
                                             value={this.state.AssetTags}
                                             isClearable
                                             isSearchable
                                             options={this.state.Assets}
                                             onChange={this.handleChangeAssetSelect}
                                             className="basic-multi-select"
                                             classNamePrefix="select"
                                          />
                                          {this.state.IsMultiselectInValid ? <span className="form-error is-visible"><IntlMessages id="Required" /></span> : ''}
                                       </FormGroup>
                                    </div>
                                 </div>

                                 <div className="row">
                                    <div className="col-sm-2 col-md-2 col-xl-2 mr-30 mb-3">
                                       <FormControl component="fieldset">
                                          <FormLabel component="legend"><IntlMessages id="vulnerability.Facing" /></FormLabel>
                                          <RadioGroup aria-label="Facing" name="Facing" value={this.state.Vulnerability.Facing}
                                             onChange={(e) => this.handleChangeRadio(e, 'Facing')} >
                                             <FormControlLabel value={1} control={<Radio />} label="Public" className="vulnRadiobuttons" />
                                             <FormControlLabel value={2} control={<Radio />} label="External" className="vulnRadiobuttons" />
                                             <FormControlLabel value={3} control={<Radio />} label="Internal" className="vulnRadiobuttons" />
                                          </RadioGroup>
                                       </FormControl>
                                    </div>
                                    <div className="col-sm-2 col-md-2 col-xl-2 mr-30 mb-3">
                                       <FormControl component="fieldset">
                                          <FormLabel component="legend"><IntlMessages id="vulnerability.Risk" /></FormLabel>
                                          <RadioGroup aria-label="Risk" name="Risk" value={this.state.Vulnerability.Risk}
                                             onChange={(e) => this.handleChangeRadio(e, 'Risk')} >
                                             <FormControlLabel value={4} control={<Radio />} label="Low" className="vulnRadiobuttons" />
                                             <FormControlLabel value={3} control={<Radio />} label="Medium" className="vulnRadiobuttons" />
                                             <FormControlLabel value={2} control={<Radio />} label="High" className="vulnRadiobuttons" />
                                             <FormControlLabel value={1} control={<Radio />} label="Extreme" className="vulnRadiobuttons" />
                                          </RadioGroup>
                                       </FormControl>
                                    </div>
                                    <div className="col-sm-2 col-md-2 col-xl-2 mr-30 mb-3">
                                       <FormControl component="fieldset">
                                          <FormLabel component="legend"><IntlMessages id="vulnerability.Complexity" /></FormLabel>
                                          <RadioGroup aria-label="Complexity" name="Complexity" value={this.state.Vulnerability.Complexity}
                                             onChange={(e) => this.handleChangeRadio(e, 'Complexity')} >
                                             <FormControlLabel value={3} control={<Radio />} label="Low" className="vulnRadiobuttons" />
                                             <FormControlLabel value={2} control={<Radio />} label="Medium" className="vulnRadiobuttons" />
                                             <FormControlLabel value={1} control={<Radio />} label="High" className="vulnRadiobuttons" />
                                          </RadioGroup>
                                       </FormControl>
                                    </div>
                                    <div className="col-sm-2 col-md-2 col-xl-2 mr-30 mb-3">
                                       <FormControl component="fieldset">
                                          <FormLabel component="legend"><IntlMessages id="vulnerability.Effort" /></FormLabel>
                                          <RadioGroup aria-label="Effort" name="Effort" value={this.state.Vulnerability.Effort}
                                             onChange={(e) => this.handleChangeRadio(e, 'Effort')} >
                                             <FormControlLabel value={3} control={<Radio />} label="Low" className="vulnRadiobuttons" />
                                             <FormControlLabel value={2} control={<Radio />} label="Medium" className="vulnRadiobuttons" />
                                             <FormControlLabel value={1} control={<Radio />} label="High" className="vulnRadiobuttons" />
                                          </RadioGroup>
                                       </FormControl>
                                    </div>
                                    <div className="col-sm-2 col-md-2 col-xl-2 mb-3">
                                       <FormControl component="fieldset">
                                          <FormLabel component="legend"><IntlMessages id="vulnerability.Status" /></FormLabel>
                                          <RadioGroup aria-label="IssueType" name="IssueType" value={this.state.Vulnerability.IssueType}
                                             onChange={(e) => this.handleChangeRadio(e, 'IssueType')} >
                                             <FormControlLabel value={2} control={<Radio />} label={<IntlMessages id="Unknown" />} className="vulnRadiobuttons" />
                                             <FormControlLabel value={1} control={<Radio />} label={<IntlMessages id="Known" />} className="vulnRadiobuttons" />
                                             <FormControlLabel value={3} control={<Radio />} label={<IntlMessages id="Solved" />} className="vulnRadiobuttons" />
                                          </RadioGroup>
                                       </FormControl>
                                    </div>
                                 </div>
                                 <br />
                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="SystemsDesc"><IntlMessages id="vulnerability.DescriptionofVulnerability" /></Label>
                                          <Textarea className="form-control" value={this.state.Vulnerability.SystemsDesc} onChange={(e) => this.handleChange(e, 'SystemsDesc')} name="SystemsDesc" id="SystemsDesc" placeholder={this.context.intl.formatMessage({ id: 'placeholders.VulnDesc' })} ></Textarea>
                                       </FormGroup>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="RiskDesc"><IntlMessages id="vulnerability.Descriptionofrisk" /></Label>
                                          <Textarea className="form-control" name="RiskDesc" id="RiskDesc" value={this.state.Vulnerability.RiskDesc} onChange={(e) => this.handleChange(e, 'RiskDesc')} placeholder={this.context.intl.formatMessage({ id: 'placeholders.VulnRisk' })} ></Textarea>
                                       </FormGroup>
                                    </div>
                                 </div>
                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup>
                                          <Label for="VulnSolution"><IntlMessages id="vulnerability.VulnSolution" /></Label>
                                          <Textarea className="form-control" value={this.state.Vulnerability.VulnSolution} onChange={(e) => this.handleChange(e, 'SystemsDesc')} name="VulnSolution" id="VulnSolution" placeholder={this.context.intl.formatMessage({ id: 'placeholders.VulnSolution' })} ></Textarea>
                                       </FormGroup>
                                    </div>
                                 </div>

                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                       <FormGroup>
                                          <Label for="VulnSolution"><IntlMessages id="vulnerability.VulnSolution" /></Label>
                                          <ul className="list-group new-customer-list">
                                             {this.state.Opportunities.map((opp, key) => (
                                                <li className="list-group-item d-flex flex-wrap justify-content-between" key={key}>
                                                   <div className="col-lg-9 col-xl-9 mb-3 mb-lg-0">
                                                      <div className="media">
                                                         <div className="media-left mr-15">
                                                            {key + 1}.</div>
                                                         <div className="media-body">
                                                            <span className="d-block fs-14">{opp.Name}</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div className="col-md-3 col-lg-3 col-xl-3 mb-3 mb-md-0">
                                                      <FormControl fullWidth>
                                                         <InputLabel htmlFor="age-simple"></InputLabel>
                                                         <NativeSelect key={key} onChange={(e) => this.ChangeOrAddPencheckListStatus(pentestcheck.Id, pentestcheck.PenTestCheckListStatusId, e.target.value)}
                                                            inputProps={{ name: 'age', id: 'age-simple', }}>
                                                            <option value={0}>{this.context.intl.formatMessage({ id: 'None' })}</option>
                                                            <option value={1}>Correct</option>
                                                            <option value={2}>Incorrect</option>
                                                            <option value={3}>N.V.T</option>
                                                         </NativeSelect>
                                                      </FormControl>
                                                   </div>
                                                </li>
                                             ))}
                                          </ul>
                                       </FormGroup>
                                    </div>
                                 </div>
                                 
                                 <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-6">
                                       <FormGroup row>
                                          <Label for="File-1" ><IntlMessages id="vulnerability.Attachments" /></Label>
                                          <Col sm={10}>
                                             {/* <Input type="file" name="file" id="File-1" multiple /> */}
                                             <Input type="file" onChange={(e) => this.setState({ UploadFile: e.target.files })} name="file" id="File-1" multiple />
                                             <FormText color="muted">
                                             </FormText>
                                          </Col>
                                       </FormGroup>
                                    </div>
                                 </div>

                                 {this.state.Vulnerability.VlunerabilityAttachments ? this.state.Vulnerability.VlunerabilityAttachments.length > 0 ? <div className="row">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                       <FormGroup>
                                          <Label for="assignedcomp" style={{ fontWeight: 'bolder' }}>Files</Label>
                                          {this.state.Vulnerability.VlunerabilityAttachments ? this.state.Vulnerability.VlunerabilityAttachments.map((att, key) => (
                                             <div className="row">
                                                <div className="col-sm-4">{att.FileName}</div>
                                                <Tooltip className="col-sm-1" style={{ padding: '0' }} id="tooltip-icon" title="delete">
                                                   <IconButton className="text-danger" onClick={() => this.onDeleteAttachment(cryptr.encrypt(att.id))} aria-label="Add an alarm"><i className="zmdi zmdi-delete"></i></IconButton>
                                                </Tooltip>
                                             </div>
                                          )) : ''}
                                       </FormGroup>
                                    </div>
                                 </div> : '' : ''}

                                 <div className="row">
                                    <Label className="col-sm-1 col-md-1 col-xl-1" for="assignedcomp">Port</Label>
                                    <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"></Label>
                                    <Label className="col-sm-2 col-md-2 col-xl-2" for="assignedcomp"></Label>
                                    <Label className="col-sm-1 col-md-1 col-xl-1" for="assignedcomp">Host</Label>
                                 </div>

                                 <div style={{ minHeight: '200px', maxHeight: '300px', overflow: 'auto' }}>
                                    <div className="row">
                                       {this.state.Hosts.map((host, key) => (
                                          <div className="col-sm-12 d-flex flex-wrap p-0 ipcontainer">
                                             <div className="col-sm-1 col-md-1 col-12">
                                                <FormGroup style={{ marginBottom: '5px' }}>
                                                   <Input type="text" key={key} maxLength="4" validations={[required, OnlyNumCheck]} value={host.PortNo} onChange={(e) => this.handleChange2(e.target.value, host, 'PortNo')} className="form-control" name="assignedcomp" id="assignedcomp" placeholder={this.context.intl.formatMessage({ id: 'placeholders.Port' })} />
                                                </FormGroup>
                                             </div>
                                             <div className="col-sm-2 col-md-2 col-12">
                                                <FormGroup style={{ marginBottom: '5px' }}>
                                                   <Select
                                                      value={host.Protocol}
                                                      options={this.state.Hostoptions1}
                                                      onChange={(e) => this.handleChangeHostProtocols(e.value, host, 'Protocol')}
                                                   />
                                                </FormGroup>
                                             </div>
                                             <div className="col-sm-2 col-md-2 col-12">
                                                <FormGroup style={{ marginBottom: '5px' }}>
                                                   <Select
                                                      value={host.ProtocolType}
                                                      options={this.state.Hostoptions2}
                                                      onChange={(e) => this.handleChangeHostProtocols(e.value, host, 'ProtocolType')}
                                                   />
                                                </FormGroup>
                                             </div>
                                             <div className="col-sm-3 col-md-3 col-12">
                                                <FormGroup style={{ marginBottom: '5px' }}>
                                                   <Input type="text" validations={[required, IPCheck]} className="form-control" key={key} value={host.HostIP} onChange={(e) => this.handleChange2(e.target.value, host, 'HostIP')} name="Host" id="Host" placeholder={this.context.intl.formatMessage({ id: 'placeholders.Host' })} />
                                                </FormGroup>
                                             </div>
                                             {key == (this.state.Hosts.length - 1) ? <div className="col-sm-2 col-md-2 col-12">
                                                <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                                   <IconButton className="text-primary" onClick={() => this.AddHost()} aria-label="plus"><i className="zmdi zmdi-plus"></i></IconButton>
                                                   {/* <IconButton className="text-danger" onClick={() => this.AddHost()} aria-label="plus"><i className="zmdi zmdi-minus"></i></IconButton> */}
                                                </FormGroup>
                                             </div> : ''}
                                             {key != (this.state.Hosts.length - 1) ? <div className="col-sm-2 col-md-2 col-12" style={{ paddingRight: '1px' }}>
                                                <FormGroup style={{ marginBottom: '0px', marginTop: '-5px' }}>
                                                   <IconButton className="text-danger" onClick={() => this.RemoveHost(key)} aria-label="plus"><i className="zmdi zmdi-minus"></i></IconButton>
                                                </FormGroup>
                                             </div> : ''}
                                          </div>
                                       ))}
                                    </div>
                                 </div>
                              </Form>
                           </RctCollapsibleCard>
                        </div>
                     </div>
                  </DialogContentText>
               </DialogContent>
               <DialogActions>
                  <Button variant="contained" onClick={this.handleClose} style={{ color: '#5d92f4' }} className="btn-default modalcancel">
                     <IntlMessages id="vulnerability.Cancel" />
                  </Button>
                  <Button variant="contained" onClick={() => this.SaveVulnerability()} className="btn-primary text-white modalsave">
                     <IntlMessages id="vulnerability.CreateUpdate" />
                  </Button>
               </DialogActions>
            </Dialog>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
               message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
               onConfirm={() => this.delete()}
            />
         </div>
      );
   }
}
