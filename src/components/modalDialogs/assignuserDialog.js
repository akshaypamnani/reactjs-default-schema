/**
 * Assigned User Dialog
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import MatButton from '@material-ui/core/Button';
import { FormGroup, Label } from 'reactstrap';
import Form from 'react-validation/build/form';
import Select from 'react-validation/build/select';

//API Services
import * as ApiService from '../../actions/ApiServices';
import RctSectionLoader from '../RctSectionLoader/RctSectionLoader';
import IntlMessages from 'Util/IntlMessages';

let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');

const required = (value, props) => {
   if (!value || value.toString().trim().length <= 0 || (props.isCheckable && !props.checked)) {
      return <span className="form-error is-visible"><IntlMessages id="Required" /></span>;
   }
};

export default class AssignedUserDialog extends React.Component {

   state = {
      open: false,
      CompanyId: 0,
      UserId: "",
      CompanyUserId: 0,
      Users: [],
      Index: -1,
      loading: false
   };

   // testprop = this.testprop.bind(this);

   // testprop() {
   //    // this.setState({ query: event.target.value }});
   //    this.props.testprop(1);
   //  }

   async componentDidMount() {
      this.setState({ loading: true });
      await this.GetUsers();
      this.setState({ loading: false });
   }

   async GetUsers() {
      var Users = await ApiService.GetAPI('User');
      this.setState({ Users: Users });
   }

   handleClickOpen = () => {
      this.setState({ open: true });
   };

   handleAddClickOpen = (companyid) => {
      this.setState({ open: true });
      this.setState({ CompanyId: companyid });
      // this.setState({ UserId: userid });
      // this.setState({ CompanyUserId: companyuserid });
      // this.setState({ Index: index });
   };

   handleEditClickOpen = (companyid, userid, companyuserid) => {
      this.setState({ open: true });
      this.setState({ CompanyId: companyid });
      this.setState({ UserId: userid });
      this.setState({ CompanyUserId: companyuserid });
      // this.setState({ Index: index });
   };

   handleChange(event) {
      this.setState({ UserId: event.target.value });
   }

   async SaveAssignedUser() {
      this.form.validateAll();
      this.setState({ loading: true });
      if (this.form.getChildContext()._errors.length <= 0) {
         var UserCompany;
         var UserCompanyData = {
            CompanyId: parseInt(this.state.CompanyId),
            UserId: parseInt(this.state.UserId)
         };
         if (this.state.CompanyUserId != 0) {
            UserCompany = await ApiService.PostAPI('pivot/UserCompany/' + this.state.CompanyUserId, UserCompanyData);
         } else {
            UserCompany = await ApiService.PutAPI('pivot/UserCompany', UserCompanyData);
         }
         if (UserCompany) {
            // this.props.history.push('/app/editcompany/' + this.state.CompanyUserId);
            let CompanyId = this.state.CompanyId;
            this.setState({ open: false });
            this.setState({ CompanyId: 0 });
            this.setState({ UserId: 0 });
            this.setState({ CompanyUserId: 0 });
            this.props.backFromProps(cryptr.encrypt(CompanyId));
         }
      }
      this.setState({ loading: false });
   }

   handleClose = () => {
      this.setState({ open: false });
      this.setState({ CompanyId: 0 });
      this.setState({ UserId: 0 });
      this.setState({ CompanyUserId: 0 });
   };

   render() {
      const { loading } = this.state;
      return (
         <div>
            {/* <Button variant="contained" className="btn-primary text-white btn-block" onClick={this.handleClickOpen}>Assign user</Button> */}
            <Dialog maxWidth="sm" fullWidth="true" open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
               <DialogTitle id="form-dialog-title" className="modaltl"><IntlMessages id="Assign_user_to_this_company" /></DialogTitle>
               <DialogContent className="modalbd">
                  <DialogContentText>
                     <div className="row">
                        <div className="col-sm-12 col-md-12 col-xl-12 p-0">
                           <RctCollapsibleCard>
                              {loading ? <RctSectionLoader /> : ''}
                              <Form ref={c => { this.form = c }}>
                                 <div className="row row-12">
                                    <div className="col-sm-12 col-md-12 col-xl-12">
                                       <FormGroup>
                                          <Label for="firstname"><IntlMessages id="select_username" />
                                             <span className="required">*</span>
                                          </Label>
                                          <Select className="form-control" validations={[required]} value={this.state.UserId} name="usrselect" onChange={(e) => this.handleChange(e)} id="Select-2">
                                             <option value="">Selecteer</option>
                                             {this.state.Users.map((usr, key) => (
                                                <option key={key} value={usr.id}>{usr.FirstName + ' ' + usr.LastName}</option>
                                             ))}
                                          </Select>
                                       </FormGroup>
                                    </div>
                                 </div>
                              </Form>
                           </RctCollapsibleCard>
                        </div>
                     </div>
                  </DialogContentText>
               </DialogContent>
               <DialogActions>
                  <Button variant="contained" onClick={this.handleClose} style={{ color: '#5d92f4' }} className="btn-default modalcancel">
                  <IntlMessages id="Cancel" />
            		</Button>
                  <Button variant="contained" onClick={() => this.SaveAssignedUser()} className="btn-primary text-white modalsave">
                  <IntlMessages id="Create/Update" />
            		</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }
}
