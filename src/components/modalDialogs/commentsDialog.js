/**
 * Comments Dialog
 */
import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Scrollbars } from 'react-custom-scrollbars';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import * as TranslationService from '../../actions/TranslationService';

// Api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';

// card component
import { RctCardFooter } from 'Components/RctCard';
import { Fab } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';

//API Services
import * as ApiService from '../../actions/ApiServices';
import DeleteConfirmationDialog from '../DeleteConfirmationDialog/DeleteConfirmationDialog';
import RctSectionLoader from '../RctSectionLoader/RctSectionLoader';
import { intlShape } from 'react-intl';
let Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');
// const parseDate = (dateString) => {
//    var months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
//    let datesstring = new Date(dateString);
//    return datesstring.getDay() + ' ' + months[datesstring.getMonth()] + ' ' + datesstring.getFullYear();
// }

export default class CommentsDialog extends React.Component {

   static contextTypes = {
      intl: intlShape,
   };

   state = {
      open: false,
      PencheckListStatusId: 0,
      PentestId: 0,
      PentestComments: [],
      Comment: '',
      selectedDeleteCommentId: 0,
      VulnerabilityId: 0
   };

   state = {
      comments: null,
      loading: false
   }

   componentDidMount() {
   }

   handleAddClickOpen = (penchecklistStatusid, vulnerabilityid, pentestid) => {
      this.setState({ open: true });
      this.setState({ PencheckListStatusId: penchecklistStatusid });
      this.setState({ VulnerabilityId: vulnerabilityid });
      this.setState({ PentestId: pentestid });
      this.GetPenTestComments(penchecklistStatusid);
      // this.AddHost();
   };

   async SendComment() {
      this.setState({ loading: true });
      var UserID = localStorage.getItem('user_id');
      if (UserID) {
         var comment = {
            PenTestId: cryptr.decrypt(this.state.PentestId),
            PenTestChecklistStatusId: this.state.PencheckListStatusId,
            VulnerabilityId: parseInt(this.state.VulnerabilityId) != 0 ? this.state.VulnerabilityId : null,
            Comment: this.state.Comment,
            CommentorId: parseInt(UserID),
            CommentDate: new Date()
         }
         var NewComment = await ApiService.PutAPI('PenTestComments', comment);

         if (NewComment) {

            var FieldsArray = [{ columnName: 'Comment', Title: NewComment.Comment }];
            var IdforUpdate = 0;
            var lang = this.context.intl.locale
            await TranslationService.SaveTranslations([], FieldsArray, 5, 'Comment', NewComment.id, IdforUpdate, lang);

            await this.setState({ Comment: ' ' });
            await this.GetPenTestComments(this.state.PencheckListStatusId);

            if (this.state.VulnerabilityId && parseInt(this.state.VulnerabilityId) != 0) {
               var NotificationData = {
                  PenTestId: cryptr.decrypt(this.state.PentestId),
                  CommentId: NewComment.id
               }
               var Pentest = await ApiService.PutWithoutmsgAPI('Notificationapi/AddNotification', NotificationData);
            }
            // this.setState({ PencheckListStatusId: 0, PentestId: 0, open: false });
         }
      }
      this.setState({ loading: false });
   }

   async GetPenTestComments(penchecklistStatusid) {
      this.setState({ loading: true });
      var penchecklistStatusdata = {
         Where: {
            PenTestChecklistStatusId: penchecklistStatusid,
            IsActive: 1
         }
      }
      var PentestComments = await ApiService.PostWhereAPI('execute/withoutnestedconditions/PenTestComments', penchecklistStatusdata);

      if (PentestComments && PentestComments.length > 0) {
         PentestComments.forEach(comment => {
            if (comment.LanguageTranslations && comment.LanguageTranslations.length > 0) {
               comment.LanguageTranslations = comment.LanguageTranslations.filter(x => x.PageRefId == 5);
            }
         });
      }

      this.setState({ PentestComments: PentestComments });
      this.setState({ loading: false });
   }

   GetTranslatedText(translatedData, field, value) {
      return TranslationService.GetTranslatedText(translatedData, field, value, this.context.intl.locale);
   }

   handleClickOpen = () => {
      this.setState({ open: true });
   };

   handleClose = () => {
      this.setState({ open: false });
   };

   onDeleteComment(commentid) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedDeleteCommentId: cryptr.encrypt(commentid) });
   }

   // delete
   async delete() {
      this.setState({ loading: true });
      if (this.state.selectedDeleteCommentId != 0) {

         var PenTestComment = await ApiService.DeleteAPI('PenTestComments/' + this.state.selectedDeleteCommentId);
         if (PenTestComment) {
            // this.props.history.push('app/')
         }
         this.setState({ selectedDeleteCommentId: 0 });
         this.refs.deleteConfirmationDialog.close();
         this.GetPenTestComments(this.state.PencheckListStatusId);
      }
      this.setState({ loading: false });
   }

   render() {
      const { PentestComments, loading } = this.state;

      const SendComment = e => {
         if (e.keyCode == 13 && e.shiftKey == false) {
            // handleSubmit()
            this.SendComment();
         }
      }

      return (
         <div>
            {/* <Button variant="contained" className="btn-primary text-white btn-block" onClick={this.handleClickOpen}>Comment</Button> */}
            <Dialog maxWidth="sm" fullWidth="true" open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
               <DialogTitle id="form-dialog-title" className="modaltl"><IntlMessages id="Comments" /></DialogTitle>
               <DialogContent className="modalbd modalcomment">
                  <DialogContentText>
                     <div className="row">
                        {loading ? <RctSectionLoader /> : ''}
                        <Fragment>
                           <Scrollbars className="rct-scroll" style={{ height: '300px' }} autoHeightMin={100} autoHeightMax={424} autoHide>
                              <List className="list-group aqua-ripple p-0">
                                 {PentestComments && PentestComments.map((comment) => (
                                    <ListItem className="d-flex px-0 align-items-start" key={comment.id} button>
                                       <div className="row w-100">
                                          <div className="avatar-wrap col-1 mr-10">
                                             <img src={require('Assets/avatars/user.svg')} alt="project logo" className="rounded-circle commentimg" width="30" height="30" />
                                          </div>
                                          <div className="comment-wrap col-10">
                                             <h5 className="d-flex mb-5">{comment.User != null ? (comment.User.FirstName + ' ' + comment.User.LastName) : ''} <span className="d-block comment-date" style={{ textAlign: 'left' }}>{comment.CommentDate ? moment(comment.CommentDate).format('DD-MM HH:mm') : '--'}</span></h5>
                                             <p className="mb-5 font-xs">{this.GetTranslatedText(comment.LanguageTranslations, 'Comment', comment.Comment)}</p>
                                             <p className="my-10">
                                                {/* <a class="font-sm commentreply">Reply</a> */}
                                                <a class="font-sm commentdelete" onClick={() => this.onDeleteComment(comment.id)}><IntlMessages id="delete" /></a>
                                             </p>
                                          </div>
                                          <hr className="w-100 ml-10 mr-10 my-0"></hr>
                                       </div>
                                    </ListItem>
                                 ))}
                              </List>
                           </Scrollbars>
                           <RctCardFooter customClasses="d-flex justify-content-between align-items-center rounded-bottom commentboxbgclr" style={{ width: '100%' }}>
                              <div className="d-flex align-items-center bg-white" style={{ width: '100%' }} fullWidth>
                                 <input type="text" value={this.state.Comment} onKeyDown={SendComment} onChange={(e) => { this.setState({ Comment: e.target.value }) }} placeholder={this.context.intl.formatMessage({ id: 'Enter_your_comment_here' })} className="form-control commentText"></input>
                                 <Button onClick={() => this.SendComment()} variant="round" size="small" color="primary" tooltip="Send Comment" className="sendcommentbtn bg-white">
                                    <i className="material-icons">send</i>
                                 </Button>
                              </div>
                           </RctCardFooter >
                        </Fragment>
                     </div>
                  </DialogContentText>
               </DialogContent>
               <DialogActions>
                  <Button variant="contained" onClick={this.handleClose} color="primary" className="text-white modalcancel">
                     <IntlMessages id="Cancel" />
                  </Button>
                  <DeleteConfirmationDialog
                     ref="deleteConfirmationDialog"
                     title={<IntlMessages id="Are_You_Sure_Want_To_Delete?" />}
                     message={<IntlMessages id="Are_You_Sure_Want_To_Delete_Permanently_This" />}
                     onConfirm={() => this.delete()}
                  />
               </DialogActions>
            </Dialog>
         </div>
      );
   }
}
