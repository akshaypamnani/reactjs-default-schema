import appLocaleData from 'react-intl/locale-data/nl';
import nlMessages from '../locales/nl_NL.js';

const NlLang = {
    messages: {
        ...nlMessages
    },
    locale: 'nl-NL',
    data: appLocaleData
};
export default NlLang;